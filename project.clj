(defproject sa-draw "0.2.7-SNAPSHOT"
  :description "Contains canvas drawing code for the sa project.
                0.2.1 is the first increment with dragging on paper working."

  :url "http://example.com/FIXME"

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}


  :plugins [[lein-cljsbuild  "1.1.7" :exclusions [[org.clojure/clojure]]]
            [lein-figwheel   "0.5.18"]
            [lein-tools-deps "0.4.5"]
            [lein-pprint     "1.2.0"]
            [lein-git-info   "0.1.2"]]

  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]

  :lein-tools-deps/config {:config-files ["deps.edn"]}

  :source-paths ["src/cljc"]
  :test-paths   ["test/clj"]


  :profiles {:demo {
                    :cljsbuild
                     {:builds
                      [{:id "demo"
                        :source-paths ["src/cljs" "src/cljc"]
                        :compiler
                           {:output-dir           "tmp/js"
                            :output-to            "demo/js/main.js"
                            :main                 "sa-draw.core"
                            :asset-path           "js"
                            :pretty-print false
                            :optimizations :simple}}]}}

             :community {
                          :cljsbuild
                            {:builds
                             [{:id "demo"
                               :source-paths ["src/cljs" "src/cljc"]
                               :compiler
                                  {:output-dir           "tmp/js"
                                   :output-to            "demo/js/main.js"
                                   :main                 "sa-draw.core"
                                   :asset-path           "js"
                                   :pretty-print false
                                   :optimizations :simple}}]}}
             :prod {
                    :cljsbuild
                     {:builds
                      [{:id "prod"
                        :source-paths ["src/cljs" "src/cljc"]
                        :compiler
                           {:output-dir           "tmp/js"
                            :output-to            "production/js/main.js"
                            :main                 "sa-draw.core"
                            :asset-path           "js"
                            :optimizations :advanced
                            :pseudo-names true
                            :pretty-print true
                            :npm-deps false
                            :infer-externs true
                            :externs ["tmp/js/inferred_externs.js" "externs.js"]}}]}}

;lein with-profile dev figwheel
             :dev  {:dependencies [[com.bhauman/figwheel-main       "0.1.5"]
                                   [com.bhauman/rebel-readline-cljs "0.1.4"]]



                    :cljsbuild
                      {:builds
                        [{:id "dev"
                          :source-paths ["src/cljs" "src/cljc"]
                          :compiler
                             {:output-dir           "resources/public/js"
                              :asset-path           "js"
                              :source-map           true
                              :source-map-timestamp true
                              :optimizations :none
                              :pretty-print true
                              :npm-deps false
                              :modules {:main {:entries #{sa-draw.main}
                                               :output-to "resources/public/js/main.js"}
                                        :home {:entries #{sa-draw.code-splits.home-split}
                                               :output-to "resources/public/js/home.js"}
                                        :edit {:entries #{sa-draw.code-splits.editor-split}
                                               :output-to "resources/public/js/edit.js"}}}

                          :figwheel {:on-jsload "sa-draw.main/init"}}]}}

;lein with-profile devcards figwheel
             :devcards
              {:dependencies [[re-frisk "0.5.4"]
                              [org.clojure/test.check "0.9.0"]]
               :plugins      [[lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]
                              [lein-ring      "0.12.4"]
                              [lein-figwheel  "0.5.17"]]
               :test-paths   ["test/cljs"]
               :cljsbuild
                 {:builds
                   [{:id "devcards"
                     :source-paths ["src/cljs" "src/cljc" "test/cljs"]
                     :compiler
                        {:main                 "sa_draw.model.test_proto_model_devcards"
                         :asset-path           "js/devcards_out"
                         :output-to            "resources/public/js/test_proto_model_devcards.js"
                         :output-dir           "resources/public/js/devcards_out"
                         :source-map-timestamp true
                         :source-map           true
                         :optimizations :none
                         :pretty-print true}
                     :figwheel {:on-jsload "sa_draw.model.test_proto_model_devcards/init"}}]}}
                                ;:load-warninged-code true}}]}}

             :uberjar {:auto-clean false}}



  :figwheel {:server-port 3000
             :repl        true
             :css-dirs ["resources/public/css"]} ;; watch and update CSS

  :clean-targets ^{:protect false} ["resources/public/js"
                                    "target"
                                    "demo/js"
                                    "production/js"
                                    "dev/js"
                                    "tmp"])
