(ns sa-draw.model.proto-model
  "
    Provides a common set of operations to navigate and update the core
    data model.
  ")

; These context types are used to help dispatch operations on context objects
(def context-type  (->  (make-hierarchy)
                        (derive ::prod   ::data-type)
                        (derive ::cons   ::data-type)
                        (derive ::source ::data-type)
                        (derive ::sink   ::data-type)))


(defprotocol SaModel
  "
   Protocol describes the operations needed to access and manipulate an
   sa model
  "

  (model-meta-version
    [this model]
    "
      Supplies the model meta version.  ie what version of the protocol does this
      model conform to.
    ")
  (name-space
    [this])

  (new-model
    [this]
    "
      Creates a new conforming model.
    ")

  (new-model-from-model
    [this model]
    [this model model-uuid model-title]
    "
      Creates a new model from the given model
      Creates a new model from the given model applying the uuid and title
    ")


  (make-model
    [this model-id model-title]
    "
      Creates a model with the given id and title.
    ")

  (find-process-model
    [this model]
    "
      Finds the process hierarchy in the model
    ")

  (find-flow-model
    [this model]
    "
      Finds the flow model in the model
    ")

  (find-dictionary
    [this model]
    "
      Finds the data dictionary.
    ")

  (find-context-model
    [this model])

  (find-icon-model
    [this model])

  (find-prototype-model
    [this model])

  (update-process-model
    [this model proc-model])

  (update-flow-model
    [this model flow-model])

  (update-dictionary
    [this model dictionary])

  (update-icon-model
    [this model iconsv])

  (update-prototype-icon-model
    [this model iconsv])

  (context-objects-map
    [this model])

  (context-objects
    [this model])


  (update-model-context-objects
    [this model context-objects]))


(defprotocol ContextObjects
  "
  Protocol describes how to operate on context objects
  "

  (find-producer
    [this context-objects id])

  (find-context-object-by-id
    [this context-objects id]
    "
      Given a seq of contexts objects find the one with the given id.
    ")


  (update-object
    [this context-object title description terms link])

  (new-producer
    [this context-type id title description dic-link link]
    [this context-type id title description dic-link link icon])

  (new-consumer
    [this context-type id title description dic-link link]
    [this context-type id title description dic-link link icon])

  (new-source
    [this context-type id title description dic-link]
    [this context-type id title description dic-link icon])

  (new-sink
    [this context-type id title description dic-link]
    [this context-type id title description dic-link icon])

  (context-object-id
    [this context-object])

  (context-object-title
    [this context-object])

  (context-object-description
    [this context-object])

  (dic-link
    [this context-object])

  (terms
    [this context-object])

  (link
    [this context-object])

  (context-object-type
    [this context-object])

  (context-object-icon
   [this context-object])



  (context-details
    [this context-object])

  (context-object-details
    [this context-object])

  (new-context-object
    [this type id title description terms dic-link link]
    [this type id title description terms dic-link link icon])

  (update-context-objects
    [this context-objects type context-object title description terms link])

  (add-to-context-objects
    [this context-objects type context-object])

  (delete-from-context-objects
    [this context-objects type context-object]))


(defprotocol SaProcs
  "
   Protocol describes the operations needed to access and manipulate
   an sa process model.
  "

  (find-node
    [this proc-model node-id]
    "
      Supplies the model meta version.  ie what version of the protocol does this
      model conform to.
    ")

  (find-node-in-list
    [this id procs]
    "
      Given a proc id find a node or nil in the given list of nodes.  This
      is a flat list search
    ")

  (remove-node-from-list
    [this id procs]
    "
      Removes the node with the given id from a list of nodes or does nothing.
    ")

  (remove-node
    [this proc-model node-id]
    "
      Removes a node and all its children from the model.
    ")

  (update-node
    [this id updated-node procs]
    "
      Replaces the node with the given id with provided updated-node.
    ")

  (patch-node
    [this procs-model id title description input-data output-data]
    [this procs-model id title description input-data output-data new-link]
    [this procs-model id title description input-data output-data new-link icon]
    "
      Updates parts of the node with the given id.
    ")

  (make-iconic-node
    [this id title data-in data-out description icon]
    "
      The icon is a map like {:type type :source source :name name}
      that maps into the prototype icon collection.
    ")

  (make-node
   [this id title data-in data-out description new-link])

  (add-node
   [this id new proc-model])

  (children
    [this node])

  (next-child
    [this parent-id children])

  (swap-left
    [this proc-model id]
    "
      If this node is not logically, the lowest id node on a layer,
      swap its id with the node logically on the left of it.  If there
      is no node on the left, do nothing.
    ")

  (swap-right
    [this proc-model id]
    "
      If this node is not logically, the highest id node on a layer,
      swap its id with the node logically on the right of it.  If there
      is no node on the right, do nothing.
    ")

  (compact
    [this node-list]
    [this node-list namespace]
    "
      Given a list of nodes, renumber nodes to fill in any gaps.
    ")


  (move-up
    [this proc-model id]
    "
      If this node is not at the first level, move it up a level and its sub tree.
    ")

  (add-new-child-node
    [this proc-model id]
    "
      Add a new child node to the given node.
    ")

  (add-new-child-node-with-icon
    [this proc-model id icon-spec]
    "
        Add a new child node with the specified icon to the given node.
    ")


  (id
    [this node])

  (data-in
    [this node]
    "
      Gets the data inputs for the node
    ")

  (data-out
    [this node]
    "
      Gets the data outputs for the node
    ")

  (title
    [this node]
    "
      Gets the title of the given node
    ")


  (description
    [this node]
    "
      Gets the description of the given node
    ")

  (details
    [this node]
    "
      Breaks the given node down ito its top level components
    ")

  (delegate
    [this node])

  (icon
    [this node])

  (cids
    [this node]
    "
      Find all the child ids of the given node all the way down
    ")

  (id-as-key
    [this node]
    "
    Given a node make a keyword out of the id
    like :namespace/id
  ")

  (char-id-as-key
    [this id]
    "
    Given a node make a keyword out of the id
    like :namespace/id
  ")

  (parent-id
    [this id]))
