(ns sa-draw.model.layout
  "
    Provides a common set of operations to find andcreate layouts.
  ")

(defprotocol SaLayout
  "
   Protocol describes the operations needed to access and manipulate an
   sa model layouts.
  "

  (new-layout
    [this]
    [this [max-x max-y]])

  (find-layout
    [this layout id]
    "
      Finds the layout for the given id.
    ")

  (add-layout
    [this layout id posn]
    "
      Adds a new layout for the given id.
      Will overwrite if the layout already exists.
    ")

  (known-layout?
    [this layout id]
    "
      true => we have the layout information for the given id.
    ")

  (remove-layout
    [this layout id]
    "
      Removes the layout with the given id.
    ")

  (key-namespace
    [this layout id])

  (serialise
    [this layout])

  (deserialise
    [this in]))
