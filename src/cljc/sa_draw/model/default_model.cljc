(ns sa-draw.model.default-model)

(def default-namespace "sadf")

(def empty-model
    #:sadf{:procs #:sadf{:id "0",
                         :title "Empty Model",
                         :description "Context Diagram",
                         :meta #:sadf {}
                         :data-in []
                         :data-out []
                         :children []
                         :dictionary {}
                         :flows []
                         :context-objects #:sadf{}
                         :icons []}
           :meta #:sadf{:namespace default-namespace
                        :name "",
                        :uuid "8a43df75-b0f2-4fc3-af5d-df97f9d03db7",
                        :summary "",
                        :detail #:sadf{:ownership "", :contact "", :contact-email ""}}
           :default-cloud-icons []
           :layout {(keyword "sadf" "0") [399 299]}})

;
; This is the new model you get when you press the '+' bitton on the app bar.
;
(def new-model
    #:sadf{:procs
            #:sadf{:id "0",
                   :title "Empty Model",
                   :description "Context Diagram",
                   :delegate ""
                   :meta #:sadf {}
                   :data-in []
                   :data-out []
                   :children []}


           :dictionary {}

           :flows []

           :context-objects #:sadf{:sources [#:sadf{:id "3472d649-a79c-4225-97ff-96f6e47a2ad1"
                                                    :dictionary-link "123"
                                                    :title "Operator 1"
                                                    :description ""
                                                    :context-type :sadf/sources}]
                                   :sinks [#:sadf{:id "6c921ea4-e1ce-420e-8e0a-63c1d5208d87"
                                                  :dictionary-link "321"
                                                  :title "End User 1"
                                                  :description ""
                                                  :context-type :sadf/sinks}]
                                   :producers [#:sadf{ :id "7a7e85a7-331f-4845-9a7e-06e675bef89d"
                                                       :dictionary-link "1956661"
                                                       :title "Pre Production Process"
                                                       :description ""
                                                       :context-type :sadf/producers}]
                                   :consumers [#:sadf{ :id "9dc6731d-d098-481c-b880-bdc56c9a1d5a"
                                                       :dictionary-link "874646"
                                                       :title "Post Production Processes"
                                                       :description ""
                                                       :context-type :sadf/consumers}]
                                   :symbols   []}

           :icons []
           :meta #:sadf{:namespace default-namespace
                        :name "empty-model",
                        :uuid "8a43df75-b0f2-4fc3-af5d-df97f9d03db7",
                        :summary "This is a blank model.",
                        :detail #:sadf{:ownership "Owner name", :contact "Contact name.", :contact-email "a@b.com"}}
           :sadf/layout {(keyword "sadf" "3472d649-a79c-4225-97ff-96f6e47a2ad1") [100 200] ;Dont apply the sadf namespace to the layout kw
                         (keyword "sadf" "6c921ea4-e1ce-420e-8e0a-63c1d5208d87") [700 500]
                         (keyword "sadf" "7a7e85a7-331f-4845-9a7e-06e675bef89d") [100 500]
                         (keyword "sadf" "9dc6731d-d098-481c-b880-bdc56c9a1d5a") [700 200]
                         (keyword "sadf" "0") [399 299]}
           :sadf/default-cloud-icons [{:title "Local"
                                       :licence {:name "TBD"
                                                 :uri    ""
                                                 :commit ""}
                                       :source "Software By Numbers Ltd"
                                       :symbols [{:name "actor"
                                                  :uri  "/svg/local/actor.svg"
                                                  :svg nil}
                                                 {:name "cloud"
                                                  :uri  "/svg/local/cloud.svg"
                                                  :svg nil}
                                                 {:name "note"
                                                  :uri  "/svg/local/note.svg"
                                                  :svg nil}
                                                 {:name "external link"
                                                  :uri  "/svg/local/link.svg"
                                                  :svg nil}]}

                                      {:title "Power"
                                       :licence {:name "svgrepo.com"
                                                 :uri    "Icons by svgrepo.com"
                                                 :commit ""}
                                       :source "https://www.svgrepo.com/collection/electricity-infrastructure-vectors/"
                                       :symbols [{:name "Lines"
                                                  :uri  "/svg/power/lines.svg"
                                                  :svg nil}
                                                 {:name "Generator"
                                                  :uri  "/svg/power/generator.svg"
                                                  :svg nil}
                                                 {:name "Power Station"
                                                  :uri  "/svg/power/power-station.svg"
                                                  :svg nil}]}
                                      {:title "Local K8S Octo-Technologies"
                                       :licence {:name   "TBD"
                                                 :uri    "https://github.com/octo-technology/kubernetes-icons/blob/master/LICENSE"
                                                 :commit "https://github.com/octo-technology/kubernetes-icons/commit/9839ad60ee4daff818cabeeaa599fc654e42b032"}
                                       :source "Local K8S Octo-Technology"
                                       :symbols [{:name "etc.d"
                                                  :uri  "/svg/octo/k8s/etcd.svg"
                                                  :svg nil}
                                                 {:name "master"
                                                  :uri  "/svg/octo/k8s/master.svg"
                                                  :svg nil}
                                                 {:name "node"
                                                  :uri  "/svg/octo/k8s/node.svg"
                                                  :svg nil}
                                                 {:name "kublet"
                                                  :uri  "/svg/octo/k8s/kubelet.svg"
                                                  :svg nil}
                                                 {:name "pod"
                                                  :uri  "/svg/octo/k8s/pod.svg"
                                                  :svg nil}]}]})


(def default-model
    #:sadf{:procs
            #:sadf{:id "0",
                   :title "Default Model",
                   :description "The context diagram",
                   :meta #:sadf {}
                   :data-in []
                   :data-out []
                   :children []}

           :dictionary {}

           :flows []

           :context-objects #:sadf{:sources [#:sadf{:id "1234-A"
                                                    :dictionary-link "123"
                                                    :title "Operator 1"}]
                                   :sinks [#:sadf{:id "A-1234"
                                                  :dictionary-link "321"
                                                  :title "End User 1"}]}
           :layout {(keyword "sadf" "0") [399 299]}
           :icons []
           :meta #:sadf{:namespace default-namespace
                        :name "test-model-2",
                        :uuid "8a43df75-b0f2-4fc3-af5d-df97f9d03db7",
                        :summary "This is a test model with id 123",
                        :detail #:sadf{:ownership "Owened by dept q", :contact "Fred Smith", :contact-email "a@b.com"}}})



(def demo-model-1
  {:sadf/procs
    {:sadf/id "0",
     :sadf/title "Sample Model",
     :sadf/description "Context Diagram",
     :sadf/data-in [],
     :sadf/data-out [],
     :sadf/children [{:sadf/id "1",
                      :sadf/title "Meta data Production",
                      :sadf/data-in [],
                      :sadf/data-out [],
                      :sadf/icon nil,
                      :sadf/description "",
                      :sadf/delegate "",
                      :sadf/children [{:sadf/id "1.1",
                                       :sadf/title "",
                                       :sadf/data-in [],
                                       :sadf/data-out [],
                                       :sadf/icon nil,
                                       :sadf/description "",
                                       :sadf/delegate "",
                                       :sadf/children []}]}
                     {:sadf/id "2",
                      :sadf/title "Proc 2 Title",
                      :sadf/data-in [{:label "a",
                                      :id "d8cf1fe1-3f2b-447b-90f4-b5eb95046e5d"}
                                     {:label "b",
                                      :id "5f95b289-6c73-437b-ba6c-3a1bb86793ed"}],
                      :sadf/data-out [],
                      :sadf/icon nil,
                      :sadf/description "Proc 2 description.",
                      :sadf/delegate "",
                      :sadf/children [{:sadf/id "2.1",
                                       :sadf/title "Title Of Proc 2.1",
                                       :sadf/data-in [],
                                       :sadf/data-out [],
                                       :sadf/icon nil,
                                       :sadf/description "Description of proc 2.1",
                                       :sadf/children []
                                       :sadf/delegate ""}

                                      {:sadf/id "2.2",
                                       :sadf/title "Proc 2.2",
                                       :sadf/data-in [],
                                       :sadf/data-out [],
                                       :sadf/icon nil,
                                       :sadf/description "Description of proc 2.2",
                                       :sadf/delegate "",
                                       :sadf/children []}]}]}
   :sadf/dictionary {},
   :sadf/flows [{:sadf/id "2aa7be96-e790-4b95-8943-f5ca548c01b5",
                 :sadf/data [{:sadf/term {:label "pre-production-meta-data", :id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}, :sadf/term-id "38dfa550-9be8-4a8e-8ee9-1a8b65e39380"}],
                 :sadf/source [{:sadf/posn [139 501], :sadf/id "48c790ed-5c1e-412a-8d7b-9edc448a881c", :sadf/proc-id :sadf/context}],
                 :sadf/sink [{:sadf/posn [319 455], :sadf/id "4604dfc7-e1f8-4799-88a3-b8023ef83258", :sadf/proc-id :sadf/context}],
                 :sadf/control-point [{:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [279 518], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "5165be40-d12f-4b52-be4f-20f5ae284021",
                 :sadf/data [{:sadf/term {:label "operator-controls", :id "98064324-188f-4411-9f83-bfcb234396a9"}, :sadf/term-id "98064324-188f-4411-9f83-bfcb234396a9"}],
                 :sadf/source [{:sadf/posn [110 150], :sadf/id "6bfadc45-5dfa-4002-893b-88532c7fe73a", :sadf/proc-id :sadf/context}],
                 :sadf/sink [{:sadf/posn [265 194], :sadf/id "59ff2f12-509b-4b40-a416-8edceb209e94", :sadf/proc-id :sadf/context}],
                 :sadf/control-point [{:sadf/posn [224 113], :sadf/id "0a270eae-c248-464c-bec4-2cb4ea9185be", :sadf/proc-id nil} {:sadf/posn [224 113], :sadf/id "0a270eae-c248-464c-bec4-2cb4ea9185be", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "e97e3ae1-78db-4246-83ee-db00ec7f4dc9",
                 :sadf/data [{:sadf/term {:label "end-user-monitoring-data", :id "d48a7a87-d038-450f-bc86-c23a6a53768b"}, :sadf/term-id "d48a7a87-d038-450f-bc86-c23a6a53768b"}],
                 :sadf/source [{:sadf/posn [523 181], :sadf/id "657735ca-4ef3-4ee9-8fae-ae98cf4dc087", :sadf/proc-id :sadf/context}],
                 :sadf/sink [{:sadf/posn [661 87], :sadf/id "f74c06f2-0174-4027-a672-25c3b535a9d1", :sadf/proc-id :sadf/context}],
                 :sadf/control-point [{:sadf/posn [591 115], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [591 115], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "0868a3ce-3141-4788-abee-d874179e3643",
                 :sadf/data [{:sadf/term {:label "post-production-meta-data", :id "736cd0fd-907a-433e-9a97-2c16c79a988a"}, :sadf/term-id "736cd0fd-907a-433e-9a97-2c16c79a988a"}],
                 :sadf/source [{:sadf/posn [539 395], :sadf/id "f64cfdb2-e074-46a7-9e2f-4d1fbdffe394", :sadf/proc-id :sadf/context}],
                 :sadf/sink [{:sadf/posn [666 435], :sadf/id "04c656ac-cb6a-4614-8212-da4e02e39ac3", :sadf/proc-id :sadf/context}],
                 :sadf/control-point [{:sadf/posn [639 412], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [639 412], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "49497fef-6645-4c68-9dd3-bf1cbe924daf", :sadf/data [{:sadf/term {:label "data-1", :id "8ae65217-4169-436f-9c4b-eeb970299c0f"}, :sadf/term-id "8ae65217-4169-436f-9c4b-eeb970299c0f"}],
                 :sadf/source [{:sadf/posn [307 222], :sadf/id "d680ae27-15cb-44c3-89cb-7c8c9af1d2f2", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/sink [{:sadf/posn [485 282], :sadf/id "98f7804e-a738-4c10-ad45-fd5c411d0f35", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/control-point [{:sadf/posn [447 180], :sadf/id "baef0bcd-e02a-4d2e-a559-47e2c7ce69b8", :sadf/proc-id nil} {:sadf/posn [447 180], :sadf/id "baef0bcd-e02a-4d2e-a559-47e2c7ce69b8", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "1ece2e78-8a48-4f79-84d8-874cdd8a9381",
                 :sadf/data ({:sadf/term {:label "data-1", :id "7d788447-def5-4edd-ba6e-14c10d3549a5"}, :sadf/term-id "7d788447-def5-4edd-ba6e-14c10d3549a5"} {:sadf/term {:label "default", :id "5c3497c3-285f-467d-ab89-2b3bd4f0ce32"}, :sadf/term-id "5c3497c3-285f-467d-ab89-2b3bd4f0ce32"}),
                 :sadf/source [{:sadf/posn [550 277], :sadf/id "9c68ff68-5316-4be1-923e-cdee8ad50606", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/sink [{:sadf/posn [710 195], :sadf/id "c405a9cd-6378-4802-bbc4-a00a126deff9", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/control-point [{:sadf/posn [586 193], :sadf/id "0a270eae-c248-464c-bec4-2cb4ea9185be", :sadf/proc-id nil} {:sadf/posn [586 193], :sadf/id "0a270eae-c248-464c-bec4-2cb4ea9185be", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "cbfe3c6b-3a70-4527-a197-62f7593588f7",
                 :sadf/data [{:sadf/term {:label "a-data-b", :id "d8361066-85d5-4277-84ae-c24b50a9b7a0"}, :sadf/term-id "d8361066-85d5-4277-84ae-c24b50a9b7a0"}],
                 :sadf/source [{:sadf/posn [552 403], :sadf/id "2e0a895e-2351-425a-98bb-1c26d3097273", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/sink [{:sadf/posn [754 454], :sadf/id "46751df3-e22b-48f4-9499-437ae49ecaf5", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/control-point [{:sadf/posn [580 507], :sadf/id "baef0bcd-e02a-4d2e-a559-47e2c7ce69b8", :sadf/proc-id nil} {:sadf/posn [580 507], :sadf/id "baef0bcd-e02a-4d2e-a559-47e2c7ce69b8", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "42bb48b9-d872-4f92-ac16-e11c3eef5916",
                 :sadf/data [{:sadf/term {:label "my-data", :id "af5e06e8-53d2-451f-a840-5c74b51aeff6"}, :sadf/term-id "af5e06e8-53d2-451f-a840-5c74b51aeff6"}],
                 :sadf/source [{:sadf/posn [20 143], :sadf/id "8c818d6c-4c53-4d1b-ae61-d118ec1071a9", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/sink [{:sadf/posn [184 189], :sadf/id "b8191596-a904-4013-a55a-4d93c66c9f74", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/control-point [{:sadf/posn [137 135], :sadf/id "0a270eae-c248-464c-bec4-2cb4ea9185be", :sadf/proc-id nil} {:sadf/posn [137 135], :sadf/id "0a270eae-c248-464c-bec4-2cb4ea9185be", :sadf/proc-id nil}],
                 :sadf/description ""}
                {:sadf/id "bfdcfb67-8faf-40cb-8939-0026e463e71d",
                 :sadf/data ({:sadf/term {:label "data-1-22", :id "f5620d16-13d8-4226-a9a5-a593d2d3458b"}, :sadf/term-id "f5620d16-13d8-4226-a9a5-a593d2d3458b"} {:sadf/term {:label "data-a-b", :id "5fab5a97-0652-4482-9a0d-653368be4873"}, :sadf/term-id "5fab5a97-0652-4482-9a0d-653368be4873"}),
                 :sadf/source [{:sadf/posn [38 459], :sadf/id "69df6db6-5315-4dc3-9cd2-49ce9cbc9ff8", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/sink [{:sadf/posn [463 379], :sadf/id "1fca0de8-48a1-4c7e-bab1-3189cda7f7e1", :sadf/proc-id (keyword "sadf" "0")}],
                 :sadf/control-point [{:sadf/posn [253 515], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil} {:sadf/posn [253 515], :sadf/id "677eec3a-09f3-476d-886f-4e839a360347", :sadf/proc-id nil}],
                 :sadf/description ""}],
   :sadf/context-objects {:sadf/sources   [{:sadf/id "3472d649-a79c-4225-97ff-96f6e47a2ad1", :sadf/dictionary-link "123", :sadf/title "Operator 1", :sadf/description "" :sadf/context-type :sadf/sources}],
                          :sadf/sinks     [{:sadf/id "6c921ea4-e1ce-420e-8e0a-63c1d5208d87", :sadf/dictionary-link "321", :sadf/title "End User 1", :sadf/description "" :sadf/context-type :sadf/sinks}],
                          :sadf/producers [{:sadf/id "7a7e85a7-331f-4845-9a7e-06e675bef89d", :sadf/dictionary-link "1956661", :sadf/title "Pre Production Process", :sadf/description "" :sadf/context-type :sadf/producers}],
                          :sadf/consumers [{:sadf/id "9dc6731d-d098-481c-b880-bdc56c9a1d5a", :sadf/dictionary-link "874646", :sadf/title "Post Production Processes", :sadf/description "" :sadf/context-type :sadf/consumers}]},
   :sadf/icons []
   :sadf/meta {:sadf/namespace "sadf",
               :sadf/name "Initial Base Model",
               :sadf/uuid "a-sample-model",
               :sadf/app-version {:sa-draw.config.defaults/major "0", :sa-draw.config.defaults/minor "2", :sa-draw.config.defaults/patch "6", :sa-draw.config.defaults/pre-release nil, :sa-draw.config.defaults/build-meta nil},
               :sadf/summary "A basic model as a starting point.\n\nEdit this model as an initial model.\n\nUnlock the model then save it.",
               :sadf/detail {:sadf/ownership "Owner name",
                             :sadf/contact "Contact name.",
                             :sadf/contact-email "a@b.com"},
               :sadf/creation-date-time "2019-06-20 15:34:42",
               :sadf/board-link "https://trello.com/b/xPlwFlb9/test-model"},
   :sadf/layout {(keyword "sadf" "1.2") [521 128],
                 (keyword "sadf" "6c921ea4-e1ce-420e-8e0a-63c1d5208d87") [661 53],
                 (keyword "sadf" "9dc6731d-d098-481c-b880-bdc56c9a1d5a") [702 470],
                 (keyword "sadf" "2") [525 342],
                 (keyword "sadf" "3472d649-a79c-4225-97ff-96f6e47a2ad1") [75 150],
                 (keyword "sadf" "2.1") [440 215],
                 (keyword "sadf" "1.1") [330 292],
                 (keyword "sadf" "2.2") [223 111],
                 (keyword "sadf" "1") [237 236],
                 (keyword "sadf" "7a7e85a7-331f-4845-9a7e-06e675bef89d") [90 498]
                 (keyword "sadf" "0") [399 299]}
   :sadf/default-cloud-icons [{:title "Local"
                               :licence {:name "TBD"
                                         ;set these to nil or empty string if they dont exist
                                         :uri  ""
                                         :commit ""}
                               :source "Software By Numbers Ltd"
                               :symbols [{:name "actor"
                                          :uri  "/svg/local/actor.svg"
                                          :svg nil}
                                         {:name "cloud"
                                          :uri  "/svg/local/cloud.svg"
                                          :svg nil}
                                         {:name "note"
                                          :uri  "/svg/local/note.svg"
                                          :svg nil}
                                         {:name "external link"
                                          :uri  "/svg/local/link.svg"
                                          :svg nil}]}
                              {:title "Local K8S Octo-Technologies"
                               :licence {:name   "TBD"
                                         :uri    "https://github.com/octo-technology/kubernetes-icons/blob/master/LICENSE"
                                         :commit "https://github.com/octo-technology/kubernetes-icons/commit/9839ad60ee4daff818cabeeaa599fc654e42b032"}
                               :source "Local K8S Octo-Technology"
                               :symbols [{:name "etc.d"
                                          :uri  "/svg/octo//k8s/etcd.svg"
                                          :svg nil}
                                         {:name "master"
                                          :uri  "/svg/octo/k8s/master.svg"
                                          :svg nil}
                                         {:name "node"
                                          :uri  "/svg/octo/k8s/node.svg"
                                          :svg nil}
                                         {:name "kublet"
                                          :uri  "/svg/octo/k8s/kubelet.svg"
                                          :svg nil}
                                         {:name "pod"
                                          :uri  "/svg/octo/k8s/pod.svg"
                                          :svg nil}]}]})
