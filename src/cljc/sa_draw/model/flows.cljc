(ns sa-draw.model.flows
  "Flow model.  An abstract view of the flows in the system.")


(defprotocol SaFlows
  "
   Protocol describes the operations needed to access and manipulate
   an sa flow model.
  "

  (new-flow
   [this v sourcev sinkv]
   [this v sourcev sinkv description])

  (id
    [this flow])

  (find-flow
    [this flowsv flow-id])

  (remove-flow
    [this flowsv flow])

  (remove-flow-by-id
    [this flowsv flow-id])

  (update-flow
    [this flowsv flow flow-id])

  (update-flow-description
    [this flow description])

  (update-flow-data
    [this flow data])

  (data-items
    [this flow])

  (sources
    [this flow])

  (sources-sinks-for-layer
    [this flows layer])

  (endpoint-details
    [this endpoint])

  (sinks
    [this flow])

  (cps
    [this flow])

  (description
    [this flow])

  (details
    [this flow])

  (duplicate-flow-to-layer
    [this flow layer]))
