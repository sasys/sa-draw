(ns sa-draw.model.icons
  "
    An abstract view of the icons in the system.
    Icons have a set of prototypes which are realised as instances in the model.
    This absraction only deals with the icons model.  Any updates to this
    model need to be applied to the actually sa model using the sa model abstraction.
  ")


(defprotocol Icons
  "
   Protocol describes the operations needed to access and manipulate
   the icons associated with an sa model.
  "
  ;Prototype operations.  These operations relate to manipulation of prototype
  ;icons either default or servr sourced.

  ;Instance operations  These operations relate to instances of icons within a specific
  ;model.

  (new-icon
    [this prototype-icon source icon-source-title layer]
    [this prototype-icon source icon-source-title layer description]
    [this prototype-icon source icon-source-title layer description title]
    "
      Create a new icon instance from a proptotype icon.
    ")

  (add-icon
    [this iconsv new-icon]
    "
      Add the given icon to the icons model.
      If the icon has no uuid we create one.
      Returns the icons model with the icon added.
    ")

  (add-icons
    [this iconsv new-icons]
    "
      Add the given icons to the icons model.
      If the icon has no uuid we create one.
      Returns the icons model with the icon added.
    ")

  (find-icon-instance
    [this iconsv icon-id]
    "
      Find an icon given the icon model and the icon uuid.
      Returns the icon instance or nill.
    ")

  (find-icon-instances
    [this iconsv source title icon-name]
    "
      Find all the icon instances for the given the icon model and the prototype icon..
      Returns a vector of instances that may be empty.
    ")

  (icons-for-layer
    [this iconsv layer]
    "
      Find all the icons that are on the given layer/
    ")

  (title
    [this icon]
    "
      The title for the icon.
    ")

  (description
    [this icon]
    "
      The description for the icon.
    ")

  (link
    [this icon]
    "
      The link for the icon.
    ")

  (id
    [this icon])

  (svg-for-icon
    [this prototype-icons icon]
    "
      Given a prototype icon get the svg for that icon.
    ")

  (remove-icon
    [this iconsv icon]
    "
      Remove the given icon from the icons model.
      Returns the updated icons model.
    ")

  (update-title
    [this title icon]
    "
      Update the title of the given icon
    ")

  (update-description
    [this description icon]
    "
      Update the description of the given icon
    ")

  (update-link
    [this link icon]
    "
      Update the link of the given icon
    ")

  (update-icon
    [this iconsv icon]))
