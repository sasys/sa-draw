(ns sa-draw.data.dd
  "Data dictionary model.  An abstract view of the flows in the system.")


(defprotocol Dictionary
  "
   Protocol describes the operations needed to access and manipulate
   data in the dictionary.
  "

  (new-dictionary
    [this])

  (as-map
    [this dictionary])

  (add-term
    [this term dictionary]
    [this term parent dictionary]
    "
    Creates a new data item.
    ")

  (update-term
    [this term dictionary])



  (term-by-id
    [this term-id dictionary])

  (list-terms
    [this dictionary])

  (term-exists?
    [this t dictionary])

  (remove-term
   [this id dictionary])

  (clear-dd
    [this dictionary]))




(defprotocol Term
  "

  "
  (new-term
    [this label]
    [this label description])

  (id
    [this term])

  (label
    [this term])

  (description
    [this term])

  (details
    [this term])

  (clean
    [this t]))

  ;
  ; (parent
  ;   [this term dictionary])
  ;
  ; (sibs
  ;   [this term dictionary])
  ;
  ; (prim?
  ;   [this term dictionary])
  ;
  ; (top?
  ;   [this term dictionary])
  ;
  ; (label
  ;   [this term]
  ;   "
  ;     Displayable label.
  ;   ")
  ;
  ; (description
  ;   [this term]
  ;   "
  ;     Displayable description.
  ;   ")
  ;
  ; (meta-data
  ;   [this term]
  ;   "
  ;     Meta data / tags on this term.
  ;   "))
