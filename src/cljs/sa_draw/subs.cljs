(ns sa-draw.subs
  "
    Top level subs.
  "
  (:require [re-frame.core  :as rf]
            [sa-draw.events :as ev]))


(rf/reg-sub
  ::layout
  (fn [db _]
    (get-in db [::ev/graphics ::ev/layout])))

(rf/reg-sub
  ::user
  (fn [db _]
    (get-in db [::ev/user])))


;; Subs related to the currently selected layer and its details

(rf/reg-sub
  ::current-proc
  (fn [db _]
    [(get-in db [::ev/graphics ::ev/current-layer ::ev/layer])
     (get-in db [::ev/graphics ::ev/current-layer ::ev/title])
     (get-in db [::ev/graphics ::ev/current-layer ::ev/description])
     (get-in db [::ev/graphics ::ev/current-layer ::ev/data-in])
     (get-in db [::ev/graphics ::ev/current-layer ::ev/data-out])
     (get-in db [::ev/graphics ::ev/current-layer ::ev/name-space])]))

(rf/reg-sub
  ::current-layer :<- [::current-proc]
  (fn [[layer _ _ _ _ _] _]
    layer))

(rf/reg-sub
  ::current-layer-title :<- [::current-proc]
  (fn [[_ title _ _ _ _] _]
    title))

(rf/reg-sub
  ::current-layer-description :<- [::current-proc]
  (fn [[_ _ description _ _ _] _]
    description))

(rf/reg-sub
  ::current-layer-data-in :<- [::current-proc]
  (fn [[_ _ _ data-in  _ _] _]
    data-in))

(rf/reg-sub
  ::current-layer-data-out :<- [::current-proc]
  (fn [[_ _ _ _  data-out _] _]
    data-out))


(rf/reg-sub
  ::current-layer-name-space :<- [::current-proc]
  (fn [[_ _ _ _ _ name-space] _]
    name-space))

(rf/reg-sub
  ::current-layer-context?
  (fn [db _]
    (get-in db [::ev/graphics ::ev/current-layer ::ev/show-context?])))

(rf/reg-sub
  ::edit-locked
  (fn [db _]
    (get-in db [::ev/graphics ::ev/edit-locked])))


;Model changes
(rf/reg-sub
  ::model
  (fn [db _]
    (get-in db [::ev/model])))

(rf/reg-sub
  ::flows :<- [::model]
  (fn [model _]
    (:sadf/flows model)))

(rf/reg-sub
  ::procs :<- [::model]
  (fn [model _]
    (:sadf/procs model)))

(rf/reg-sub
  ::contexts :<- [::model]
  (fn [model _]
    (:sadf/context-objects model)))

(rf/reg-sub
  ::icons :<- [::model]
  (fn [model _]
    (:sadf/icons model)))

(rf/reg-sub
  ::default-icons :<- [::model]
  (fn [model _]
    (:sadf/default-cloud-icons model)))

(rf/reg-sub
  ::summaries
  (fn [db _]
    (get-in db [::ev/model-summaries])))

(rf/reg-sub
  ::browser-summaries
  (fn [db _]
    (get-in db [::ev/model-summaries :sa.sa-store.store-factory/key-store])))

(rf/reg-sub
  ::dirty
  (fn [db _]
    (::ev/dirty db)))

(rf/reg-sub
  ::store
  (fn [db _]
    (-> db ::ev/config ::ev/current-store)))


;; Subs related to the config

(rf/reg-sub
  ::config
  (fn [db _]
    (::ev/config db)))

(rf/reg-sub
  ::style :<- [::config]
  (fn [config]
    (::ev/style config)))

(rf/reg-sub
  ::theme :<- [::style]
  (fn [style]
    (::ev/theme style)))

(rf/reg-sub
  ::dimensions :<- [::style]
  (fn [style]
    (::ev/dimensions style)))

;Available stores
(rf/reg-sub
  ::available-stores :<- [::config]
  (fn [config]
    (-> config ::ev/defaults :sa-draw.config.defaults/available-stores)))


;;;;markdown related
(rf/reg-sub
  ::markdown
  (fn [db _]
    (::ev/markdown db)))
