(ns sa-draw.data.simple-dd
  "
  Simple dd and term implementation
  "
  (:require [sa-draw.data.dd :as dic :refer [Dictionary Term]]
            [sa-draw.model.utils :as ut :refer [uid]]))


(declare term)

(deftype DataDictionary []
  Dictionary

  (new-dictionary
    [this]
    {})

  (as-map
    [this dictionary]
    dictionary)

  (add-term
    [this t dictionary]
    (assoc dictionary (keyword (dic/id term t)) {:id (dic/id term t) :label (dic/label term t)}))


  (add-term
    [this term parent dictionary]
    nil)

  (update-term
    [this t dictionary]
    (dic/add-term this t dictionary))


  (term-by-id
    [this term-id dictionary]
    (let [term-key (keyword term-id)]
      (-> dictionary term-key)))

  (list-terms
    [this dictionary]
    (sort-by :label (vals dictionary)))

  (term-exists?
    [this t dictionary]
    (some #{(:label t)} (map :label (vals dictionary))))


  (remove-term
   [this id dictionary]
   (dissoc dictionary (keyword id)))


  (clear-dd
    [this dictionary]
    {}))


(deftype DataTerm []
  Term

  (new-term
    [this label]
    {:label label
     :id (uid)})


  (new-term
    [this label description]
    {:id (uid)
     :label label
     :description description})

  (id
    [this term]
    (:id term))

  (label
    [this term]
    (:label term))

  (description
    [this term]
    (:description term))

  (details
    [this term] 
    [(dic/label this term)
     (dic/id this term)
     (dic/description this term)])

  (clean
    [this t]
    {:id (dic/id this t) :label (dic/label this t) :description (dic/description this t)}))

(def dd (DataDictionary.))
(def term (DataTerm.))
