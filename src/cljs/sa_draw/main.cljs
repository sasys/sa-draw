(ns sa-draw.main
  "
   The main application display framework
   Mounts the fx listeners, creates the html elements and popups.
  "
  (:require [cljs.loader                           :as loader]
            [material-ui                           :as mui]
            [reagent.core                          :as r]
            [re-frame.core                         :as rf]
            [sa-draw.events                        :as ev]
            [sa-draw.model.proto-model             :as proto-model]
            [sa-draw.views.help.help-viewer        :as hui]
            [sa-draw.config.subs                   :as cfsb]
            [sa-draw.config.defaults               :as df]
            [sa-draw.views.app-bar                 :refer [application-tools]]
            [sa-draw.model.map-impl                :refer [map-model proc-model]]))
            ;[re-frisk.core                         :refer [enable-re-frisk!]]))


;(set! *warn-on-infer* true)

(defonce home? (r/atom true))

(defn framework
  [flows layer svg-name]

  (let [theme  @(rf/subscribe [:sa-draw.subs/theme])]

    (fn [flows layer svg-name]
     (let [show-markdown-viewer?   @(rf/subscribe [::cfsb/show-markdown-viewer])
           show-structured-editor? @(rf/subscribe [::cfsb/show-structured-editor])
           show-icon-manager?      @(rf/subscribe [::cfsb/show-icon-manager])]
      [:div (if @home? {:id "inner-app" :class "home"} {:id "inner-app" :class "edit"})
        [:<>
          [:> mui/CssBaseline]
          [:> mui/MuiThemeProvider {:theme theme}
            [:> mui/Grid {:container    true
                          :direction    "column"
                          :justify      "center"
                          :spacing 2
                          :id "sa-framework-base"}

              [application-tools home?]
              [hui/help-ui]

              ;This div is the target for the tool bar
              [:div  (if @home?  {:id "tool-bar" :style {:display "none"}} {:id "tool-bar"})]

              ;This div is the target for the tool drawers
              [:div {:id "tool-bar-drawers"}]

              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         false
                            :justify      "center"
                            :alignItems   "center"
                            :spacing      8}

                ;This is the div in which the model editer will go.
                [:div (if @home?  {:id "node-div" :style {:display "none"}} {:id "node-div"})]

                ;This is the div in which the spec editer will go.
                [:div (if @home?  {:id "spec-div" :style {:display "none"}} {:id "spec-div"})]

                [:> mui/Grid {:container    true
                              :direction    "row"
                              :item         false
                              :justify      "center"
                              :alignItems   "stretch"
                              :spacing      8}

                  ;This is the div in which the markdown editer will go.  If home page dont show, if not home and visible show.
                  [:div (if (and (not @home?) show-markdown-viewer?)  {:id "markdown-div"} {:id "markdown-div" :style {:display "none"}})]

                  ;This is the markdown editor div
                  [:div (if (and (not @home?) show-structured-editor?)  {:id "markdown-editor-div"} {:id "markdown-editor-div" :style {:display "none"}})]

                  ;This is the icon manager div
                  [:div (if (and (not @home?) show-icon-manager?)  {:id "icon-manager-div"} {:id "icon-manager-div" :style {:display "none"}})]]

                [:div (if @home? {:id "home-screen" :style {:width "60%" :margin-top "7%"}}  {:id "hidden-home-screen" :style {:display "none"}})
                  [:> mui/Grid {:container    true
                                :direction    "row"
                                :item         false
                                :justify      "space-between"
                                :alignItems   "center"
                                :spacing      8
                                :id           "home-screen-parts"}]]]

              ;This div is the target for the edit code split after it is loaded.
              [:div {:id "edit-div"}]]]]]))))


(defn mount-root
  ([]
   (let [start-time (.now js/Date)]
     (.log js/console (str "mount root started    : " start-time))
     (rf/clear-subscription-cache!)
     (let [sub-cleared-time (- (.now js/Date) start-time)]
       (.log js/console (str "sub cleared in     : " sub-cleared-time)))


     (let [default-layer (::df/default-proc-id df/default-config)
           model (proto-model/new-model map-model);
           process-model (proto-model/find-process-model map-model model)
           ; name-space (proto-model/name-space map-model)
           start-node (proto-model/find-node proc-model process-model default-layer)
           start-node-details (proto-model/details proc-model start-node)
           context-layout {:0 [450 450]}
           flow-model (proto-model/find-flow-model map-model model)
           ready-to-render-time (.now js/Date)]

        (rf/dispatch-sync [::ev/initialize-db context-layout model "layout-engine" start-node-details])

        (.log js/console (str "ready to render    : " ready-to-render-time))
        (r/render [framework flow-model default-layer "svg-name"]  (.getElementById js/document "app"))
        (let [render-done-time (.now js/Date)]
          (.log js/console (str "framework rendered in     : " (- render-done-time ready-to-render-time))))

        ;UNUSED SPLIT - PLACEHOLDER FOR NOW
        ;render split home
        (let [home-start (.now js/Date)]
          (loader/load :home
            (fn []
              ((resolve 'sa-draw.code-splits.home-split/load-home-screen) home? "home-screen-parts")))
          (.log js/console (str "home screen loaded in      : " (- (.now js/Date) home-start))))

        ;Here we supply the id of the div to target with the edit elements and the tool bar elements
        (let [edit-start (.now js/Date)]
          (loader/load :edit
            (fn []
              ((resolve 'sa-draw.code-splits.editor-split/edit-tools) default-layer "svg-name" "edit-div" "tool-bar" "tool-bar-drawers" "node-div" "spec-div" "markdown-div" "markdown-editor-div" "icon-manager-div")))
          (.log js/console (str "edit screen loaded in      : " (- (.now js/Date) edit-start)))))))


  ([name-space layer svg-name model]
   (let [flow-model (proto-model/find-flow-model map-model model)]


      ;Initialise the app state


      ;Render the main frame of the app
      (r/render [framework flow-model layer svg-name]  (.getElementById js/document "app"))


      ;UNUSED SPLIT - PLACEHOLDER FOR NOW
      ;render split home
      (loader/load :home
        (fn []
          ((resolve 'sa-draw.code-splits.home-split/load-home-screen) home? "home-screen-parts")))

      ;Here we supply the id of the div to target with the edit elements and the tool bar elements
      (loader/load :edit
        (fn []
          ((resolve 'sa-draw.code-splits.editor-split/edit-tools) layer svg-name "edit-div" "tool-bar" "tool-bar-drawers" "node-div" "spec-div"))))))


(defn ^:export init
  "
    The main entry point on a page reload.
  "
  []
  (.log js/console "Initialising the drawing test application")
  ;(enable-re-frisk!)
  (let [mount-time (.now js/Date)]
    (.log js/console (str "mount root    : " mount-time))
    (mount-root)
    (let [mounted-time (.now js/Date)
          time-to-mount (- mounted-time mount-time)]
      (.log js/console (str "mounted root  : " mounted-time))
      (.log js/console (str "mounted delta : " time-to-mount)))))

(loader/set-loaded! :main)
