(ns sa-draw.global-state
  "
    TODO Move into the app state.
    Contains global state.
  "
  (:require [reagent.core               :as r]))

(def project-open (r/atom false))

(def layers-drawer-open (r/atom false))
(def layers-drawer-pinned (r/atom false))

(def information-menu-open (r/atom false))
(def information-menu-pinned (r/atom false))

(def config-drawer-pinned (r/atom false))
(def config-drawer-open (r/atom false))

(def model-info-drawer-pinned (r/atom false))
(def model-info-drawer-open (r/atom false))

(def user-menu-open (r/atom false))
(def user-menu-pinned (r/atom false))

(def meta-data-editor-open (r/atom nil))
