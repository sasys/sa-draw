(ns sa-draw.views.licence.licence-viewer-events
  (:require [re-frame.core  :as rf]))

(rf/reg-event-db
  ::licence-viewer-open
  (fn [db [_ [open? source]]]
    (let [organisation (:source source)
          title        (:title source)
          licence-name (get-in source [:licence :name])
          licence-uri  (get-in source [:licence :uri])
          commit       (get-in source [:licence :commit])]

      (if open?
        (assoc-in db [:sa-draw.events/views ::licence-viewer] {::open? true
                                                               ::licence-details {::organisation organisation
                                                                                  ::title title
                                                                                  ::licence-name licence-name
                                                                                  ::licence-uri licence-uri
                                                                                  ::commit commit}})
        (assoc-in db [:sa-draw.events/views ::licence-viewer] {::open? false
                                                               ::licence-details {::organisation nil
                                                                                  ::title        nil
                                                                                  ::licence-name nil
                                                                                  ::licence-uri  nil
                                                                                  ::commit       nil}})))))
