(ns sa-draw.views.licence.licence-viewer-subs
  (:require [re-frame.core  :as rf]))


(rf/reg-sub
  ::licence-viewer
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.licence.licence-viewer-events/licence-viewer])))

(rf/reg-sub
  ::licence-viewer-open :<- [::licence-viewer]
  (fn [licence-viewer _]
    (:sa-draw.views.licence.licence-viewer-events/open? licence-viewer)))

(rf/reg-sub
  ::licence-details :<- [::licence-viewer]
  (fn [licence-details _]
    (:sa-draw.views.licence.licence-viewer-events/licence-details licence-details)))
