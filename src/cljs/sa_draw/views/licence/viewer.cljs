(ns sa-draw.views.licence.viewer
  "
    Presents the licence details for a set of icons.
  "
  (:require [re-frame.core  :as rf]
            ["@material-ui/core"                              :as mui]
            [material-ui-icons                                :as muicons]
            [sa-draw.views.licence.licence-viewer-subs        :as lvs]
            [sa-draw.views.licence.licence-viewer-events      :as lve]))

;TODO MAKE THIS DEF COMMON SOMEWHERE
(def anchor-element-id "edit-div")

(defn licence-viewer-ui []
  (let [licence-details (rf/subscribe [::lvs/licence-details])]
    (fn []
      (let [{::lve/keys [organisation title licence-name licence-uri commit]} @licence-details]
        [:> mui/Grid { :container    true
                       :direction    "column"
                       ;:item         true
                       :justify      "space-between"
                       :alignItems   "stretch"
                       :style {:margin 10}}
          [:> mui/Typography {:variant "caption" } (str "Title : "        title)]
          [:> mui/Typography {:variant "caption" } (str "Organisation : " organisation)]
          [:> mui/Typography {:variant "caption" } (str "Licence Name : " licence-name)]

          [:> mui/Grid { :container    true
                         :direction    "row"
                         ;:item         true
                         :justify      "space-between"
                         :alignItems   "center"
                         :style {:margin 0}}
            [:> mui/Typography {:variant "caption" } (str "Licence : ")]
            [:> mui/Tooltip (if (or (= licence-uri "") (nil? licence-uri)) {:title "No Licence Text Available"} {:title "Licence Text"})
              [:> mui/IconButton {:variant "text" :href licence-uri :target "_blank"}
                [:> muicons/Link]]]]


          [:> mui/Grid { :container    true
                         :direction    "row"
                         ;:item         true
                         :justify      "space-between"
                         :alignItems   "center"
                         :style {:margin 0}}
            [:> mui/Typography {:variant "caption" } (str "Licence Commit : ")]
            [:> mui/Tooltip (if (or (= commit "") (nil? commit)) {:title "No Licence Commit Available"} {:title "Licence Commit"})
                [:> mui/IconButton {:variant "text" :href commit :target "_blank"}
                  [:> muicons/Link]]]]]))))
