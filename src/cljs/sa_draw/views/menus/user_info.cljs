(ns sa-draw.views.menus.user-info
  (:require [material-ui                           :as mui]
            [material-ui-icons                     :as muicons]
            [re-frame.core                         :as rf]
            [sa-draw.global-state                  :refer [user-menu-open user-menu-pinned]]))


(defn user-menu [anchor-element-id id-to-offset]
  (fn []
    (let [vertical-offset (:sa-draw.events/app-bar-height @(rf/subscribe [:sa-draw.subs/dimensions]))
          user @(rf/subscribe [:sa-draw.subs/user])
          user-name (:sa-draw.events/user-name user)
          user-email (:sa-draw.events/user-email user)]
      [:> mui/Menu {:open @user-menu-open
                    :anchorEl (.getElementById js/document anchor-element-id)
                    :style {:margin-top (str vertical-offset "px")}}
        [:> mui/MenuItem  (str user-name "  " user-email "  ")]


        [:> mui/Divider]
        [:> mui/MenuItem {:onClick #(if-not @user-menu-pinned (reset! user-menu-open false))} "User Profile"]
        [:> mui/MenuItem {:onClick #(if-not @user-menu-pinned (reset! user-menu-open false))} "User Settings"]
        [:> mui/Divider]
        [:> mui/MenuItem {:onClick #(if-not @user-menu-pinned (reset! user-menu-open false))} "Log Out"]
        [:> mui/Divider]
        [:> mui/Grid {:container    true
                      :direction    "row"
                      :item         true
                      :justify      "space-between"
                      :alignItems   "center"}
          [:> mui/Tooltip {:title "Unpin menu"}
            [:> mui/IconButton {:onClick (fn [] (reset! user-menu-pinned false)
                                                (reset! user-menu-open false))}
              [:> muicons/Close]]]
          (if (not @user-menu-pinned)
            [:> mui/Tooltip {:title "Pin menu"}
              [:> mui/IconButton {:onClick #(swap! user-menu-pinned not @user-menu-pinned)}
                [:> muicons/OfflinePin]]])]])))
