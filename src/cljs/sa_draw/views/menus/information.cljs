(ns sa-draw.views.menus.information
  (:require [material-ui                           :as mui]
            [material-ui-icons                     :as muicons]
            [re-frame.core                         :as rf]
            [sa-draw.utils.sem-ver                 :as semver]
            [sa-draw.config.defaults               :as de]
            [sa-draw.global-state                  :refer [information-menu-open information-menu-pinned]]))

(defn information-menu [anchor-element-id id-to-offset]
  (fn []
    (let [vertical-offset (:sa-draw.events/app-bar-height @(rf/subscribe [:sa-draw.subs/dimensions]))]
      [:> mui/Menu {:open @information-menu-open
                    :anchorEl (.getElementById js/document anchor-element-id)
                    :style {:margin-top (str vertical-offset "px")}}
        (if @information-menu-open
          [:div
            [:> mui/Divider]
            [:> mui/MenuItem  (str "Application Version : " (semver/format-semver de/version))]
            [:> mui/Divider]
            [:> mui/Divider]
            [:> mui/MenuItem  (str "See Tom De Marco - Structured Analysis And System Specification")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "Tested on Chrome  Version 74.0.3729.108 (Official Build) (64-bit)")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "Verified on Firefox")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "Verified on Edge  42.17134.10.")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "We store the models in transit form in your browser keystore.")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "The context model objects with triangles are producers and consumers, these are used ")]
            [:> mui/MenuItem (str "to link models together.  Use th uid from the browser keystore model name to make a link")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "A process can be created with the id of a delegate model. ")]
            [:> mui/MenuItem (str "The delegate must be in your browser store.")]
            [:> mui/MenuItem (str "The link will navigate to the delegate model but the navigation is one way.")]
            [:> mui/Divider]
            [:> mui/MenuItem (str "See the Trello link for bugs and notes.") [:> mui/Link {:href "https://trello.com/b/KePK2v6d/project-sa" :target "_blank"} "Trello Board"]]
            [:> mui/Divider]
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Tooltip {:title "Close menu"}
                [:> mui/IconButton {:onClick (fn [] (reset! information-menu-pinned false)
                                                    (reset! information-menu-open false))}
                  [:> muicons/Close]]]
              (if (not @information-menu-pinned)
                [:> mui/Tooltip {:title "Pin menu"}
                  [:> mui/IconButton {:onClick #(swap! information-menu-pinned not @information-menu-pinned)}
                    [:> muicons/OfflinePin]]])]]
          [:div])])))
