(ns sa-draw.views.menus.model-info.model-info-drawer
  (:require [material-ui              :as mui]
            [material-ui-icons        :as muicons]
            [re-frame.core            :as rf]
            [sa-draw.utils.sem-ver    :as semver]
            [sa-draw.views.menus.model-info.subs   :as mtsb]
            [sa-draw.global-state     :refer [model-info-drawer-pinned model-info-drawer-open]]))

(defn model-info-panel
  []
  (fn []
    (let [meta @(rf/subscribe [::mtsb/model-meta-data])
          model-name (get-in meta [:sadf/name])
          model-id (get-in meta [:sadf/uuid])
          summary (get-in meta [:sadf/summary])
          tag-data (get-in meta[:sadf/detail])
          app-version (get-in meta [:sadf/app-version])]
      [:> mui/Drawer {:anchor "right"
                      :variant "persistent"
                      :open (or @model-info-drawer-open @model-info-drawer-pinned)
                      :onClick #(reset! model-info-drawer-open false)}
        ;SET THE MARGIN_TOP OTHERWISE THE SLIDE GOES UNDER THE TOP BAR
        [:> mui/GridList {:cols 1
                          :cellHeight "auto"
                          :style {:width "230px"
                                  :margin-top "130px"}}
            [:> mui/Typography {:variant "h5" :style {:margin 5}} "Model Information"]
            [:> mui/Divider]
            [:> mui/Typography {:variant "h6" :style {:margin 5}} "Meta Data"]
            [:> mui/Typography {:variant "body1" :style {:margin 5}} model-name]
            [:> mui/Typography {:variant "body1" :style {:margin 5}} summary]
            [:> mui/Divider]
            [:> mui/Typography {:variant "h6" :style {:margin 5}} "Tag Data"]
            (for [tag-key (keys tag-data)]
                ^{:key tag-key}[:> mui/Typography {:variant "body1" :style {:margin 5}} (str (name tag-key) " : " (tag-key tag-data))])
            [:> mui/Divider]
            [:> mui/Typography {:variant "h6" :style {:margin 5}} "Application Version"]
            [:> mui/Typography {:variant "body1" :style {:margin 5}} (semver/format-semver app-version)]
            [:> mui/Divider]
            [:> mui/Typography {:variant "h6" :style {:margin 5}} "Model ID"]
            [:> mui/Typography {:variant "body1" :style {:margin 5}}  model-id]
            [:> mui/Divider]
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "flex-start"
                          :alignItems   "center"}
                          ;:spacing      16}
              (if @model-info-drawer-pinned
                [:> mui/Tooltip {:title "Unpin config editor"}
                  [:> mui/IconButton {:onClick #(swap! model-info-drawer-pinned not @model-info-drawer-pinned)}
                    [:> muicons/Close]]]
                [:> mui/Tooltip {:title "Pin config editor"}
                  [:> mui/IconButton {:onClick #(swap! model-info-drawer-pinned not @model-info-drawer-pinned)}
                    [:> muicons/OfflinePin]]])]]])))
