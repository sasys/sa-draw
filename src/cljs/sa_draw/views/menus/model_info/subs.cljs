(ns sa-draw.views.menus.model-info.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
  ::model-meta-data
  (fn [db _]
    (get-in db [:sa-draw.events/model :sadf/meta])))

(rf/reg-sub
  ::summary :<- [::model-meta-data]
  (fn [model-meta-data _]
    (:sadf/summary model-meta-data)))

(rf/reg-sub
  ::name :<- [::model-meta-data]
  (fn [model-meta-data _]
    (:sadf/name model-meta-data)))
