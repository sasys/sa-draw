(ns sa-draw.views.menus.model-info.events
  (:require [re-frame.core              :as rf]))


(rf/reg-event-fx
  ::meta-data-change
  (fn [cofx [_ updated-meta]]
    (let [db (:db cofx)]
       {:db (assoc-in db [:sa-draw.events/model :sadf/meta] updated-meta)})))
