(ns sa-draw.views.help.help-events
  (:require [re-frame.core              :as rf]))

(rf/reg-event-db
  ::help-open
  (fn [db [_ open?]]
    (-> db
        (assoc-in  [:sa-draw.events/views :sa-draw.views.help.help-events/help :sa-draw.views.help.help-events/open?] open?)
        (assoc-in  [:sa-draw.events/views :sa-draw.views.help.help-events/help :sa-draw.views.help.help-events/type] :sa-draw.views.editors.dispatcher/help))))
