(ns sa-draw.views.help.help-subs
  (:require [re-frame.core              :as rf]))

(rf/reg-sub
  ::help
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.help.help-events/help])))

(rf/reg-sub
  ::viewer-open :<- [::help]
  (fn [help]
    (:sa-draw.views.help.help-events/open? help)))

(rf/reg-sub
  ::type :<- [::help]
  (fn [help _]
    (:sa-draw.views.help.help-events/type help)))
