(ns sa-draw.views.help.help-viewer
  "
    Viewer for the sa app help pages.
  "
  (:require [material-ui                     :as mui]
            [material-ui-icons               :as muicons]
            [re-frame.core                   :as rf]
            [sa-draw.views.help.help-subs    :as hsub]
            [sa-draw.views.help.help-events  :as hev]
            [sa-draw.views.help.help-text-md :as ht]
            [cljsjs.markdown-it]
            [cljsjs.react-draggable]))


(defn to-html [md]
  (-> (clj->js {:html true})
      (js/markdownit.)
      (.render  md)))


(defn help-ui []
  (let [open? (rf/subscribe [::hsub/viewer-open])]
    (fn []
      [:> js/ReactDraggable {:handle "strong"}
        [:> mui/Popover  {:open (if @open? true false)
                          :anchorReference "anchorPosition"
                          :anchorPosition {:left 40 :top 40}
                          :style {:width "40%"}}
          [:> mui/Paper
           [:div
               [:> mui/Grid {:container    true
                             :direction    "row"
                             :item         true
                             :justify      "space-between"
                             :alignItems   "center"}
                 [:> mui/Grid {:item true}
                   [:strong [:> muicons/DragIndicator]]]
                 [:> mui/Grid {:item true}
                   [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} "Project SA User Guide"]]
                 [:> mui/Grid {:item true}
                   [:> mui/Grid {:container      true
                                 :direction      "row"
                                 :justify        "flex-end"
                                 :alignItems     "center"}
                     [:> mui/Tooltip {:title "Close"}
                       [:> mui/IconButton {:onClick #(rf/dispatch [::hev/help-open false])}
                         [:> muicons/Close]]]
                     [:strong [:> muicons/DragIndicator]]]]]
               [:> mui/Divider]
               [:> mui/Paper
                 [:div {:style {:padding "10px"  :height "660px" :width "500px" :overflow "auto"}}
                   [:div {:dangerouslySetInnerHTML {:__html (to-html ht/text)}}]]]
               [:> mui/Divider]
               [:> mui/Grid {:container    true
                             :direction    "row"
                             :justify      "space-between"
                             :alignItems   "center"}
                 [:> mui/Grid {:item true}
                   [:strong [:> muicons/DragIndicator]]]

                 [:> mui/Grid {:item true}
                   [:> mui/Grid {:container      true
                                 :direction      "row"
                                 :justify        "flex-end"
                                 :alignItems     "center"}
                     [:> mui/Tooltip {:title "Close"}
                       [:> mui/IconButton {:onClick #(rf/dispatch [::hev/help-open false])}
                         [:> muicons/Close]]]
                     [:> mui/Grid {:item true}
                       [:strong [:> muicons/DragIndicator]]]]]]]]]])))
