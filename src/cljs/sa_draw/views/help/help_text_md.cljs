(ns sa-draw.views.help.help-text-md)


(def text
  "

## Introduction

This project is a first cut of a browser based dfd drawing program.

There is no data dictionary, data items on flows are really
only just text labels.


The tool does support linked models at the context and process
level.  A model can link to another model via a producer or a consumer on the
context diagram.
A model can link to a another model as a sub model via a
process.

## 1 - Home Page


### 1.1 - Model Meta Data

Each model on the home page has some meta-data associated with it, including :

* The model id

* The store where the model is located - limited to the browser keystore on
  this demo application

* The date/time when the model was created.  This can be used to identify a newly
  created model.

* The version of the application that created the model.  There are no incompatibilities
  between models and the tool as of 0.2.6.


Model meta data can be edited by pressing the continuation markers '...'

The editor allows the model name and model description to be changed.  remember to
press the save button when the edit complete, there is no dirty marker on the meta data editor.

The model can deleted from the editor.  This is a non-recoverable operation unless you have a backup,
see below.

The model can have a trello board associated with it.  This will appear on the App bar and
navigation will be to a new tab.

The model can be copied to the local file system (Download on osx).



### 1.2 - Creating New Models

There are two ways to create a new model :


#### 1.2.1 - Copying The Sample Model

On the home page edit the sample model, unlock the model and then save it.  The new
model will appear on the home page under the 'Models In Browser Store' table.

#### 1.2.2 - The '+'' Button On The App Bar

The '+' button will create a new empty model.


<img src=\"help-text/images/new-model.gif\" height=\"300\" width=\"600\" />


## 2 - Editing

Context objects, processes and flows can be dragged and dropped.  Flows can have their shapes
changed by dragging an end or the control point.  Flows can also be dragged by dragging inside the flow curve.  They
can sometimes be difficult to grab.

<img src=\"help-text/images/drag.gif\" height=\"300\" width=\"600\" />

### 2.1 - The Process Card

The process card can be used to set the title and description of the current process. Click the pencil to enable edits.  We can also add
inputs and outputs using the '+' button.  Inputs and outputs do not appear on the diagram.  They help to define the process.  The Inputs and outputs
can be viewed by expanding the lists using the 'v' button.

<img src=\"help-text/images/process-card.gif\" height=\"300\" width=\"550\" />


### 2.2 - Context Objects

Context objects can be created using the FAB at the bottom left of the diagram card when on the context view.
Select between source, sink producer and consumer.  The source and sink follow DeMarcos original
source sink concept.  The Producer and Consumer provide inter model links with a visible clue.

### 2.2.1 - Context Linked Models


Context diagrams can link between models.  A producer or consumer must have the model id
of the linked model added shown in the gif below.  The model id is the id shown on
the model meta data on the home screen.  The local settings can turn link visibility
on and off.


<img src=\"help-text/images/linked-models.gif\" height=\"300\" width=\"600\" />


### 2.3 - Submodels

Sub models can be created by using the Layers menue and navigating to the parent level.  From here the
FAB or the pencil icon can be used to create new child processes.

The process card can be used to set the title and description of the current process.


### 2.4 - Flows

On any layer flows can be edited by double clicking inside their curve.

<img src=\"help-text/images/create-flows.gif\" height=\"300\" width=\"600\" />

A flow can be copied to one or more lower levels by opening the flow for edit and then
selecting the lower levels from the list and pressing the DUPLICATE button.




## 3 - Other Notes

### 3.1 Downloading SVG

An image can be download (to the download directory) as SVG.  \nThis will have the proper
svg header like :

    <?xml version=\"1.0\" encoding=\"UTF-8\" ?><svg id=\"svg\" xmlns=\"http://www.w3.org/2000/svg\">

and can be opened directly in a browser.

This works on Linux (Ubuntu) with Firefox, OSX with Chrome and Firefox and Windows with chrome.  However,
the download does not seem to work with windows on Edge.

### 3.2 Downloading And Uploading Models

A model can be downloaded to the local filesystem (download directory) using
the cloud save model button on the model meta data editor.  The model will be
downloaded in transit format only.  Json will be an option in the future.

A model can be uploaded from the file system using the upload button on the
application bar.  The upload operation will store the model based on the
model id.  If a model already exists with the same id the model will be over
written.

The download and upload button provide a way to backup and restore models on the
local file system.  We recommend this action if you ever use the debug tools on the
project url; some debug tools will erase the local browser key store.

")
