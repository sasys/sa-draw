(ns sa-draw.views.editors.file-upload-selector-events
  (:require [re-frame.core                  :as rf]))

(rf/reg-event-db
  ::open-fus
  (fn [db [_]]
    (-> db
      (assoc-in  [:sa-draw.events/views ::file-upload-selector ::open?] true)
      (assoc-in  [:sa-draw.events/views ::file-upload-selector ::type] :sa-draw.views.editors.dispatcher/file-upload-selector))))

(rf/reg-event-db
  ::close-fus
  (fn [db [_]]
    (assoc-in  db [:sa-draw.events/views ::file-upload-selector ::open?] false)))
