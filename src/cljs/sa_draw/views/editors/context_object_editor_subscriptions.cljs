(ns sa-draw.views.editors.context-object-editor-subscriptions
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::context-object-editor
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.context-object-editor-events/context-object-editor])))

(rf/reg-sub
  ::edit-open :<- [::context-object-editor]
  (fn [context-object-editor _]
    (:sa-draw.views.editors.context-object-editor-events/open? context-object-editor)))


(rf/reg-sub
  ::edit-new :<- [::context-object-editor]
  (fn [context-object-editor _]
    (:sa-draw.views.editors.context-object-editor-events/new context-object-editor)))


(rf/reg-sub
  ::edit :<- [::context-object-editor]
  (fn [context-object-editor _]
    (:sa-draw.views.editors.context-object-editor-events/edit context-object-editor)))


(rf/reg-sub
  ::edit-type :<- [::context-object-editor]
  (fn [context-object-editor _]
    (:sa-draw.views.editors.context-object-editor-events/type context-object-editor)))
