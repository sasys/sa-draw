(ns sa-draw.views.editors.file-upload-selector
  "See https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop"
  (:require [re-frame.core              :as rf]
            [reagent.core               :as r]
            ["@material-ui/core"        :as mui]
            ["@material-ui/icons"       :as muicons]
            [sa-draw.events             :as ev]
            [sa-draw.views.editors.file-upload-selector-events :as fsupev]
            [cognitect.transit :as transit]))

(def selected-file (r/atom nil))


(defn the-selected-file-ui [file-name]
    (fn []
      (let [file (r/atom @file-name)]
        [:> mui/Button {:variant "outlined"} (if @file @file "None selected")])))


(defn reader-on-load [e]
  (let [r (transit/reader :json)
        trans (-> e .-target .-result)
        model (transit/read r trans)]
    (rf/dispatch [::ev/upload-model model])))

(defn allow-drop [e]
  (.preventDefault e))


(defn drop-handler [e]
  ;;NOTE I needed both of these and the allow-drop function to prevent chrome
  ;;from navigating to the json file!!
  (.preventDefault e)
  (.stopPropagation e)

  (if-let [items (-> e .-dataTransfer .-items)]
    (if (= (.-kind (aget items 0)) "file")
      (let [file (.getAsFile (aget items 0))
            reader (new js/FileReader)]
        (reset! selected-file (.-name file))
        (set! (.-onload reader) reader-on-load)
        (.readAsText reader file))
      (print "was not file"))))




(defn file-upload-selector
  "

  "
  []
  (fn []
    [:> mui/Grid {:container    true
                  :direction    "column"
                  :item         true
                  :justify      "space-between"
                  :alignItems   "center"}

        [:> mui/Button {:variant "outlined" :component "label" } "Select File To Upload"
          [:> mui/Input {:type "file" :id "file-selector" :accept ".json" :style {:display "none"} :onChange (fn [e] (reset! selected-file  (.. e -target -value)))}]]
        [:div {:id "drop_zone" :onDrop (fn [e](drop-handler e)) :onDragOver (fn [e] (allow-drop e)) :style {:border "1px dashed gray";
                                                                                                            :width  "200px";
                                                                                                            :height "100px"}};}}
          [:p "Drag one file to load into this Drop Zone ..."]]

        [the-selected-file-ui selected-file]
        [:> mui/Grid {:container    true
                      :direction    "row"
                      :item         true
                      :justify      "flex-start"
                      :alignItems   "center"}
          [:> mui/Tooltip {:title "Close"}
            [:> mui/IconButton {:onClick #(rf/dispatch [::fsupev/close-fus])}
              [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]]]))
