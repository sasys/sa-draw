(ns sa-draw.views.editors.flow
  "
    A popover editor for in place editing of a flow.
  "
  (:require ["@material-ui/core"                  :as mui]
            ["@material-ui/icons"                 :as muicons]
            [reagent.core                         :as r]
            [re-frame.core                        :as rf]
            [sa-draw.model.flows                  :as flows]
            [sa-draw.subs                         :as subs]
            [sa-draw.views.editors.flow-events    :as fev]
            [sa-draw.views.editors.data-item-editor :as die]
            [sa-draw.views.editors.data-item-editor-events :as dievts]
            [sa-draw.views.editors.data-item-editor-subs   :as diesubs]
            [sa-draw.events                       :as ev]
            [sa-draw.data.dd                      :as dic :refer [new-term id label remove-term]]
            [sa-draw.data.simple-dd               :refer [dd term]]
            [sa-draw.model.simple-flow-impl       :refer [fm]]
            [sa-draw.model.default-model          :refer [default-namespace]]
            [sa-draw.model.map-impl                :refer [map-model proc-model]]
            [sa-draw.model.proto-model             :as proto-model]))


(defn new-flow-term
  "

  "
  [dictionary-term name-space]
  {(keyword name-space "term") dictionary-term
   (keyword name-space "term-id") (id term dictionary-term)})


(defn new-flow-terms
  [terms]
  (for [term terms]
    (new-flow-term term default-namespace)))


(defn data-list-editor
  "
    An editor for adding and removing data elements from the presented list.
    The terms are a list of terms with state added.  The list is assumed to be an r/atom
  "
    [terms new-terms deleted-terms editor-open? edit-locked?]

    (rf/dispatch-sync [::dievts/initial-terms @terms])
    (let [data-name (r/atom "data-name")
          rows-open? (r/atom true)
          current-terms (rf/subscribe [::diesubs/terms])]
      (fn [terms new-terms deleted-terms editor-open? edit-locked?]

          [:div
            ;The existings data items
            (for [t @current-terms]
              ^{:key (id term t)} [die/data-item-editor t edit-locked? rows-open? :sa-draw.views.editors.data-item-editor-events/remove-term])

            [:div (if (not @editor-open?) {:style {:display "none"}})
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/TextField {:label @data-name
                                   :variant "outlined"
                                   :rows 1
                                   :style {:width 170}
                                   :onChange (fn [e] (reset! data-name (.. e -target -value)))}]

                [:> mui/Tooltip {:title "Done"}
                  [:> mui/IconButton {:onClick #(let [t (new-term term @data-name)]
                                                    (swap! new-terms conj t)
                                                    (rf/dispatch [::dievts/add-term t])
                                                    (swap! editor-open? not @editor-open?))}
                    [:> muicons/Done {:classes {:root "material-icons md-18"}}]]]
                [:> mui/Tooltip {:title "Discard"}
                  [:> mui/IconButton {:onClick #(swap! editor-open? not @editor-open?)}
                    [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]]])))


(defn data-list-filter
  "Remove items from the data list that are flagged as deleted and remove the
   deleted flags from the remaining data items"
  [data-list tag]
  (filter #(not= tag @(:state %)) data-list))


(defn data-list-filter-new
  "Remove items from the data list that are flagged as deleted and remove the
   deleted flags from the remaining data items"
  [data-list]
  (filter #(= :new @(:state %)) data-list))

(defn data-list-filter-deleted
  "Remove items from the data list that are flagged as deleted and remove the
   deleted flags from the remaining data items"
  [data-list]
  (filter #(= :deleted @(:state %))) data-list)


(defn add-state-flag
  "
    Adds an atom to each data item in the list and adds the complete list to an atom.
  "
  [data-list]
  (for [data data-list]
     (assoc data :state (atom :na))))


(defn copy-item [cid selected-cids]
  [:> mui/Grid {:container    true
                :direction    "row"
                ;:item         true
                :justify      "space-between"
                ;:alignItems   "center"
                :style {:margin 5}}
    [:p cid]
    (if (contains? @selected-cids cid)
      [:> mui/Tooltip {:title (str "Dont dup to layer " cid)}
        [:> mui/IconButton {:id cid
                            :label "dup"
                            :variant "contained"
                            :onClick #(if (contains? @selected-cids cid) (swap! selected-cids disj cid) (swap! selected-cids conj cid))}
          [:> muicons/Done]]]
      [:> mui/Tooltip {:title (str "Dup to layer " cid)}
        [:> mui/IconButton {:id cid
                            :label "dup"
                            :variant "contained"
                            :onClick #(if (contains? @selected-cids cid) (swap! selected-cids disj cid) (swap! selected-cids conj cid))}
          [:> muicons/Clear]]])])


(def selected-cids (r/atom #{}))

(defn flow-editor
  "A popover editor for a data flow."
  [flow-id]
  (let [popper-open? (r/atom false)
        new-terms (r/atom [])
        deleted-terms (r/atom [])
        flows (rf/subscribe [::subs/flows])
        flow  (r/atom (flows/find-flow fm @flows flow-id))
        [id datas _ _ _ description] (flows/details fm @flow)
        terms (r/atom  (for [data datas] (:sadf/term data)))
        updated-terms (rf/subscribe [::diesubs/terms])
        current-layer (rf/subscribe [::subs/current-layer])
        model (rf/subscribe [::subs/model])]

    (fn [flow-id]
      (let [edit-locked?  @(rf/subscribe [::subs/edit-locked])
            proc-details @(rf/subscribe [::subs/current-proc])
            updated-description (r/atom description)
            procs (proto-model/find-process-model map-model @model)
            proc (if (= "context" @current-layer) (proto-model/find-node proc-model procs "0") (proto-model/find-node proc-model procs @current-layer))
            cids (remove #(= @current-layer %) (proto-model/cids proc-model proc))]
        [:> mui/Paper
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} "Edit Flow"]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Abandon/Close"}
                    [:> mui/IconButton {:onClick (fn [] (reset! new-terms [])
                                                        (reset! deleted-terms [])
                                                        (reset! selected-cids #{})
                                                        (rf/dispatch [::fev/close]))}
                      [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                  [:strong [:> muicons/DragIndicator]]]]]
            [:> mui/Divider]
            [:div {:style {:padding "10px" :height "660px" :overflow "auto"}}
              [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} id]
              [:> mui/Grid {:container    true
                            :direction    "column"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "flex-start"}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}}
                  "Description"]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :item         true
                              :justify      "space-between"
                              :alignItems   "center"}
                  [:> mui/TextField {:label "Flow Description"
                                     :variant "outlined"
                                     :multiline true
                                     :rows 3
                                     :style {:width 250}
                                     :defaultValue description
                                     :onChange (fn [e] (reset! updated-description (.. e -target -value)))}]
                  [:> mui/Tooltip {:title "An optional description / notes on this flow."} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :item         true
                              :justify      "space-between"
                              :alignItems   "center"}
                  [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}}
                    "Data"]
                  [:> mui/Tooltip {:title "New data"}
                    [:> mui/IconButton {:id "flow-popper"
                                        :label "Add"
                                        :variant "contained"
                                        :onClick #(reset! popper-open? true)}
                      [:> muicons/Add]]]]
                [data-list-editor terms new-terms deleted-terms popper-open? edit-locked?]

                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}}
                  "Duplicate Flow"]

                (if (> (count cids) 0)
                  [:div
                    (for [cid cids]
                      ^{:key cid} [copy-item cid selected-cids])


                    [:> mui/Tooltip {:title "Duplicate Flow To Lower Levels"}
                      [:> mui/Button {:onClick (fn [](doseq [cid (seq @selected-cids)] (rf/dispatch [::fev/duplicate-flow-to-layer @flow cid]))
                                                     (reset! selected-cids #{}))} "Duplicate"]]])]]
           [:> mui/Divider]
           [:> mui/Grid {:container    true
                         :direction    "row"
                         :justify      "space-between"
                         :alignItems   "center"}
             [:> mui/Grid {:item true}
               [:> mui/Grid {:container      true
                             :direction      "row"
                             :justify        "flex-end"
                             :alignItems     "center"}
                 [:> mui/Grid {:item true}
                   [:strong [:> muicons/DragIndicator]]]
                 [:> mui/Grid {:item true}
                   [:> mui/Tooltip {:title "Delete this flow"}
                     ;NOTE we add the dummy element because new-current-layer-v expects the first element to be children and we dont use it.
                     ;Needs a re-think.
                     [:> mui/IconButton {:onClick (fn [] (rf/dispatch [::fev/delete-flow id])
                                                         (rf/dispatch [::ev/new-current-layer-v (cons "dummy" proc-details)])
                                                         (rf/dispatch [::fev/close]))}
                       [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]]]
             [:> mui/Grid {:item true}
               [:> mui/Grid {:container      true
                             :direction      "row"
                             :justify        "flex-end"
                             :alignItems     "center"}
                 ;When we save, remove the deleted data items, they have a flag
                 [:> mui/Tooltip {:title "Save changes to this flow"}
                   ;NOTE we add the dummy element because new-current-layer-v expects the first element to be children and we dont use it.
                   ;Needs a re-think.
                   [:> mui/IconButton {:onClick (fn [] (reset! new-terms [])
                                                       (reset! deleted-terms [])
                                                       (rf/dispatch [::fev/update-flow @flow @updated-description (new-flow-terms @updated-terms)])
                                                       (rf/dispatch [::ev/new-current-layer-v (cons "dummy" proc-details)])
                                                       (rf/dispatch [::ev/dirty true]))}
                     [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
                 [:> mui/Grid {:item true}
                   [:strong [:> muicons/DragIndicator]]]]]]]]))))
