(ns sa-draw.views.editors.quick-flow
  "
    Uses the atom quick-flow-open? map to control visability

    atom is a map containing the current proc id

    Creates a flow on the current layer with a default term that is added to the dic (if it doesnt exist).
  "
  (:require ["@material-ui/core"                  :as mui]
            ["@material-ui/icons"                 :as muicons]
            [reagent.core                         :as r]
            [re-frame.core                        :as rf]
            [sa-draw.model.flows                  :as flows]
            ;[sa-draw.views.dd.events              :as ddev]
            [sa-draw.views.editors.flow-events    :as fev]
            [sa-draw.subs                         :as subs]
            [sa-draw.events                       :as ev]
            [sa-draw.views.editors.quick-flow-events :as qfe]
            [clojure.string                       :refer [split]]
            [sa-draw.model.simple-flow-impl       :refer [fm]]
            [sa-draw.data.dd                      :refer [add-term term-by-id new-term id label clear-dd remove-term list-terms term-exists?]]
            [sa-draw.data.simple-dd               :refer [dd term]]))

(defn uid
  []
  (str (random-uuid)))

(defn new-flow-terms
  "

  "
  [dictionary-terms name-space]
  (reduce #(conj %1 {(keyword name-space "term") %2
                     (keyword name-space "term-id") (id term %2)}) [] dictionary-terms))

(defn new-terms
  [data-names]
  (map #(new-term term %) data-names))

(defn form-new-data
  [data name-space layer]
  (let [split-data (split data #", ")
        terms (new-terms split-data)
        flow-terms (new-flow-terms terms "sadf")
        flow (flows/new-flow fm  flow-terms [{:sadf/posn [200 200]
                                                   :sadf/id (uid)
                                                   :sadf/proc-id (keyword "sadf" layer)}]
                                            [{:sadf/posn [250 250]
                                              :sadf/id (uid)
                                              :sadf/proc-id (keyword "sadf" layer)}])]
    (rf/dispatch [::fev/update-flow flow "" flow-terms])))
    ;Add to the data dictionary
    ; (doseq [t terms]
    ;   (rf/dispatch [::ddev/new-term t ""]))))


(defn quick-flow
  []
  (let [new-data (r/atom "")]
    (fn []
      (let [name-space @(rf/subscribe [::subs/current-layer-name-space])
            current-layer @(rf/subscribe [::subs/current-layer])
            current-layer-title @(rf/subscribe [::subs/current-layer-title])
            current-layer-description @(rf/subscribe [::subs/current-layer-description])
            show-context? (true? @(rf/subscribe [::subs/current-layer-context?]))
            data-in @(rf/subscribe [::subs/current-layer-data-in])
            data-out @(rf/subscribe [::subs/current-layer-data-out])]
        [:div
          [:> mui/Grid {:container    true
                        :direction    "row"
                        :item         true
                        :justify      "space-between"
                        :alignItems   "center"}
            [:> mui/Grid {:item true}
              [:strong [:> muicons/DragIndicator]]]
            [:> mui/Grid {:item true}
              [:> mui/Typography {:variant "subtitle2" :style {:margin 5}}
                  "Create New Flow"]]
            [:> mui/Grid {:item true}
              [:> mui/Grid {:container      true
                            :direction      "row"
                            :justify        "flex-end"
                            :alignItems     "center"}
                [:> mui/Tooltip {:title "Abandon/Close"}
                      [:> mui/IconButton {:onClick #(rf/dispatch [::qfe/close])}
                        [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                [:strong [:> muicons/DragIndicator]]]]]
          [:> mui/Divider]
          [:> mui/Grid {:container    true
                        :direction    "column"
                        :item         true
                        :justify      "flex-start"
                        :alignItems   "center"}
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "flex-start"
                          :alignItems   "center"}
              [:> mui/TextField {:variant "outlined"
                                 :label "a, b"
                                 :defaultValue @new-data
                                 ;:disabled false
                                 :style {:margin 5}
                                 :onChange #(reset! new-data (.. % -target -value))}]
              [:> mui/Tooltip {:title "Data on the flow.  EIther a single string like my-data or comma space separated strins like a, b, my-data"} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]
          [:> mui/Divider]
          [:> mui/Grid {:container    true
                        :direction    "row"
                        :justify      "space-between"
                        :alignItems   "center"}
            [:> mui/Grid {:item true}
              [:strong [:> muicons/DragIndicator]]]
            [:> mui/Grid {:item true}
              [:> mui/Grid {:container      true
                            :direction      "row"
                            :justify        "flex-end"
                            :alignItems     "center"}
            ; [:> mui/Tooltip {:title "Abandon   edit"}
            ;   [:> mui/IconButton {:onClick #(rf/dispatch [::qfe/close])}
            ;     [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                [:> mui/Tooltip {:title "Save new flow"}
                  [:> mui/IconButton {:onClick (fn [] (form-new-data  @new-data name-space current-layer))
                                      (rf/dispatch [::ev/new-current-layer current-layer current-layer-title  current-layer-description data-in data-out show-context?])
                                      (rf/dispatch [::ev/dirty true])}
                    [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
                [:> mui/Grid {:item true}
                  [:strong [:> muicons/DragIndicator]]]]]]]))))
