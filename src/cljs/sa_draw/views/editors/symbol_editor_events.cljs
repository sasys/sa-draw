(ns sa-draw.views.editors.symbol-editor-events
  (:require [re-frame.core              :as rf]
            [ajax.core                  :refer [GET]]
            [ajax.protocols             :refer [-body]]
            [cljs.core.async            :refer [<! >! go-loop alts! chan] :refer-macros [go]]
            [sa-draw.views.dfd.svg.draw :as draw]
            [sa-draw.model.icons        :as icons]
            [sa-draw.model.icons-impl   :refer [im]]
            [sa-draw.model.proto-model  :as pm]
            [sa-draw.model.map-impl     :refer [map-model]]))


(defn uid
  []
  (str (random-uuid)))


(rf/reg-event-db
  ::symbol-editor-edit
  (fn  [db [_ tab-id]]
    (assoc-in db [:sa-draw.events/views ::symbol-editor] {::open? true
                                                          ::new? false
                                                          ::type :sa-draw.views.editors.dispatcher/symbol-editor
                                                          ::tab  tab-id})))



(rf/reg-event-db
  ::symbol-editor-new
  (fn  [db [_ tab-id]]
    (assoc-in db [:sa-draw.events/views ::symbol-editor] {::open? true
                                                          ::new? true
                                                          ::type :sa-draw.views.editors.dispatcher/symbol-editor
                                                          ::tab tab-id})))


(rf/reg-event-db
  ::symbol-editor-tab
  (fn [db [_ tab-id]]
    (assoc-in db [:sa-draw.events/views ::symbol-editor] {::open? true
                                                          ::new? false
                                                          ::type :sa-draw.views.editors.dispatcher/symbol-editor
                                                          ::tab tab-id})))



(rf/reg-event-db
  ::close
  (fn [db [_]]
    (assoc-in db [:sa-draw.events/views ::symbol-editor] {::open? false
                                                          ::new? nil
                                                          ::type nil})))

;;Add a single icon instance to the model
;;Add a default position for the new icon to the graphic layout
(rf/reg-event-db
  ::add-icon
  (fn [db [_ new-icon]]
      (as-> (pm/find-icon-model map-model (:sa-draw.events/model db)) iconsv
            (icons/add-icon im iconsv new-icon)
            (pm/update-icon-model map-model (:model db) iconsv)
            (assoc-in db [:sa-draw.events/model] iconsv)
            (assoc-in iconsv [::graphics ::layout (keyword "sadf" (:sadf/id new-icon))] [60 60]))))

;;Add new icon instances to the model
;;Add a default position for each new icon to the graphic layout
(rf/reg-event-db
  ::add-icons
  (fn [db [_ new-icons]]
    (let [icon-model (pm/find-icon-model map-model (:sa-draw.events/model db))
          aug_icons (icons/add-icons im icon-model new-icons)
          upd  (pm/update-icon-model map-model (:sa-draw.events/model db) aug_icons)
          upd-db (assoc-in db [:sa-draw.events/model] upd)]
      upd-db)))



;;;;;;;;;;;;;
;Load external zip file from url
;;;;;;;;;;;;;


(defn read-resp [xhrio]
   (-> xhrio -body))

(defn resp-handler
  [chan resp]
  (if resp
   (go (>! chan (js->clj resp)))
   (go (>! chan ::no-resp))))

(defn error-handler [resp](println "error : " resp))




(defn load-zip-handler
  "Its not a zip!!"
  [zip url icon-name source-name]
  (if-not (= ::no-resp zip)
    (let [svg (draw/full-stack-svg-import zip draw/default-scale-factor draw/default-translate draw/default-unwanted-keys draw/default-view-box)];-1)]
      (rf/dispatch [::load-sym svg source-name icon-name url]))
    (println "no resp")))

(rf/reg-fx
  ::load-zip-fx
  (fn [req]
    (go
      (let [get-chan (chan)]
        (GET (::url req) {:handler (partial resp-handler get-chan)
                          :error-handler error-handler})
        (load-zip-handler (<! get-chan) (::url req) (::name req) (::source req))))))


(rf/reg-event-fx
  ::load-zip
  (fn [cofx [_ url icon-name source-name]]
    {::load-zip-fx {::url    url
                    ::name   icon-name
                    ::source source-name}
      :db          (:db cofx)}))


(defn update-sources
  [sources source-name source]
  (reduce (fn [c s] (if (= (:source s) source-name)
                      (conj c source)
                      (conj c s)))
          [] sources))

(defn update-symbols
  [symbols icon-name svg]
  (reduce (fn [c s] (if (= (:name s) icon-name)
                      (conj c (assoc s :svg svg))
                      (conj c s)))
          [] symbols))


(defn add-svg
  [sources source-name icon-name url svg]
  (if-let [source (first (filter #(= (:source %) source-name) sources))]
    (let [symbols (:symbols source)]
      (as-> symbols s
          (update-symbols s icon-name svg)
          (assoc source :symbols s)
          (update-sources sources source-name s)))
    sources))


(rf/reg-event-db
  ::load-sym
  (fn [db [_ svg source-name icon-name url]]
    (as-> (get-in db [:sa-draw.events/model :sadf/default-cloud-icons]) sources
          (add-svg  sources source-name icon-name url svg)
          (assoc-in db [:sa-draw.events/model :sadf/default-cloud-icons] sources))))



(rf/reg-event-db
  ::syms-loaded
  (fn [db [_]]
    (assoc-in db [:sa-draw.events/model :sadf/meta :syms-loaded] true)))


(rf/reg-event-db
  ::load-syms
  (fn [db [_ icons]]
    (doall (for [source icons
                 icon (:symbols source)]
            (if icon
              (rf/dispatch [::load-zip (:uri icon) (:name icon) (:source source)]))))
    (rf/dispatch [::syms-loaded])))
