(ns sa-draw.views.editors.quick-flow-subs
  "
    Subscriptions for quick-flow editor events
  "
  (:require [re-frame.core  :as rf]))



(rf/reg-sub
  ::edit-open
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.quick-flow-events/quick-flow :sa-draw.views.editors.quick-flow-events/open?])))


(rf/reg-sub
  ::edit-type
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.quick-flow-events/quick-flow :sa-draw.views.editors.quick-flow-events/type])))
