(ns sa-draw.views.editors.context-object-editor
  "
    A popover editor for in place editing of a flow.
  "
  (:require [reagent.core                                        :as reagent]
            ["@material-ui/core"                                 :as mui]
            ["@material-ui/icons"                                :as muicons]
            [re-frame.core                                       :as rf]
            [sa-draw.model.utils                                 :as ut]
            [sa-draw.events                                      :as ev]
            [sa-draw.views.editors.common.symbols                :as cs]
            [sa-draw.views.editors.symbol-editor-subs            :as sesubs]
            [sa-draw.views.editors.symbol-editor-events          :as sesevs]
            [sa-draw.subs                                        :as subs]
            [sa-draw.views.editors.context-object-editor-events  :as coee]
            [sa-draw.model.proto-model                           :as proto-model :refer [find-dictionary]]
            [sa-draw.model.map-impl                              :as mimpl :refer [map-model proc-model context-model]]))

(def selected-tab  (reagent/atom 0))

;; This determines which checkbox has been selected.  Also has a default.
(def selected (reagent/atom mimpl/producers-keyword))


;; A collection of terms selected in the datadictionary explorer (or anywhere)
(def selected-dde-terms (reagent/atom []))


(defn edit-context-object
  "
    An editor for context objects
  "
  [id]
  (let [model  @(rf/subscribe [::subs/model])
        procs-model (proto-model/find-process-model map-model model)
        current-node (proto-model/find-node proc-model procs-model "0")
        context-objects (proto-model/context-objects map-model model)
        context-object-id (if id id (ut/uid))
        context-object (if id (proto-model/find-context-object-by-id context-model context-objects context-object-id) {})
        [_ title description link _ terms context-object-type] (proto-model/context-object-details context-model context-object)
        new-title (reagent/atom (if id title "New Title"))
        new-description (reagent/atom (if id description "New description."))
        new-link (reagent/atom (if id link ""))
        deleted-terms (reagent/atom [])
        new-terms (atom [])]
    (fn [id]
      [:> mui/Grid {:container    true
                    :direction    "column"
                    :item         true
                    :justify      "space-between"}
                    ;:alignItems   "center"}

        [:> mui/Grid {:container    true
                      :direction    "row"
                      :item         true
                      :justify      "space-between"
                      :alignItems   "center"}

          (if id
            [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (str "Edit Context Object : " title)]
            [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (str "Create New Context Object")])
          [:> mui/Tooltip {:title "Abandon/Close"}
            [:> mui/IconButton {:onClick #(rf/dispatch [::coee/close])}
              [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]]

        [:> mui/TextField {:variant "outlined"
                           :label "Title"
                           :defaultValue  (if title title "")
                           :onChange #(reset! new-title (.. % -target -value))
                           :style {:margin 5}}]

        [:> mui/TextField {:variant "outlined"
                           :label "Description"
                           :multiline true
                           :defaultValue  (if description description "")
                           :onChange #(reset! new-description (.. % -target -value))
                           :style {:margin 5}}]
        (if (some #{:sadf/producers :sadf/consumers} [context-object-type])
          [:> mui/Grid {:container    true
                        :direction    "row"
                        :item         true
                        :justify      "space-between"
                        :alignItems   "center"}
            [:> mui/TextField {:variant "outlined"
                               :label (if (= :sadf/producers context-object-type) "Source Link" "Destination Link")
                               :defaultValue  (if link link "")
                               :onChange #(reset! new-link (.. % -target -value))
                               :style {:margin 5}}]])


        [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (str "ID : " context-object-id)]
        [:> mui/Grid {:container    true
                      :direction    "row"
                      :item         true
                      :justify      "space-between"
                      :alignItems   "center"}

          [:> mui/Tooltip {:title "Save changes to this context object"}
            [:> mui/IconButton {:onClick (fn []
                                            (let [gross-terms (concat @new-terms terms)
                                                  net-terms (remove (set @deleted-terms) gross-terms)
                                                  [_ _ context-title context-description _ _] (proto-model/details proc-model current-node)]
                                                (rf/dispatch [::coee/update context-object-type context-object @new-title @new-description net-terms @new-link])
                                                (rf/dispatch [::ev/new-current-layer "context" context-title context-description []  []]) ;[::ev/new-current-layer "context" current-title current-description [] []])
                                                (rf/dispatch [::ev/dirty true])))}

              [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
          [:> mui/Tooltip {:title "Remove this producer"}
            [:> mui/IconButton {:onClick (fn []
                                            (let [[_ _ context-title context-description _ _] (proto-model/details proc-model current-node)]
                                              (rf/dispatch [::coee/delete context-object-type id context-object])
                                              (rf/dispatch [::ev/new-current-layer "context" context-title context-description []  []])
                                              (rf/dispatch [::coee/close])))}
              [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]])))

;The selected icons, the uris of each icon we select.
(def selected-icons (reagent/atom #{}))

;The selected context element
(def proc-selected (reagent/atom false))


(defn edit-new-context-object
  "
    Editor creates a new context object
  "
  []
  (let [model  @(rf/subscribe [::subs/model])
        symbols-loaded (rf/subscribe [::sesubs/syms-loaded])
        procs-model (proto-model/find-process-model map-model model)
        current-node (proto-model/find-node proc-model procs-model "0")
        new-title (reagent/atom "New Title")
        new-description (reagent/atom "New description.")
        new-link (reagent/atom "")
        new-dictionary-link (reagent/atom "")
        default-icons @(rf/subscribe [::subs/default-icons])
        selected-tab  (reagent/atom (:title (first default-icons)))]
    (fn []
      (let [new-id (reagent/atom (ut/uid))]
        (if-not @symbols-loaded (rf/dispatch-sync [::sesevs/load-syms default-icons]))
        [:> mui/Paper
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} "Create a new context object"]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Abandon/Close"}
                    [:> mui/IconButton {:onClick #(rf/dispatch [::coee/close])}
                      [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                  [:strong [:> muicons/DragIndicator]]]]]
            [:> mui/Divider]
            [:div {:style {:padding "10px" :height "660px" :overflow "auto"}}
              [:> mui/Grid {:item    true}
                [:> mui/TextField {:variant "outlined"
                                   :label "Title"
                                   :defaultValue  @new-title
                                   :onChange #(reset! new-title (.. % -target -value))
                                   :style {:margin 5 :width 270}}]
                [:> mui/Grid {:item    true}
                  [:div {:class "MuiFormControl-root MuiTextField-root" :style {:margin "5px"}}
                    [:> mui/TextareaAutosize {:class "sbnl-autoresize"
                                              :variant "outlined"
                                              :label "Description"
                                              :defaultValue ""
                                              :placeholder "Description"
                                              :rows 3
                                              :onChange (fn [e] (reset! new-description (.. e -target -value)))
                                              ;;THIS STYLE IS TO MAKE THE TEXT AREA LOOK LIKE THE TEXT FIELD!!
                                              ;;NOTE that the style sheet gets overriden and does not get fully applied.
                                              :style {:border-radius "4px"
                                                      :margin 1
                                                      :background-color "transparent"
                                                      :box-sizing "inherit"
                                                      :font-family "Roboto"
                                                      :font-size "16px"
                                                      :width 270
                                                      :padding-top "5px"}}]]]]
              [:> mui/Grid {:container      true
                            :direction      "row"
                            :justify        "space-between"
                            :alignItems     "center"
                            :style {:margin 5}}
                [:> mui/Grid {:item true}
                  [:> mui/Typography {:variant "subtitle2"} "Producer"]]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "flex-end"
                                :alignItems     "center"
                                :style          {:margin 5}}
                    [:> mui/Radio {:checked (= @selected mimpl/producers-keyword)
                                   :value "producer"
                                   :color "default"
                                   :name "context-object-type"
                                   :onClick #(reset! selected mimpl/producers-keyword)}]
                    [:> mui/Tooltip {:title "A producer is a consumer of another model."}
                      [:> mui/IconButton
                        [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]]

              [:> mui/Grid {:container    true
                            :direction    "row"
                            :justify      "space-between"
                            :alignItems   "center"
                            :style {:margin 5}}
                [:> mui/Grid {:item true}
                  [:> mui/Typography {:variant "subtitle2"} "Consumer"]]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "flex-end"
                                :alignItems     "center"
                                :style          {:margin 5}}
                    [:> mui/Radio {:checked (= @selected mimpl/consumers-keyword)
                                   :value "consumer"
                                   :color "default"
                                   :name "context-object-type"
                                   :onClick #(reset! selected mimpl/consumers-keyword)}]
                    [:> mui/Tooltip {:title "A consumer is a producer to another model."}
                      [:> mui/IconButton
                        [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :justify      "space-between"
                            :alignItems   "center"
                            :style {:margin 5}}
                [:> mui/Grid {:item true}
                  [:> mui/Typography {:variant "subtitle2"} "Source"]]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container    true
                                :direction    "row"
                                :justify      "flex-end"
                                :alignItems   "center"
                                :style {:margin 5}}
                    [:> mui/Radio {:checked (= @selected mimpl/sources-keyword)
                                   :value "source"
                                   :color "default"
                                   :name "context-object-type"
                                   :onClick #(reset! selected mimpl/sources-keyword)}]
                    [:> mui/Tooltip {:title "A source is an externalised producer of data."}
                      [:> mui/IconButton
                        [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :justify      "space-between"
                            :alignItems   "center"
                            :style {:margin 5}}
                [:> mui/Grid {:item true}
                  [:> mui/Typography {:variant "subtitle2"} "Sink"]]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container    true
                                :direction    "row"
                                :justify      "flex-end"
                                :alignItems   "center"
                                :style {:margin 5}}
                    [:> mui/Radio {:checked (= @selected mimpl/sinks-keyword)
                                   :value "sink"
                                   :color "default"
                                   :name "context-object-type"
                                   :onClick #(reset! selected mimpl/sinks-keyword)}]
                    [:> mui/Tooltip {:title "A sink is an externalised consumer of data."}
                      [:> mui/IconButton
                        [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]]

              [cs/icon-selector selected-tab selected-icons]

              (if (some #{:sadf/producers :sadf/consumers} [@selected])
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :alignItems   "center"}

                  [:> mui/TextField {:variant "outlined"
                                     :label (if (= :sadf/producers @selected) "Source Link (uuid from your browser keystore)" "Destination Link (uuid from your browser keystore)")
                                     :defaultValue  @new-link
                                     :onChange (fn [e] (reset! new-link (.. e -target -value)))
                                     :style {:padding "10px"}}];:margin 5}}]

                  [:> mui/Tooltip {:title "The UUID of the model you want to link to."} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]])]

            [:> mui/Divider]
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Create this context object."}

                    [:> mui/IconButton {:onClick (fn []
                                                  (let [[_ _ context-title context-description _ _] (proto-model/details proc-model current-node)
                                                        context-object (if (some identity (seq @selected-icons))
                                                                         (proto-model/new-context-object context-model @selected @new-id @new-title @new-description @selected-dde-terms @new-dictionary-link @new-link (first (seq @selected-icons)))
                                                                         (proto-model/new-context-object context-model @selected @new-id @new-title @new-description @selected-dde-terms @new-dictionary-link @new-link))]
                                                    (rf/dispatch [::coee/context-object-editor-new  @new-id @selected context-object])
                                                    (rf/dispatch [::ev/new-current-layer "context" context-title context-description []  []])
                                                    (reset! new-id (ut/uid))))}
                      [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
                  [:> mui/Grid {:item true}
                    [:strong [:> muicons/DragIndicator]]]]]]]]))))
