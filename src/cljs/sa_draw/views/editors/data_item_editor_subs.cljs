(ns sa-draw.views.editors.data-item-editor-subs
  (:require [re-frame.core                  :as rf]))

(rf/reg-sub
  ::terms
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.data-item-editor-events/die :sa-draw.views.editors.data-item-editor-events/terms])))


(rf/reg-sub
  ::input-terms
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.data-item-editor-events/die :sa-draw.views.editors.data-item-editor-events/input-terms])))

(rf/reg-sub
  ::output-terms
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.data-item-editor-events/die :sa-draw.views.editors.data-item-editor-events/output-terms])))
