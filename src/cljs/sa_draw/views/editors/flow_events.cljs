(ns sa-draw.views.editors.flow-events
  (:require [re-frame.core                  :as rf]
            [sa-draw.model.flows            :as flows]
            [sa-draw.model.proto-model      :as proto-model]
            [sa-draw.model.simple-flow-impl :refer [fm]]
            [sa-draw.model.map-impl         :refer [map-model]]))


(rf/reg-event-fx
  ::update-flow
  (fn [cofx [_ flow description data]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          flow-id (flows/id fm flow)
          flows (proto-model/find-flow-model map-model model)
          updated-flows (as-> flow uf
                             (flows/update-flow-description fm uf description)
                             (flows/update-flow-data fm uf data)
                             (flows/update-flow fm flows uf flow-id))
          updated-model (proto-model/update-flow-model map-model model updated-flows)]
      {:db (assoc-in db [:sa-draw.events/model] updated-model)})))


(rf/reg-event-fx
  ::delete-flow
  (fn [cofx [_ flow-id]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          flows (proto-model/find-flow-model map-model model)
          updated-flows (flows/remove-flow-by-id fm flows flow-id)
          updated-model (proto-model/update-flow-model map-model model updated-flows)]
      {:db (assoc-in db [:sa-draw.events/model] updated-model)})))


(rf/reg-event-db
  ::edit-flow
  (fn [db [_ flow-id]]
    (assoc-in db [:sa-draw.events/views  ::flow] {::open? true
                                                  ::new "dummy-id" ;we dont pass in the new id for now
                                                  ::edit flow-id
                                                  ::type :sa-draw.views.editors.dispatcher/flow-editor})))

(rf/reg-event-db
  ::create-flow
  (fn [db _]
    (assoc-in db [:sa-draw.events/views  ::flow] {::open? true
                                                  ::new "dummy-id" ;we dont pass in the new id for now
                                                  ::edit nil
                                                  ::type :sa-draw.views.editors.dispatcher/flow-editor})))

(rf/reg-event-db
  ::close
  (fn  [db [_]]
    (assoc-in db [:sa-draw.events/views ::flow] {::open? false
                                                 ::new nil
                                                 ::edit nil})))


(rf/reg-event-db
  ::duplicate-flow-to-layer
  (fn [db [_ flow layer]]
    (let [dup-flow (flows/duplicate-flow-to-layer fm flow layer)
          flow-id (flows/id fm dup-flow)
          model (:sa-draw.events/model db)
          flows (proto-model/find-flow-model map-model model)
          updated-flows (flows/update-flow fm flows dup-flow flow-id)
          updated-model (proto-model/update-flow-model map-model model updated-flows)]
     (assoc-in db [:sa-draw.events/model] updated-model))))
