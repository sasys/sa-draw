(ns sa-draw.views.editors.data-item-editor-events
  (:require [re-frame.core                  :as rf]))


(rf/reg-event-db
  ::initial-terms
  (fn [db [_ terms]]
    (assoc-in db [:sa-draw.events/views ::die ::terms] terms)))

(rf/reg-event-db
  ::initial-input-terms
  (fn [db [_ terms]]
    (assoc-in db [:sa-draw.events/views ::die ::input-terms] terms)))

(rf/reg-event-db
  ::initial-output-terms
  (fn [db [_ terms]]
    (assoc-in db [:sa-draw.events/views ::die ::output-terms] terms)))

(rf/reg-event-db
  ::add-term
  (fn [db [_ term]]
    (let [current-terms (get-in db [:sa-draw.events/views ::die ::terms])]
      (assoc-in db [:sa-draw.events/views ::die ::terms] (conj current-terms term)))))

(rf/reg-event-db
  ::add-proc-input-term
  (fn [db [_ term]]
    (let [current-terms (get-in db [:sa-draw.events/views ::die ::input-terms])]
      (assoc-in db [:sa-draw.events/views ::die ::input-terms] (conj current-terms term)))))

(rf/reg-event-db
  ::add-proc-output-term
  (fn [db [_ term]]
    (let [current-terms (get-in db [:sa-draw.events/views ::die ::output-terms])]
      (assoc-in db [:sa-draw.events/views ::die ::output-terms] (conj current-terms term)))))

(rf/reg-event-db
  ::remove-term
  (fn [db [_ term]]
    (let [current-terms (get-in db [:sa-draw.events/views ::die ::terms])]
      (assoc-in db [:sa-draw.events/views ::die ::terms] (remove #(= (:id %) (:id term)) current-terms)))))

(rf/reg-event-db
  ::remove-proc-input-term
  (fn [db [_ term]]
    (let [current-terms (get-in db [:sa-draw.events/views ::die ::input-terms])]
      (assoc-in db [:sa-draw.events/views ::die ::input-terms] (remove #(= (:id %) (:id term)) current-terms)))))

(rf/reg-event-db
  ::remove-proc-output-term
  (fn [db [_ term]]
    (let [current-terms (get-in db [:sa-draw.events/views ::die ::output-terms])]
      (assoc-in db [:sa-draw.events/views ::die ::output-terms] (remove #(= (:id %) (:id term)) current-terms)))))
