(ns sa-draw.views.editors.dispatcher
  (:require ["@material-ui/core"                            :as mui]
            [re-frame.core                                  :as rf]
            [sa-draw.views.editors.context-object-editor     :as cedit]
            [sa-draw.views.editors.context-object-editor-subscriptions :as csb]
            [sa-draw.views.editors.node-subs                 :as nsb]
            [sa-draw.views.editors.node                      :as ne]
            [sa-draw.views.editors.flow                      :as flw]
            [sa-draw.views.editors.flow-subs                 :as fsb]
            [sa-draw.views.editors.quick-flow                :as qf]
            [sa-draw.views.editors.quick-flow-subs           :as qfs]
            [sa-draw.views.editors.file-upload-selector-subs :as fusub]
            [sa-draw.views.editors.file-upload-selector      :as fus]
            [sa-draw.views.editors.markdown-importer-subs    :as misub]
            [sa-draw.views.editors.markdown-importer         :as mi]
            [sa-draw.views.editors.symbol-editor             :as se]
            [sa-draw.views.editors.symbol-editor-subs        :as sesub]
            [sa-draw.views.editors.icon-editor               :as ice]
            [sa-draw.views.editors.icon-editor-subs          :as icesub]
            [sa-draw.views.editors.md.editor                 :as mded]
            [sa-draw.views.editors.md.subs                   :as mdedsub]
            [sa-draw.views.help.help-viewer                  :as help]
            [sa-draw.views.help.help-subs                    :as helpsub]))

(def data-type  (->  (make-hierarchy)
                     (derive ::proc                  ::data-type)
                     (derive ::prod                  ::data-type)
                     (derive ::cons                  ::data-type)
                     (derive ::source                ::data-type)
                     (derive ::sink                  ::data-type)
                     (derive ::create-context-object ::data-type)
                     (derive ::quick-flow            ::data-type)
                     (derive ::flow-editor           ::data-type)
                     (derive ::node-editor           ::data-type)
                     (derive ::new-node-editor       ::data-type)
                     (derive ::file-upload-selector  ::data-type)
                     (derive ::markdown-importer     ::data-type)
                     (derive ::symbol-editor         ::data-type)
                     (derive ::icon-editor           ::data-type)
                     (derive ::markdown-editor       ::data-type)
                     (derive ::help                  ::data-type)))


(defmulti get-editor
  (fn [data-type] data-type))


(defmethod get-editor ::source
  [data-type]
  (let [open? @(rf/subscribe [::csb/edit-open])
        id @(rf/subscribe [::csb/edit])
        type @(rf/subscribe [::csb/edit-type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::source))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [cedit/edit-context-object id]]))

(defmethod get-editor ::sink
  [data-type]
  (let [open? @(rf/subscribe [::csb/edit-open])
        id @(rf/subscribe [::csb/edit])
        type @(rf/subscribe [::csb/edit-type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::sink))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [cedit/edit-context-object id]]))


(defmethod get-editor ::prod
  [data-type]
  (let [open? @(rf/subscribe [::csb/edit-open])
        id @(rf/subscribe [::csb/edit])
        type @(rf/subscribe [::csb/edit-type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::prod))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [cedit/edit-context-object id]]))

(defmethod get-editor ::cons
  [data-type]
  (let [open? @(rf/subscribe [::csb/edit-open])
        id @(rf/subscribe [::csb/edit])
        type @(rf/subscribe [::csb/edit-type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::cons))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [cedit/edit-context-object id]]))

(defmethod get-editor ::create-context-object
  [data-type]
  (let [open? @(rf/subscribe [::csb/edit-open])
        type @(rf/subscribe [::csb/edit-type])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::create-context-object))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}
                        :style {:width "25%"}}
        [cedit/edit-new-context-object]]]))

(defmethod get-editor ::quick-flow
  [data-type]
  (let [open? @(rf/subscribe [::qfs/edit-open])
        type @(rf/subscribe [::qfs/edit-type])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::quick-flow))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}}
        [qf/quick-flow]]]))

(defmethod get-editor ::flow-editor
  [data-type]
  (let [open? @(rf/subscribe [::fsb/edit-open])
        type  @(rf/subscribe [::fsb/edit-type])
        id    @(rf/subscribe [::fsb/edit])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::flow-editor))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}}
        [flw/flow-editor id]]]))


(defmethod get-editor ::node-editor
  [data-type]
  (let [open? @(rf/subscribe [::nsb/node-edit-open])
        type  @(rf/subscribe [::nsb/node-edit-type])
        id    @(rf/subscribe [::nsb/node-edit-edit])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::node-editor))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}
                        :style {:width "25%"}}
        [ne/edit-node id]]]))

(defmethod get-editor ::new-node-editor
  [data-type]
  (let [open? @(rf/subscribe [::nsb/node-edit-open])
        type  @(rf/subscribe [::nsb/node-edit-type])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::new-node-editor))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}
                        :style {:width "25%"}}
        [ne/new-node]]]))

(defmethod get-editor ::file-upload-selector
  [data-type]
  (let [open? @(rf/subscribe [::fusub/file-upload-selector-open])
        type  @(rf/subscribe [::fusub/file-upload-selector-type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::file-upload-selector))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [fus/file-upload-selector]]))

::markdown-importer

(defmethod get-editor ::markdown-importer
  [data-type]
  (let [open? @(rf/subscribe [::misub/markdown-importer-selector-open])
        type  @(rf/subscribe [::misub/markdown-importer-selector-type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::markdown-importer))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [mi/file-upload-selector]]))

(defmethod get-editor ::symbol-editor
  [data-type]
  (let [open? @(rf/subscribe [::sesub/edit-open])
        type  @(rf/subscribe [::sesub/type])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::symbol-editor))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}
                        :style {:width "25%"}}
        [se/symbol-editor-ui]]]))

(defmethod get-editor ::icon-editor
  [data-type]
  (let [open? @(rf/subscribe [::icesub/edit-open])
        type  @(rf/subscribe [::icesub/type])]
    [:> js/ReactDraggable {:handle "strong"}
      [:> mui/Popover  {:open (and (if open? true false) (= type ::icon-editor))
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}}
        [ice/icon-editor-ui]]]))

(defmethod get-editor ::markdown-editor
  [data-type]
  (let [open? @(rf/subscribe [::mdedsub/edit-open])
        type  @(rf/subscribe [::mdedsub/type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::markdown-editor))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [mded/editor-ui]]))

(defmethod get-editor ::help
  [data-type]
  (let [open? @(rf/subscribe [::helpsub/viewer-open])
        type  @(rf/subscribe [::helpsub/type])]
    [:> mui/Popover  {:open (and (if open? true false) (= type ::help))
                      :anchorReference "anchorPosition"
                      :anchorPosition {:left 40 :top 40}}
      [help/help-ui]]))
