(ns sa-draw.views.editors.meta-data-events
  (:require [re-frame.core                  :as re-frame]
            [cljs.core.async   :as async :refer [chan <!]]
            [sa.sa-store.store                      :as sa]
            [sa.sa-store.store-factory              :as sf])
  (:require-macros [cljs.core.async.macros :refer [go]]))


(defn save-summary
  [store-id model-id name description board-link detail]
  (let [store (sf/get-store store-id)
        sum-chan (chan)
        mod-chan (chan)]
    (go
      (sa/get-model store mod-chan model-id)
      (let [model (<! mod-chan)
            updated-summary (assoc (:sadf/meta model) :sadf/name name :sadf/summary description :sadf/board-link board-link :sadf/detail detail)]
        (sa/update-summary store sum-chan model-id updated-summary)
        (print "FROM UPDATE : " (<! sum-chan))))))



(re-frame/reg-event-db
  ::save-meta-data
  (fn [db [_ store-id model-id name description board-link detail]]
    (save-summary store-id model-id name description board-link detail)
    db))
