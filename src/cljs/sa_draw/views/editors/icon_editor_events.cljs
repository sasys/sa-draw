(ns sa-draw.views.editors.icon-editor-events
  (:require [re-frame.core                            :as rf]
            [sa-draw.model.proto-model                :as pm]
            [sa-draw.model.map-impl                   :refer [map-model]]
            [sa-draw.model.icons                      :as ic]
            [sa-draw.model.icons-impl                 :refer [im]]))


(rf/reg-event-db
  ::close
  (fn  [db [_]]
    (assoc-in db [:sa-draw.events/views ::icon-editor] {::open? false
                                                        ::new nil
                                                        ::edit nil})))

(rf/reg-event-db
  ::edit-icon
  (fn [db [_ icon-id]]
    (assoc-in db [:sa-draw.events/views ::icon-editor] {::open? true
                                                        ::edit icon-id
                                                        ::type :sa-draw.views.editors.dispatcher/icon-editor})))

(rf/reg-event-db
  ::update-icon
  (fn [db [_ _ updated-icon _]]
    (let [iconsv (pm/find-icon-model map-model (:sa-draw.events/model db))
          updated-iconsv (ic/update-icon im iconsv updated-icon)]
      (assoc-in db [:sa-draw.events/model :sadf/icons] updated-iconsv))))

(rf/reg-event-db
  ::delete
  (fn [db [_ icon]]
    (let [layout (get-in db [:sa-draw.events/graphics :sa-draw.events/layout])
          iconsv (pm/find-icon-model map-model (:sa-draw.events/model db))
          updated-iconsv (ic/remove-icon im iconsv icon)
          id     (ic/id im icon)]
      (-> db
          (assoc-in [:sa-draw.events/model :sadf/icons] updated-iconsv)
          (assoc-in [:sa-draw.events/graphics :sa-draw.events/layout] (dissoc layout (keyword "sadf" id)))))))
