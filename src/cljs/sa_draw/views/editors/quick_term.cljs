(ns sa-draw.views.editors.quick-term
  (:require [reagent.core               :as r]
            ["@material-ui/core"        :as mui]
            ["@material-ui/icons"       :as muicons]
            [re-frame.core              :as rf]
            [sa-draw.data.simple-dd     :refer [term]]
            [sa-draw.data.dd            :refer [new-term]]
            [clojure.string             :refer [split]]))


(defn new-terms
  [data-names]
  (map #(new-term term %) data-names))


(defn quick-term
  "A quick way to make a term.  Comma space separated  (a, b) strings turned into terms.
   The target list is an atom of the list to add the new term(s) to.
   Title is an atom that can be nil but should be the working title to use.
   Visible is a boolean atom
  "
  [title visible? add-event]
  (let [new-data (r/atom "a, b")]
    (fn [title visible? add-event]
      [:> mui/Popover  {:open @visible?
                        :anchorReference "anchorPosition"
                        :anchorPosition {:left 40 :top 40}}
        [:> mui/Grid {:container    true
                      :direction    "row"
                      :item         true
                      :justify      "flex-start"
                      :alignItems   "center"}
            [:> mui/Typography {:variant "subtitle2" :style {:margin 5}}
                (if title @title
                    "Quck Create New terms ")]
            [:> mui/Tooltip {:title "Abandon   edit"}
              [:> mui/IconButton {:onClick #(reset! visible? false)}
                [:> muicons/Close]]]]
        [:> mui/Grid {:container    true
                      :direction    "column"
                      :item         true
                      :justify      "flex-start"
                      :alignItems   "center"}
          [:> mui/TextField {:label @new-data
                             :variant "outlined"
                             :rows 1
                             :onChange #(reset! new-data (.. % -target -value))}]]

        [:> mui/Grid {:container    true
                      :direction    "row"
                      :item         true
                      :justify      "flex-start"
                      :alignItems   "center"}
          [:> mui/Tooltip {:title "Abandon   edit"}
            [:> mui/IconButton {:onClick #(reset! visible? false)}
              [:> muicons/Close]]]
          [:> mui/Tooltip {:title "Save edit"}
            [:> mui/IconButton {:onClick (fn [] (as-> @new-data ts
                                                      (split ts  #", ")
                                                      (new-terms ts)
                                                      (doseq [term ts] (rf/dispatch [add-event term]))))}
              [:> muicons/Save]]]]])))
