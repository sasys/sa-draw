(ns sa-draw.views.editors.node-events
  (:require [re-frame.core                          :as re-frame]
            [sa-draw.model.proto-model  :as proto-model]
            [sa-draw.model.map-impl     :refer [map-model proc-model]]))

(re-frame/reg-event-db
  ::node-edit-new
  (fn  [db [_ _]]
    (-> db
      (assoc-in  [:sa-draw.events/views ::node-edit ::open?] true)
      (assoc-in  [:sa-draw.events/views ::node-edit ::new] "dummy-id")
      (assoc-in  [:sa-draw.events/views ::node-edit ::edit] nil)
      (assoc-in  [:sa-draw.events/views ::node-edit ::type] :sa-draw.views.editors.dispatcher/new-node-editor))))
    ; (assoc-in db [:sa-draw.events/views] {::node-edit {::open? true
    ;                                                    ::new "dummy-id" ;we dont pass in the new id for now
    ;                                                    ::edit nil
    ;                                                    ::type :sa-draw.views.editors.dispatcher/new-node-editor}})))

(re-frame/reg-event-db
  ::node-edit-edit
  (fn  [db [_ proc-id]]
    (-> db
      (assoc-in  [:sa-draw.events/views ::node-edit ::open?] true)
      (assoc-in  [:sa-draw.events/views ::node-edit ::new] nil)
      (assoc-in  [:sa-draw.events/views ::node-edit ::edit] proc-id)
      (assoc-in  [:sa-draw.events/views ::node-edit ::type] :sa-draw.views.editors.dispatcher/node-editor))))
    ; (assoc-in db [:sa-draw.events/views] {::node-edit {::open? true
    ;                                                    ::new nil
    ;                                                    ::edit proc-id
    ;                                                    ::type :sa-draw.views.editors.dispatcher/node-editor}})))


(re-frame/reg-event-db
  ::node-edit-close
  (fn  [db [_]]
    (-> db
      (assoc-in  [:sa-draw.events/views ::node-edit ::open?] false)
      (assoc-in  [:sa-draw.events/views ::node-edit ::new] nil)
      (assoc-in  [:sa-draw.events/views ::node-edit ::edit] nil)
      (assoc-in  [:sa-draw.events/views ::node-edit ::type] nil))))
    ; (assoc-in db [:sa-draw.events/views] {::node-edit {::open? false
    ;                                                    ::new nil
    ;                                                    ::edit nil}})))


(re-frame/reg-event-db
  ::node-edit-delete
  (fn [db [_ id]]
    (let [model (:sa-draw.events/model db)
          procs-model (proto-model/find-process-model map-model model)
          layout (get-in db [:sa-draw.events/graphics :sa-draw.events/layout])]
      (-> db
        (assoc-in  [:sa-draw.events/model] (proto-model/update-process-model map-model model (proto-model/remove-node proc-model procs-model id)))
        (assoc-in [:sa-draw.events/graphics :sa-draw.events/layout] (dissoc layout (keyword "sadf" id)))))))
