(ns sa-draw.views.editors.markdown-importer-subs
  (:require [re-frame.core                  :as rf]
            [sa-draw.views.editors.markdown-importer-events :as miev]))

(rf/reg-sub
  ::markdown-importer-selector-open
  (fn [db _]
    (get-in db [:sa-draw.events/views ::miev/markdown-importer ::miev/open?])))


(rf/reg-sub
  ::markdown-importer-selector-type
  (fn [db _]
    (get-in db [:sa-draw.events/views ::miev/markdown-importer ::miev/type])))
