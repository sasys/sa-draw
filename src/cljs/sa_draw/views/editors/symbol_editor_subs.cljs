(ns sa-draw.views.editors.symbol-editor-subs
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::symbol-editor
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.symbol-editor-events/symbol-editor])))

(rf/reg-sub
  ::edit-open :<- [::symbol-editor]
  (fn [symbol-editor _]
    (:sa-draw.views.editors.symbol-editor-events/open? symbol-editor)))


(rf/reg-sub
  ::edit-new :<- [::symbol-editor]
  (fn [symbol-editor _]
    (:sa-draw.views.editors.symbol-editor-events/new? symbol-editor)))

(rf/reg-sub
  ::type :<- [::symbol-editor]
  (fn [symbol-editor _]
    (:sa-draw.views.editors.symbol-editor-events/type symbol-editor)))

(rf/reg-sub
  ::tab :<- [::symbol-editor]
  (fn [symbol-editor _]
    (:sa-draw.views.editors.symbol-editor-events/tab symbol-editor)))

(rf/reg-sub
  ::new-svg
  (fn [db _]
    (:input-svg db)))

(rf/reg-sub
  ::syms-loaded
  (fn [db]
    (get-in db [:sa-draw.events/model :sadf/meta :syms-loaded])))
