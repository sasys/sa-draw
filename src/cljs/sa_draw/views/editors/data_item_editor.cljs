(ns sa-draw.views.editors.data-item-editor
  (:require ["@material-ui/core"      :as mui]
            ["@material-ui/icons"     :as muicons]
            [reagent.core             :as r]
            [re-frame.core            :as rf]
            ;[sa-draw.views.dd.events  :as ddev]
            [sa-draw.data.dd          :refer [label new-term id]]
            [sa-draw.data.simple-dd   :refer [term]]))



(defn data-item-editor
  "
    Edits a single data item.

    t is a term
    deleted-terms is an atom of a list that will have the term added to it if the term is deleted.
    local-terms is an atom of a list that will have a deleted term removed from it.
    edit-locked? is a boolean telling us if we should freeze the controls.
  "
  [t edit-locked? rows-open? delete-event]
  (let [row-open? (if @rows-open? (r/atom @rows-open?) (r/atom false))]
    (fn [t edit-locked? rows-open? delete-event]
      [:> mui/Grid {:container    true
                    :direction    "row"
                    :item         true
                    :justify      "space-between"
                    :alignItems   "center"}
    ;     ;ButtonBase instead of Button allows the styles to be applied, Button overrides them here.
          [:> mui/ButtonBase {:variant "contained"
                              :size "small"
                              :disabled edit-locked?
                              :onClick #(reset! row-open? (not @row-open?))}
                            (str (label term t))]

          [:div (if (not @row-open?) {:style {:display "none"}})
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Tooltip {:title "Done"}
                [:div
                  [:> mui/IconButton {:disabled edit-locked?
                                      :onClick #(swap! row-open? not @row-open?)}
                    [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]]
              [:> mui/Tooltip {:title "Delete"}
                [:div
                  [:> mui/IconButton {:disabled edit-locked?
                                      :onClick (fn [] (swap! row-open? not @row-open?)
                                                      (rf/dispatch [delete-event t]))}

                    [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]]]])))



(defn data-list-editor
  "
    An editor for adding and removing data elements from the presented list.
    The terms are a list of terms.  The list is assumed to be an r/atom
    editor-open? should be a boolean atom
    edit-locked? should be a boolean atom
  "
    [terms new-terms deleted-terms editor-open? edit-locked?]

    (let [local-terms (r/atom @terms)
          data-name (r/atom "data-name")]
      (fn [terms new-terms deleted-terms editor-open? edit-locked?]

          [:div
            ;The existings data items
            (for [t @local-terms]
              ^{:key (id term t)} [data-item-editor t deleted-terms local-terms @edit-locked?])
            ;The new terms
            (for [t @new-terms]
              ^{:key (id term t)} [data-item-editor t deleted-terms local-terms @edit-locked?])

            [:div (if (not @editor-open?) {:style {:display "none"}})
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/TextField {:label @data-name
                                   :variant "outlined"
                                   :multiline true
                                   :rows 1
                                   ;:defaultValue @data-name
                                   :style {:width 170}
                                   :onChange (fn [e] (reset! data-name (.. e -target -value)))}]

                [:> mui/Tooltip {:title "Done"}
                  [:> mui/IconButton {:onClick #(let [t (new-term term @data-name)]
                                                  (swap! new-terms conj t)
                                                  ;(rf/dispatch [::ddev/new-term t])
                                                  (swap! editor-open? not @editor-open?))}
                    [:> muicons/Done {:classes {:root "material-icons md-18"}}]]]
                [:> mui/Tooltip {:title "Discard"}
                  [:> mui/IconButton {:onClick #(swap! editor-open? not @editor-open?)}
                    [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]]])))
