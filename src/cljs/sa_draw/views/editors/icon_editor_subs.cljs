(ns sa-draw.views.editors.icon-editor-subs
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::icon-editor
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.icon-editor-events/icon-editor])))

(rf/reg-sub
  ::edit-open :<- [::icon-editor]
  (fn [icon-editor _]
    (:sa-draw.views.editors.icon-editor-events/open? icon-editor)))

(rf/reg-sub
  ::type :<- [::icon-editor]
  (fn [editor-type _]
    (:sa-draw.views.editors.icon-editor-events/type editor-type)))

(rf/reg-sub
  ::edit-icon-id :<- [::icon-editor]
  (fn [icon-edit-id _]
    (:sa-draw.views.editors.icon-editor-events/edit icon-edit-id)))
