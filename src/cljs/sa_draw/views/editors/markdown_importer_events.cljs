(ns sa-draw.views.editors.markdown-importer-events
  (:require [re-frame.core                   :as rf]
            [sa-draw.events                  :as ev]
            [sa-draw.model.proto-model       :as proto-model]
            [sa-draw.model.map-impl          :refer [map-model]]
            [sa-draw.model.utils             :refer [add-change-namespace remove-context-objects uid default-namespace]]
            [sa-layout.layout-factory        :refer [get-layout]]
            [sa-layout.layout                :refer [generate-layout]]
            [sa-draw.config.defaults         :refer [max-x max-y]]))

(defn complete-model
  "
    Given a proc model, create a complete model.
    Uses the default namespace
  "
  [proc-model]
  (let [lay (get-layout :sa-layout.layout-factory/fixed-grid)
        uuid (random-uuid)
        fixed (-> (js->clj (.parse js/JSON proc-model) :keywordize-keys true)
                  (add-change-namespace default-namespace)
                  (assoc (keyword "sadf" "id") "0"))


        layout (generate-layout lay fixed default-namespace [max-x max-y])
        new-model (-> (proto-model/make-model map-model uuid (str "IMPORTED MODEL-" uuid))
                      (assoc (keyword default-namespace "layout") layout)
                      (remove-context-objects))]
    (proto-model/update-process-model map-model new-model fixed)))


(defn fire-md1 [md]
  (rf/dispatch [::ev/update-markdown md]))

;
; This fx causes the local markdown parser to run
;
(rf/reg-fx
  ::markdown-from-json-fx-local
  (fn [req]
    (.then (.mdtoj js/parser  (::md req))
      (fn [%1] (as-> (complete-model %1) model
                     (rf/dispatch [::ev/upload-model model]))))))


(rf/reg-event-fx
  ::markdown-from-json
  (fn [cofx [_ md]]
    {::markdown-from-json-fx-local {::md md
                                    ::db (:db cofx)}}))



(rf/reg-event-db
  ::open-mi
  (fn [db [_]]
    (-> db
      (assoc-in  [:sa-draw.events/views ::markdown-importer ::open?] true)
      (assoc-in  [:sa-draw.events/views ::markdown-importer ::type] :sa-draw.views.editors.dispatcher/markdown-importer))))

(rf/reg-event-db
  ::close-mi
  (fn [db [_]]
    (assoc-in  db [:sa-draw.events/views ::markdown-importer ::open?] false)))
