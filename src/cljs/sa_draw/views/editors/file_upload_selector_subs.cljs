(ns sa-draw.views.editors.file-upload-selector-subs
  (:require [re-frame.core                  :as rf]
            [sa-draw.views.editors.file-upload-selector-events :as fusev]))

(rf/reg-sub
  ::file-upload-selector-open
  (fn [db _]
    (get-in db [:sa-draw.events/views ::fusev/file-upload-selector ::fusev/open?])))


(rf/reg-sub
  ::file-upload-selector-type
  (fn [db _]
    (get-in db [:sa-draw.events/views ::fusev/file-upload-selector ::fusev/type])))
