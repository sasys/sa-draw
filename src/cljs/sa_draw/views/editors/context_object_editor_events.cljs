(ns sa-draw.views.editors.context-object-editor-events
  (:require [re-frame.core              :as rf]
            [sa-draw.model.proto-model  :as proto-model]
            [sa-draw.model.map-impl     :as mimpl :refer [map-model proc-model context-model]]))


(def context-type  (->  (make-hierarchy)
                        (derive ::prod   ::data-type)
                        (derive ::cons   ::data-type)
                        (derive ::source ::data-type)
                        (derive ::sink   ::data-type)))



;Adds the given context object of the given type into the model
(rf/reg-event-db
  ::context-object-editor-new
  (fn  [db [_ id type new-context-object]]
    (let [model (:sa-draw.events/model db)
          context-objects (proto-model/find-context-model map-model model)
          updated-objects (proto-model/add-to-context-objects context-model context-objects type new-context-object)
          updated-model (proto-model/update-model-context-objects map-model model updated-objects)]
      (-> db
        (assoc-in [:sa-draw.events/graphics :sa-draw.events/layout (keyword "sadf" id)] [60 60])
        (assoc-in [:sa-draw.events/model] updated-model)))))


(rf/reg-event-db
  ::context-object-editor-edit
  (fn  [db [_ context-object-id]]
    (assoc-in db [:sa-draw.events/views ::context-object-editor] {::open? true
                                                                  ::new nil
                                                                  ::edit context-object-id})))

(rf/reg-event-db
  ::close
  (fn  [db [_]]
    (assoc-in db [:sa-draw.events/views ::context-object-editor] {::open? false
                                                                  ::new nil
                                                                  ::edit nil})))

(rf/reg-event-db
  ::create
  (fn [db _]
    (assoc-in db [:sa-draw.events/views  ::context-object-editor] {::open? true
                                                                   ::new "dummy-id" ;we dont pass in the new id for now
                                                                   ::edit nil
                                                                   ::type :sa-draw.views.editors.dispatcher/create-context-object})))


;Updates the given context object of the given type into the model
(rf/reg-event-db
  ::update
  (fn [db [_ type context-object title description terms link]]
    (let [model (:sa-draw.events/model db)
          context-objects (proto-model/find-context-model map-model model)
          updated-objects  (proto-model/update-context-objects context-model context-objects type context-object title description terms link)
          updated-model (proto-model/update-model-context-objects map-model model updated-objects)]
      (-> db
        (assoc-in [:sa-draw.events/model] updated-model)))))

;Updates the given context object of the given type into the model with an icon
(rf/reg-event-db
  ::update-with-icon
  (fn [db [_ type context-object title description terms link _]]
    (let [model (:sa-draw.events/model db)
          context-objects (proto-model/find-context-model map-model model)
          updated-objects  (proto-model/update-context-objects context-model context-objects type context-object title description terms link)
          updated-model (proto-model/update-model-context-objects map-model model updated-objects)]
      (-> db
        (assoc-in [:sa-draw.events/model] updated-model)))))



;Deletes the given context object of the given type from the model
(rf/reg-event-db
  ::delete
  (fn [db [_ type id context-object]]
    (let [model (:sa-draw.events/model db)
          context-objects (proto-model/find-context-model map-model model)
          updated-objects  (proto-model/delete-from-context-objects context-model context-objects type context-object)
          updated-model (proto-model/update-model-context-objects map-model model updated-objects)
          layout (get-in db [:sa-draw.events/graphics :sa-draw.events/layout])]
      (-> db
        (assoc-in [:sa-draw.events/model] updated-model)
        (assoc-in [:sa-draw.events/graphics :sa-draw.events/layout] (dissoc layout (keyword "sadf" id)))))))
