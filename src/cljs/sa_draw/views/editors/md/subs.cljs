(ns sa-draw.views.editors.md.subs
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::markdown-editor
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.md.events/markdown-editor])))

(rf/reg-sub
  ::edit-open :<- [::markdown-editor]
  (fn [markdown-editor _]
    (:sa-draw.views.editors.md.events/open? markdown-editor)))

(rf/reg-sub
  ::type :<- [::markdown-editor]
  (fn [markdown-editor _]
    (:sa-draw.views.editors.md.events/type markdown-editor)))

(rf/reg-sub
  ::all-nodes :<- [::markdown-editor]
  (fn [markdown-editor _]
    (:sa-draw.views.editors.md.events/node-open markdown-editor)))

(rf/reg-sub
  ::node-open :<- [::node-open]
  (fn [markdown-editor node-id]
    (get-in markdown-editor [:sa-draw.views.editors.md.events/node-open node-id])))
