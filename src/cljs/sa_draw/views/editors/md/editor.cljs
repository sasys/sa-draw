(ns sa-draw.views.editors.md.editor
  "
    A structured editor intended to allow the creation and modification of a
    model through markdown instead of drawing.
  "
  (:require [reagent.core                                   :as r]
            [material-ui                                    :as mui]
            [material-ui-icons                              :as muicons]
            [re-frame.core                                  :as rf]
            [sa-draw.subs                                   :as subs]
            [sa-draw.views.markdown.markdown-viewer-events  :as mdev]
            [sa-draw.model.utils                            :as utils]
            [sa-draw.components.card-node-edit              :as ne]))
            ;[clojure.string]))


;
; A markdown editor that creates and modifies model elements
; Editor constrain what the user can do to ensure the md complies with the
; model rules.
;

(def input-component
  (r/reactify-component
    (fn [props]
      [:input (-> props
                  (assoc :ref (:inputRef props))
                  (dissoc :inputRef))])))

(defn make-list-items
  "Given a node make a list structure from that node and all the child nodes."
  [node open-nodes]
  [:<>
    ^{:key (str (:sadf/id node) "- edit")}[ne/edit node open-nodes]
    (if (and (> (count (:sadf/children node)) 0) ((keyword (:sadf/id node)) @open-nodes))
      (doall (for [child (utils/sort-node-list "sadf" (:sadf/children node))]
               (make-list-items child open-nodes))))])

(defn editor-ui
  "Initial editor card"
  []
  (let [open-nodes (r/atom {})]
    (fn []
      (let [procs      (rf/subscribe [::subs/procs])]

        ;Make sure the 0 node is open otherwise we cant open anything below it!!
        (swap! open-nodes assoc (keyword "0") true)

        [:> js/ReactDraggable {:handle "strong"}

          [:> mui/Paper
            [:div
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:strong [:> muicons/DragIndicator]]
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}}  (str "Model Editor - "(:sadf/title @procs))]
                [:> mui/Tooltip {:title "Refresh"}
                  [:> mui/IconButton {:onClick #(rf/dispatch [::mdev/markdown-from-json])}
                    [:> muicons/RestorePage]]]
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Divider]
              [:> mui/Paper
                [:div {:style {:padding "10px" :height "660px" :width "500px" :overflow "auto"}}
                    (make-list-items @procs open-nodes)]]
              [:> mui/Divider]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}

                [:strong [:> muicons/DragIndicator]]
                [:> mui/Tooltip {:title "Refresh"}
                  [:> mui/IconButton {:onClick #(rf/dispatch [::mdev/markdown-from-json])}
                    [:> muicons/RestorePage]]]
                [:strong [:> muicons/DragIndicator]]]]]]))))
