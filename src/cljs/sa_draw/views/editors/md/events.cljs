(ns sa-draw.views.editors.md.events
  (:require [re-frame.core             :as rf]
            [sa-draw.model.proto-model :as proto-model]
            [sa-draw.model.map-impl    :refer [proc-model map-model]]))



(rf/reg-event-db
  ::close
  (fn  [db [_]]
    (-> db
      (assoc-in [:sa-draw.events/views ::markdown-editor ::open?] false)
      (assoc-in [:sa-draw.events/views ::markdown-editor ::type] nil))))

(rf/reg-event-db
  ::edit-markdown
  (fn [db [_]]
    (-> db
      (assoc-in [:sa-draw.events/views ::markdown-editor ::open?] true)
      (assoc-in [:sa-draw.events/views ::markdown-editor ::type] :sa-draw.views.editors.dispatcher/markdown-editor))))


(rf/reg-event-db
  ::node-open
  (fn [db [_ node-id open?]]
    (assoc-in db [:sa-draw.events/views ::markdown-editor ::node-open (keyword node-id)] open?)))

(rf/reg-event-db
  ::move-right
  (fn [db [_ node-id]]
    (let [updated-proc-model (proto-model/swap-right proc-model (proto-model/find-process-model map-model (:sa-draw.events/model db)) node-id)]
      (assoc-in db [:sa-draw.events/model :sadf/procs] updated-proc-model))))

(rf/reg-event-db
  ::move-left
  (fn [db [_ node-id]]
    (let [updated-proc-model (proto-model/swap-left proc-model (proto-model/find-process-model map-model (:sa-draw.events/model db)) node-id)]
      (assoc-in db [:sa-draw.events/model :sadf/procs] updated-proc-model))))

(rf/reg-event-db
  ::move-up
  (fn [db [_ node-id]]
    (let [updated-proc-model (proto-model/move-up proc-model (proto-model/find-process-model map-model (:sa-draw.events/model db)) node-id)]
      (assoc-in db [:sa-draw.events/model :sadf/procs] updated-proc-model))))

(rf/reg-event-db
  ::compact
  (fn [db [_ node-id]]
    (let [process-model         (proto-model/find-process-model map-model (:sa-draw.events/model db))
          node               (proto-model/find-node proc-model process-model node-id)
          node-list          (proto-model/children proc-model node)
          updated-node-list  (proto-model/compact proc-model node-list)
          updated-process-model (proto-model/update-node proc-model node-id (assoc node :sadf/children updated-node-list) process-model)]
      (assoc-in db [:sa-draw.events/model :sadf/procs] updated-process-model))))

(rf/reg-event-db
  ::add-node
  (fn [db [_ node-id]]
    (let [updated-proc-model (proto-model/add-new-child-node proc-model (proto-model/find-process-model map-model (:sa-draw.events/model db)) node-id)]
      (assoc-in db [:sa-draw.events/model :sadf/procs] updated-proc-model))))


(rf/reg-event-db
  ::add-node-with-icon
  (fn [db [_ node-id _]]
    (let [updated-proc-model (proto-model/add-new-child-node proc-model (proto-model/find-process-model map-model (:sa-draw.events/model db)) node-id)]
      (assoc-in db [:sa-draw.events/model :sadf/procs] updated-proc-model))))
