(ns sa-draw.views.editors.meta-data-editor
  (:require [reagent.core               :as reagent]
            ["@material-ui/core"        :as mui]
            ["@material-ui/icons"       :as muicons]
            [re-frame.core              :as rf]
            [sa-draw.views.editors.meta-data-events :as mev]
            [sa-draw.events             :as ev]
            [sa.sa-store.store-factory  :as sf]
            [clojure.string             :refer [blank?]]
            [sa-draw.global-state      :refer [meta-data-editor-open]]))

;(def meta-data-editor-open (reagent/atom nil))


(defn make-detail-panel
  [details]
  (let [ks (keys details)]
    [:> mui/Grid {:item true}
      (for [k ks]
         ^{:key k}[:p (str (name k) " : " (k details))])]))


(defn show-meta-data-editor
  [home?]
  (let [editing? (reagent/atom false)]
    (fn [home?]
      (let [id (:id @meta-data-editor-open)
            descr (:summary @meta-data-editor-open)
            model-name  (:name @meta-data-editor-open)
            board-link (:board-link @meta-data-editor-open)
            updated-name (reagent/atom model-name)
            updated-descr (reagent/atom descr)
            updated-board-link (reagent/atom board-link)
            store-id (:store-id @meta-data-editor-open)
            detail (:detail @meta-data-editor-open)
            creation-date (:creation-date @meta-data-editor-open)
            app-version (:app-version @meta-data-editor-open)]
       [:> js/ReactDraggable {:handle "strong"}
        [:> mui/Popover  {:open (boolean (and (not (nil? @meta-data-editor-open)) home?)) ;Without the boolean we get js errors.
                          :anchorReference "anchorPosition"
                          :anchorPosition {:left 40 :top 40}}

                  [:> mui/Grid { :container    true
                                 :direction    "row"
                                 :item         true
                                 :justify      "space-between"
                                 :alignItems   "center"}
                                 ;:spacing      8}
                      [:strong [:> muicons/DragIndicator]]
                      [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} "Edit Model Meta Data"]
                      [:> mui/Tooltip {:title "Close"}
                        [:> mui/IconButton {:onClick  #(do
                                                          (rf/dispatch [::ev/load-summaries ::sf/key-store])
                                                          (rf/dispatch [::ev/load-summaries ::sf/rest-store])
                                                          (reset! editing? false)
                                                          (reset! meta-data-editor-open nil))}
                          [:> muicons/Close]]]]


                  [:div {:style {:display (if @editing? "block" "none")}}

                    [:> mui/Grid {:container    true
                                  :direction    "row"
                                  :item         true
                                  :justify      "space-between"
                                  :alignItems   "center"}
                      ;[:> mui/Grid {:container true}
                      [:> mui/TextField {:variant "outlined"
                                         :label "Name"
                                         ;:margin "normal"
                                         ;:fullWidth true
                                         :style {:margin 5}
                                         :defaultValue  model-name
                                         :onChange (fn [e] (reset! updated-name (.. e -target -value)))}]
                      [:> mui/Tooltip {:title "Model name.  This will be visible on the home page and the tool bar when the model is edited."} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]
;; FIXME: TextareaAutosize fails here so we have to use a multiline text field.  We cant find the component
;;        even though its in the cljsjjs library!!
                    [:> mui/Grid {:container    true
                                  :direction    "row"
                                  :item         true
                                  :justify      "space-between"
                                  :alignItems   "center"}
                      ;[:> mui/Grid {:container true}
                      [:> mui/TextField {:variant "outlined"
                                         :label "Description"
                                         ;:margin "normal"
                                         ;:fullWidth true
                                         :style {:margin 5}
                                         :multiline true
                                         :defaultValue  descr
                                         :onChange (fn [e] (reset! updated-descr (.. e -target -value)))}]
                      [:> mui/Tooltip {:title "Model description.  This will be visible on the home page."} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]

                    [:> mui/Grid {:container    true
                                  :direction    "row"
                                  :item         true
                                  :justify      "space-between"
                                  :alignItems   "center"}
                                         ;:css {:margin 20}}
                      ; [:> mui/Grid {:container true}
                        [:> mui/TextField {:variant "outlined"
                                           :label "Board"
                                           ;:margin "normal"
                                           ;:fullWidth true
                                           :style {:margin 5}
                                           :defaultValue  board-link
                                           :onChange (fn [e] (reset! updated-board-link (.. e -target -value)))}]
                        [:> mui/Tooltip {:title "The url of of a trello board."} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]

                  [:div {:style {:display (if @editing? "none" "block")}}

                    [:> mui/Grid {:item true
                                  :container true
                                  :direction "column"
                                  :justify "space-between"
                                  :alignitems "center"
                                        :style {:margin 10}}

                      [:> mui/Grid {:item true}
                        [:> mui/Typography {:variant "subtitle2" } model-name]]
                      [:> mui/Grid {:item true}
                        [:> mui/TextField {:disabled true
                                           :variant "standard"
                                           :fullWidth true
                                           :multiline true
                                           :defaultValue  descr
                                           :InputProps {:disableUnderline true}}]]
                      (if (not (blank? board-link))
                        [:> mui/Grid {:item true}
                          [:> mui/IconButton {:variant "text" :href board-link :target "_blank"}
                            [:> mui/SvgIcon {:titleAccess "A Status Board (new tab)"}
                             [:path {:class "st1" :d "M 20 2 L 4 2 C 2.894531 2 2 2.894531 2 4 L 2 20 C 2 21.105469 2.894531 22 4 22 L 20 22 C 21.105469 22 22 21.105469 22 20 L 22 4 C 22 2.894531 21.105469 2 20 2 Z M 11 18 C 11 18.550781 10.550781 19 10 19 L 5 19 C 4.445313 19 4 18.550781 4 18 L 4 5 C 4 4.449219 4.445313 4 5 4 L 10 4 C 10.550781 4 11 4.449219 11 5 Z M 20 11.996094 C 20 12.550781 19.550781 13 18.996094 13 L 14.003906 13 C 13.449219 13 13 12.550781 13 11.996094 L 13 5.003906 C 13 4.449219 13.449219 4 14.003906 4 L 18.996094 4 C 19.550781 4 20 4.449219 20 5.003906 Z "}]]]])]]

                  [:> mui/Grid { :container    true
                                 :direction    "column"
                                 ;:item         true
                                 :justify      "space-between"
                                 :alignItems   "stretch"
                                 :style {:margin 10}}
                    [:> mui/Divider]
                    [:> mui/Grid {:item true}
                      [:> mui/Typography {:variant "caption" } (str "App Version : " app-version)]]
                    [:> mui/Grid {:item true}
                      [:> mui/Typography {:variant "caption" } (str "Created : " creation-date)]]
                    [:> mui/Grid {:item true}
                      [:> mui/Typography {:variant "caption" } (str "id : " id)]]
                    [:> mui/Grid {:item true}
                      [:> mui/Typography {:variant "caption" } (str "Model from : " store-id)]]]

                  [:> mui/Grid {:container    true
                                :direction    "row"
                                :item         true
                                :justify      "space-between"
                                :alignItems   "center"}
                    [:> mui/Tooltip {:title "Close"}
                      [:> mui/IconButton {:onClick #(do
                                                        (rf/dispatch [::ev/load-summaries ::sf/key-store])
                                                        (rf/dispatch [::ev/load-summaries ::sf/rest-store])
                                                        (reset! editing? false)
                                                        (reset! meta-data-editor-open nil))}
                        [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                    [:> mui/Tooltip {:title "Save Model As Transit To File System"}
                      [:div
                        [:> mui/IconButton {:onClick (fn [] (rf/dispatch [::ev/download-transit model-name id]))}
                          [:> muicons/Backup]]]]
                    [:> mui/Tooltip {:title "Save changes to this meta data"}
                      [:span

                        [:> mui/IconButton {:onClick (fn [] (rf/dispatch [::mev/save-meta-data store-id id @updated-name @updated-descr @updated-board-link detail])
                                                            (rf/dispatch [::ev/load-summaries store-id])
                                                            (reset! editing? false)
                                                            (swap! meta-data-editor-open assoc :name @updated-name :summary @updated-descr :board-link @updated-board-link))
                                            :disabled (not @editing?)}
                          [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]]
                    [:> mui/Tooltip {:title "Delete Model!!!"}
                      [:span

                        [:> mui/IconButton {:onClick (fn [] (rf/dispatch-sync [::ev/delete-model-by-id store-id id])
                                                            (rf/dispatch [::ev/load-summaries store-id])
                                                            (reset! editing? false)
                                                            (reset! meta-data-editor-open  nil))
                                                             ;(rf/dispatch [::mev/save-meta-data store-id id @updated-name @updated-descr detail])
                                            :disabled (not @editing?)}
                          [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]
                    [:> mui/Tooltip {:title "Edit meta data"}
                      [:> mui/IconButton {:onClick #(swap! editing? not @editing?)}
                        [:> muicons/Edit {:classes {:root "material-icons md-18"}}]]]]]]))))
