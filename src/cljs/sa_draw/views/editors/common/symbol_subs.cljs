(ns sa-draw.views.editors.common.symbol-subs
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::symbol-editor
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.common.symbol-events/symbol-editor])))


(rf/reg-sub
  ::tab :<- [::symbol-editor]
  (fn [symbol-editor _]
    (:sa-draw.views.editors.common.symbol-events/tab symbol-editor)))
