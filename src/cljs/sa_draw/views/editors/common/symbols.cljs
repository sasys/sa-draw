(ns sa-draw.views.editors.common.symbols
  "
    A lot of code is shared between the node editor (new and edit) and the
    symbol editor.  This namespace contains the common code.
  "
  (:require [reagent.core                                     :as reagent]
            [re-frame.core                                    :as rf]
            ["@material-ui/core"                              :as mui]
            ["@material-ui/icons"                             :as muicons]
            [sa-draw.model.icons                              :as icons]
            [sa-draw.views.licence.licence-viewer-events      :as lve]
            [sa-draw.views.licence.licence-viewer-subs        :as lvs]
            [sa-draw.views.editors.common.symbol-events    :as seev]
            [sa-draw.views.licence.viewer                     :refer [licence-viewer-ui]]
            [sa-draw.model.icons-impl                         :refer [im]]
            [sa-draw.subs                                     :as subs]))



;The tab selection
;(def selected-tab  (reagent/atom 0))

(def proc-selected (reagent/atom false))
;(def selected-icons (reagent/atom #{}))

(defn form-icon-instance
  [prototype-icon source icon-source-title layer uri]
  (assoc (icons/new-icon im prototype-icon source icon-source-title layer) :uri uri))


(defn make-source-card
  "
    Makes a card that describes a single icon including selection and deselction operations
    tab-id is the id of the tab for this card
    current layer is the current layer id
    selected-icons is an atom containing a list of the currently selected icons
    enabled? is an atom indicating if selections are enabled
    single? is a boolean indicating if we allow multi selection (true)
    selected-tab is an atom of the id of the tab as an atom
  "
  [tab-id current-layer selected-icons enabled? single? selected-tab]
  (let [default-icons @(rf/subscribe [::subs/default-icons])]
    (println (str "\n\ntab-id " tab-id))
    (let [source        (reagent/atom (first (filter #(= (:title %) tab-id) default-icons)))
          organisation  (:source @source)
          title         tab-id
          lv-open?      @(rf/subscribe [::lvs/licence-viewer-open])]
      (println (str "\n\n====\nsource        " @source "\n\n"))
      (println (str "\n\ntab id        " tab-id "\n\n"))
      (println (str "\n\n selected tab " @selected-tab "\n====\n\n"))
      [:div {:style (if (= tab-id @selected-tab) {} {:display "none"})}
        [:> mui/Card
          [:> mui/CardContent
            [:> mui/Grid {:container    true
                          :direction    "column"
                          :item         true
                          :justify      "space-between"}
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (str "Source : " organisation)]

                [:> mui/IconButton {:id tab-id
                                    :label "more"
                                    :variant "contained"
                                    :disabled false
                                    :onClick #(rf/dispatch [::lve/licence-viewer-open [(not lv-open?) @source]])}
                  [:> muicons/MoreHoriz {:classes {:root "material-icons md-18"}}]]]
              [:div (if lv-open? {:id "licence-open"}{:id "licence-closed"  :style {:display "none"}})
                [licence-viewer-ui]]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/Typography {:variant "subtitle2" :text-decoration "underline" :style {:margin 5}} "Name"]
                [:> mui/Typography {:variant "subtitle2" :text-decoration "underline" :style {:margin 5}} "Symbol"]
                [:> mui/Typography {:variant "subtitle2" :text-decoration "underline" :style {:margin 5}} "Selected"]]

              (if (:symbols @source)
                (into [:div]
                  (doall
                    (for [symbol (:symbols @source)]
                      (let [name (:name symbol)
                            uri  (:uri symbol)
                            svg  (:svg symbol)
                            icon-instance (form-icon-instance symbol organisation title current-layer uri)]
                        [:> mui/Grid {:container    true
                                      :direction    "row"
                                      :justify      "space-between"
                                      :alignItems   "center"
                                      :style {:margin 5}}
                            ^{:key name}[:> mui/Typography {:variant "subtitle2" :style {:margin 5}} name]
                            ^{:key uri}svg
                            (if (some #(= (:uri %) uri) @selected-icons)
                              [:> mui/Tooltip {:title (str "Dont add " name " to layer ")}
                                [:span
                                  [:> mui/IconButton {:id name
                                                      :label "remove"
                                                      :variant "contained"
                                                      :disabled @enabled?
                                                      :onClick #(reset! selected-icons (set (filter (fn [icon] (not= (:uri icon) uri )) (seq @selected-icons))))}

                                    [:> muicons/Done]]]]
                              [:> mui/Tooltip {:title (str "Add " name " to current layer ")}
                                [:span
                                  [:> mui/IconButton {:id name
                                                      :label "add"
                                                      :variant "contained"
                                                      :disabled @enabled?
                                                      :onClick #(if single? (reset! selected-icons #{icon-instance}) (swap! selected-icons conj icon-instance))}
                                    [:> muicons/Clear {:root "material-icons md-18"}]]]])])))))]]]])))




(defn icon-selector
  [selected-tab selected-icons]
  (let [current-layer @(rf/subscribe [::subs/current-layer])
        default-icons (rf/subscribe [::subs/default-icons])]
    (reset! selected-tab (:title (first @default-icons)))
    (fn [selected-tab selected-icons]
      (println (str "Tab names : " (map :title @default-icons)))

      [:div
        [:> mui/Tabs {:variant "scrollable"
                      :selectionFollowsFocus true
                      :indicatorColor "primary"
                      :textColor "primary"
                      :value @selected-tab
                      :onChange (fn [e v] (reset! selected-tab v)
                                          (rf/dispatch [::seev/symbol-editor-tab v]))}

          ;Make a tab for each symbol source
          (for [icon-set (map :title @default-icons)]
            ^{:key icon-set}[:> mui/Tab {:label icon-set
                                         :wrapped true
                                         :disableFocusRipple true
                                         :disableRipple true
                                         :value icon-set}])]

        [:div {:id "node-edit-content" :style {:padding "10px" :height "500px" :width "400px" :overflow "auto"}}
          [:> mui/Grid {:item true}
            [:> mui/Grid {:container true
                          :direction "row"
                          :justify "space-between"
                          :align-items "center"}

              ;Add a card for each symbol source (tab)
              (for [icon-set (map :title @default-icons)]
                ^{:key icon-set}[make-source-card icon-set current-layer selected-icons proc-selected true selected-tab])]]]])))
