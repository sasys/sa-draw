(ns sa-draw.views.editors.common.symbol-events
  (:require [re-frame.core              :as rf]))

(rf/reg-event-db
  ::symbol-editor-tab
  (fn [db [_ tab-id]]
    (assoc-in db [:sa-draw.events/views ::symbol-editor] {::open? true
                                                          ::new? false
                                                          ::type :sa-draw.views.editors.dispatcher/symbol-editor
                                                          ::tab tab-id})))
