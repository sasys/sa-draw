(ns sa-draw.views.editors.quick-flow-events
  (:require [re-frame.core              :as rf]))


(rf/reg-event-db
  ::create
  (fn [db _]
    (assoc-in db [:sa-draw.events/views  ::quick-flow] {::open? true
                                                        ::new "dummy-id" ;we dont pass in the new id for now
                                                        ::edit nil
                                                        ::type :sa-draw.views.editors.dispatcher/quick-flow})))

(rf/reg-event-db
  ::close
  (fn  [db [_]]
    (assoc-in db [:sa-draw.events/views ::quick-flow] {::open? false
                                                       ::new nil
                                                       ::edit nil})))
