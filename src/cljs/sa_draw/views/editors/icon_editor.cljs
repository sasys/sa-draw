(ns sa-draw.views.editors.icon-editor
  "
    The symbol editor allows the user to set a title and a description on the
    symbol.  Also allows the user to remove the symbol.
  "
  (:require [reagent.core                             :as reagent]
            ["@material-ui/core"                      :as mui]
            ["@material-ui/icons"                     :as muicons]
            [sa-draw.events                           :as ev]
            [re-frame.core                            :as rf]
            [sa-draw.subs                             :as subs]
            [sa-draw.views.editors.icon-editor-events :as icev]
            [sa-draw.views.editors.icon-editor-subs   :as icsub]
            [sa-draw.model.icons                      :as ic]
            [sa-draw.model.icons-impl                 :refer [im]]))



(defn icon-editor-ui
  []
  (let [new-title (reagent/atom nil)
        new-description (reagent/atom nil)]
    (fn []
      (let [current-layer @(rf/subscribe [::subs/current-layer])
            icons @(rf/subscribe [::subs/icons])
            icon-id @(rf/subscribe [::icsub/edit-icon-id])
            icon (ic/find-icon-instance im icons icon-id)
            title       (ic/title im icon)
            description (ic/description im icon)]
        [:> mui/Paper
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between";"flex-start"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Edit Icon"]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Abandon/Close"}
                    [:> mui/IconButton {:onClick #(rf/dispatch [::icev/close])}
                      [:> muicons/Close]]]
                  [:strong [:> muicons/DragIndicator]]]]]
            [:> mui/Divider]
            [:div {:style {:padding "10px" :height "400px" :overflow "auto"}}
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}

                [:> mui/TextField {:variant "outlined"
                                   :label "Title"
                                   :defaultValue  (if title title "")
                                   :style {:margin 5}
                                   :onChange (fn [e] (reset! new-title (.. e -target -value)))}]
                [:> mui/Tooltip {:title "The optional title of this icon."} [:> mui/IconButton  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:div {:class "MuiFormControl-root MuiTextField-root" :style {:margin "5px"}}
                  [:> mui/TextareaAutosize {:class "sbnl-autoresize"
                                            :variant "outlined"
                                            :label "Description"
                                            :defaultValue (if description description "")
                                            :rows 3
                                            :onChange (fn [e] (reset! new-description (.. e -target -value)))
                                            ;;THIS STYLE IS TO MAKE THE TEXT AREA LOOK LIKE THE TEXT FIELD!!
                                            ;;NOTE that the style sheet gets overriden and does not get fully applied.
                                            :style {:border-radius "4px"
                                                    :margin 1
                                                    :background-color "transparent"
                                                    :box-sizing "inherit"
                                                    :font-family "Roboto"
                                                    :font-size "16px"
                                                    :width 230
                                                    :padding-top "5px"}}]]
                [:> mui/Tooltip {:title "Optional description of this icon."}
                  [:> mui/IconButton
                    [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]
            [:> mui/Divider]
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Grid {:item true}
                    [:strong [:> muicons/DragIndicator]]]
                  [:> mui/Grid {:item true}
                    [:> mui/Tooltip {:title "Remove this icon"}
                      [:> mui/IconButton {:onClick (fn [](rf/dispatch [::icev/delete icon])
                                                         (rf/dispatch [::icev/close])
                                                         (rf/dispatch [::ev/dirty true]))}
                        [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Save changes to this icon."}
                    [:> mui/IconButton {:onClick (fn [](let [updated-icon (->> icon
                                                                               (ic/update-title im (if @new-title @new-title title))
                                                                               (ic/update-description im (if @new-description @new-description description)))]
                                                          (rf/dispatch [::icev/update-icon current-layer updated-icon icon-id])
                                                          (rf/dispatch [::ev/dirty true])))}

                      [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
                  [:> mui/Grid {:item true}
                        [:strong [:> muicons/DragIndicator]]]]]]]]))))
