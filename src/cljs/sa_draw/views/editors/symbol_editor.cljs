(ns sa-draw.views.editors.symbol-editor
  "
    Allows the user to select an icon to add to the current layer.
    If the prototype icons have not been loaded, the functionality is
    disabled until the user presses the reload button on the panel.
  "
  (:require [reagent.core                                     :as reagent]
            [re-frame.core                                    :as rf]
            ["@material-ui/core"                              :as mui]
            ["@material-ui/icons"                             :as muicons]
            [sa-draw.subs                                     :as subs]
            [sa-draw.views.editors.symbol-editor-events       :as seev]
            [sa-draw.views.editors.symbol-editor-subs         :as sesubs]
            [sa-draw.model.icons                              :as icons]
            [sa-draw.views.editors.common.symbols             :as cs]
            [sa-draw.model.icons-impl                         :refer [im]]))

;The tab selection
(def selected-tab  (reagent/atom 0))

;The selected icons, the uris of each icon we select.
(def selected-icons (reagent/atom #{}))

(defn form-icon-instance
  [prototype-icon source icon-source-title layer uri]
  (assoc (icons/new-icon im prototype-icon source icon-source-title layer) :uri uri))


(defn symbol-editor-ui
  "
    Panel allows stock default icons to be imported into the model, and
    makes them available for use in the main diagrams.
  "
  []
  (fn []
    (let [symbols-loaded @(rf/subscribe [::sesubs/syms-loaded])
          default-icons  @(rf/subscribe [::subs/default-icons])
          current-layer  @(rf/subscribe [::subs/current-layer])]
      (if-not symbols-loaded (rf/dispatch-sync [::seev/load-syms default-icons]))
      [:> mui/Paper
        [:div
          [:> mui/Grid {:container    true
                        :direction    "row"
                        :item         true
                        :justify      "space-between"
                        :alignItems   "center"}
            [:> mui/Grid {:item true}
              [:strong [:> muicons/DragIndicator]]]
            [:> mui/Grid {:item true}
              [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Create Icon")]]
            [:> mui/Grid {:item true}
              [:> mui/Grid {:container      true
                            :direction      "row"
                            :justify        "flex-end"
                            :alignItems     "center"}
                [:> mui/Tooltip {:title "Abandon/Close"}
                  [:> mui/IconButton {:onClick #(rf/dispatch [::seev/close])}
                    [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                [:strong [:> muicons/DragIndicator]]]]]
          [:> mui/Divider]
          [:div {:style {:padding "10px" :height "660px" :overflow "auto"}}
            [:> mui/Tabs {:variant "scrollable"
                          :selectionFollowsFocus true
                          :indicatorColor "primary"
                          :textColor "primary"
                          :value @selected-tab
                          :onChange (fn [e v] (reset! selected-tab v)
                                              (rf/dispatch [::seev/symbol-editor-tab v]))}

              ;Make a tab for each symbol source
              (for [i (range (count default-icons))]
                  ^{:key i}[:> mui/Tab {:label (:title (nth default-icons i))
                                        :wrapped true
                                        :disableFocusRipple true
                                        :disableRipple true
                                        :value i}])]

            [:div {:id "icon-content" :style {:padding "10px" :height "500px" :width "400px" :overflow "auto"}}
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container true
                              :direction "row"
                              :justify "space-between"
                              :align-items "center"}]

                ;Add a card for each symbol source
                (for [tab-id (range (count default-icons))]
                  ^{:key tab-id}[cs/make-source-card tab-id current-layer selected-icons (reagent/atom false) true selected-tab])]]]

          [:> mui/Divider]
          [:> mui/Grid {:container    true
                        :direction    "row"
                        :justify      "space-between"
                        :alignItems   "center"}
            [:> mui/Grid {:item true}
              [:> mui/Grid {:container      true
                            :direction      "row"
                            :justify        "flex-end"
                            :alignItems     "center"}
                [:> mui/Grid {:item true}
                  [:strong [:> muicons/DragIndicator]]]
                [:> mui/Grid {:item true}
                  [:> mui/Tooltip {:title "Refresh icons if not shown."}
                    [:> mui/IconButton {:onClick #(rf/dispatch [::seev/load-syms default-icons])}
                      [:> muicons/Redo {:root "material-icons md-18"}]]]]]]
            [:> mui/Grid {:item true}
              [:> mui/Grid {:container      true
                            :direction      "row"
                            :justify        "flex-end"
                            :alignItems     "center"}
                [:> mui/Tooltip {:title "Save changes to this new icon"}
                  [:> mui/IconButton {:onClick (fn [] (rf/dispatch [::seev/add-icons (vec @selected-icons)])
                                                      (reset! selected-icons #{}))
                                      :disabled (not (seq @selected-icons))}
                    [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
                [:> mui/Grid {:item true}
                  [:strong [:> muicons/DragIndicator]]]]]]]])))
