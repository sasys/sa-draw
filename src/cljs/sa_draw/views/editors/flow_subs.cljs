(ns sa-draw.views.editors.flow-subs
  "
    Subscriptions for flow editor events
  "
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::edit-open
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.flow-events/flow :sa-draw.views.editors.flow-events/open?])))

(rf/reg-sub
  ::edit
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.flow-events/flow :sa-draw.views.editors.flow-events/edit])))

(rf/reg-sub
  ::edit-type
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.flow-events/flow :sa-draw.views.editors.flow-events/type])))
