(ns sa-draw.views.editors.node-subs
  (:require [re-frame.core  :as re-frame]))

(re-frame/reg-sub
  ::node-edit
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.editors.node-events/node-edit])))

(re-frame/reg-sub
  ::node-edit-open :<- [::node-edit]
  (fn [node-edit _]
    (:sa-draw.views.editors.node-events/open? node-edit)))

(re-frame/reg-sub
  ::node-edit-new :<- [::node-edit]
  (fn [node-edit _]
    (:sa-draw.views.editors.node-events/new node-edit)))

(re-frame/reg-sub
  ::node-edit-edit :<- [::node-edit]
  (fn [node-edit _]
    (:sa-draw.views.editors.node-events/edit node-edit)))

(re-frame/reg-sub
  ::node-edit-type :<- [::node-edit]
  (fn [node-edit _]
    (:sa-draw.views.editors.node-events/type node-edit)))
