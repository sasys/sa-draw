(ns sa-draw.views.editors.node
  (:require [reagent.core               :as reagent]
            ["@material-ui/core"        :as mui]
            ["@material-ui/icons"       :as muicons]
            [re-frame.core              :as rf]
            [sa-draw.views.editors.node-events :as ne]
            [sa-draw.subs               :as subs]
            [sa-draw.events             :as ev]
            [sa-draw.views.pspec.events :as pev]
            [sa-draw.model.proto-model  :as proto-model]
            [sa-draw.views.markdown.markdown-viewer-events :as mdev]
            [sa-draw.config.subs                           :as cfgsubs]
            [sa-draw.views.editors.common.symbols          :as cs]
            [sa-draw.views.editors.symbol-editor-subs      :as sesubs]
            [sa-draw.views.editors.symbol-editor-events    :as sesevs]
            [sa-draw.model.map-impl     :refer [map-model proc-model context-model]]))


(def data-type  (->  (make-hierarchy)
                     (derive ::proc   ::data-type)
                     (derive ::prod   ::data-type)
                     (derive ::cons   ::data-type)
                     (derive ::source ::data-type)
                     (derive ::sink   ::data-type)))


(def edit-type  (-> (make-hierarchy)
                    (derive ::new     ::edit-type)
                    (derive ::edit    ::edit-type)))

(defn new-proc-id []
  (let  [current-layer @(rf/subscribe [::subs/current-layer])
         procs-model (proto-model/find-process-model map-model @(rf/subscribe [::subs/model]))
         current-node (proto-model/find-node proc-model procs-model current-layer)
         child-procs (proto-model/children proc-model current-node)]
    (proto-model/next-child proc-model current-layer child-procs)))


(def selected-icon (reagent/atom nil))

;The selected icons, the uris of each icon we select.
(def selected-icons (reagent/atom #{}))

(def proc-selected (reagent/atom false))

(defn edit-node
  "
    An editor for a process node.
  "
  [id]
  (let [model  @(rf/subscribe [::subs/model])
        current-layer @(rf/subscribe [::subs/current-layer])
        update-markdown @(rf/subscribe [::cfgsubs/update-markdown-on-save])
        symbols-loaded (rf/subscribe [::sesubs/syms-loaded])
        procs-model (proto-model/find-process-model map-model model)
        current-node (proto-model/find-node proc-model procs-model current-layer)
        edit-node (proto-model/find-node proc-model procs-model id)
        [children _ title description _ _ _] (proto-model/details proc-model edit-node)
        delegate-link (proto-model/delegate proc-model edit-node)
        new-title (reagent/atom title)
        new-description (reagent/atom description)
        new-link (reagent/atom delegate-link)
        default-icons @(rf/subscribe [::subs/default-icons])
        selected-tab  (reagent/atom (:title (first default-icons)))]
    (fn [id]
        (if-not @symbols-loaded (rf/dispatch-sync [::sesevs/load-syms default-icons]))
        [:> mui/Paper
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Edit Node : " id)]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Abandon/Close"}
                    [:> mui/IconButton {:onClick #(rf/dispatch [::ne/node-edit-close])}
                      [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                  [:strong [:> muicons/DragIndicator]]]]]
            [:> mui/Divider]
            [:div {:style {:padding "10px" :height "660px" :overflow "auto"}}
              [:> mui/Grid {:item true}
                [:> mui/TextField {:variant "outlined"
                                   :fullWidth true
                                   :label "Title"
                                   :defaultValue  title
                                   :style {:margin 5}
                                   :onChange (fn [e] (reset! new-title (.. e -target -value)))}]]


              [:> mui/Divider]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (str "Edit As A Process : " id)]
                (if @proc-selected
                  [:> mui/Tooltip {:title "As an icon"}
                    [:span
                      [:> mui/IconButton {:label "remove"
                                          :variant "contained"
                                          :onClick #(reset! proc-selected false)}
                        [:> muicons/Done]]]]
                  [:> mui/Tooltip {:title "As a process"}
                    [:span
                      [:> mui/IconButton {:label "add"
                                          :variant "contained"
                                          :onClick #(reset! proc-selected true)}
                        [:> muicons/Clear]]]])]

              [cs/icon-selector selected-tab selected-icons]

              [:> mui/Grid {:item true}
                [:> mui/TextField {:variant "outlined"
                                   :fullWidth true
                                   :label "Description"
                                   :defaultValue description
                                   :multiline true
                                   :rows 3
                                   :style {:resize "both" :margin 5}
                                   :onChange (fn [e] (reset! new-description (.. e -target -value)))}]]


              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/TextField {:variant "outlined"
                                   :style {:resize "both" :margin 5}
                                   :label "Sub model / delegate model"
                                   :disabled (if (seq children) true false)
                                   :defaultValue  delegate-link
                                   :onChange (fn [e] (reset! new-link (.. e -target -value)))}]
                [:> mui/Tooltip {:title "If the node has no children, a model id here will allow navigation."}
                  [:> mui/IconButton
                    [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]
            [:> mui/Divider]
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Grid {:item true}
                    [:strong [:> muicons/DragIndicator]]]
                  [:> mui/Grid {:item true}
                    [:> mui/Tooltip {:title "Remove this process and all its children"}
                      [:> mui/IconButton {:onClick (fn []
                                                      (rf/dispatch [::ne/node-edit-delete id])
                                                      (rf/dispatch [::ev/new-current-layer-v (proto-model/details proc-model current-node)])
                                                      (rf/dispatch [::ne/node-edit-close])
                                                      (if update-markdown (rf/dispatch [::mdev/markdown-from-json])))}
                        [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Save changes to this process spec"}
                    [:span
                        [:> mui/IconButton {:onClick (fn []
                                                        (rf/dispatch [::pev/update-node-with-icon id @new-title @new-description [] [] @new-link (if-not @proc-selected  (some identity @selected-icons) nil)]) ;@icon])
                                                        (if (= "context" current-layer)
                                                          (rf/dispatch [::ev/new-current-layer-v (proto-model/context-details context-model current-node)])
                                                          (rf/dispatch [::ev/new-current-layer-v (proto-model/details proc-model current-node)]))
                                                        (rf/dispatch [::ev/dirty true])
                                                        (if update-markdown (rf/dispatch [::mdev/markdown-from-json])))}
                          [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]]
                  [:> mui/Grid {:item true}
                    [:strong [:> muicons/DragIndicator]]]]]]]])))

(defn new-node
  "
    An editor for a new process node.
  "
  []
  (let [model @(rf/subscribe [::subs/model])
        current-layer @(rf/subscribe [::subs/current-layer])
        update-markdown @(rf/subscribe [::cfgsubs/update-markdown-on-save])
        symbols-loaded (rf/subscribe [::sesubs/syms-loaded])
        procs-model (proto-model/find-process-model map-model model)
        current-node (proto-model/find-node proc-model procs-model current-layer)
        new-title (reagent/atom "")
        new-description (reagent/atom "")
        new-link (reagent/atom "")
        default-icons (rf/subscribe [::subs/default-icons])
        selected-tab  (reagent/atom (:title (first @default-icons)))]

    (fn []
      (let [id (new-proc-id)]
        (if-not @symbols-loaded (rf/dispatch-sync [::sesevs/load-syms @default-icons]))
        [:> mui/Paper
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Create Node : " id)]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Tooltip {:title "Abandon/Close"}
                        [:> mui/IconButton {:onClick #(rf/dispatch [::ne/node-edit-close])}
                          [:> muicons/Close {:classes {:root "material-icons md-18"}}]]]
                  [:strong [:> muicons/DragIndicator]]]]]
            [:> mui/Divider]
            [:div {:style {:padding "10px" :height "660px" :overflow "auto"}}
              [:> mui/Grid {:item true}
                  [:> mui/TextField {:variant "outlined"
                                     :fullWidth true
                                     :label "Title"
                                     :defaultValue  ""
                                     :style {:margin 5}
                                     :onChange (fn [e] (reset! new-title (.. e -target -value)))}]]

              [:> mui/Divider]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (if @proc-selected (str "Create As A Process : " id) (str "Create As An Icon : " id))]
                (if @proc-selected
                  [:> mui/Tooltip {:title "Dont create as a process"}
                    [:span
                      [:> mui/IconButton {:label "remove"
                                          :variant "contained"
                                          :onClick #(reset! proc-selected false)}

                        [:> muicons/Done]]]]
                  [:> mui/Tooltip {:title "Create as a process"}
                    [:span
                      [:> mui/IconButton {:label "add"
                                          :variant "contained"
                                          :onClick #(reset! proc-selected true)}
                        [:> muicons/Clear]]]])]
              [cs/icon-selector selected-tab selected-icons]

              [:> mui/Grid {:item true}
                [:> mui/TextField {:variant "outlined"
                                   :fullWidth true
                                   :label "Description"
                                   :defaultValue ""
                                   :multiline true
                                   :rows 3
                                   :style {:resize "both" :margin 5}
                                   :onChange (fn [e] (reset! new-description (.. e -target -value)))}]]

              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/TextField {:variant "outlined"
                                   :style {:resize "both" :margin 5}
                                   :label "Sub model / delegate model"
                                   :defaultValue  ""
                                   :onChange (fn [e] (reset! new-link (.. e -target -value)))}

                  [:> mui/Tooltip {:title "If the node has no children, a model id here will allow navigation."}
                    [:> mui/IconButton
                      [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]
              [:> mui/Divider]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/Grid {:item true}
                  [:strong [:> muicons/DragIndicator]]]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "flex-end"
                                :alignItems     "center"}
                    [:> mui/Tooltip {:title "Create this process spec"}
                      [:span
                        [:> mui/IconButton {:onClick (fn []
                                                        (let [new-node (proto-model/make-iconic-node proc-model id  @new-title [] [] @new-description (if (some identity @selected-icons) (first @selected-icons) nil))]
                                                          (rf/dispatch [::ev/add-new-node current-layer new-node id])
                                                          (rf/dispatch [::ev/new-current-layer-v (proto-model/details proc-model current-node)])
                                                          (rf/dispatch [::ev/dirty true])
                                                          (if update-markdown(rf/dispatch [::mdev/markdown-from-json]))))
                                            :disabled (not (or (some identity @selected-icons)   @proc-selected))}
                            [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]]
                    [:> mui/Grid {:item true}
                      [:strong [:> muicons/DragIndicator]]]]]]]]]))))
