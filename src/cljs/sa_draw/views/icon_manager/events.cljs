(ns sa-draw.views.icon-manager.events
  (:require [re-frame.core  :as re-frame]))

(re-frame/reg-event-db
  ::update-prototype-icons
  (fn  [db [_ prototype-icons]]
    (let [model (:sa-draw.events/model db)]
      (assoc db :sa-draw.events/model  (assoc model :sadf/default-cloud-icons prototype-icons)))))

(re-frame/reg-event-db
  ::add-icon-to-set
  (fn  [db [_ set-name icon-name icon-url icon-svg]]
    ;
    ;   Find the icon set with the given name
    ;   Add the icon to the set
    ;   Add the updated set to the model
    ;
    (let [model (:sa-draw.events/model db)
          icon {:name icon-name
                :uri icon-url
                :svg icon-svg}
          default-icons (:sadf/default-cloud-icons model)
          icon-set (first (filter #(= (:title %) set-name) default-icons))
          symbols    (:symbols icon-set)
          updated-symbols (cons icon symbols)
          updated-icon-set (assoc icon-set :symbols updated-symbols)
          updated-default-icons (reduce
                                  (fn [coll {:keys [title] :as set}]
                                    (conj coll (if (= title set-name)
                                                 updated-icon-set
                                                 set)))
                                  []
                                  default-icons)]
      (println (str "\n\n Icon to add: " icon "\n\n"))
      (assoc db :sa-draw.events/model  (assoc model :sadf/default-cloud-icons updated-default-icons)))))
