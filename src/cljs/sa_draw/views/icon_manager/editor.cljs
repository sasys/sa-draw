(ns sa-draw.views.icon-manager.editor
  "
    An editor that supports the import, modification (scale and transform) and
    persistence and removal of icons for a model.
  "
  (:require [reagent.core                                   :as r]
            [re-frame.core                                  :as rf]
            ["@material-ui/core"                            :as mui]
            ["@material-ui/icons"                           :as muicons]
            [sa-draw.subs                                   :as subs]
            [sa-draw.views.icon-manager.panels.about        :refer [about]]
            [sa-draw.views.icon-manager.panels.add-icon-set :refer [add-icon-set]]
            [sa-draw.views.icon-manager.panels.import-icon  :refer [import-icon]]
            [sa-draw.views.icon-manager.panels.remove-icon-set :refer [remove-icon-set]]))




(defn icon-manager-ui
  []
  (fn []
    (let [model    @(rf/subscribe [::subs/model])
          icons    (:sadf/default-cloud-icons model)
          expand-icon (r/create-element
                         "my-more"
                         #js{:className "my-more"}
                         (r/as-element [:> muicons/ExpandMore]))]
      (println (str icons))
      [:> js/ReactDraggable {:handle "strong"}
        [:> mui/Paper {:elevation 5}
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:> mui/Grid {:item true}
                [:strong [:> muicons/DragIndicator]]]
              [:> mui/Grid {:item true}
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Model Icon Manager")]]
              [:> mui/Grid {:item true}
                [:> mui/Grid {:container      true
                              :direction      "row"
                              :justify        "flex-end"
                              :alignItems     "center"}
                  [:> mui/Grid {:item true}
                    [:strong [:> muicons/DragIndicator]]]]]]
            [:> mui/Divider {:style {:width "100%"}}]
            [:> mui/Grid {:container      true
                          :direction      "column"
                          :justify        "space-between"
                          :alignItems     "center"}
              [:div {:style {:padding "10px" :height "660px" :width "500px" :overflow "auto"}}
                [:> mui/Accordion
                  [:> mui/AccordionSummary {:id "a1s" :expandIcon expand-icon}
                    [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "About ....")]]
                  [:> mui/AccordionDetails
                    [about]]]
                [:> mui/Accordion
                  [:> mui/AccordionSummary {:id "a2s" :expandIcon expand-icon}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Add Icon Set")]
                      [:> mui/Typography {:variant "body2" :style {:margin 5}} (str "This process process allows a new, empty, icon set to be created.")]]]
                  [:> mui/AccordionDetails
                    [add-icon-set]]]
                ; [:> mui/Accordion
                ;   [:> mui/AccordionSummary {:id "a3s" :expandIcon expand-icon}
                ;     [:> mui/Grid {:item true}
                ;       [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Remove Selected Icons From Set")]]]
                ;   [:> mui/AccordionDetails
                ;     [:> mui/Grid {:container      true
                ;                   :direction      "column"
                ;                   :justify        "space-between"
                ;                   :alignItems     "center"}]]]
                [:> mui/Accordion
                  [:> mui/AccordionSummary {:id "a4s" :expandIcon expand-icon}
                    [:> mui/Grid {:container      true
                                  :direction      "column"
                                  :alignItems     "center"}
                      [:> mui/Grid {:item true}
                        [:> mui/Grid {:container      true
                                      :direction      "row"
                                      :justify        "flex-start"
                                      :alignItems     "center"}
                          [:> mui/IconButton {:disabled true :style {:color "red"}}[:> muicons/Warning]]
                          [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Remove Icon Set")]]
                        [:> mui/Typography {:variant "body2" :style {:margin 5}} (str "This process will remove ALL THE ICONS from a set and the icon set itself.")]]]]
                  [:> mui/AccordionDetails
                    [remove-icon-set]]]
                [:> mui/Accordion
                  [:> mui/AccordionSummary {:id "a5s" :expandIcon expand-icon}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Import Icon To Set")]
                      [:> mui/Typography {:variant "body2" :style {:margin 5}} (str "This process allows you to specify an svg icon to load into an icon set.")]]]
                  [:> mui/AccordionDetails
                    [import-icon]]]]
                ; [:> mui/Accordion
                ;   [:> mui/AccordionSummary {:id "a6s" :expandIcon expand-icon}
                ;     [:> mui/Grid {:item true}
                ;       [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Import Icon Set")]]]
                ;   [:> mui/AccordionDetails
                ;     [:> mui/Grid {:container      true
                ;                   :direction      "column"
                ;                   :justify        "space-between"
                ;                   :alignItems     "center"}]]]]
                ; [:> mui/Accordion
                ;   [:> mui/AccordionSummary {:id "a7s" :expandIcon expand-icon}
                ;     [:> mui/Grid {:item true}
                ;       [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Drag Icon Into Set")]]]
                ;   [:> mui/AccordionDetails
                ;     [:> mui/Grid {:container      true
                ;                   :direction      "column"
                ;                   :justify        "space-between"
                ;                   :alignItems     "center"}]]]
                ; [:> mui/Accordion
                ;   [:> mui/AccordionSummary {:id "a8s" :expandIcon expand-icon}
                ;     [:> mui/Grid {:item true}
                ;       [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Import Icon Set From Git")]]]
                ;   [:> mui/AccordionDetails
                ;     [:> mui/Grid {:container      true
                ;                   :direction      "column"
                ;                   :justify        "space-between"
                ;                   :alignItems     "center"}]]]
                ; [:> mui/Accordion
                ;   [:> mui/AccordionSummary {:id "a9s" :expandIcon expand-icon}
                ;     [:> mui/Grid {:item true}
                ;       [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Upload Icon To Set (Restore)")]
                ;       [:> mui/Typography {:variant "body2" :style {:margin 5}} (str "This process will replace the icons in a set with the restored icons")]]]
                ;   [:> mui/AccordionDetails
                ;     [:> mui/Grid {:container      true
                ;                   :direction      "column"
                ;                   :justify        "space-between"
                ;                   :alignItems     "center"}]]]
                ; [:> mui/Accordion
                ;   [:> mui/AccordionSummary {:id "a10s" :expandIcon expand-icon}
                ;     [:> mui/Grid {:item true}
                ;       [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} (str "Download Icon Set (Backup)")]
                ;       [:> mui/Typography {:variant "body2" :style {:margin 5}} (str "This process will create backup file of icons for a given set")]]]
                ;   [:> mui/AccordionDetails
                ;     [:> mui/Grid {:container      true
                ;                   :direction      "column"
                ;                   :justify        "space-between"
                ;                   :alignItems     "center"}]]]]
              [:> mui/Divider {:style {:width "100%"}}]
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :justify      "space-between"
                            :alignItems   "center"}
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "flex-end"
                                :alignItems     "center"}
                    [:> mui/Grid {:item true}
                      [:strong [:> muicons/DragIndicator]]]
                    [:> mui/Grid {:item true}
                      [:> mui/Tooltip {:title "Refresh icons if not shown."}
                        [:> mui/IconButton ;{:onClick #(rf/dispatch [::seev/load-syms default-icons])}
                          [:> muicons/Redo {:root "material-icons md-18"}]]]]]]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "flex-end"
                                :alignItems     "center"}
                    [:> mui/Tooltip {:title "Save changes to this new icon"}
                      [:> mui/IconButton ;{:onClick (fn [] (rf/dispatch [::seev/add-icons (vec @selected-icons)])
                                         ;                  (reset! selected-icons #{})
                                         ;  :disabled (not (seq @selected-icons))
                        [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]
                    [:> mui/Grid {:item true}
                      [:strong [:> muicons/DragIndicator]]]]]]]]]])))
