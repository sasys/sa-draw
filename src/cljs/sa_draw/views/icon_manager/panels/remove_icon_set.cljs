(ns sa-draw.views.icon-manager.panels.remove-icon-set
  "
    Text describing the remove icon set operation.
  "
  (:require [reagent.core                           :as r]
            [re-frame.core                          :as rf]
            ["@material-ui/core"                    :as mui]
            ["@material-ui/icons"                   :as muicons]
            [sa-draw.subs                           :as subs]
            [sa-draw.views.editors.symbol-editor-subs            :as sesubs]
            [sa-draw.views.editors.symbol-editor-events          :as sesevs]
            [sa-draw.views.icon-manager.events      :as imev]
            [sa-draw.views.editors.common.symbols   :refer [make-source-card]]))

(defn make-list-item
  "
    selected is an r/atom of a the name of the icon set that has been selected.
  "
  [icon-set-title icon-svg selected]
  (let [check (r/atom false)]
    (fn [icon-set-title icon-svg selected]
      [:> mui/ListItem
        [:> mui/ListItemIcon
          [:> mui/IconButton [:> icon-svg]]]
        [:> mui/ListItemText icon-set-title]
        [:> mui/Checkbox {:checked (= @selected icon-set-title) :onChange (fn [e] (reset! check (not @check))
                                                                                  (if (not= @selected icon-set-title)
                                                                                      (reset! selected icon-set-title)
                                                                                      (reset! selected nil)))}]])))

(defn remove-icon-set
  []
  (let [;model    (rf/subscribe [::subs/model])
        icons    (rf/subscribe [::subs/default-icons])
        symbols-loaded (rf/subscribe [::sesubs/syms-loaded])
        active-step (r/atom 0)
        ;icon-set-titles (map :title @icons)
        selected-set (r/atom nil)
        default-icons @(rf/subscribe [::subs/default-icons])]

    (fn []
      (println (str "\n\n ICONS: " (map :title @icons)))
      (let [icon-set-titles (map :title @icons)]
        (if-not @symbols-loaded (rf/dispatch-sync [::sesevs/load-syms default-icons]))
        [:> mui/Grid {:container      true
                      :direction      "column"
                      :justify        "space-between"
                      :alignItems     "center"}
          [:> mui/Typography {:variant "body2" :paragraph true :style {:margin 5}}
            (str "This panel allows a complete set of icons, including the icon tab to be
                      removed from a model.  Any icon instances in the model will be drawn as
                      process after the set has been removed.")]
          [:> mui/Stepper {:orientation "vertical" :activeStep @active-step}
            [:> mui/Step {:key "file" :index 0}
              [:> mui/StepLabel
                [:> mui/Typography "Select Icon Set"]]
              [:> mui/StepContent
                [:> mui/List
                  (into [:<>] (for [title icon-set-titles]
                                [make-list-item title muicons/Warning selected-set]))]
                [:> mui/Grid {:item true}
                  [:> mui/Button {:variant "contained"
                                  :onClick #(swap! active-step inc)} "Next"]]]]
            [:> mui/Step {:key "confirm" :index 1}
              [:> mui/StepLabel
                [:> mui/Typography "Confirm action to remove the following icon set and icons:"]]
              [:> mui/StepContent
                [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}} @selected-set]
                [make-source-card @selected-set "1" (atom (list)) (atom true) false selected-set]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "space-between"
                                :alignItems     "center"}
                    [:> mui/Button {:variant "contained"
                                    :onClick #(swap! active-step inc)} "Confirm"]
                    [:> mui/Button {:variant "contained"
                                    :onClick #(swap! active-step dec)} "Back"]]]]]
            [:> mui/Step {:key "complete" :index 2}
              [:> mui/StepLabel
                [:> mui/Typography "Complete Icon set deletion"]]
              [:> mui/StepContent
                [:> mui/Typography  "Conform you wish to delete all the above icons from this model."]
                [:> mui/Grid {:item true}
                  [:> mui/Grid {:container      true
                                :direction      "row"
                                :justify        "space-around"
                                :alignItems     "center"}
                    [:> mui/Button {:variant "contained"
                                    :onClick (fn [icon] (let [new-icons (filter #(not= (:title %)  @selected-set) @icons)]
                                                          (rf/dispatch [::imev/update-prototype-icons new-icons])
                                                          (swap! active-step inc)))} "Complete"]
                    [:> mui/Button {:variant "contained"
                                    :onClick #(reset! active-step 0)} "Abandon"]
                    [:> mui/Button {:variant "contained"
                                    :onClick #(swap! active-step dec)} "Back"]]]]]]
          [:> mui/Button {:variant "contained" :onClick #(reset! active-step 0)}  "Reset Flow"]]))))
