(ns sa-draw.views.icon-manager.panels.add-icon-set
  "
    A panel to allow an icon set to be added.
  "
  (:require [reagent.core                           :as r]
            [re-frame.core                          :as rf]
            [material-ui                            :as mui]
            ["@material-ui/icons"                   :as muicons]
            [sa-draw.subs                           :as subs]
            [sa-draw.model.proto-model              :as proto-model]
            [sa-draw.views.icon-manager.events      :as imev]
            [sa-draw.model.map-impl                 :refer [map-model proc-model]]))


(def icon-map-key :sadf/default-cloud-icons)
(def new-icon-map
  {:title "New Icon Set"
   :licence {:name "Icon Set Licence Name"
             :uri  ""
             :commit ""}
   :source "New Icon Source"
   :symbols []})

(def tab-helper-text-already-exists "Tab title already exists")
(def tab-helper-text "The tab title must be new/unique")


(defn add-icon-set
  []
  (let [tab-error (r/atom false)
        tab-name  (r/atom "")
        licence-name   (r/atom "")
        licence-uri    (r/atom "")
        licence-commit (r/atom "")]
    (fn []
      (let [
            colour (r/atom "primary")
            model    @(rf/subscribe [::subs/model])
            icons    (proto-model/find-prototype-model map-model model)
            icon-set-titles (map :title icons)]
        [:> mui/Grid {:container      true
                      :direction      "column"
                      :justify        "space-between"
                      :alignItems     "center"}
          [:> mui/Grid {:container      true
                        :direction      "row"
                        :justify        "space-between"
                        :alignItems     "center"}
            [:> mui/TextField {:variant "outlined"
                               :label "Tab Name"
                               :error @tab-error
                               :color @colour
                               :helper-text (if @tab-error tab-helper-text-already-exists tab-helper-text)
                               :defaultValue  @tab-name
                               :onChange (fn [e] (if (some #{(.. e -target -value)} icon-set-titles)
                                                     (do (println (str "PRESENT " (.. e -target -value) " IS IN " icon-set-titles))
                                                         (reset! tab-error true)
                                                         (reset! colour "error"))
                                                     (do (println (str "NOT PRESENT " (.. e -target -value) " IS NOT IN " icon-set-titles))
                                                         (if (not= @colour "primary") (reset! colour "primary"))
                                                         (reset! tab-error false)
                                                         (reset! tab-name (.. e -target -value)))))
                               :style {:margin 5}}]
            [:> mui/Tooltip {:title "New icon set tab title"}
              [:> mui/IconButton
                [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]
          [:> mui/Grid {:container      true
                        :direction      "row"
                        :justify        "space-between"
                        :alignItems     "center"}
            [:> mui/TextField {:variant "outlined"
                               :label "Licence Name"
                               :defaultValue  @licence-name
                               :onChange #(reset! licence-name (.. % -target -value))
                               :style {:margin 5}}]
            [:> mui/Tooltip {:title "New icon set licence name"}
              [:> mui/IconButton
                [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]

          [:> mui/Grid {;:item           true
                        :container      true
                        :direction      "row"
                        :justify        "space-between"
                        :alignItems     "center"}
            [:> mui/TextField {:variant "outlined"
                               :label "Licence URI"
                               :defaultValue @licence-uri
                               :onChange #(reset! licence-uri (.. % -target -value))
                               :style {:margin 5}}]
            [:> mui/Tooltip {:title "New icon set licence uri"}
              [:> mui/IconButton
                [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]

          [:> mui/Grid {;:item           true
                        :container      true
                        :direction      "row"
                        :justify        "space-between"
                        :alignItems     "center"}
            [:> mui/TextField {:variant "outlined"
                               :label "Licence Commit"
                               :defaultValue  @licence-commit
                               :onChange #(reset! licence-commit (.. % -target -value))
                               :style {:margin 5}}]
            [:> mui/Tooltip {:title "New icon set licence commit"}
              [:> mui/IconButton
                [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]
          [:> mui/Grid {;:item           true
                        :container      true
                        :direction      "row"
                        :justify        "space-between"
                        :alignItems     "center"}
              [:> mui/IconButton {:disabled @tab-error
                                  :onClick (fn [] (let [new-icon-set (-> new-icon-map
                                                                         (assoc :title @tab-name)
                                                                         (assoc-in [:licence :name]   @licence-name)
                                                                         (assoc-in [:licence :uri]    @licence-uri)
                                                                         (assoc-in [:licence :commit] @licence-commit))
                                                        new-icons (conj icons new-icon-set)]
                                                    (rf/dispatch [::imev/update-prototype-icons new-icons])))}


                [:> muicons/Add {:classes {:root "material-icons md-18"}}]]
              [:> mui/Tooltip {:title "Add new icon set"}
                [:> mui/IconButton
                  [:> muicons/Help {:classes {:root "material-icons md-18"}}]]]]]))))
