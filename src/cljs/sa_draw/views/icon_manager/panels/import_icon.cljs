(ns sa-draw.views.icon-manager.panels.import-icon
  "
  "
  (:require [reagent.core                            :as r]
            [re-frame.core                           :as rf]
            [material-ui                             :as mui]
            ["@material-ui/icons"                    :as muicons]
            [sa-draw.subs                            :as subs]
            [sa-draw.views.dfd.svg.draw              :as drw]
            [sa-svg.io-impl.io                       :refer [IO]]
            [sa-svg.io                               :as io]
            [sa-svg.logic                            :as logic]
            [sa-svg.logic-impl.logic                 :refer [Logic]]
            [sa-draw.views.icon-manager.panels.utils :refer [check-file-type]]
            [sa-draw.views.icon-manager.events       :as imev]
            [clojure.core.async                      :refer [take! go put! chan <! alts! timeout]]
            [hickory.convert                         :as hicon]
            [hickory.core                            :as hc]
            [hickory.zip                             :as hzip]
            [clojure.zip                             :as zip]))


(def no-source "none")
(def file-source "file")
(def web-source  "web")

(def svg-file-type "svg")

;These because color "error" or "success" is not working!!
(def error-colour "red")
(def success-colour "green")

(def file-not-found-text "The specified file could not be found")
(def file-helper-text "The tab title must be new/unique")

;The id of the input element we use to get the icon file
(def input-element-id "IN-ID")


(defn make-list-item
  "
    selected is an r/atom of a the name of the icon set that has been selected.
  "
  [icon-set-title icon-svg selected]
  (let [check (r/atom false)]
    (fn [icon-set-title icon-svg selected]
      [:> mui/ListItem
        [:> mui/ListItemIcon
          [:> mui/IconButton [:> icon-svg]]]
        [:> mui/ListItemText icon-set-title]
        [:> mui/Checkbox {:checked (= @selected icon-set-title) :onChange (fn [e] (reset! check (not @check))
                                                                                  (if (not= @selected icon-set-title)
                                                                                      (reset! selected icon-set-title)
                                                                                      (reset! selected nil)))}]])))


(defn import-icon
  []
  (let [file-error  (r/atom false)
        file-name   (r/atom nil)
        active-step (r/atom 0)
        source      (r/atom no-source)
        icon-file   (r/atom nil)
        icon-url    (r/atom nil)
        icon-name   (r/atom nil)
        icon        (r/atom nil)
        decorated-icon (r/atom nil)
        reader-on-load (fn [e] (reset! icon (-> e .-target .-result))
                               (println (str "ICON : " @icon)))
        reader        (new js/FileReader)
        icons       (rf/subscribe [::subs/default-icons])
        icon-set-titles (map :title @icons)
        selected-set (r/atom nil)]

    (fn []
      (let [import-action (fn []
                              (println (str "Import action url " @icon-url "\n\n"))
                              (go
                                (let [import-chan (chan)]
                                  (logic/import-icon-url Logic import-chan @icon-url)
                                  (let [res (<! import-chan)]
                                    (do
                                      ; (reset! icon (as-> (hicon/hickory-to-hiccup res) a
                                      ;                    (hzip/hickory-zip a)
                                      ;                    (logic/filter-kvs Logic a)
                                      ;                  ;(drw/filter-kvs drw/edit-remove-attrs drw/default-unwanted-keys)
                                      ;                    (zip/node a)))
                                      (reset! icon (logic/filter-kvs Logic res))
                                      (println (str "\nNEW ICON \n" @icon "\n")))))))]

        [:> mui/Grid {:container      true
                      :direction      "column"
                      :justify        "space-between"
                      :alignItems     "center"}
          [:> mui/Grid {:container      true
                        :direction      "row"
                        :alignItems     "center"}
            [:> mui/Button {:variant "contained" :onClick #(do (reset! source file-source)
                                                               (reset! active-step 0))}  "File"]
            [:> mui/Button {:variant "contained" :onClick #(do (reset! source web-source)
                                                               (reset! active-step 0))} "Web"]]
          [:div (if (not= @source file-source) {:style {:display "none"}})
            [:> mui/Stepper {:orientation "vertical" :activeStep @active-step}
              [:> mui/Step {:key "file" :index 0}
                [:> mui/StepLabel
                  [:> mui/Typography "Select SVG File"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step will load an svg icon from your local file system."]]
                    [:> mui/Grid {:item true}
                      [:> mui/Button {:variant "contained" :component "label" :style {:color (if @file-error error-colour success-colour)}} "Select SVG File"
                        [:> mui/Input {:type "file"
                                       :id input-element-id
                                       :accept ".svg"
                                       :style {:display "none"}
                                       :onChange (fn [e]
                                                    (reset! file-name  (.. e -target -value))
                                                    (swap! file-error #(not (check-file-type @file-name svg-file-type)))
                                                    (if (not @file-error) (do (reset! icon-file (aget (.. e -target -files) 0))
                                                                              (set! (.-onload reader) reader-on-load)
                                                                              (.readAsText reader @icon-file))))}]]]

                    [:> mui/Grid {:item true}
                      [:div {:style {:padding "5px" :background-color "lightgrey" :white-space "pre-wrap" :display (if (or @file-error (nil? @file-name)) "none" "block") :margin-top "5px" :margin-bottom "5px"}  :suppressContentEditableWarning true :contentEditable false :id "icon-file"}   @file-name]]
                    [:> mui/Grid {:item true}
                      [:> mui/Button {:variant "contained"
                                      :disabled  (or @file-error (nil? @file-name))
                                      :onClick #(swap! active-step inc)} "Next"]]]]]


;document.getElementById('input').files[0];
              [:> mui/Step {:key "decorate" :index 1}
                [:> mui/StepLabel
                  [:> mui/Typography "Decorate"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step will add the required svg decorations to the icon."]]
                    [:> mui/Button {:variant "contained" :component "label" :style {:color (if @file-error error-colour success-colour)}
                                    :onClick #(do (reset! decorated-icon (drw/full-stack-svg-import @icon 3 [2.5 2.5] drw/default-unwanted-keys drw/default-view-box))
                                                  (println (str "\n\n UPDATED ICON " @decorated-icon)))} "Add SVG Decortaions"]
                    [:> mui/Grid {:item true}
                      [:> mui/Grid {:container      true
                                    :direction      "row"
                                    :justify        "space-between"
                                    :alignItems     "center"}
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Next"]]]]]]

              [:> mui/Step {:key "preview" :index 2}
                [:> mui/StepLabel
                  [:> mui/Typography "Previews"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step gives a preview of the icon after scaling for each application."]]
                    [:> mui/Grid {:item true}
                      [:> mui/Grid {:container      true
                                    :direction      "row"
                                    :justify        "space-between"
                                    :alignItems     "center"}
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Next"]]]]]]

              [:> mui/Step {:key "select-icon-set" :index 3}
                [:> mui/StepLabel
                  [:> mui/Typography "Select Icon Set"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step selects which icon set to add the icon."]]
                    [:> mui/Grid {:item true}
                      [:> mui/Grid {:container      true
                                    :direction      "row"
                                    :justify        "space-between"
                                    :alignItems     "center"}
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Next"]]]]]]

              [:> mui/Step {:key "finish" :index 4}
                [:> mui/StepLabel
                  [:> mui/Typography "Finish"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "Complete icon import process"]]
                    [:> mui/Grid {:item true}
                      [:> mui/Grid {:container      true
                                    :direction      "row"
                                    :justify        "space-between"
                                    :alignItems     "center"}
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                        [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Complete"]]]]]]]]


          [:div (if (not= @source web-source) {:style {:display "none"}})
            [:> mui/Stepper {:orientation "vertical" :activeStep @active-step}

              [:> mui/Step {:key "web" :index 1}
                [:> mui/StepLabel
                  [:> mui/Typography "Select URL"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step will load an svg icon a url."]]
                    [:> mui/Grid {:item true}
                      [:> mui/TextField {:id "svg-url"
                                         :label "https://"
                                         :variant "filled"
                                         :size "small"
                                         :defaultValue  (if @icon-url @icon-url "")
                                         :onChange #(reset! icon-url (.. % -target -value))}]]
                    [:> mui/Grid {:item true}
                      [:> mui/Button {:variant "contained" :onClick import-action} "Import From URL"]]
                    [:> mui/Grid {:container      true
                                  :direction      "row"
                                  :justify        "space-between"
                                  :alignItems     "center"}
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Next"]]]]]

              [:> mui/Step {:key "select-icon-set" :index 2}
                [:> mui/StepLabel
                  [:> mui/Typography "Select Icon Set"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step selects which icon set to add the icon."]]
                    [:> mui/List
                      (into [:<>] (for [title icon-set-titles]
                                    [make-list-item title muicons/Warning selected-set]))]

                    [:> mui/Grid {:container      true
                                  :direction      "row"
                                  :justify        "space-between"
                                  :alignItems     "center"}
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Next"]]]]]


              [:> mui/Step {:key "set-icon-name" :index 3}
                [:> mui/StepLabel
                  [:> mui/Typography "Set Icon Name"]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step sets the icon name."]]
                    [:> mui/Grid {:item true}
                      [:> mui/TextField {:id "svg-name"
                                         :label "Name"
                                         :variant "filled"
                                         :size "small"
                                         :defaultValue  (if @icon-name @icon-name "")
                                         :onChange #(reset! icon-name (.. % -target -value))}]]
                    [:> mui/Grid {:container      true
                                  :direction      "row"
                                  :justify        "space-between"
                                  :alignItems     "center"}
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step inc)} "Next"]]]]]

              [:> mui/Step {:key "add-to-set" :index 4}
                [:> mui/StepLabel
                  [:> mui/Typography "Add icon to set."]]
                [:> mui/StepContent
                  [:> mui/Grid {:container      true
                                :direction      "column"
                                :justify        "space-evenly"
                                :spacing        1
                                :alignItems     "flex-start"}
                    [:> mui/Grid {:item true}
                      [:> mui/Typography "This step will add the imported icon to the selected set."]]
                    [:> mui/Grid {:container      true
                                  :direction      "row"
                                  :justify        "space-between"
                                  :alignItems     "center"}
                      [:> mui/Button {:variant "contained" :onClick #(swap! active-step dec)} "Back"]
                      [:> mui/Button {:variant "contained" :onClick (fn [_]
                                                                      (swap! active-step inc)
                                                                      (rf/dispatch [::imev/add-icon-to-set @selected-set
                                                                                                           @icon-name
                                                                                                           @icon-url
                                                                                                           (hicon/hickory-to-hiccup (logic/react-style->hiccup Logic (first @icon)))]))}
                                     "Add Icon"]]]]]]]

          [:> mui/Button {:variant "contained" :onClick #(do (reset! source file-source)
                                                             (reset! active-step 0))}  "Reset Flow"]]))))
