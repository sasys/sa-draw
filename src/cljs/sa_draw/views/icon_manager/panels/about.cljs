(ns sa-draw.views.icon-manager.panels.about
  "
    The about text for the icon manager.
  "
  (:require [material-ui                                    :as mui]))


(defn about
  []
  [:> mui/Typography {:variant "body2" :paragraph true :style {:margin 5}}
    (str "This panel allows individual icons and sets of icons
          to be added/editied and removed from/to the prototype
          icon set of this model.  Note, this does not apply to
          the tool as a whole.  If you create a new icon set in
          here you should back it up by downloading the icon
          set to your local drive, then upload it to a new model.")])
