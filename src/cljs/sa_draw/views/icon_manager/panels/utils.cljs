(ns sa-draw.views.icon-manager.panels.utils
  (:require [clojure.string :refer  [split]]))


(defn check-file-type
  "
    Checkto see if the path is valid and that a readable file existson the path.
  "
  [path type]
  (= (last (split path ".")) type))
