(ns sa-draw.views.icon-manager.subs
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::icon-manager
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.icon-manager.subs/icon-manager])))

(rf/reg-sub
  ::viewer-open :<- [::icon-manager]
  (fn [icon-manager _]
    (:sa-draw.views.icon-manager.events/open? icon-manager)))

(rf/reg-sub
  ::type :<- [::icon-manager]
  (fn [icon-manager _]
    (:sa-draw.views.icon-manager.events/type icon-manager)))
