(ns sa-draw.views.tool-bar
  "Project level tool bar contains navigation, creation, editing and status tools"
  (:require [material-ui                           :as mui]
            [material-ui-icons                     :as muicons]
            [re-frame.core                         :as rf]
            [sa-draw.events                        :as ev]
            [sa-draw.mark-dirty                    :as md]
            [sa-draw.subs                          :as subs]
            [sa.sa-store.store-factory             :as sf]
            [sa-draw.views.editors.node-events     :as ne]
            [sa-draw.global-state                  :refer [project-open model-info-drawer-pinned model-info-drawer-open layers-drawer-pinned layers-drawer-open config-drawer-pinned config-drawer-open]]
            [clojure.string                        :refer [blank?]]))


(defn make-save-option [store edit-locked? model-dirty?]
  (fn [store edit-locked? model-dirty?]
    [:> mui/Tooltip {:title (str "Save changes to this model to the " (name store))}
      [:div
        [:> mui/IconButton {:onClick (fn [] (rf/dispatch [:sa-draw.events/save-model store])
                                            (rf/dispatch [:sa-draw.events/dirty false]))
                            :disabled edit-locked?
                            :style {:color (if model-dirty? "red" "black")}}
          [:> muicons/Save]]]]))

(defn project-tools
  []
  (fn []
    (let [current-layer @(rf/subscribe [::subs/current-layer])
          current-title @(rf/subscribe [::subs/current-layer-title])
          current-description @(rf/subscribe [::subs/current-layer-description])
          model-name    (get-in @(rf/subscribe [::subs/model]) [:sadf/meta :sadf/name])
          board-link    (get-in @(rf/subscribe [::subs/model]) [:sadf/meta :sadf/board-link])
          model-uuid    (get-in @(rf/subscribe [::subs/model]) [:sadf/meta :sadf/uuid])
          edit-locked?  @(rf/subscribe [::subs/edit-locked])
          model-dirty?  @(rf/subscribe [::subs/dirty])]

      [:> mui/AppBar {:position "relative" :id "project-tools"}
        [:> mui/Toolbar
          [:> mui/Grid {:container    true
                        :direction    "row"
                        :item         true
                        :justify      "space-between"
                        :alignItems   "center"
                        :spacing      1}
            [:> mui/Grid {:item true}
              [:> mui/Tooltip {:title "List layers"}
                [:> mui/Button {:variant "outlined" :onClick #(reset! layers-drawer-open (not @layers-drawer-open))}
                    "Layers"
                  [:> muicons/Menu]]]]

            [:> mui/Grid {:item true}
              [:> mui/Grid {:container    true
                            :direction    "column"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"
                            :spacing      1}
                [:> mui/Grid {:item true}
                  [:> mui/Typography {:variant "subtitle2" :color "textPrimary"} (str "Editing Model: " model-name)]]
                [:> mui/Grid {:item true}
                  [:> mui/Typography {:variant "subtitle2" :color "textPrimary"} (str current-title " " current-layer)]]]]


            [:> mui/Grid {:item true}
              [:> mui/Tooltip {:title "Context"}
                [:> mui/Button {:variant "outlined" :onClick #(rf/dispatch [::ev/new-current-layer "context" current-title current-description [] []])}
                  "Context"]]]

            ;[:> mui/Grid {:item true}
            [:> mui/GridList {:cols (if (not (blank? board-link)) 8 7)
                              :cellHeight "auto"
                              :spacing 4
                              :style {:margin-top "10px"
                                      :margin-right "10px"}}

              [:> mui/GridListTile {:cols 1}
                (if edit-locked?
                  [:> mui/Tooltip {:title "Unlock Edits"}
                    [:> mui/IconButton {:onClick #(rf/dispatch [::ev/lock-edit  (not edit-locked?)])}
                      [:> muicons/Lock]]]
                  [:> mui/Tooltip {:title "Lock Edits"}
                    [:> mui/IconButton {:onClick #(rf/dispatch [::ev/lock-edit  (not edit-locked?)])}
                      [:> muicons/LockOpen]]])]

              [:> mui/GridListTile {:cols 1}
                [:> mui/Tooltip {:title "Download SVG"}
                  [:> mui/IconButton {:onClick (fn [] (rf/dispatch [::ev/download-svg model-name model-uuid current-layer edit-locked?]))}
                    [:> muicons/SaveAlt]]]]

              [:> mui/GridListTile {:cols 1}
                [:> mui/Tooltip {:title "Create Process"}
                  [:> mui/IconButton {:onClick (fn [] (rf/dispatch [::ne/node-edit-new  ""]))
                                      :disabled (or edit-locked? (= "context" current-layer))}
                    [:> muicons/Edit]]]]

              [:> mui/GridListTile {:cols 1}
                [:> mui/Tooltip {:title (str "Save changes to this model to " (name ::sf/key-store))}
                  [:> mui/IconButton {:onClick (fn [] (rf/dispatch [:sa-draw.events/save-model ::sf/key-store])
                                                      (rf/dispatch [::md/dirty false]))
                                      :disabled edit-locked?
                                      :style {:color (if model-dirty? "red" "black")}}
                    [:> muicons/Save]]]]

              [:> mui/GridListTile {:cols 1}
                [:> mui/Tooltip {:title "Load model from local store"}
                  [:> mui/IconButton {:onClick #(rf/dispatch [:sa-draw.events/load-model model-uuid ::sf/key-store])
                                      :disabled edit-locked?}
                    [:> muicons/RestorePage]]]]

              (if (not (blank? board-link))
                [:> mui/Tooltip {:title "Model Board (new tab)"}
                  [:> mui/IconButton {:variant "outlined" :href board-link :target "_blank"}
                    [:> mui/SvgIcon
                                 [:path {:class "st1" :d "M 20 2 L 4 2 C 2.894531 2 2 2.894531 2 4 L 2 20 C 2 21.105469 2.894531 22 4 22 L 20 22 C 21.105469 22 22 21.105469 22 20 L 22 4 C 22 2.894531 21.105469 2 20 2 Z M 11 18 C 11 18.550781 10.550781 19 10 19 L 5 19 C 4.445313 19 4 18.550781 4 18 L 4 5 C 4 4.449219 4.445313 4 5 4 L 10 4 C 10.550781 4 11 4.449219 11 5 Z M 20 11.996094 C 20 12.550781 19.550781 13 18.996094 13 L 14.003906 13 C 13.449219 13 13 12.550781 13 11.996094 L 13 5.003906 C 13 4.449219 13.449219 4 14.003906 4 L 18.996094 4 C 19.550781 4 20 4.449219 20 5.003906 Z "}]]]])

              [:> mui/GridListTile {:cols 1}
                [:> mui/Tooltip {:title "View model meta data"}
                  [:> mui/IconButton
                    {:onClick #(reset! model-info-drawer-open (not @model-info-drawer-open))}
                    [:> muicons/Info]]]]

              [:> mui/GridListTile {:cols 1}
                [:> mui/Tooltip {:title "Edit local settings"}
                  [:> mui/IconButton {:onClick #(reset! config-drawer-open (not @config-drawer-open))}
                    [:> muicons/Settings]]]]]]]])))
