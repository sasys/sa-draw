(ns sa-draw.views.pspec.events
 (:require [re-frame.core              :as re-frame]
           [sa-draw.model.utils        :as ut]
           [sa-draw.model.proto-model  :as proto-model]
           [sa-draw.model.flows        :as flows]
           [sa-draw.model.map-impl     :refer [map-model proc-model]]
           [sa-draw.model.simple-flow-impl     :refer [fm]]))

(re-frame/reg-event-db
  ::update-node
  (fn [db [_ proc-id new-title new-description data-in data-out new-link]]
    (let [model (:sa-draw.events/model db)
          procs-model (proto-model/find-process-model map-model model)
          updated-node (proto-model/patch-node proc-model procs-model proc-id new-title new-description data-in data-out new-link)
          updated-procs-model (proto-model/update-node proc-model proc-id updated-node procs-model)]
      (-> db (assoc-in [:sa-draw.events/model] (proto-model/update-process-model map-model model updated-procs-model))))))


(re-frame/reg-event-db
  ::update-node-with-icon
  (fn [db [_ proc-id new-title new-description data-in data-out new-link icon]]
    (let [model (:sa-draw.events/model db)
          procs-model (proto-model/find-process-model map-model model)
          updated-node (proto-model/patch-node proc-model procs-model proc-id new-title new-description data-in data-out new-link icon)
          updated-procs-model (proto-model/update-node proc-model proc-id updated-node procs-model)]
      (-> db (assoc-in [:sa-draw.events/model] (proto-model/update-process-model map-model model updated-procs-model))))))

(re-frame/reg-event-fx
  ::new-input-data
  (fn [cofx [_ layer flow data]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          process-model (proto-model/find-process-model map-model model)
          flow-model (as-> (proto-model/find-flow-model map-model model) mod
                           (flows/update-flow fm mod flow (flows/id fm flow)))
          node (proto-model/find-node proc-model process-model layer)

          data-in (into (proto-model/data-in proc-model node) data)
          [_ id title description _ data-out] (proto-model/details proc-model node)
          updated-node (proto-model/patch-node proc-model process-model id title description data-in data-out)
          updated-procs-model (proto-model/update-node proc-model id updated-node process-model)
          updated-model (as-> (proto-model/update-process-model map-model model updated-procs-model) mod
                              (proto-model/update-flow-model map-model mod flow-model))
          updated-db    (assoc-in db [:sa-draw.events/model] (into {} updated-model))
          ;THIS IS A FIX TO GET THE CARD TO SHOW THE NEW DATA!!
          updated-current-layer-data-in (-> db :sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/data-in (into data))]
      {:db (assoc-in updated-db [:sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/data-in] updated-current-layer-data-in)
       ::update-layer-drawing {::model updated-model
                               ::layer layer
                               ::layout (-> cofx :db :sa-draw.events/graphics :sa-draw.events/layout)
                               ::drawing-config (-> cofx :db :sa-draw.events/config :sa-draw.events/drawing-defaults)}})))

(re-frame/reg-event-fx
  ::new-output-data
  (fn [cofx [_ layer flow data]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          process-model (proto-model/find-process-model map-model model)
          flow-model (as-> (proto-model/find-flow-model map-model model) mod
                           (flows/update-flow fm mod flow (flows/id fm flow)))
          node (proto-model/find-node proc-model process-model layer)

          data-out (into (proto-model/data-out proc-model node) data)
          [_ id title description data-in _] (proto-model/details proc-model node)
          updated-node (proto-model/patch-node proc-model process-model id title description data-in data-out)
          updated-procs-model (proto-model/update-node proc-model id updated-node process-model)
          updated-model (as-> (proto-model/update-process-model map-model model updated-procs-model) mod
                              (proto-model/update-flow-model map-model mod flow-model))
          updated-db    (assoc-in db [:sa-draw.events/model] (into {} updated-model))
          ;THIS IS A FIX TO GET THE CARD TO SHOW THE NEW DATA!!
          updated-current-layer-data-out (-> db :sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/data-out (into data))]
      {:db (assoc-in updated-db [:sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/data-out] updated-current-layer-data-out)
       ::update-layer-drawing {::model updated-model
                               ::layer layer
                               ::layout (-> cofx :db :sa-draw.events/graphics :sa-draw.events/layout)
                               ::drawing-config (-> cofx :db :sa-draw.events/config :sa-draw.events/drawing-defaults)}})))

(defn reduce-data-items
  "
    Removes data with the given id from the vector at the given key.
    Returns an updated model a reduced data set.
  "
  [data-id layer data-key model]
  (let [process-model (proto-model/find-process-model map-model model)
        node (proto-model/find-node proc-model process-model layer)
        data (-> node data-key)
        reduced-data (into [] (remove #(= (-> % :sadf/id) data-id) data))
        updated-node (assoc node data-key reduced-data)
        proc-id (proto-model/id proc-model node)
        updated-procs-model (proto-model/update-node proc-model proc-id updated-node process-model)
        updated-model (proto-model/update-process-model map-model model updated-procs-model)]
    [reduced-data updated-model]))


(re-frame/reg-event-fx
  ::remove-input-data-from-spec
  (fn [cofx [_ data-id layer]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          [updated-data updated-model] (reduce-data-items data-id layer :sadf/data-in model)
          updated-db (assoc-in db [:sa-draw.events/model] (into {} updated-model))]
      {:db (assoc-in updated-db [:sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/data-in] updated-data)
       ::update-layer-drawing {::model updated-model
                               ::layer layer
                               ::layout (-> cofx :db :sa-draw.events/graphics :sa-draw.events/layout)
                               ::drawing-config (-> cofx :db :sa-draw.events/config :sa-draw.events/drawing-defaults)}})))



(re-frame/reg-event-fx
  ::remove-output-data-from-spec
  (fn [cofx [_ data-id layer]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          [updated-data updated-model] (reduce-data-items data-id layer :sadf/data-out model)
          updated-db (assoc-in db [:sa-draw.events/model] (into {} updated-model))]
      {:db (assoc-in updated-db [:sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/data-out] updated-data)
       ::update-layer-drawing {::model updated-model
                               ::layer layer
                               ::layout (-> cofx :db :sa-draw.events/graphics :sa-draw.events/layout)
                               ::drawing-config (-> cofx :db :sa-draw.events/config :sa-draw.events/drawing-defaults)
                               ::edit-locked (get-in (:db cofx) [::graphics ::edit-locked])}})))


(defn remove-data-from-flows
  "Given a seq of flows remove the data with the given id from the flow with the given id."
  [flows flow-id data-id]
  (let [flow (flows/find-flow fm flows flow-id)
        flow-data (flows/data-items fm flow)
        reduced-flow-data (into [] (remove #(= (-> % :sadf/id) data-id) flow-data))
        updated-flow (assoc flow :sadf/data reduced-flow-data)
        reduced-flows (flows/update-flow fm flows updated-flow flow-id)]
    reduced-flows))


(re-frame/reg-event-db
  ::remove-input-data-from-flow
  (fn [db [_ flow-id data-id _]]
    ;find the node in the model
    ;find the flow in the inputs list
    ;find the data item in the flow
    ;if the flow has no data remove the flow
    ;update the flow with the reduced number of datas
    ;add the flow back into the data in list of the process
    (let [model (:sa-draw.events/model db)
          flow-model (proto-model/find-flow-model map-model model)
          updated-flow-model (remove-data-from-flows flow-model flow-id data-id)
          updated-model (proto-model/update-flow-model map-model model updated-flow-model)
          updated-db    (assoc-in db [:sa-draw.events/model] (into {} updated-model))]
      updated-db)))


(re-frame/reg-event-db
  ::remove-output-data-from-flow
  (fn [db [_ flow-id data-id layer]]
    ;find the node in the model
    ;find the flow in the inputs list
    ;find the data item in the flow
    ;if the flow has no data remove the flow
    ;update the flow with the reduced number of datas
    ;add the flow back into the data in list of the process
    (let [namespace (-> db :sa-draw.events/model :meta :namespace)
          node (ut/find-node-in-tree layer (-> db :sa-draw.events/model :data) namespace)
          out-flows (-> node :sa-trees.proc.tree/data-out)
          updated-out-flows (remove-data-from-flows out-flows flow-id data-id)
          updated-node (assoc node :sa-trees.proc.tree/data-out updated-out-flows)
          updated-model (ut/update-node layer updated-node (-> db :sa-draw.events/model :data) namespace)
          final-db (assoc-in db [:sa-draw.events/model :data] updated-model)]
      final-db)))


(re-frame/reg-event-db
  ::remove-process-tree
  (fn [db [_ layer]]
    (print ":sa-draw.views.pspec.events/remove-process-tree event not implemented for layer " layer)))
