(ns sa-draw.views.pspec.card
    (:require [reagent.core               :as r]
              [material-ui                :as mui]
              [material-ui-icons          :as muicons]
              [re-frame.core              :as rf]
              [sa-draw.subs               :as subs]
              [sa-draw.views.pspec.events :as pev]
              [sa-draw.events             :as ev]
              [sa-draw.views.editors.quick-term              :as qt]
              [sa-draw.views.editors.data-item-editor        :as die]
              [sa-draw.views.editors.data-item-editor-events :as dievts]
              [sa-draw.views.editors.data-item-editor-subs   :as diesubs]
              [sa-draw.data.simple-dd     :refer [dd term]]
              [sa-draw.data.dd            :refer [new-term id]]))



(defn show-spec
  "Returns a card with a spec description written on it"
  ([]
   (let [editing? (r/atom false) ;reflects if we are locked for edit.

         quick-input-term-open? (r/atom false)  ;what kind of data are we creating
         quick-output-term-open? (r/atom false)
         quick-term-title (r/atom nil)          ;title can be input or output!

         ;These change as we navigate the layers
         current-layer (rf/subscribe [::subs/current-layer])
         current-layer-title (rf/subscribe [::subs/current-layer-title])
         current-layer-description (rf/subscribe [::subs/current-layer-description])
         data-in (rf/subscribe [::subs/current-layer-data-in])
         data-out (rf/subscribe [::subs/current-layer-data-out])

         ;when we lock for edit we copy the required data into here
         locked-proc-id (atom nil)
         locked-title (atom nil)
         locked-description (atom nil)
         locked-data-in (atom [])
         locked-data-out (atom [])

         rows-open? (r/atom true)

         show-context? (true? @(rf/subscribe [::subs/current-layer-context?]))
         inputs-expanded (r/atom false)
         outputs-expanded (r/atom false)]

    (fn []

      (let [edit-locked?  @(rf/subscribe [::subs/edit-locked])
            ;when we lock for edit these subs will reflect  the current state of editing of inputs and outputs
            die-data-in (rf/subscribe [::diesubs/input-terms])
            die-data-out (rf/subscribe [::diesubs/output-terms])]

        [:> js/ReactDraggable {:handle "strong"}
          [:> mui/Card {:raised true :style {:margin-top "40px" :margin-left "10px" :height "660px" :overflow "auto"}};width "340px"}}
              [:> mui/CardActions
                [:> mui/CardContent
                    [:> mui/Grid {:container    true
                                  :direction    "row"
                                  :item         true
                                  :justify      "space-between"
                                  :alignItems   "center"}
                      [:strong [:> muicons/DragIndicator]]
                      (if (not @editing?)
                        [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} @current-layer-title]
                        [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (str "LOCKED FOR EDIT  ")])
                      [:> mui/Tooltip {:title "Abandon edit"}
                        [:> mui/IconButton {:onClick (fn [] (reset! editing? false))}
                          [:> muicons/Close]]]]
                    [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} (if @editing? @locked-proc-id @current-layer)]
                    (if @editing?
                        [:> mui/TextField {:variant "outlined"
                                           :disabled edit-locked?
                                           :defaultValue @locked-title
                                           :onChange (fn [e] (reset! locked-title (.. e -target -value)))}])
                    ;INPUTS
                    (if @editing?

                      [:div {:style {:padding-top "5px"}}
                        [:> mui/Grid {:container    true
                                      :direction    "row"
                                      :item         true
                                      :justify      "space-between"
                                      :alignItems   "center"}
                          [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Create Input"]
                          [:> mui/Tooltip {:title "Create new input"}
                            [:div
                              [:> mui/IconButton {:onClick (fn [] (swap! quick-input-term-open? not @quick-input-term-open?)
                                                                  (reset! quick-term-title "Create New Input Terms"))
                                                  :disabled edit-locked?}
                                [:> muicons/Add]]]]
                          [qt/quick-term quick-term-title  quick-input-term-open? :sa-draw.views.editors.data-item-editor-events/add-proc-input-term]]
                        [:> mui/ExpansionPanel {:expanded (if @inputs-expanded true false) :onChange #(reset! inputs-expanded (not @inputs-expanded))}
                          [:> mui/ExpansionPanelSummary {:aria-controls "panel1a-content"
                                                         :id "panel1a-header"
                                                         :expandIcon (r/create-element muicons/ExpandMore)}
                             [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Edit Inputs"]]
                          [:> mui/ExpansionPanelDetails
                            [:div
                              (for [t @die-data-in]
                                ^{:key (id term t)}[die/data-item-editor t edit-locked? rows-open? :sa-draw.views.editors.data-item-editor-events/remove-proc-input-term])]]]]

                      [:div {:style {:padding-top "5px"}}
                        [:> mui/ExpansionPanel {:expanded (if @inputs-expanded true false) :onChange #(reset! inputs-expanded (not @inputs-expanded))}
                          [:> mui/ExpansionPanelSummary {:aria-controls "panel1a-content"
                                                         :id "panel1a-header"
                                                         :expandIcon (r/create-element muicons/ExpandMore)}
                            [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Inputs"]]
                          [:> mui/ExpansionPanelDetails
                              [:div
                                (for [t @data-in]
                                  ^{:key (id term t)}[die/data-item-editor t true rows-open? :sa-draw.views.editors.data-item-editor-events/remove-proc-input-term])]]]])

                    ;OUTPUTS
                    (if @editing?
                      [:div {:style {:padding-top "5px"}}
                        [:> mui/Grid {:container    true
                                      :direction    "row"
                                      :item         true
                                      :justify      "space-between"
                                      :alignItems   "center"}
                            [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Create Output"]
                            [:> mui/Tooltip {:title "Create new output"}
                              [:div
                                [:> mui/IconButton {:onClick (fn [] (swap! quick-output-term-open? not @quick-output-term-open?)
                                                                    (reset! quick-term-title "Create New Output Terms"))
                                                    :disabled edit-locked?}
                                  [:> muicons/Add]]]]
                          [qt/quick-term  quick-term-title  quick-output-term-open? :sa-draw.views.editors.data-item-editor-events/add-proc-output-term]]
                        [:> mui/ExpansionPanel {:expanded (if @outputs-expanded true false) :onChange #(reset! outputs-expanded (not @outputs-expanded))}
                          [:> mui/ExpansionPanelSummary {:aria-controls "panela-content"
                                                         :id "panel2a-header"
                                                         :expandIcon (r/create-element muicons/ExpandMore)}
                             [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Edit Outputs"]]
                          [:> mui/ExpansionPanelDetails
                            [:div
                              (for [t @die-data-out]
                                ^{:key (id term t)}[die/data-item-editor t edit-locked? rows-open? :sa-draw.views.editors.data-item-editor-events/remove-proc-output-term])]]]]

                      [:div {:style {:padding-top "5px"}}
                        [:> mui/ExpansionPanel {:expanded (if @outputs-expanded true false) :onChange #(reset! outputs-expanded (not @outputs-expanded))}
                          [:> mui/ExpansionPanelSummary {:aria-controls "panel1a-content"
                                                         :id "panel1a-header"
                                                         :expandIcon (r/create-element muicons/ExpandMore)}
                            [:> mui/Typography {:variant "subtitle2" :style {:margin 5}} "Outputs"]]
                          [:> mui/ExpansionPanelDetails
                            [:div
                              (for [t @data-out]
                                ^{:key (id term t)}[die/data-item-editor t true rows-open? :sa-draw.views.editors.data-item-editor-events/remove-proc-output-term])]]]])


                    (if @editing?

                      [:div {:class "MuiFormControl-root MuiTextField-root" :style {:margin-top "3px"}}
                        [:> mui/TextareaAutosize {:variant "outlined"
                                                  :label "Description"
                                                  :disabled edit-locked?
                                                  :defaultValue @locked-description
                                                  :onChange (fn [e] (reset! locked-description (.. e -target -value)))
                                                  ;;THIS STYLE IS TO MAKE THE TEXT AREA LOOK LIKE THE TEXT FILED!!
                                                  ;;NOTE that the style sheet gets overriden and does not get fully applied.
                                                  :style {:border-radius "4px"
                                                          :margin 1
                                                          :margin-top "3px"
                                                          :background-color "transparent"
                                                          :box-sizing "inherit"
                                                          :font-family "Roboto"
                                                          :font-size "16px"
                                                          :width 250
                                                          :padding-top "5px"
                                                          :padding-left "1px"}}]]


                      [:> mui/TextField {:variant "outlined"
                                         :multiline true
                                         :rows 6
                                         :disabled true
                                         :style {:width 250  :padding-top "5px"}
                                         :value @current-layer-description}])

                    [:> mui/Grid {:container    true
                                  :direction    "row"
                                  :item         true
                                  :justify      "space-between"
                                  :alignItems   "center"}
                      [:> mui/Tooltip {:title "Save changes to this process spec"}
                        [:div
                          [:> mui/IconButton {:onClick (fn []
                                                           (rf/dispatch [::pev/update-node (if (= "context" @locked-proc-id) "0" @locked-proc-id) @locked-title @locked-description @die-data-in @die-data-out])
                                                           (rf/dispatch [::ev/new-current-layer @locked-proc-id @locked-title  @locked-description  @die-data-in @die-data-out show-context?])
                                                           (reset! editing? false)
                                                           (rf/dispatch [::ev/dirty true]))

                                              :disabled (or edit-locked? (not @editing?))}
                            [:> muicons/Save]]]]
                      [:> mui/Tooltip {:title "Edit this process spec"}
                        [:div
                          [:> mui/IconButton {:onClick (fn[] (swap! editing? not @editing?)
                                                             (reset! locked-proc-id @current-layer)
                                                             (reset! locked-title @current-layer-title)
                                                             (reset! locked-description @current-layer-description)
                                                             (reset! locked-data-in @data-in)
                                                             (reset! locked-data-out @data-out)
                                                             ;here we capture the current inputs and outputs of the process
                                                             (rf/dispatch [::dievts/initial-input-terms @locked-data-in])
                                                             (rf/dispatch [::dievts/initial-output-terms @locked-data-out]))
                                              :disabled edit-locked?}
                            [:> muicons/Edit]]]]]]]]])))))
