(ns sa-draw.views.app-bar
  "
    Contains summary, home and searching tools
  "
  (:require [material-ui                                        :as mui]
            [material-ui-icons                                  :as muicons]
            [re-frame.core                                      :as rf]
            [sa-draw.events                                     :as ev]
            [sa.sa-store.store-factory                          :as sf]
            [sa-draw.views.editors.file-upload-selector-events  :as fsupev]
            [sa-draw.views.editors.markdown-importer-events     :as miev]
            [sa-draw.views.help.help-events                     :as hev]
            [sa-draw.views.help.help-subs                       :as hsub]
            [sa-draw.model.utils                   :refer [uid]]
            [sa-draw.views.menus.user-info         :refer [user-menu]]
            [sa-draw.views.menus.information       :refer [information-menu]]
            [sa-draw.global-state                  :refer [project-open user-menu-pinned user-menu-open model-info-drawer-pinned model-info-drawer-open layers-drawer-pinned layers-drawer-open information-menu-pinned information-menu-open config-drawer-pinned config-drawer-open]]))


(def user-info-anchor-id "user-info-menu-anchor")
(def information-menu-anchor-id "information-menu-anchor")
(def app-bar-id "app-bar-id")


(defn application-tools
  [home?]
  (let [help-open? (rf/subscribe [::hsub/viewer-open])]
    (fn [home?]

        [:> mui/AppBar {:id app-bar-id :position "static" :color "primary"}
          [:> mui/Toolbar
            [:> mui/Grid {:container    true
                          :direction    "row"
                          ;:item         true
                          :justify      "space-between"
                          :alignItems   "center"
                          :spacing      1}
                [:> mui/GridList {:cols 2
                                  :cellHeight "auto"}
                  [:> mui/GridListTile {:cols 1}
                    [:> mui/Tooltip {:title "Home Screen"}
                      [:> mui/IconButton {:onClick (fn [] (reset! project-open false)
                                                          (reset! home? true)
                                                          (reset! layers-drawer-open false)
                                                          (reset! layers-drawer-pinned false)
                                                          (reset! config-drawer-pinned false)
                                                          (reset! config-drawer-open false)
                                                          (reset! model-info-drawer-pinned false)
                                                          (reset! model-info-drawer-open false)
                                                          (rf/dispatch-sync [::ev/empty-model])
                                                          (rf/dispatch [::ev/load-summaries ::sf/key-store]))}
                        [:> muicons/Home]]]]
                  [:> mui/GridListTile {:cols 1}
                    [:> mui/Tooltip {:title "Tool Trello Board (new tab)"}
                      [:> mui/IconButton {:variant "text" :href "https://trello.com/b/KePK2v6d/project-sa" :target "_blank"} ;"Trello"
                        [:> mui/SvgIcon
                          [:path {:class "st1" :d "M 20 2 L 4 2 C 2.894531 2 2 2.894531 2 4 L 2 20 C 2 21.105469 2.894531 22 4 22 L 20 22 C 21.105469 22 22 21.105469 22 20 L 22 4 C 22 2.894531 21.105469 2 20 2 Z M 11 18 C 11 18.550781 10.550781 19 10 19 L 5 19 C 4.445313 19 4 18.550781 4 18 L 4 5 C 4 4.449219 4.445313 4 5 4 L 10 4 C 10.550781 4 11 4.449219 11 5 Z M 20 11.996094 C 20 12.550781 19.550781 13 18.996094 13 L 14.003906 13 C 13.449219 13 13 12.550781 13 11.996094 L 13 5.003906 C 13 4.449219 13.449219 4 14.003906 4 L 18.996094 4 C 19.550781 4 20 4.449219 20 5.003906 Z "}]]]]]]
                [:> mui/Typography {:variant "h6" :style {:margin 20} :color "textPrimary"} "Processes In Context - Community Edition"]

                [:> mui/GridList {:cols 5
                                  :cellHeight "auto"
                                  :spacing 4
                                  :style {:margin-top "10px"
                                          :margin-right "10px"}}

                  [:> mui/GridListTile {:cols 1 :id information-menu-anchor-id}
                    [:> mui/Tooltip {:title "Import Markdown As A Model"}
                      [:div
                        [:> mui/IconButton {:disabled (not @home?) :onClick #(rf/dispatch [::miev/open-mi])}
                          [:> muicons/CloudDownload]]]]]

                  [:> mui/GridListTile {:cols 1 :id information-menu-anchor-id}
                    [:> mui/Tooltip {:title "Restore Model From Filesystem"}
                      [:div
                        [:> mui/IconButton {:disabled (not @home?) :onClick #(rf/dispatch [::fsupev/open-fus])}
                          [:> muicons/Restore]]]]]

                  [:> mui/GridListTile {:cols 1 :id information-menu-anchor-id}
                    [:> mui/Tooltip {:title "New Model"}
                      [:div
                        [:> mui/IconButton {:disabled (not @home?) :onClick #(rf/dispatch [::ev/new-model "new-model" (uid) ::sf/key-store])}
                          [:> muicons/Add]]]]]

                  [:> mui/GridListTile {:cols 1 :id information-menu-anchor-id}
                    [:> mui/Tooltip {:title "Help"}
                      [:> mui/IconButton {:onClick #(rf/dispatch [::hev/help-open (not @help-open?)])}
                        [:> muicons/Help]]]]

                  [:> mui/GridListTile {:cols 1 :id information-menu-anchor-id}
                    [:> mui/Tooltip {:title "Information"}
                      [:> mui/IconButton {:onClick #(reset! information-menu-open true)}
                        [:> muicons/Info]]]]]

                [information-menu information-menu-anchor-id app-bar-id]
                [user-menu user-info-anchor-id app-bar-id]]]])))
