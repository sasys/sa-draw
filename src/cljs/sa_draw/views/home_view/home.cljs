(ns sa-draw.views.home-view.home
  "
    Home screen.  Made visible on initial page load.
  "
  (:require [material-ui                            :as mui]
            [material-ui-icons                      :as muicons]
            [re-frame.core                          :as rf]
            [cljs-time.core                         :as dt]
            [cljs-time.format                       :as dtfm]
            [sa-draw.model.default-model            :as dm]
            [sa-draw.model.proto-model              :as pm]
            [sa-draw.utils.sem-ver                  :as semver]
            [sa-draw.subs                           :as subs]
            [sa-draw.events                         :as ev]
            [sa.sa-store.store-factory              :as sf]
            [sa-draw.model.map-impl                 :refer [map-model proc-model]]
            [sa-draw.model.utils                    :refer [uid]]
            [sa-draw.global-state                   :refer [project-open meta-data-editor-open]]))

            ;[sa-draw.views.editors.meta-data-editor :refer [meta-data-editor-open]]))


(defn make-model-card
  ([summary home? store-id]
   (make-model-card summary home? store-id false))

  ([{:sadf/keys [uuid name summary detail board-link creation-date-time app-version]} home? store-id example?]
   ^{:key uuid}[:> mui/GridListTile
                 [:> mui/Card
                     [:> mui/CardContent
                         [:> mui/Typography {:variant "subtitle2" :style {:margin 0}} name]
                         [:> mui/TextField {:disabled true
                                            :variant "standard"
                                            :multiline true
                                            :fullWidth true
                                            :value summary}] ;Interesting point, default value doesnt change when value changes - ie use value for disabled fields!!
                         (if app-version
                           [:> mui/Grid {:item true}
                               [:> mui/Typography {:variant "caption"} (str "Application Version : " (semver/format-semver app-version))]]
                           [:> mui/Grid {:item true}
                               [:> mui/Typography {:variant "caption"} (str "Application Version : " "MISSING")]])
                         [:> mui/Grid {:item true}
                             [:> mui/Typography {:variant "caption"} (str "Created : " creation-date-time)]]
                         [:> mui/Grid {:item true}
                             [:> mui/Typography {:variant "caption" :style {:margin 0}} (str "id : " uuid)]]
                         [:> mui/Grid {:item true}
                             [:> mui/Typography {:variant "caption" :style {:margin 0}} (str "Model from : " store-id)]]
                         [:> mui/CardActions
                           [:> mui/Grid {:container    true
                                         :direction    "row"
                                         :item         false
                                         :justify      "space-between"
                                         :alignItems   "center"
                                         :spacing      8}
                                 (if example?
                                   (let [creation-date-time (->> (dt/now) (dtfm/unparse (dtfm/formatter "yyyy-MM-dd HH:mm:ss")))
                                         context-model (pm/find-process-model map-model dm/demo-model-1)
                                         [_ _ title description data-in data-out] (pm/details proc-model context-model)]
                                     [:> mui/Tooltip {:title "Copy This Model And Edit"}
                                       [:> mui/IconButton {:onClick (fn [] (rf/dispatch-sync [:incoming-model (-> dm/demo-model-1
                                                                                                                  (assoc-in [:sadf/meta :sadf/uuid] (uid))
                                                                                                                  (assoc-in [:sadf/meta :sadf/creation-date-time] creation-date-time))])
                                                                           (rf/dispatch [::ev/new-current-layer "context" title description data-in data-out])
                                                                           (reset! project-open true)
                                                                           (reset! home? false))}
                                           [:> muicons/Edit {:classes {:root "material-icons md-18"}}]]])
                                   [:> mui/Tooltip {:title "Edit Model"}
                                     [:> mui/IconButton {:onClick (fn []
                                                                      ; Needs to be done before we load the markdown!!
                                                                      (rf/dispatch [::ev/load-model uuid store-id])
                                                                      (reset! project-open true)
                                                                      (reset! home? false))}

                                        [:> muicons/Edit {:classes {:root "material-icons md-18"}}]]])


                             [:> mui/Tooltip {:title "Options"}
                               [:div
                                 [:> mui/IconButton {:onClick #(reset! meta-data-editor-open {:id uuid
                                                                                              :summary summary
                                                                                              :name name
                                                                                              :store-id store-id
                                                                                              :board-link board-link
                                                                                              :detail detail
                                                                                              :creation-date creation-date-time
                                                                                              :app-version (semver/format-semver app-version)})
                                                     :disabled example?}
                                   [:> muicons/MoreHoriz {:classes {:root "material-icons md-18"}}]]]]]]]]]))

(defn filter-summaries [store-key summaries]
  "
    Given a list of maps where the key :sa-draw.events/store identifies the Store
    return the summaries map for the store
  "
  (filter (fn [store-summaries] (= (:sa-draw.events/store store-summaries) store-key)) summaries))

(defn show-home-store-models
  [home? store title]
  (fn [home? store title]
   (let [all-store-model-summaries (rf/subscribe [::subs/summaries])
         model-summaries (store @all-store-model-summaries)]
    [:> js/ReactDraggable {:handle "strong"}
     [:div
       [:> mui/Grid { :container    true
                      :direction    "row"
                      :item         false
                      :justify      "flex-start" ;"space-between"
                      :alignItems   "center"
                      :spacing      8}
         [:strong [:> muicons/DragIndicator]]
         [:> mui/Typography {:variant "h6" :align "center" :style {:margin 5}} title]]
       [:> mui/GridList {:cellHeight "auto"
                         :spacing 4
                         :cols 3
                         :style { :width "35vw" :height "75vh" :margin-top (str 20 "px") :root {:display "flex"  :justifyContent "space-around" :overflow "hidden"}}}
        (for [summary model-summaries]
          (make-model-card summary home? store))]]])))

(defn show-home-screen-models
  [home?]
  (fn [home?]
    [:<>
      [show-home-store-models home? ::sf/key-store "Models In Browser Store"]]))

(defn show-home-screen-new-model
  "
    This gives the user a partially filled out starting model.
  "
  [home?]
  (fn [home?]
   [:> js/ReactDraggable {:handle "strong"}
    [:div
        [:> mui/Grid { :container    true
                       :direction    "row"
                       :item         false
                       :justify      "flex-start"
                       :alignItems   "center"
                       :spacing      8}
          [:strong [:> muicons/DragIndicator]]
          [:> mui/Typography {:variant "h6" :align "center" :style {:margin 5}} "Initial Base Model"]]
      [:> mui/GridList {:cellHeight "auto"
                        :spacing 4
                        :cols 2
                        :style { :width "30vw" :height "75vh" :margin-top (str 20 "px") :root {:display "flex"  :justifyContent "space-around" :overflow "hidden"}}}
          ;Here we make a sample model card - just for now, we alsoe update the sample model in the browser store.
          ;This forces its creation if its not already there.  We fix the uid of the sample model.
          ;If the user edits the sample model we make a new uuid
          (make-model-card {:sadf/uuid "dummy-uuid"
                            :sadf/name (-> dm/demo-model-1 :sadf/meta :sadf/name)
                            :sadf/summary (-> dm/demo-model-1 :sadf/meta :sadf/summary)
                            :sadf/detail (-> dm/demo-model-1 :sadf/meta :sadf/detail)
                            :sadf/app-version (-> dm/demo-model-1 :sadf/meta :sadf/app-version)} home? :sa.sa-store.store-factory/key-store true)]]]))
