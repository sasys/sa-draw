(ns sa-draw.views.dd.events
  (:require [re-frame.core                          :as re-frame]
            [sa-draw.model.proto-model             :as proto-model]
            [sa-draw.model.map-impl                :refer [map-model proc-model]]
            [sa-draw.data.dd                       :refer [add-term term-by-id new-term update-term id label clear-dd remove-term list-terms term-exists? details]]
            [sa-draw.data.simple-dd                :refer [dd term]]))

; (re-frame/reg-event-fx
;   ::new-term
;   (fn [cofx [_ t]]))
;;FIXME: DD is disabled for now
    ; (let [model (-> cofx :db :sa-draw.events/model)
    ;       dictionary (proto-model/find-dictionary map-model model)
    ;       updated-dictionary (add-term dd t dictionary)
    ;       updated-model (proto-model/update-dictionary map-model model updated-dictionary)]
    ;   {::add-term {::t t
    ;                ::model (:model cofx)}
    ;    :db (-> (:db cofx)
    ;            (assoc-in [:sa-draw.events/model] updated-model))})))

(re-frame/reg-event-fx
  ::remove-term
  (fn [cofx [_ id]]
    (let [model (-> cofx :db :sa-draw.events/model)
          dictionary (proto-model/find-dictionary map-model model)
          updated-dictionary (remove-term dd id dictionary)
          updated-model (proto-model/update-dictionary map-model model updated-dictionary)]
      {::remove-term {::id id
                      ::model (:model cofx)}
       :db (-> (:db cofx)
               (assoc-in [:sa-draw.events/model] updated-model))})))

(re-frame/reg-event-fx
  ::update-term
  (fn [cofx [_ term]]
    (let [model (-> cofx :db :sa-draw.events/model)
          dictionary (proto-model/find-dictionary map-model model)
          updated-dictionary (update-term dd term dictionary)
          updated-model (proto-model/update-dictionary map-model model updated-dictionary)]
      {::update-term {::term term
                      ::model (:model cofx)}
       :db (-> (:db cofx)
               (assoc-in [:sa-draw.events/model] updated-model))})))

(re-frame/reg-event-db
  ::term-selection
  (fn [db [_ selected-terms]]
    (assoc-in db [:sa-draw.events/views ::selected-entries] selected-terms)))
