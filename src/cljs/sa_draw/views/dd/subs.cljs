(ns sa-draw.views.dd.subs
  (:require [re-frame.core  :as rf]))

(rf/reg-sub
  ::selected-terms
  (fn [db _]
    (get-in db [:sa-draw.events/views :sa-draw.views.dd.events/selected-entries])))
