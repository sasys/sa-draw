(ns sa-draw.views.dd.browser
  "
    A popover browser for the data dictionary
  "
  (:require ["@material-ui/core"                  :as mui]
            ["@material-ui/icons"                 :as muicons]
            [reagent.core                         :as reagent]
            [re-frame.core                        :as rf]
            [sa-draw.model.map-impl               :refer [map-model proc-model]]
            [sa-draw.subs                         :as subs]
            [sa-draw.views.dd.events              :as ddev]
            [sa-draw.model.proto-model            :refer [find-dictionary]]
            [sa-draw.data.dd        :as dd :refer [as-map add-term term-by-id new-term label clear-dd remove-term list-terms term-exists? details]]
            [sa-draw.data.simple-dd :refer [dd term]]))


(def dd-browser-open? (reagent/atom false))



(defn make-table-header [selected-entries]
  [:> mui/TableRow
       [:> mui/TableCell [:> mui/Typography {:variant "subtitle2" :style {:margin 20} :color "textPrimary"} "Label"]]
       [:> mui/TableCell [:> mui/Typography {:variant "subtitle2" :style {:margin 20} :color "textPrimary"} "ID"]]
       [:> mui/TableCell [:> mui/Typography {:variant "subtitle2" :style {:margin 20} :color "textPrimary"} "Description"]]
       [:> mui/TableCell [:> mui/Typography {:variant "subtitle2" :style {:margin 20} :color "textPrimary"} "Options"]]
       [:> mui/TableCell [:> mui/Typography {:variant "subtitle2" :style {:margin 20} :color "textPrimary"} [:> mui/Button
                                                                                                                {:onClick (fn [](reset! selected-entries [])
                                                                                                                                (rf/dispatch [::ddev/term-selection []]))}
                                                                                                                "Clear"]]]])


(defn make-row [t selected-entries]
  (fn [t selected-entries]
    (let [[lable id descr] (details term t)]
      [:> mui/TableRow
           [:> mui/TableCell lable]
           [:> mui/TableCell id]
           [:> mui/TableCell descr]
           [:> mui/TableCell
              [:> mui/Grid {:container    true
                            :direction    "row"
                            :item         true
                            :justify      "space-between"
                            :alignItems   "center"}
                        [:> mui/Tooltip {:title "More options"}
                          [:div
                            [:> mui/IconButton {:disabled true}
                              [:> muicons/MoreHoriz {:classes {:root "material-icons md-18"}}]]]]
                        [:> mui/Tooltip {:title "Delete term"}
                          [:> mui/IconButton {:onClick #(rf/dispatch [::ddev/remove-term id])}
                            [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]
                        [:> mui/Tooltip {:title "Edit"}
                          [:div
                            [:> mui/IconButton {:disabled true}
                              [:> muicons/Edit {:classes {:root "material-icons md-18"}}]]]]]]
           [:> mui/TableCell
                [:> mui/Tooltip {:title "select"}
                  [:> mui/IconButton {:onClick (fn [](if (some #(= id (dd/id term %)) @selected-entries)
                                                        (reset! selected-entries (remove #(= id (id term %)) @selected-entries))
                                                        (swap! selected-entries conj t))
                                                     (rf/dispatch [::ddev/term-selection @selected-entries]))
                                      :disableRipple true}
                      (if (some #(= id (dd/id term %)) @selected-entries)
                        [:> muicons/CheckBox]
                        [:> muicons/CheckBoxOutlineBlank])]]]])))

              ; [:> mui/Checkbox {:checked (if (some #{id} @selected-entries) true false)
              ;                   :value "consumer"
              ;                   :color "default"
              ;                   :name "context-object-type"
              ;                   :onChange (fn [](if (some #{id} @selected-entries)
              ;                                     (reset! selected-entries (remove #(= id %) @selected-entries))
              ;                                     (swap! selected-entries conj id))
              ;                                   (rf/dispatch [::ddev/term-selection @selected-entries]))}]]])

(defn dd-browser
  "A popover to allow the dd to be browsed"
  [dd-browser-open?]
  (let [selected-entries (reagent/atom [])]
    (fn [dd-browser-open?]
      (let [dictionary (as-map dd (find-dictionary map-model @(rf/subscribe [::subs/model])))
            terms (list-terms dd dictionary)]

        [:> js/ReactDraggable {:handle "strong"}
          [:> mui/Popover {:open @dd-browser-open?
                           :anchorReference "anchorPosition"
                           :anchorPosition {:left 40 :top 40}}
                  [:> mui/Grid {:container    true
                                :direction    "column"
                                :item         true
                                :justify      "space-between"
                                :alignItems   "center"}
                    [:> mui/Grid {:container    true
                                  :direction    "row"
                                  :item         true
                                  :justify      "space-between"
                                  :alignItems   "center"}
                      [:strong [:> muicons/DragIndicator]]
                      [:> mui/Typography {:variant "subtitle2" :style {:margin 5 :text-decoration "underline"}}
                        "Data Dictionary Browser"]
                      [:> mui/Tooltip {:title "Abandon/Close"}
                        [:> mui/IconButton {:onClick #(swap! dd-browser-open? not @dd-browser-open?)}
                          [:> muicons/Close]]]]
                    [:> mui/Grid {:item true}
                      [:> mui/Table {:size "small"}
                        [:> mui/TableHead
                         [make-table-header selected-entries]]
                        [:tbody
                          (for [t terms]
                            ^{:key (dd/id term t)}[make-row t selected-entries])]]
                      [:> mui/Tooltip {:title "NOT IMPLEMENTED Add Term"}
                        [:div
                          [:> mui/Fab {:disabled true}
                            [:> muicons/Add]]]]]]]]))))
