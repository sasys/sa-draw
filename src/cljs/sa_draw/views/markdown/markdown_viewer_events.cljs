(ns sa-draw.views.markdown.markdown-viewer-events
  (:require [re-frame.core            :as rf]
            [sa-draw.events           :as ev]
            [sa-draw.utils.download   :as dld]))





(def MARKDOWN_PROCESSOR_URL "http://store.sa.softwarebynumbers.com/api/transform/modeltomd")


(defn fire-md1 [md]
  (rf/dispatch [::ev/update-markdown md]))


;
; This fx causes the local markdown parser to run
;
(rf/reg-fx
  ::markdown-from-json-fx-local
  (fn [req]
    (.then (.jtomd js/parser  (.stringify js/JSON (clj->js (:sadf/procs (::json req)))))
      (fn [%1] (fire-md1 %1)))))


(rf/reg-event-fx
  ::markdown-from-json
  (fn [cofx [_]]
    {::markdown-from-json-fx-local {::json (:sa-draw.events/model (:db cofx))
                                    ::db (:db cofx)}}))

(rf/reg-event-fx
  ::markdown-viewer-refresh
  (fn [cofx [_]]
    {::markdown-viewer-from-json-fx-local {::model (:sa-draw.events/model (:db cofx))
                                           ::db (:db cofx)}}))

(rf/reg-fx
  ::download-markdown-fx
  (fn [req]
    (dld/download! (::file-name req) (::markdown req))))

(rf/reg-event-fx
 ::download-markdown
 (fn [cofx [_ model-name]]
   {::download-markdown-fx {::markdown (:sa-draw.events/markdown (:db cofx))
                            ::file-name model-name
                            ::db (:db cofx)}}))
