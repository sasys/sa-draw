(ns sa-draw.views.markdown.markdown-viewer
  "
    A read only view of the markdown representation of a model.
  "
  (:require [material-ui                :as mui]
            [material-ui-icons          :as muicons]
            [re-frame.core              :as rf]
            [sa-draw.subs               :as subs]
            [sa-draw.views.markdown.markdown-viewer-events :as mdev]))


(defn markdown-view
  "Returns a card with a spec description written on it"
  []
  (fn markdown-view []
    (let [procs     (rf/subscribe [::subs/procs])]
      [:> js/ReactDraggable {:handle "strong"}
        [:> mui/Paper
          [:div
            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:strong [:> muicons/DragIndicator]]
              [:> mui/Typography {:variant "subtitle2" :style {:margin "5px" :text-decoration "underline"}}  (str "Markdown Presentation - " (:sadf/title @procs))]
              [:> mui/Tooltip {:title "Refresh"}
                [:> mui/IconButton {:onClick #(rf/dispatch [::mdev/markdown-from-json])}
                  [:> muicons/RestorePage]]]
              [:> mui/Tooltip {:title "Download markdown"}
                [:> mui/IconButton {:onClick #(rf/dispatch [::mdev/download-markdown (str (:sadf/title @procs) ".md")])}
                  [:> muicons/CloudDownload]]]
              [:strong [:> muicons/DragIndicator]]]
            [:> mui/Divider]
            [:> mui/Paper
              [:div {:id "markdown-content" :style {:padding "10px" :height "660px" :width "500px" :overflow "auto"}}]]

            ; NOTE This Divider causes a runtime failure under full optimisation build (prod),
            ; when showing the markdown viewer and I have no idea why!!
            ; It seems to cause the div above to be found when the markdown is to be
            ; injected into the child of the div above!!
            ;[:> mui/Divider]

            [:> mui/Grid {:container    true
                          :direction    "row"
                          :item         true
                          :justify      "space-between"
                          :alignItems   "center"}
              [:strong [:> muicons/DragIndicator]]
              [:> mui/Tooltip {:title "Refresh"}
                [:> mui/IconButton {:onClick #(rf/dispatch [::mdev/markdown-from-json])}
                  [:> muicons/RestorePage]]]
              [:strong [:> muicons/DragIndicator]]]]]])))
