(ns sa-draw.views.layers
  "
    Draws a selectable view of the layers in the model.
  "
  (:require [material-ui                :as mui]
            [material-ui-icons          :as muicons]
            [re-frame.core              :as rf]
            [sa-draw.events             :as ev]
            [sa-draw.subs               :as subs]
            [sa-draw.model.proto-model  :as proto-model]
            [sa-draw.global-state       :refer [layers-drawer-open layers-drawer-pinned]]
            [sa-draw.model.map-impl     :refer [map-model proc-model]]))


(defn make-model-layer-tile [node]
  (let [[_ id title description data-in data-out] (proto-model/details proc-model node)]
    [:> mui/ListItem {:dense true :button true
                      :onClick #(rf/dispatch [::ev/new-current-layer id title description data-in data-out])
                      :onMouseOver #(rf/dispatch [::ev/new-current-layer id title description data-in data-out])}
               (str id " - "  title)]))

;; TODO THIS COULD JUST BE AN OVERLOAD OF THE ONE ABOVE!!
(defn make-model-context-tile [node]
  (let [[_ _ title description data-in data-out] (proto-model/details proc-model node)]
    [:> mui/ListItem {:dense true :button true
                      :onClick #(rf/dispatch [::ev/new-current-layer "context" title description data-in data-out])
                      :onMouseOver #(rf/dispatch [::ev/new-current-layer "context" title description data-in data-out])}
               (str "context" " - "  title)]))


(defn layers-panel
  "
    Draw the layers panel.
  "
  []
  (fn []
    (let [model  @(rf/subscribe [::subs/model])
          procs (proto-model/find-process-model map-model model)
          layers (proto-model/cids proc-model procs)]
     [:> mui/Drawer {:anchor "left"
                     :variant "persistent"
                     :open (or @layers-drawer-open @layers-drawer-pinned)
                     :onClick #(reset! layers-drawer-open false)}
            (if (or @layers-drawer-open @layers-drawer-pinned)
                [:> mui/List {:style {:width "230px"
                                      :margin-top "130px"}}
                   ;Insert the context diagram
                   ^{:key "context"} [make-model-context-tile (proto-model/find-node proc-model procs "0")]
                   ;Add the other layers
                   ;NOTE we can't use a thread last macro here because the meta data
                   ;prevents it from working.
                   (for [layer layers]
                     ^{:key layer} [make-model-layer-tile (proto-model/find-node proc-model procs layer)])
                   [:> mui/Grid {:container    true
                                 :direction    "row"
                                 :item         true
                                 :justify      "flex-start"
                                 :alignItems   "center"}
                     (if @layers-drawer-pinned
                       [:> mui/Tooltip {:title "Unpin layers"}
                         [:> mui/IconButton {:onClick #(swap! layers-drawer-pinned not @layers-drawer-pinned)}
                           [:> muicons/Close]]]
                       [:> mui/Tooltip {:title "Pin layers"}
                         [:> mui/IconButton {:onClick #(swap! layers-drawer-pinned not @layers-drawer-pinned)}
                           [:> muicons/OfflinePin]]])]]
                [:div])])))
