(ns sa-draw.views.controls
  "
    NOT CURRENTLY IN USE!!!
    Draws a set of controls in a sliding drawer.
    This is to allow pre built items to be added.
  "
  (:require [material-ui           :as mui]
            [material-ui-icons     :as muicons]
            [sa-draw.events        :as ev]
            [re-frame.core         :as rf]
            [reagent.core          :as r]))


(def drawer-open (r/atom false))
(def drawer-pinned (r/atom false))

(defn control-panel
  []
  (fn []
     [:> mui/Drawer {:anchor "left"
                     :variant "persistent"
                     :open (or @drawer-open @drawer-pinned)
                     :onClick #(reset! drawer-open false)}
                 ;SET THE MARGIN_TOP OTHERWISE THE SLIDE GOES UNDER THE TOP BAR
                 [:> mui/GridList {:cols 1
                                   :cellHeight "auto"
                                   :style {:width "230px"
                                           :margin-top "130px"}}
                   [:> mui/Typography {:variant "h5" :style {:margin 5}} "Tools"]

                   [:> mui/Typography {:variant "h6" :style {:margin 5}} "Model Management"]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "prepack"
                           :onClick #(rf/dispatch [::ev/freehand-pen])}
                        [:> muicons/Edit {:style {:marginLeft 5}}]
                        "Metadata Inspector"]]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "prepack"
                           :onClick #(rf/dispatch [::ev/freehand-pen])}
                        [:> muicons/Edit {:style {:marginLeft 5}}]
                        "Metadata Editor"]]
                   [:> mui/Divider]
                   [:> mui/Typography {:variant "h6" :style {:margin 5}} "Model"]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                         {:variant "text"
                          :label   "add-proc"
                          :onClick #(rf/dispatch [::ev/show-add-proc])}
                        [:> muicons/Edit {:style {:marginLeft 5}}]
                        "Add Node"]]
                   [:> mui/Divider]
                   [:> mui/Typography {:variant "h6" :style {:margin 5}} "Drawing"]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "freehand"
                           :onClick #(rf/dispatch [::ev/freehand-mouse])}
                        [:> muicons/Mouse {:style {:marginLeft 5}}]
                        "Freehand Mouse"]]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "prepack"
                           :onClick #(rf/dispatch [::ev/freehand-pen])}
                        [:> muicons/Edit {:style {:marginLeft 5}}]
                        "Freehand Pen"]]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "prepack"
                           :onClick #(rf/dispatch [::ev/prepacked])}
                        [:> muicons/BubbleChart {:style {:marginLeft 5}}]
                        "Prepacked Process"]]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "prepack"
                           :onClick #(rf/dispatch [::ev/prepacked])}
                        [:> muicons/Redo {:style {:marginLeft 5}}]
                        "Prepacked Flow"]]
                   [:> mui/GridListTile {:cols 1}
                     [:> mui/Button
                          {:variant "text"
                           :label   "prepack"
                           :onClick #(rf/dispatch [::ev/prepacked])}
                        [:> muicons/Store {:style {:marginLeft 5}}]
                        "Prepacked Store"]]
                   [:> mui/GridListTile {:cols 1}]
                   [:> mui/Grid {:container    true
                                 :direction    "row"
                                 :item         true
                                 :justify      "flex-start"
                                 :alignItems   "center"}
                     (if @drawer-pinned
                       [:> mui/Tooltip {:title "Unpin tools"}
                         [:> mui/IconButton {:onClick #(swap! drawer-pinned not @drawer-pinned)}
                           [:> muicons/Close]]]
                       [:> mui/Tooltip {:title "Pin tools"}
                         [:> mui/IconButton {:onClick #(swap! drawer-pinned not @drawer-pinned)}
                           [:> muicons/OfflinePin]]])]]]))
