(ns sa-draw.views.dfd.svg.utility
  " SVG support utilities.
    See https://stackoverflow.com/questions/28257750/how-to-convert-html-tag-with-style-to-hiccup-react-problems
  "
  (:require  [clojure.string                 :as s]
             [clojure.walk                   :as w]
             [sa-draw.model.flows            :as flows]
             [sa-draw.model.simple-flow-impl :refer [fm]]
             [sa-draw.model.icons-impl       :refer [im]]
             [sa-draw.model.icons            :as icons]))

(set! *warn-on-infer* true)

;; FIXME: DO we still need these?
(def svg-element (atom nil))
(def svg-point (atom nil))





(defn wrap-object [type object]
  {:node object :type type})

(defn unwrap-object [object]
  (:node object))


;See https://stackoverflow.com/questions/29261304/how-to-get-the-click-coordinates-relative-to-svg-element-holding-the-onclick-lis
(defn adjust-event-posns
  " The positions form mouse events in SVG need to be corrected for the edge of the svg region.
  "
  [[x y]]
  (if  @svg-element
    (do
      (if (nil? @svg-point)
        (do
          (print "no svg-point")
          (reset! svg-point (.createSVGPoint ^js/SVG @svg-element))))
      (let [p ^js/SVGPoint (doto @svg-point (aset "x"  x)
                                            (aset "y"  y))
            ^js/DOMMatrix corrected (.matrixTransform ^js/SVGPoint p ^js/DOMMatrix (.inverse ^js/DOMMatrix (.getScreenCTM  ^js/SVG @svg-element)))]
        [(.-x ^js/DOMMatrix corrected) (.-y ^js/DOMMatrix corrected)]))
    [0 0]))


;; FIXME: DO we still need this?

(defn filter-flows
  [flows layer]
  ;flows should have a source or a destination that has the parent id as layer
  ;or source or terminate in a child of the current layer.
  ;
  (flows/sources-sinks-for-layer fm flows layer))

(defn filter-icons
  [iconsv layer]
  (icons/icons-for-layer im iconsv layer))


(defn deltaxy [[x1 y1] [x2 y2]]
   [(- x2 x1) (- y2 y1)])


;; For the functions below see the solution by sbensu here:
;; https://stackoverflow.com/questions/28257750/how-to-convert-html-tag-with-style-to-hiccup-react-problems


(defn find-transform-term
  "Given an svg transform string find the term (either translate or scale),
   return the term or nil"
  [transform term]
  (if-let [tr (->> (s/split transform #"\)")
                   (filter #(not= (s/index-of % term) nil))
                   (map s/trim)
                   (first))]
    (str tr ")")
    nil))



(defn convert-key
  "
      Given a of the form abc-xyz,
      convert them to the form abcXyz
      or just return the key.
  "
  [k]
  (let [[f e] (s/split k #"-")]
    (if e
      (str f (s/capitalize e))
      f)))


(defn convert-keys
  "
    Given a list of keys where some may be of the form abc-xyz,
    convert them to the form abcXyz

    This allows us to fix errors where react complains about bad style keys, for example :
    Warning: Unsupported style property stroke-dashoffset. Did you mean strokeDashoffset?

  "
  [style]
  (zipmap (map convert-key (keys style)) (vals style)))


(defn string->tokens
  "Takes a string with styles and parses it into properties and value tokens.
   Turns \"color:red;background:black; font-style: normal    ;font-size : 20px\" into
   (\"color\" \"red\" \"background\" \"black\" \"font-style\" \"normal\" \"font-size\" \"20px\")
  "
  [style]
  {:pre [(string? style)]
   :post [(even? (count %))]}
  (->> (s/split style #";")
       (mapcat #(s/split % #":"))
       (map s/trim)))

(defn tokens->string
  "
      Takes a map of keys and values and makes a style string.
      EG turns {\"color\" \"red\" \"background\" \"black\"}
      into \"color:red;background:black\"
  "
  [style-map]
  (apply str (interpose ";" (map #(str %1 ":" %2)  (map name (keys style-map)) (vals style-map)))))


(defn tokens->map
  "Takes a seq of tokens with the properties (even) and their values (odd)
    and returns a map of {properties values}"
  [tokens]
  {:pre [(even? (count tokens))]
   :post [(map? %)]}
  (zipmap (keep-indexed #(if (even? %1) %2) tokens)
          (keep-indexed #(if (odd? %1) %2) tokens)))


(defn style->map
  "Takes an inline style attribute string and converts it to a React Style map"
  [style]
  (tokens->map (string->tokens style)))


(defn react-style->hiccup
  "
    Transforms a style inline attribute into a style map for React
    See https://stackoverflow.com/questions/28257750/how-to-convert-html-tag-with-style-to-hiccup-react-problems
    Converts a form like {:style \"a:b;c:d\"} into {:style {:a \"b\" :c \"d\"}}
  "
  [coll]
  (w/postwalk
   (fn [x]
     (if (and (map? x) (> (count (keys x)) 0) (some #{:style} (keys x)))
       (update-in x [:style] style->map)
       x))
   coll))


(defn hiccup-style-keys->react
  "
      Goes through the hiccup tree changing style keys with names like a-b to keys like aB
  "
  [hiccup]
  (w/postwalk
    (fn [x]
      (if (and (map? x) (> (count (keys x)) 0) (some #{:style} (keys x)))
        (update-in x [:style] convert-keys)
        x))
    hiccup))


(defn hiccup-style->react
  "
      Given a hiccup form with style attributes like :style {\"a\" \"b\" \"c\" \"d\"}
      Make a hickory readable css style form like
      :style \"a:b;c:d\"  The first form cant be read when trying to process
      hiccup using hickory.
  "
  [hiccup]
  (w/postwalk
    (fn [x]
      (if (and (map? x) (> (count (keys x)) 0) (some #{:style} (keys x)))
        (update-in x [:style] tokens->string)
        x))
    hiccup))

(defn position-string->xy
  "
      Given an xy pair as string like \"x,y\"
      make a vec like [x y]
  "
  [posn-str]
  (let [parser (fn [s] (js/parseInt s))]
    (map parser (s/split posn-str #",| "))))
