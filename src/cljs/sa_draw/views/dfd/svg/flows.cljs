(ns sa-draw.views.dfd.svg.flows
  " SVG for drawing a flow.
  "
  (:require [reagent.core                          :as r]
            [re-frame.core                         :as rf]
            [sa-draw.model.simple-flow-impl        :refer [fm]]
            [sa-draw.model.flows                   :as flows]
            [sa-draw.subs                          :as subs]
            [sa-draw.config.defaults               :as df :refer [default-config]]
            [sa-draw.views.dfd.svg.events          :as svev]
            [sa-draw.views.editors.flow-events     :as fev]
            [sa-draw.events                        :as ev]
            [sa-draw.views.dfd.svg.utility         :refer [adjust-event-posns]]))

(def fill-colour   (:fill-colour   default-config))
(def fill-opacity  (:fill-opacity  default-config))
(def stroke-colour (:stroke-colour default-config))


(defn flow-point-pointer-down
  "
    Function responds to pointer down events on data flows start end and control points.

    down          atom   - the down state
    color         atom   - the fill color
    mouse-move-fn atom   - the mouse move function
    mouse-up-fn   atom   - the mouse up function
    xys           vector - a vector of atoms of xy pairs
    flow-id       atom   - the id of the flow
  "
  ([down color mouse-move-fn mouse-up-fn xys flow-id update-event]
   (reset! down true)
   (reset! color "grey")
   (reset! mouse-up-fn (fn [_]  (reset! down false)
                                (reset! color fill-colour)
                                (reset! mouse-move-fn nil)
                                (reset! mouse-up-fn nil)))
   (reset! mouse-move-fn (fn [e] (if @down (do
                                             (doseq [xy xys]
                                                 (reset! xy (adjust-event-posns [(.-clientX e) (.-clientY e)]))
                                                 (rf/dispatch [update-event @flow-id @xy]))
                                             (rf/dispatch [::ev/dirty true])))))))



(defn draw-sp
   "Draws a start point
    xy is an atom
   "
   [mouse-up-fn mouse-move-fn flow-id xy _ id]
   (let [rad (::df/cp-radius df/default-config)
         down (r/atom false)
         color (r/atom fill-colour)]
    (fn [mouse-up-fn mouse-move-fn flow-id xy _ id]
      [:g
        [:circle {:id id
                  :cx (first @xy) :cy (second @xy) :r rad
                  :stroke-width "1" :stroke "blue" :fill-opacity fill-opacity :fill @color
                  :onPointerDown (fn [_] (flow-point-pointer-down down color mouse-move-fn mouse-up-fn [xy] flow-id ::svev/update-flow-start-position))}]])))

(defn draw-ep
  "Draws an end point.
   xy is an atom
  "
  ([mouse-up-fn mouse-move-fn flow-id xy _ id]
   (let [rad (::df/cp-radius df/default-config)
         down (atom false)
         color (r/atom fill-colour)]
    (fn [mouse-up-fn mouse-move-fn flow-id xy _ id]
      [:g
        [:circle {:id id
                  :cx (first @xy) :cy (second @xy) :r rad
                  :stroke-width "1" :stroke "blue" :fill-opacity fill-opacity :fill @color
                  :onPointerDown (fn [_] (flow-point-pointer-down down color mouse-move-fn mouse-up-fn [xy] flow-id ::svev/update-flow-end-position))}]]))))



(defn draw-cp
  "Draws a single control point representing the two actual control points.
   mouse-up-fn mouse-move-fn  atoms
   flow-id                    atom
   xy1 and xy2                atoms
   id1 id2                    ids for the svg
  "

  ([mouse-up-fn mouse-move-fn flow-id xy1 xy2 _ id1 id2]
   (let [rad (::df/cp-radius df/default-config)
         down (atom false)
         color (r/atom fill-colour)]
    (fn [mouse-up-fn mouse-move-fn flow-id xy1 xy2 _ id1 id2]
      [:g
        [:circle {:id id1
                  :cx (first @xy1) :cy (second @xy1) :r rad
                  :stroke-width "1" :stroke "blue" :fill-opacity fill-opacity :fill @color
                  :onPointerDown (fn [_] (flow-point-pointer-down down color mouse-move-fn mouse-up-fn [xy1 xy2] flow-id ::svev/update-flow-cp-position))}]]))))


(defn arrow-head-path
  "Draws an arrow head at the given position"
  [id [cpx cpy] [epx epy] width]
  (let [dx (- cpx epx)
        dy (- cpy epy)
        arrow-angle (+ (Math/atan2 dx dy) Math/PI)]
    (str  "M " (- epx (* width (Math/sin (- arrow-angle (/ Math/PI 6))))) " "
                     (- epy (* width (Math/cos (- arrow-angle (/ Math/PI 6)))))
                     " L " epx " " epy
                     " L " (- epx (* width (Math/sin (+ arrow-angle (/ Math/PI 6))))) " "
                     (- epy (* width (Math/cos (+ arrow-angle (/ Math/PI 6))))))))


(defn flow-ui
  "
    A ui component that represents a data flow with the given id.
    We draw a single control point representing both actual control points.
  "

  [mouse-up-fn mouse-move-fn flow-id]
  (fn [mouse-up-fn mouse-move-fn flow-id]
      (let [ flows (rf/subscribe [::subs/flows])
             flow  (flows/find-flow fm @flows @flow-id)
             labels (flows/data-items fm flow)
             [_ _ froms tos [cp1 cp2]] (flows/details fm flow)
             [_ _ [sx sy]] (flows/endpoint-details fm (first froms))
             [_ _ [ex ey]] (flows/endpoint-details fm (first tos))
             [_ _ [p1x p1y]] (flows/endpoint-details fm cp1)
             [_ _ [p2x p2y]] (flows/endpoint-details fm cp2)
             sxy (r/atom [sx sy])
             exy (r/atom [ex ey])
             p1xy (r/atom [p1x p1y])
             p2xy (r/atom [p2x p2y])
             local-id (r/atom @flow-id)
             down (atom false)
             down-pos (atom [])
             edit-locked?  (rf/subscribe [::subs/edit-locked])]
          [:g
              [:g {:key @local-id}
                [:path {:id "1" :d (str "M " (first @sxy) " " (second @sxy) " C "(first  @p1xy) " " (second @p1xy) ", " (first  @p2xy)  " " (second @p2xy) ", " (first @exy) " " (second @exy) " ")
                        :stroke stroke-colour :fill fill-colour :fill-opacity fill-opacity
                        :onDoubleClick  (if-not @edit-locked?  (fn [e](rf/dispatch [::fev/edit-flow @local-id])))
                        :onPointerDown   (if-not @edit-locked? (fn [e]  (reset! down true)
                                                                        (reset! down-pos (adjust-event-posns [(.-clientX e) (.-clientY e)]))
                                                                        (reset! mouse-up-fn (fn [e] (reset! down false)
                                                                                                    (reset! mouse-move-fn nil)
                                                                                                    (reset! mouse-up-fn nil)))
                                                                        (reset! mouse-move-fn (fn [e]
                                                                                                  (if @down
                                                                                                    (let [pos (adjust-event-posns [(.-clientX e) (.-clientY e)])
                                                                                                          delta-x (- (first @down-pos) (first pos))
                                                                                                          delta-y (- (second @down-pos) (second pos))]
                                                                                                      (reset! sxy [(- (first @sxy) delta-x) (- (second @sxy) delta-y)])
                                                                                                      (reset! exy [(- (first @exy) delta-x) (- (second @exy) delta-y)])
                                                                                                      (reset! p1xy [(- (first @p1xy) delta-x) (- (second @p1xy) delta-y)])
                                                                                                      (reset! p2xy [(- (first @p2xy) delta-x) (- (second @p2xy) delta-y)])
                                                                                                      (reset! down-pos pos)
                                                                                                      (rf/dispatch [::svev/update-flow-position @local-id @sxy @exy @p1xy @p2xy])
                                                                                                      (rf/dispatch [::ev/dirty true]))))))

                                                               (fn [e] nil))}]

                [:path {:id "2" :d (arrow-head-path @local-id [(first @p1xy) (second @p1xy)] [(first @exy) (second @exy)] (::df/arrow-width df/default-config))
                        :stroke stroke-colour :fill fill-colour :fill-opacity fill-opacity}]]

            ;The flow labels
            (let [[x y] [(first @p1xy) (second @p1xy)]
                  label-posns (doall (map-indexed (fn [%1 %2] [%2 (+ y (* %1 15))]) labels))]

              (into [:g {:key y}] (doall (for [[lab y-posn] label-posns]
                                            [:text.flow-term {:id y-posn :x x :y y-posn} (:label (:sadf/term lab))]))))

            ;The start end and control poits only shown on a flow if we are editing
            (if-not @edit-locked?
              [:g
                [draw-cp mouse-up-fn mouse-move-fn local-id p1xy p2xy down "a" "b"]
                [draw-sp mouse-up-fn mouse-move-fn local-id sxy down "c"]
                [draw-ep mouse-up-fn mouse-move-fn local-id exy down "d"]])])))
