(ns sa-draw.views.dfd.svg.process-card
  "
   The main / top level point for drawing model features in SVG.
   The show-node function will draw the top level node and the rquired decomposition.
  "
  (:require ["@material-ui/core"                                :as mui]
            ["@material-ui/icons"                               :as muicons]
            [re-frame.core                                      :as rf]
            [reagent.core                                       :as r]
            [sa-draw.views.editors.node-events                  :as ne]
            [sa-draw.views.editors.context-object-editor-events :as coee]
            [sa-draw.views.editors.quick-flow-events            :as qfe]
            [sa-draw.views.editors.symbol-editor-events         :as seev]
            [sa-draw.subs                                       :as subs]
            [sa-draw.model.proto-model                          :as proto-model]
            [sa-draw.model.map-impl                             :refer [map-model proc-model]]
            [sa-draw.views.dfd.svg.utility                      :refer [svg-element svg-point adjust-event-posns wrap-object unwrap-object filter-flows filter-icons]]
            [sa-draw.views.dfd.svg.contexts                     :refer [producer-ui consumer-ui source-ui sink-ui context-ui]]
            [sa-draw.views.dfd.svg.flows                        :refer [flow-ui]]
            [sa-draw.views.dfd.svg.procs                        :refer [proc-ui]]
            [sa-draw.views.dfd.svg.icons                        :refer [icon-ui]]
            [sa-draw.config.defaults                            :refer [max-x max-y]]))

;
; Dispatch a draw operation based on the type of thing to be drawn
;
(defmulti draw (fn [mouse-up-fn mouse-move-fn node layout]
                  (:type node)))

(defmethod draw ::proc [mouse-up-fn mouse-move-fn node layout]
  [proc-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node))) {:sa-draw.config.defaults/proc-radius 70}])

(defmethod draw ::producer [mouse-up-fn mouse-move-fn node layout]
    [producer-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw ::consumer [mouse-up-fn mouse-move-fn node layout]
    [consumer-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw ::source [mouse-up-fn mouse-move-fn node layout]
  [source-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw ::sink [mouse-up-fn mouse-move-fn node layout]
  [sink-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw ::context [mouse-up-fn mouse-move-fn node layout]
    [context-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw ::flow [mouse-up-fn mouse-move-fn node layout]
    [flow-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw ::icon [mouse-up-fn mouse-move-fn node layout]
    [icon-ui mouse-up-fn mouse-move-fn (r/atom (:sadf/id (unwrap-object node)))])

(defmethod draw :default [mouse-up-fn mouse-move-fn node layout]
  (print "******* Default dispatch value used. *****" node) 99)


(defn make-layer-list
  " Given a layer id and the model, makes a list of all the objects to be drawn
    on that layer.  ie all the procs, flows and icons for the given layer.
  "
  [layer model]
  (if (not= layer "context")
    (let [procs (proto-model/find-process-model map-model model)
          flows (filter-flows (proto-model/find-flow-model map-model model)  layer)
          icons (filter-icons (proto-model/find-icon-model map-model model) layer)
          current-node (proto-model/find-node proc-model procs layer)
          child-nodes (atom (proto-model/children proc-model current-node))]
      (doall (concat  (for [child-node @child-nodes] (wrap-object ::proc child-node))
                      (for [flow flows] (wrap-object ::flow flow))
                      (for [icon icons] (wrap-object ::icon icon)))))))


(defn make-context-list
  "Makes a list of all the context objects to be drawn.  The process details are
   are for layer 0 but the diagram id is \"context\"
  "
  [model]
  (let [procs (proto-model/find-process-model map-model model)
        current-node (proto-model/find-node proc-model procs "0")
        context-objects (proto-model/context-objects-map map-model model)
        sources (:sadf/sources context-objects)
        sinks (:sadf/sinks context-objects)
        producers (:sadf/producers context-objects)
        consumers (:sadf/consumers context-objects)
        icons (filter-icons (proto-model/find-icon-model map-model model) "context")
        flows (doall (filter-flows (proto-model/find-flow-model map-model model) "context"))]


    (doall (concat (list (wrap-object ::context current-node))
                   (for [source sources ] (wrap-object ::source source))
                   (for [sink sinks] (wrap-object ::sink sink))
                   (for [producer producers](wrap-object ::producer producer))
                   (for [consumer consumers] (wrap-object ::consumer consumer))
                   (for [flow flows] (wrap-object ::flow flow))
                   (for [icon icons] (wrap-object ::icon icon))))))



;;More efficient here than to have the state in the re-frame db, too slow when
;;using re-frame events
(def mouse-up-fn (r/atom nil))
(def mouse-move-fn (r/atom nil))



(defn gen-svg
  "  Creates the svg for the current layer.
  "
  []
  (let [model (rf/subscribe  [::subs/model])
        layout (rf/subscribe [::subs/layout])
        current-layer (rf/subscribe [::subs/current-layer])]
    (fn []
      (let [aug-context-disp-list  (r/atom (make-context-list @model))
            aug-proc-disp-list     (r/atom (make-layer-list @current-layer @model))]

        [:g
            (into [:g {:id (str "svg-model-" @current-layer) :visibility   (if (= @current-layer "context") "visible" "hidden")}]
              (doall (for [context-item @aug-context-disp-list]
                        [draw mouse-up-fn mouse-move-fn context-item layout])))
            (into [:g {:id (str "svg-model-proc-" @current-layer)  :visibility (if (= @current-layer "context") "hidden" "visibile")}]
              (doall (for [proc-item @aug-proc-disp-list]
                        [draw mouse-up-fn mouse-move-fn proc-item layout])))]))))


(defn show-node
  "Returns an svg context onto which a node can be drawn
   svg-name is the id applied to the top level svg element.
   proc-id is the layer we are decomposing to show on this diagram.
  "
  [svg-name proc-id]
  (let [current-layer (rf/subscribe [::subs/current-layer])]
    (fn [svg-name proc-id]
      (let [edit-locked?  @(rf/subscribe [::subs/edit-locked])]
          [:> mui/Card {:raised true :style {:margin-top (str 40 "px") :margin-left "100px"}}
            [:> mui/CardActions
              [:> mui/CardContent
                [:> mui/Grid { :container    true
                               :direction    "column"
                               :item         false
                               :justify      "flex-start"
                               :alignItems   "flex-start"
                               :spacing      1}

                    [:svg {:id svg-name
                           :viewBox (str "0 0 " max-x " " max-y)
                           :width max-x :height max-y :position "absolute" :top "50" :left "55" :zindex "-1"
                           :ref (fn [el] (if (nil? @svg-element) (do (reset! svg-element el)
                                                                     (reset! svg-point (.createSVGPoint @svg-element)))))
                           :onPointerMove (fn [e] (if @mouse-move-fn (@mouse-move-fn e)))
                           :onPointerUp   (fn [e] (if @mouse-up-fn  (@mouse-up-fn)))}

                      ;Inject SVG Here
                      [gen-svg]]

                    [:> mui/Grid { :container    true
                                   :direction    "row"
                                   :item         false
                                   :justify      "space-between"
                                   :alignItems   "flex-start"
                                   :spacing      1}
                      (if (= "context" @current-layer)
                        [:div
                          [:> mui/Fab {:onClick #(rf/dispatch [::coee/create])
                                       :disabled edit-locked?
                                       :variant "extended"}
                            [:> muicons/Add] "Context Object"]]
                        [:div
                          [:> mui/Fab {:onClick (fn [] (rf/dispatch [::ne/node-edit-new  ""])) ;no id required for now
                                       :disabled edit-locked?
                                       :variant "extended"}
                            [:> muicons/Add] "Process"]])
                      [:div
                        [:> mui/Fab {:onClick #(rf/dispatch [::seev/symbol-editor-new 0])
                                     :disabled edit-locked?
                                     :variant "extended"}
                          [:> muicons/CloudCircle] "Cloud Icon"]]
                      [:div
                        [:> mui/Fab {:onClick #(rf/dispatch [::qfe/create])
                                     :disabled edit-locked?
                                     :variant "extended"}
                          [:> muicons/Redo] "Flow"]]]]]]]))))
