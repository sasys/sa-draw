(ns sa-draw.views.dfd.svg.icons
  " SVG for drawing an icon.
  "
  (:require [re-frame.core                         :as rf]
            [reagent.core                          :as r]
            [sa-draw.model.icons                   :as icons]
            [sa-draw.subs                          :as subs]
            [sa-draw.views.dfd.svg.draw            :as sdraw]
            [sa-draw.model.icons-impl              :as iconsim :refer [im]]))

(defn icon-ui
  "
      Draws the icon with the given id.
  "
  [mouse-up-fn mouse-move-fn id]
  (fn [mouse-up-fn mouse-move-fn id]
    (let [edit-locked?        (rf/subscribe [::subs/edit-locked])
          down                (r/atom false)
          down-delta-xy       (r/atom [0 0])
          layout              (rf/subscribe [::subs/layout])
          model-icons         (rf/subscribe [::subs/icons])
          prototype-icons     (rf/subscribe [::subs/default-icons])
          icon-instance       (icons/find-icon-instance im @model-icons @id)
          icon-svg            (icons/svg-for-icon im @prototype-icons icon-instance)
          icon-title          (if icon-instance (icons/title im icon-instance) "Not Found")
          xy                  (r/atom ((keyword "sadf" @id) @layout))
          nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))
          scaled-icon-svg  (if icon-svg (sdraw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 icon-title mouse-up-fn mouse-move-fn down @id nxy down-delta-xy) iconsim/empty-svg-icon)]
      (if scaled-icon-svg scaled-icon-svg iconsim/empty-svg-icon))))
