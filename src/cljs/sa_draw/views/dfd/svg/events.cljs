(ns sa-draw.views.dfd.svg.events
  "SVG related events.
  "
  (:require [re-frame.core                                      :as re-frame]
            [sa-draw.model.flows                                :as flows]
            [sa-draw.model.proto-model                          :as proto-model]
            [sa-draw.views.editors.context-object-editor-events :as coee]
            [sa-draw.model.simple-flow-impl                     :refer [fm]]
            [sa-draw.model.map-impl                             :refer [map-model]]))


(re-frame/reg-event-db
  ::proc-edit
  (fn [db [_ id]]
    (-> db
        (assoc-in [:sa-draw.events/views :sa-draw.views.editors.node-events/node-edit :sa-draw.views.editors.node-events/open?] true)
        (assoc-in [:sa-draw.events/views :sa-draw.views.editors.node-events/node-edit :sa-draw.views.editors.node-events/edit] id))))


;;FIXME: The following four are actually the same, could be simplified.

(re-frame/reg-event-db
  ::source-edit
  (fn [db [_ id type]]
    (assoc-in db [:sa-draw.events/views ::coee/context-object-editor] {::coee/open? true
                                                                       ::coee/new nil
                                                                       ::coee/edit id
                                                                       ::coee/type type})))

(re-frame/reg-event-db
  ::sink-edit
  (fn [db [_ id type]]
    (assoc-in db [:sa-draw.events/views ::coee/context-object-editor] {::coee/open? true
                                                                       ::coee/new nil
                                                                       ::coee/edit id
                                                                       ::coee/type type})))

(re-frame/reg-event-db
  ::producer-edit
  (fn [db [_ id type]]
    (assoc-in db [:sa-draw.events/views ::coee/context-object-editor] {::coee/open? true
                                                                       ::coee/new nil
                                                                       ::coee/edit id
                                                                       ::coee/type type})))

(re-frame/reg-event-db
  ::consumer-edit
  (fn [db [_ id type]]
    (assoc-in db [:sa-draw.events/views ::coee/context-object-editor] {::coee/open? true
                                                                       ::coee/new nil
                                                                       ::coee/edit id
                                                                       ::coee/type type})))

(re-frame/reg-event-db
  ::update-position
  (fn [db [_ id position]]
    (-> db
      (assoc-in [:sa-draw.events/graphics :sa-draw.events/layout (keyword "sadf" id)] position))))


(defn update-flow-state
  ([flows dragged-id dragged-type-id dragged-type endpoint-proc-id new-position]
   (let [flow (into {} (filter #(= (:sadf/id %) dragged-id) flows))]
     (update-flow-state flows flow dragged-id dragged-type-id dragged-type endpoint-proc-id new-position)))
  ([flows flow dragged-id dragged-type-id dragged-type endpoint-proc-id new-position]
   (let [others (filter #(not= (:sadf/id %) dragged-id) flows)
         points (dragged-type flow)
         updated-points
            (map #(if (= (:sadf/id %) dragged-type-id) {:sadf/posn new-position
                                                        :sadf/id dragged-type-id
                                                        :sadf/proc-id endpoint-proc-id}
                      %)
                points)
         new-flow (assoc-in flow [dragged-type] (into [] updated-points))]
        (conj others new-flow))))


(defn point-details [point]
  [(:sadf/id point) (:sadf/proc-id point) (:sadf/posn point)])


(re-frame/reg-event-db
  ::update-flow-position
  (fn [db [_ id new-start new-end new-cp1 new-cp2]]
    (let [flows (proto-model/find-flow-model map-model (:sa-draw.events/model db))
          flow (flows/find-flow fm flows id)
          [id _ sources sinks cps _] (flows/details fm flow)
          [source-id source-proc-id _] (point-details (first sources))
          [sink-id sink-proc-id _] (point-details (first sinks))
          [control-point-1-id control-point-1-proc-id _] (point-details (first cps))
          [control-point-2-id control-point-2-proc-id _] (point-details (second cps))]
      (assoc-in db [:sa-draw.events/model :sadf/flows]
                  (-> flows
                      (update-flow-state id source-id :sadf/source source-proc-id new-start)
                      (update-flow-state id sink-id :sadf/sink sink-proc-id new-end)
                      (update-flow-state id control-point-1-id :sadf/control-point control-point-1-proc-id new-cp1)
                      (update-flow-state id control-point-2-id :sadf/control-point control-point-2-proc-id new-cp2))))))


(re-frame/reg-event-db
  ::update-flow-start-position
  (fn [db [_ id new-start]]
    (let [flows (proto-model/find-flow-model map-model (:sa-draw.events/model db))
          flow (flows/find-flow fm flows id)
          [id _ sources _ _ _] (flows/details fm flow)
          [source-id source-proc-id _] (point-details (first sources))]
      (assoc-in db [:sa-draw.events/model :sadf/flows]
                  (update-flow-state  flows flow id source-id :sadf/source source-proc-id new-start)))))


(re-frame/reg-event-db
  ::update-flow-end-position
  (fn [db [_ id new-end]]
    (let [flows (proto-model/find-flow-model map-model (:sa-draw.events/model db))
          flow (flows/find-flow fm flows id)
          [id _ _ sinks _ _] (flows/details fm flow)
          [sink-id sink-proc-id _] (point-details (first sinks))]
      (assoc-in db [:sa-draw.events/model :sadf/flows]
                  (update-flow-state flows flow id sink-id :sadf/sink sink-proc-id new-end)))))

;
; NOTE CP! and CP2 locked together here!!
;

(re-frame/reg-event-db
  ::update-flow-cp-position
  (fn [db [_ id new-cp1]]
    (let [flows (proto-model/find-flow-model map-model (:sa-draw.events/model db))
          flow (flows/find-flow fm flows id)
          [id _ _ _ cps _] (flows/details fm flow)
          [control-point-1-id control-point-1-proc-id _] (point-details (first cps))
          [control-point-2-id control-point-2-proc-id _] (point-details (second cps))]
      (assoc-in db [:sa-draw.events/model :sadf/flows]
                  (-> flows
                      (update-flow-state flow id control-point-1-id :sadf/control-point control-point-1-proc-id new-cp1)
                      (update-flow-state flow id control-point-2-id :sadf/control-point control-point-2-proc-id new-cp1))))))
