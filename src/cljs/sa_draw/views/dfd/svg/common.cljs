(ns sa-draw.views.dfd.svg.common
  " Common drawing operations for boxes, circles etc customised for our purpose.
  "
  (:require   [re-frame.core                         :as rf]
              [reagent.core                          :as r]
              [sa-draw.views.dfd.svg.events          :as svev]
              [sa-draw.events                        :as ev]
              [sa-draw.views.dfd.svg.utility         :refer [adjust-event-posns deltaxy]]
              [sa-draw.config.defaults               :refer [default-config]]))

;The fill color cannot be transparent, this is not known to tools like inkscape or
;libre offce.
;If you make the fill color none, the body of the shape cannot be dragged
;We make the fill color any color but set the opacity to 0.0.  This gives the
;shape a fill but makes it transparent, then it can be grabbed.

(def fill-colour   (:fill-colour   default-config))
(def fill-opacity  (:fill-opacity  default-config))
(def stroke-colour (:stroke-colour default-config))

(defn draw-id
  "xy is a position like [x y] - not an atom"
  ([xy id display-id]
   (draw-id xy id display-id 0 0))
  ([xy id display-id offset-x offset-y]
   (let [[x y] xy]
      [:text.proc-id {:id id :x (+ x offset-x) :y (+ y offset-y) :text-anchor "middle" :alignment-baseline "middle"} display-id])))


(defn draw-title
  "xy is a position like [x y] - not an atom"
  ([xy id title]
   (draw-title xy id title 0 0))
  ([xy id title offset-x offset-y]
   (let [[x y] xy]
     [:text.icon-title {:id id :x (+ x offset-x) :y (+ y offset-y) :text-anchor "middle" :alignment-baseline "middle"} title])))


(defn draw-box
  "Draws a box centered on xy with a side of length 2 * half-side.
   xy a clojure atom of [x y]"
  [mouse-up-fn mouse-move-fn xy id half-side edit-locked? edit-event edit-type]
  (let [color (r/atom fill-colour)
        down (r/atom false)
        down-delta-xy (r/atom [0 0])
        local-id (r/atom id)
        local-edit-event (r/atom edit-event)
        local-edit-type (r/atom edit-type)]
    (fn [mouse-up-fn mouse-move-fn xy id half-side edit-locked? edit-event edit-type]
      (let [[x y] @xy]

        (if-not @edit-locked?
          [:rect {:x x :y y :width (* 2 half-side) :height (* 2 half-side)
                  :stroke stroke-colour :fill @color :fill-opacity fill-opacity
                  :onPointerDown (fn [e] (reset! down true)
                                         (reset! color "grey")
                                         (reset! down-delta-xy (deltaxy @xy (adjust-event-posns [(.-clientX e) (.-clientY e)])))
                                         (reset! mouse-up-fn (fn [e] (reset! down false)
                                                                     (reset! color fill-colour)
                                                                     (reset! down-delta-xy [0 0])
                                                                     (reset! mouse-move-fn nil)
                                                                     (reset! mouse-up-fn nil)))
                                         (reset! mouse-move-fn (fn [e]
                                                                   (if @down
                                                                     (do
                                                                       (reset! xy (deltaxy @down-delta-xy (adjust-event-posns [(.-clientX e) (.-clientY e)])))
                                                                       (rf/dispatch [::svev/update-position id (deltaxy @down-delta-xy (adjust-event-posns [(.-clientX e) (.-clientY e)]))])
                                                                       (rf/dispatch [::ev/dirty true]))))))
                  :onDoubleClick  (if edit-event (if-not @edit-locked?  (fn [e](rf/dispatch [@local-edit-event @local-id @local-edit-type]))))}]
          [:rect {:x x :y y :width (* 2 half-side) :height (* 2 half-side)
                  :stroke stroke-colour :fill-opacity fill-opacity :fill @color}])))))


(defn draw-circ
  "Draws a circle centered on xy with a radius of rad
   xy a clojure atom of [x y]
   id a unique id to allow this element to be included in lists of elements"
  [mouse-up-fn mouse-move-fn xy id rad proc edit-locked? edit-event edit-type]
  (let [color (r/atom fill-colour)
        down (r/atom false)
        down-delta-xy (r/atom [0 0])
        local-id (r/atom id)
        local-edit-event (r/atom edit-event)
        local-edit-type (r/atom edit-type)]

    (fn [mouse-up-fn mouse-move-fn xy id rad proc edit-locked? edit-event edit-type]
      (if-not @edit-locked?
         [:circle {:id id :cx (first @xy) :cy (second @xy) :r rad :stroke stroke-colour :fill-opacity fill-opacity :fill @color :onPointerDown (fn [e]  (reset! down true)
                                                                                                                                                        (reset! color "grey")
                                                                                                                                                        (reset! down-delta-xy (deltaxy @xy (adjust-event-posns [(.-clientX e) (.-clientY e)])))
                                                                                                                                                        (reset! mouse-up-fn (fn [e] (reset! down false)
                                                                                                                                                                                    (reset! color fill-colour)
                                                                                                                                                                                    (reset! mouse-move-fn nil)
                                                                                                                                                                                    (reset! mouse-up-fn nil)))

                                                                                                                                                        (reset! mouse-move-fn (fn [e]
                                                                                                                                                                                  (if @down
                                                                                                                                                                                    (do
                                                                                                                                                                                     (reset! xy (deltaxy @down-delta-xy (adjust-event-posns [(.-clientX e) (.-clientY e)])))
                                                                                                                                                                                     (rf/dispatch [::svev/update-position id (deltaxy @down-delta-xy (adjust-event-posns [(.-clientX e) (.-clientY e)]))])
                                                                                                                                                                                     (rf/dispatch [::ev/dirty true]))))))

                   :onDoubleClick  (if edit-event (if-not @edit-locked?  (fn [e](rf/dispatch [@local-edit-event id @local-edit-type])))
                                                  (if-not @edit-locked?  (fn [e](rf/dispatch [::svev/proc-edit @local-id]))))}]

         [:circle {:id id :cx (first @xy) :cy (second @xy) :r rad :stroke stroke-colour :fill-opacity fill-opacity :fill @color}]))))
