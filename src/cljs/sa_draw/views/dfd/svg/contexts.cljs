(ns sa-draw.views.dfd.svg.contexts
  " Creates the svg for context diagram objects.
    This includes the context object itself and any icon variants.
    Includes the context sources, sinks, producers, consumers, and their
    associated icons.
  "
  (:require [reagent.core                       :as r]
            [re-frame.core                      :as rf]
            [sa-draw.subs                       :as subs]
            [sa.sa-store.store-factory          :as sf]
            [sa-draw.model.utils                :as ut]
            [sa-draw.views.editors.node-events  :as ne]
            [sa-draw.config.subs                :as cfsubs]
            [sa-draw.views.editors.dispatcher   :as coe]
            [sa-draw.views.dfd.svg.events       :as svev]
            [sa-draw.views.dfd.svg.draw         :as draw]
            [sa-draw.events                     :as ev]
            [clojure.string               :refer [blank? trim]]
            [sa-draw.model.map-impl       :refer [proc-model context-model map-model]]
            [sa-draw.model.proto-model    :refer [find-process-model find-producer find-context-object-by-id context-object-title context-object-icon link context-object-details details title]]
            [sa-draw.views.dfd.svg.common :refer [draw-box draw-circ draw-id draw-title]]
            [sa-draw.config.defaults      :refer [default-config]]
            [sa-draw.model.icons                   :as icons]
            [sa-draw.model.icons-impl              :as iconsim :refer [im]]))


(def fill-colour             (:fill-colour       default-config))
(def fill-opacity            (:fill-opacity      default-config))
(def model-link-colour       (:link-colour       default-config))
(def model-link-fill-colour  (:link-fill-colour  default-config))
(def model-link-fill-opacity (:link-fill-opacity default-config))
(def stroke-colour           (:stoke-colour      default-config))

(defn producer-ui
  "Draws a single producer.
   mouse-up-fn and mouse-move-fn are functions called when the mouse is moved or raised on this drawing.
   id is an atom
  "
  [mouse-up-fn mouse-move-fn id]
  (let [edit-locked?  (rf/subscribe [::subs/edit-locked])
        show-inter-model-links (rf/subscribe [::cfsubs/show-inter-model-links])]
    (fn [mouse-up-fn mouse-move-fn id]
       (let [context-objects (rf/subscribe [::subs/contexts])
             summaries @(rf/subscribe [::subs/browser-summaries])
             producers (:sadf/producers @context-objects)
             producer (find-context-object-by-id context-model  producers @id)
             title  (context-object-title context-model producer)
             link (link context-model producer)
             link-summary (if link (ut/find-summary link summaries))
             link-title (if link-summary (:sadf/name link-summary))
             link-summary (if link-title (str link-title "\n=======\n" (:sadf/summary link-summary)))
             layout (rf/subscribe [::subs/layout])
             xy (r/atom ((keyword "sadf" @id) @layout))
             local-id (r/atom @id)
             [x y] @xy
             rad 50
             third-dia (/ (* 2 rad) 3)
             prototype-icons     (rf/subscribe [::subs/default-icons])
             icon                (context-object-icon context-model producer)
             icon-svg            (icons/svg-for-icon im @prototype-icons icon)
             nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))]
         (if producer
           (if (and icon icon-svg)
             (let [down                (r/atom false)
                   down-delta-xy       (r/atom [0 0])]
               [draw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 title mouse-up-fn mouse-move-fn down @id nxy down-delta-xy ::svev/producer-edit ::coe/prod])
             [:g
               [draw-circ mouse-up-fn mouse-move-fn xy @local-id rad producer edit-locked? ::svev/producer-edit ::coe/prod]
               [draw-title @xy @id title 0 (* 1.5 rad)]
               (if (and @show-inter-model-links (not (blank? link)))
                 [:g
                   [:path {:id id
                           :onClick (if (not (blank? link)) (fn [model-id] (rf/dispatch [::ev/load-model (trim link) ::sf/key-store])))
                           :stroke stroke-colour
                           :fill (if (blank? link) fill-colour model-link-fill-colour)
                           :fill-opacity model-link-fill-opacity
                           :d (str "M " x " " (+ y (/ third-dia 2)) " L " (- x third-dia) " " (+ y (* (/ 4 6) rad) ) " L " x " " (+ y rad) " L " x " " (+ y (/ third-dia 2)))} (if link-title [:title link-summary])]
                   [:text {:id id :x (- x third-dia) :y (+ y (* (/ 4 6) rad) ) :text-anchor "end" :alignment-baseline "middle" } (:sadf/name (ut/find-summary link summaries))]])]))))))

(defn consumer-ui
  "Draws a single consumer.
   mouse-up-fn and mouse-move-fn are functions called when the mouse is moved or raised on this drawing.
   id is an atom
  "
  [mouse-up-fn mouse-move-fn id]
  (let [edit-locked?  (rf/subscribe [::subs/edit-locked])
        show-inter-model-links (rf/subscribe [::cfsubs/show-inter-model-links])]
    (fn [mouse-up-fn mouse-move-fn id]
       (let [context-objects (rf/subscribe [::subs/contexts])
             summaries @(rf/subscribe [::subs/browser-summaries])
             consumers (:sadf/consumers @context-objects)
             consumer (find-context-object-by-id context-model consumers @id)
             title  (context-object-title context-model consumer)
             link (link context-model consumer)
             link-summary (if link (ut/find-summary link summaries))
             link-title (if link-summary (:sadf/name link-summary))
             link-summary (if link-title (str link-title "\n=======\n" (:sadf/summary link-summary)))
             layout (rf/subscribe [::subs/layout])
             xy (r/atom ((keyword "sadf" @id) @layout))
             local-id (r/atom @id)
             [x y] @xy
             rad 50
             third-dia (/ (* 2 rad) 3)
             prototype-icons     (rf/subscribe [::subs/default-icons])
             icon                (context-object-icon context-model consumer)
             icon-svg            (icons/svg-for-icon im @prototype-icons icon)
             nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))]
         (if consumer
           (if (and icon icon-svg)
             (let [down                (r/atom false)
                   down-delta-xy       (r/atom [0 0])]
               [draw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 title mouse-up-fn mouse-move-fn down @id nxy down-delta-xy ::svev/consumer-edit ::coe/cons])
             [:g
               [draw-circ mouse-up-fn mouse-move-fn xy @local-id rad consumer edit-locked? ::svev/consumer-edit ::coe/cons]
               [draw-title @xy @id title 0 (* 1.5 rad)]
               (if (and @show-inter-model-links (not (blank? link)))
                 [:g
                   [:path {:id id
                           :onClick (if (not (blank? link)) (fn [model-id] (rf/dispatch [::ev/load-model (trim link) ::sf/key-store])))
                           :stroke stroke-colour
                           :fill (if (blank? link) fill-colour model-link-fill-colour)
                           :fill-opacity model-link-fill-opacity
                           :d (str "M " x " " (+ y (/ third-dia 2)) " L " (+ x third-dia) " " (+ y (* (/ 4 6) rad) ) " L " x " " (+ y rad) " L " x " " (+ y (/ third-dia 2)))} (if link-title [:title link-summary])]
                   [:text {:id id :x (+ x third-dia) :y (+ y (* (/ 4 6) rad) ) :text-anchor "start" :alignment-baseline "middle" } (if link-title link-title "not found")]])]))))))


(defn source-ui
  "Draws a single source.
   mouse-up-fn and mouse-move-fn are functions called when the mouse is moved or raised on this drawing.
   id is an atom
  "
  [mouse-up-fn mouse-move-fn id]
  (let [edit-locked?  (rf/subscribe [::subs/edit-locked])]
    (fn [mouse-up-fn mouse-move-fn id]
       (let [context-objects (rf/subscribe [::subs/contexts])
             sources (:sadf/sources @context-objects)
             source (find-context-object-by-id context-model sources @id)
             title  (context-object-title context-model source)
             layout (rf/subscribe [::subs/layout])
             xy (r/atom ((keyword "sadf" @id) @layout))
             local-id (r/atom @id)
             rad 50
             half-side (Math/sqrt (* 0.5 rad rad))
             prototype-icons     (rf/subscribe [::subs/default-icons])
             icon                (context-object-icon context-model source)
             icon-svg            (icons/svg-for-icon im @prototype-icons icon)
             nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))]
          (if source
            (if (and icon icon-svg)
              (let [down                (r/atom false)
                    down-delta-xy       (r/atom [0 0])]
                [draw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 title mouse-up-fn mouse-move-fn down @id nxy down-delta-xy ::svev/source-edit ::coe/source])

              [:g
                [draw-box mouse-up-fn mouse-move-fn xy @local-id half-side edit-locked? ::svev/source-edit ::coe/source]
                [draw-title @xy @id title half-side (* 2.5 half-side)]]))))))


(defn sink-ui
  "Draws a single sink.
   mouse-up-fn and mouse-move-fn are functions called when the mouse is moved or raised on this drawing.
   id is an atom
  "
  [mouse-up-fn mouse-move-fn id]
  (let [edit-locked?  (rf/subscribe [::subs/edit-locked])]
    (fn [mouse-up-fn mouse-move-fn id]
      (let [context-objects (rf/subscribe [::subs/contexts])
            sinks (:sadf/sinks @context-objects)
            sink (find-context-object-by-id context-model sinks @id)

            title  (context-object-title context-model sink)
            layout (rf/subscribe [::subs/layout])
            xy (r/atom ((keyword "sadf" @id) @layout))
            local-id (r/atom @id)
            rad                 50
            half-side           (Math/sqrt (* 0.5 rad rad))
            prototype-icons     (rf/subscribe [::subs/default-icons])
            icon                (context-object-icon context-model sink)
            icon-svg            (icons/svg-for-icon im @prototype-icons icon)
            nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))]
        (if sink
           (if (and icon icon-svg)
             (let [down                (r/atom false)
                   down-delta-xy       (r/atom [0 0])]
               [draw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 title mouse-up-fn mouse-move-fn down @id nxy down-delta-xy ::svev/sink-edit ::coe/sink])
             [:g
               [draw-box mouse-up-fn mouse-move-fn xy @local-id half-side edit-locked? ::svev/sink-edit ::coe/sink]
               [draw-title @xy @id title half-side (* 2.5 half-side)]]))))))


(defn context-ui
  "Draws a context bubble
   mouse-up-fn and mouse-move-fn are functions called when the mouse is moved or raised on this drawing.
   id is an atom
  "
  [mouse-up-fn mouse-move-fn id]
  (let [edit-locked?  (rf/subscribe [::subs/edit-locked])]  ;context bubble is always locked.
    (fn [mouse-up-fn mouse-move-fn id]
      (let [model (rf/subscribe [::subs/model])
            context (find-process-model map-model @model)
            title (title proc-model context)
            rad 170
            layout (rf/subscribe [::subs/layout])
            xy (r/atom ((keyword "sadf" @id) @layout))

            prototype-icons     (rf/subscribe [::subs/default-icons])
            icon                (:sadf/icon context)

            nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))]

          (if icon
            (let [icon-svg            (icons/svg-for-icon im @prototype-icons icon)
                  down                (r/atom false)
                  down-delta-xy       (r/atom [0 0])]
              [draw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 (str @id " - " title) mouse-up-fn mouse-move-fn down @id nxy down-delta-xy ::ne/node-edit-edit])
            [:g {:key @id}
              [draw-circ mouse-up-fn mouse-move-fn xy @id rad context edit-locked? ::ne/node-edit-edit]
              [draw-id @xy  (str "i-" @id)  @id   0 (* -0.5 rad)]
              [draw-title @xy (str "t-" @id) title]])))))
