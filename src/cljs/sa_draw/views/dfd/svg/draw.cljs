(ns sa-draw.views.dfd.svg.draw
  "
    # Architecture Icons

    This code supports the import and storage within a model of a set of cloud icons.
    These are imported inside the model and can be deployed onto the model.

    ## kubernetes

    Sets of Kubernetes SVG symbols found at :
    https://github.com/octo-technology/kubernetes-icons

    All are inkscape drawn and contain inkscape tags.  The full stack sbg import
    routine below removes these.

    The default k8s set only includes the labled icons.

    ## AWS

    Sets of AWS SVG Symbols found at :
    https://aws.amazon.com/architecture/icons/

    With the svgz zipped up here :
    https://d1.awsstatic.com/webteam/architecture-icons/AWS-Architecture-Icons_SVG_20200131.abfc4fb34450d2294f8c65d2dcc9ea1602b6a449.zip

    ## Google Cloud Platform

    https://cloud.google.com/icons/files/gcp-icons.zip

    This code loads up the icons from the above sources on demand, process the xml to remove unwanted namespaces and
    attributes and attemptys to position them on the icon pallet.

  "
  (:require [re-frame.core                            :as rf]
            [reagent.core                             :as r]
            [clojure.string                            :as st]
            [hickory.core                             :as hick]
            [hickory.zip                              :as hzip]
            [hickory.convert                          :as hickcon]
            [sa-draw.views.dfd.svg.events             :as svev]
            [clojure.zip                              :as zip]
            [sa-draw.views.dfd.svg.utility            :as u :refer [adjust-event-posns hiccup-style-keys->react find-transform-term string->tokens tokens->map react-style->hiccup hiccup-style->react adjust-event-posns deltaxy]]
            [sa-draw.events                           :as ev]
            [sa-draw.views.editors.icon-editor-events :as icev]
            [clojure.walk                             :as w]))


(def url-1 "https://raw.githubusercontent.com/octo-technology/kubernetes-icons/master/svg/infrastructure_components/labeled/etcd.svg")

;The default scale factor
(def default-scale-factor 2.5)

;The default translate applied to the svg element
(def default-translate [0.0 0.0])

;This is the list of keys we might find in some hiccup derived from
;an external source (perhaps created by inkscape)
(def default-unwanted-keys [:inkscape:groupmode
                            :inkscape:label
                            :xmlns:inkscape
                            :xmlns:dc
                            :xmlns:sodipodi
                            :sodipodi:role
                            :xmlns:cc
                            :inkscape:connector-curvature
                            :sodipodi:nodetypes
                            :xml:space
                            :inkscape:exportXdpi
                            :inkscape:exportYdpi
                            :inkscape:exportxdpi
                            :inkscape:exportydpi
                            :inkscape:cx
                            :inkscape:window-width
                            :inkscape:window-height
                            :inkscape:document-units
                            :inkscape:window-x
                            :inkscape:pageopacity])




;; FIXME: This is just not right, move to re-frame state model
(def mouse-up-fn (r/atom nil))
(def mouse-move-fn (r/atom nil))
(def mouse-down-fn (r/atom nil))

(def default-view-box {})

;width and height match default viewport (viewBox) all of viewport is visible.
;this is an outer svg element at this point so x and y do not effect the position of the svg element
;hence no x and y.
(def default-svg-block   [:svg {:width 100 :height 100}]);  :zindex "-1"}])


(defn valid-path
  [node]
  (and (not= (get-in node [:attrs :d]) nil) (not= (get-in node [:attrs :d]) "")  (not= (get-in node [:attrs :d]) " ")))


(defn fix-attr-case
  "
    Converts the svg attribute viewbox to viewBox.
    Hickory seems to convert viewBox to viewbox which creates a warning in
    Firefox and Chrome.
  "
  [h]
  (st/replace h "viewbox" "viewBox"))



(defn find-first-coord-pair
  "
      Given a zipper in hickory form find the first pair of coordinates of the
      first path element that has an actual path, if not return nil to
      indicate no pair was found.
  "
  [hickory-zipper]
  (loop [n hickory-zipper]
    (if (or (nil? n) (zip/end? n))
      nil
      (let [node (zip/node n)]
        (if (valid-path node)
          (second (st/split (get-in node [:attrs :d]) #" "))
          (recur (zip/next n)))))))


(defn svg->hiccup
  "
      Given a fragment of svg, convert it to hiccup.
  "
  [svg]
  (map hick/as-hiccup (hick/parse-fragment svg)))


(defn hiccup-svg->hickoryzipper
  "
      Given a hickup form of svg, convert it to a hickory zipper.
  "
  [hiccup-svg]
  (-> hiccup-svg
      hickcon/hiccup-fragment-to-hickory
      first
      hzip/hickory-zip))




(defn add-translate-group
  "
      Adds an enclosing translate group over the node provided
      of the form [:g {:transform \"translate(x y)\"}]
  "
  [node [x y]]
  {:type :element :attrs {:transform (str "translate(" x ", " y")")} :tag :g :content [node]})

(defn translate
  "
      The node must have an :attrs {} map ie a hickory attrs map.  We add in a translate factor
      string to the transform attribute if present or add in a transform attribue as well
      if its missing.  We replace any existing.
  "
  [node [tfx tfy]]
  (if-let [with-transform (get-in node [:attrs :transform])]
    (if-let [scale (u/find-transform-term with-transform "scale")]
      (assoc-in node [:attrs :transform] (str "translate("tfx", "tfy ") " scale))
      (assoc-in node [:attrs :transform] (str "translate("tfx", "tfy ") ")))
    (assoc-in node [:attrs :transform] (str "translate("tfx", "tfy ")"))))


(defn add-id-group-wrapper
  "
      Adds a group with the given id.
  "
  [node id]
  {:type :element :attrs {:id id} :tag :g :content [node]})

(defn add-title
  "
    Add the title to the svg.
    The svg must have a text node for the title to be inserted.
    The text element MUST have the id \"title\"
  "
  [node id title]
  (w/postwalk
   (fn [x]
     (if (and (map? x) (> (count (keys x)) 0) (some #{:text} (vals x)) (= (get-in x [:attrs :id]) "title"))
       (update-in x [:content] [title])
       x))
   node))


; (defn remove-title
;  "
;    The text element MUST have the id \"title\"
;  "
;  [node]
;  (w/postwalk
;   (fn [x]
;     (if (and (map? x) (> (count (keys x)) 0) (some #{:text} (vals x)) (= (get-in x [:attrs :id]) "title"))
;       (update-in x [:content] [title])
;       x))
;   node))


(defn add-context-title
  "
    Add the title string as a child of the node.
  "
  [node title]
  (let [title-element {:type :element
                       :attrs {:class "icon local sbnl context"
                               :id "title"
                               :x "150"
                               :y "200"
                               :text-anchor "middle"}
                       :tag :text
                       :content [title]}]
    (update-in node [:content] conj title-element)))




(defn add-scale-factor
  "
      The node must have an :attrs {} map ie a hickory attrs map.  We add in a scale factor
      scale string to the translate attribute if present or add in a translate attribue as well
      if its missing.
  "
  [node sf]
  (if-let [with-transform (get-in node [:attrs :transform])]
    (if-let [trans (u/find-transform-term with-transform "translate")]
      (assoc-in node [:attrs :transform] (str trans " scale("sf", "sf ")"))
      (assoc-in node [:attrs :transform] (str " scale("sf", "sf ")")))
    (assoc-in node [:attrs :transform] (str "scale("sf", "sf ")"))))


(defn add-click-funcs
  "
      edit-locked? a boolean atom indicating if editing is loacked.
  "
  ([node edit-locked? mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event]
   (add-click-funcs node edit-locked? mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event nil))
  ([node edit-locked? mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event event-parameter]
   (-> node
       (assoc-in [:attrs :onDoubleClick] (fn [e] (if-not @edit-locked? (rf/dispatch [double-click-event id event-parameter]))))
       (assoc-in [:attrs :onPointerDown] (fn [e] (if-not  @edit-locked?
                                                   (do
                                                     (reset! down true)
                                                     (reset! down-delta-xy (deltaxy @xy (adjust-event-posns [(.-clientX e) (.-clientY e)])))
                                                     (reset! mouse-up-fn (fn [e] (reset! down false)
                                                                                 (reset! down-delta-xy [0 0])
                                                                                 (reset! mouse-move-fn nil)
                                                                                 (reset! mouse-up-fn nil)))

                                                     (reset! mouse-move-fn (fn [e]
                                                                               (if @down
                                                                                 (do
                                                                                   (reset! xy (deltaxy @down-delta-xy (adjust-event-posns [(.-clientX e) (.-clientY e)])))
                                                                                   (rf/dispatch [::svev/update-position id (deltaxy @down-delta-xy (adjust-event-posns [(.-clientX e) (.-clientY e)]))])
                                                                                   (rf/dispatch [::ev/dirty true]))))))))))))




(defn edit-remove-attrs
  "Removes the specified list of attributes from the given node"
  [node ks]
  (assoc node :attrs (apply dissoc (:attrs node) ks)))

(defn filter-kvs
  "Given a hickory zipper remove any key attributes where the key matches any of the ks
   Leaves the loc at :end.
   You need to make a new zipper from this to do other processing
  "
  [hickory-zipper f ks]
  (loop [n hickory-zipper]
    (if (or (nil? n) (zip/end? n))
      n
      (let [node (zip/node n)]
        (if (not= (:attrs node) nil)
          (recur (zip/next (zip/edit n f ks)))
          (recur (zip/next n)))))))

(defn find-group
  "Given a hickory zipper return the location which is the outermost <g>"
  [hickory-zipper]
  (loop [n hickory-zipper]
     (if (or (= :g (:tag (zip/node n))) (nil? n) (zip/end? n))
      n
      (recur (zip/next n)))))

(defn find-group-node
  "Given some svg as a string find the hickory node which is the outer <g>"
  [svg]
  (-> svg
      hick/parse
      hick/as-hickory
      hzip/hickory-zip
      find-group
      zip/node))

(defn hickory-to-svg-hiccup
  ([hickory-node]
   "
    Use the default svg block as the target for the hickory node.
   "
   (hickcon/hickory-to-hiccup hickory-node))
  ([hickory-node view-box-map]
   "
     Merge the default svg with the given view box map and the hickory node
   "
   (as-> default-svg-block svg-block
      (second svg-block)
      (merge svg-block view-box-map)
      [:svg svg-block]
      (into svg-block [(hickcon/hickory-to-hiccup hickory-node)]))))


(defn add-highlight-box
  "Given a preprocessed block of svg, add a box around it for scaling."
  [svg]
  svg)

(defn remove-highlight-box
  "Given a preprocessed block of svg, remove the highlight box if present."
  [svg]
  svg)

(defn scale
  "
    Given a preprocessed block of svg, scale it by the given scale factor as a %age
    This adds a scale like scale(x%, y%) to any transform on the outer g: element
    or adds a transform if none is present.
  "
  [svg x-scale-factor y-scale-factor]

  svg)

(defn debug-svg
  ([svg]
   (debug-svg svg "0"))
  ([svg id]
   (debug-svg svg id ""))
  ([svg id msg]
   (print (str "\n\n DEBUG SVG - "msg" ID : " id " \n\n" svg "\n\n"))
   svg))

(defn full-stack-svg-import
  "
   Given a chunk of svg - say created by inkscape, remove all the
   inkskape and other attributes and namespaces, find the svg group and
   convert the result to hiccup.
   sf is a single scale factor applied to the svg x and y
   [tx ty] is a translation of x and y applied ot the svg element
   view-box is the view-box applied to the svg like {:viewBox \"0, 0, 24, 24\"}
  "
  [svg sf [tx ty] unwanted-keys view-box]
  (-> (svg->hiccup svg)
      hiccup-svg->hickoryzipper
      find-group
      zip/node
      hzip/hickory-zip
      (zip/edit add-scale-factor sf)
      (zip/edit translate [tx ty])
      zip/node
      hzip/hickory-zip
      (filter-kvs edit-remove-attrs unwanted-keys)
      zip/node
      (hickory-to-svg-hiccup view-box) ;material-ui Sized for SvgIcon
      react-style->hiccup
      hiccup-style-keys->react))


(defn scale-only-icon
  [hiccup-svg sf]
  (->  (hiccup-style->react hiccup-svg) ;Make the atrribute strings
       vector
       hickcon/hiccup-fragment-to-hickory
       first
       hzip/hickory-zip
       find-group
       zip/node
       hzip/hickory-zip
       (zip/edit add-scale-factor sf)
       zip/node
       hzip/hickory-zip
       find-group
       zip/node
       hickory-to-svg-hiccup
       react-style->hiccup))



(defn adjust-scale-factor-for-diagram
  "
      When a prototype icon is moved onto the main diagram we need to scale it.

      sf is the scale factor like [sx sy] the inceases the size of the icon to
      suite the display.
      xy is an atom - the position to draw the icon
      id is a unique id string

      down-delta--xy is an atom containing the mouse movement, supplied to the
      move functions.

      icon-title is the text for the icon

      We add in the mouse up, mv and down function atoms


  "
  ([hiccup-svg edit-locked? sf icon-title mouse-up-fn mouse-move-fn down id xy down-delta-xy]
   (adjust-scale-factor-for-diagram hiccup-svg edit-locked? sf icon-title mouse-up-fn mouse-move-fn down id xy down-delta-xy ::icev/edit-icon))

  ([hiccup-svg edit-locked? sf icon-title mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event]
   (adjust-scale-factor-for-diagram hiccup-svg edit-locked? sf icon-title mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event nil))

  ([hiccup-svg edit-locked? sf icon-title mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event event-parameter]
   (->  (hiccup-style->react hiccup-svg) ;Make the atrribute strings
        vector
        hickcon/hiccup-fragment-to-hickory
        first
        hzip/hickory-zip
        find-group
        zip/node
        hzip/hickory-zip
        (zip/edit add-scale-factor sf)
        (zip/edit add-click-funcs edit-locked? mouse-up-fn mouse-move-fn down id xy down-delta-xy double-click-event event-parameter)
        (zip/edit add-title id icon-title)
        (zip/edit add-translate-group @xy)
        (zip/edit add-id-group-wrapper id)
        zip/node
        hzip/hickory-zip
        find-group
        zip/node
        hickory-to-svg-hiccup
        react-style->hiccup)))
