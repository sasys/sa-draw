(ns sa-draw.views.dfd.svg.procs
  " Creates the svg for a process.
  "
  (:require [reagent.core                 :as r]
            [re-frame.core                :as rf]
            [sa-draw.subs                 :as subs]
            [sa-draw.events               :as ev]
            [sa.sa-store.store-factory    :as sf]
            [sa-draw.model.utils          :as ut]
            [sa-draw.views.editors.node-events :as ne]
            [sa-draw.config.subs          :as cfsubs]
            [sa-draw.model.icons          :as icons]
            [sa-draw.model.icons-impl     :as iconsim :refer [im]]
            [sa-draw.views.dfd.svg.draw   :as sdraw]
            [clojure.string               :refer [blank? trim]]
            [sa-draw.model.map-impl       :refer [proc-model]]
            [sa-draw.model.proto-model    :refer [find-node id title children delegate]]
            [sa-draw.views.dfd.svg.common :refer [draw-box draw-circ draw-id draw-title]]
            [sa-draw.config.defaults      :refer [default-config]]))


(def stroke-colour           (:stoke-colour      default-config))

(defn proc-ui
  "xy is a position atom"
  [mouse-up-fn mouse-move-fn id drawing-config]
  (let [edit-locked?  (rf/subscribe [::subs/edit-locked])
        link-color (r/atom "blue")
        show-inter-model-links (rf/subscribe [::cfsubs/show-inter-model-links])]
    (fn [mouse-up-fn mouse-move-fn id drawing-config]
       (let [rad (:sa-draw.config.defaults/proc-radius drawing-config)
             summaries @(rf/subscribe [::subs/browser-summaries])
             procs (rf/subscribe [::subs/procs])
             proc  (find-node proc-model @procs @id)
             title (title proc-model proc)
             children (children proc-model proc)
             delegate (delegate proc-model proc)
             delegate-summary (if delegate (ut/find-summary delegate summaries))
             delegate-title (if delegate-summary (:sadf/name delegate-summary))
             delegate-help (if delegate-title (str delegate-title "\n=======\n" (:sadf/summary delegate-summary)))
             layout (rf/subscribe [::subs/layout])
             xy (r/atom ((keyword "sadf" @id) @layout))
             [x y] @xy
             third-dia (/ (* 2 rad) 3)
             sixth-dia (/ third-dia 2)

             ;;;;;;;;;;;;;;;;;
             prototype-icons     (rf/subscribe [::subs/default-icons])
             icon                (:sadf/icon proc)
             icon-svg            (icons/svg-for-icon im @prototype-icons icon)
             nxy                 (if @xy (r/atom @xy) (r/atom [70 70]))
             down                (r/atom false)
             down-delta-xy       (r/atom [0 0])]

         [:g {:key @id}

             (if (and icon icon-svg)
               [sdraw/adjust-scale-factor-for-diagram icon-svg edit-locked? 4.0 (str @id " - " title) mouse-up-fn mouse-move-fn down @id nxy down-delta-xy ::ne/node-edit-edit]
               [:<>
                 [draw-circ mouse-up-fn mouse-move-fn xy @id rad proc edit-locked? ::ne/node-edit-edit]
                 [draw-id  @xy @id @id  0 (* -0.5 rad)]
                 [draw-title @xy @id title]
                 (if (and (not (seq children)) @show-inter-model-links (not (blank? delegate)))
                   [:g
                     [:path {:id @id
                             :onClick (if (not (blank? delegate)) (fn [model-id] (rf/dispatch [::ev/load-model (trim delegate) ::sf/key-store])))
                             :stroke stroke-colour
                             :fill (if (blank? delegate) "transparent" @link-color)
                             :d (str "M " (- x sixth-dia) " " (+ y (/ third-dia 2)) " L " (+ x sixth-dia) " " (+ y (/ third-dia 2)) " L " x " " (+ y rad) " L " (- x sixth-dia) " " (+ y (/ third-dia 2)))} (if delegate-title [:title delegate-help])]
                     [:text {:id @id :x x :y (+ y rad sixth-dia) :text-anchor "middle" :alignment-baseline "middle" } (:sadf/name (ut/find-summary delegate summaries))]])])]))))
