(ns sa-draw.mark-dirty
  "
    This single event is used in multiple places to mark the current model
    as dirty.  It is separated out to prevent circular dependancies.
  "
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-event-fx
  ::dirty
  ;(fn-traced  [cofx [_ dirty?]]
  (fn [cofx [_ dirty?]]
    {:db (assoc-in (:db cofx) [:sa-draw.events/dirty] dirty?)}))
