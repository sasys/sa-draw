(ns sa-draw.config.defaults)

; Note,   default in the ns gives a compiler warning about a conflict with
; a js reserved word so using defaults instead.



(def max-x 800)
(def max-y 600)

;versioning
(def version { ::major 1
               ::minor 0
               ::patch 5
               ::pre-release "d"
               ::build-meta nil})

(def default-config
    {

      ; There are a maximum of 6 inputs and 6 outputs on each bubble.
      ; Here we specify the input and the output angles
      ; This tends to give a progression from top left to bottom right
      ::max-proc-inputs 6
      ::max-proc-outputs 6

      ::proc-input-angles [0 330 300 270 240 210]

      ::proc-output-angles [180 150 120 90 60 30]

      ;size of the canvas
      ::max-x max-x
      ::max-y max-y

      ;the size and position of the context diagram
      ::context-radius 170
      ::context-position [(/ max-x 2) (/ max-y 2)]

      ;process bubble sizes
      ::proc-radius 70
      ;control and endpoints
      ::cp-radius 10

      ;scale factor for dfd canvas in the width and height (100 => 1)
      ::dfd-scale 100

      ::arrow-width 10

      ::line-stroke-style (str "rgba(0, 0, 0," "1" ")")

      ;The flow svg defaults
      :fill-colour "white"
      :fill-opacity 0.0

      ;The svg link colour (between models)
      :link-colour      "blue"
      :link-fill-colour "blue"
      :link-fill-opacity 1.0

      ;svg draw colour for strokes
      :stroke-colour "black"

      ;Drag and drop related

      ;The min time for mouse donw in millis to mean that a drag is in progress
      ::min-drag-time 5

      ;The default canvas names
      ::canvas-default-name "dfd"
      ::flow-canvas-default-name "dfd"
      ::proc-canvas-default-name "dfd"

      ;The id of the proc we open on first start.
      ::default-proc-id "0"
      ::model-namespace "safd"

      ;Fonts and sizes
      ::data-name-font-size "10"

      ;Model link defaults
      ::show-inter-model-links false
      ::show-sub-model-links false

      ;Markdown viewer defaults
      ::show-markdown-viewer true
      ::update-markdown-on-save true

      ;Structured Editor
      ::show-structured-editor true

      ;icon editor
      ::show-icon-manager true

      ;When a diagram is exported as svg this header is added
      ::svg-export-header
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <svg version=\"1.1\" id=\"svg\" xmlns=\"http://www.w3.org/2000/svg\" width=\"800\" height=\"600\" viewBox=\"0 0 800 600\">

          <style id=\"st-1\" type=\"text/css\">
            @import url('https://fonts.googleapis.com/css?family=Indie+Flower|Gamja+Flower');
            /* The text element styles */
            /* Icon */
            .icon {font-weight: bold; font-family: \"Gamja Flower\"}
            /* Context Object Producer */
            .icon-title {font-weight: bold; font-family: \"Gamja Flower\"}
            /* Data Flow Term*/
            .flow-term {font-weight: bold; font-family: \"Gamja Flower\"}
          </style>
        "

;TBD THIS NEEDS TO BE DRIVEN BY THE BUILD CONFIGURATION
      ::available-teams-stores [:sa.sa-store.store-factory/rest-store :sa.sa-store.store-factory/key-store]
      ::available-community-stores [:sa.sa-store.store-factory/key-store]
      ::available-stores  [:sa.sa-store.store-factory/key-store]})
