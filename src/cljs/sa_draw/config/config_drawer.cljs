(ns sa-draw.config.config-drawer
  "
    User adjustable configuration.
  "
  (:require ["@material-ui/core"                           :as mui]
            ["@material-ui/icons"                          :as muicons]
            [re-frame.core                                 :as rf]
            [reagent.core                                  :as r]
            [sa-draw.config.events                         :as cfev]
            [sa-draw.config.subs                           :as cfsb]
            [sa-draw.views.markdown.markdown-viewer-events :as mvev]
            [sa-draw.global-state     :refer [config-drawer-pinned config-drawer-open]]))



(defn normalise-to-num
  "Because of the js we can get strings from the controls
   so in here, if its a string we make a number."
  [x]
  (cond
    (number? x) x
    (string? x) (long x)))

(defn config-change [config proc-rad dfd-scale]
       (let [updated-config (-> config (assoc-in  [:sa-draw.events/drawing-defaults :sa-draw.config.defaults/proc-radius] (js/parseInt proc-rad))
                                       (assoc-in  [:sa-draw.events/drawing-defaults :sa-draw.config.defaults/dfd-scale] (js/parseInt dfd-scale)))]
         (rf/dispatch [::cfev/config-change updated-config])))

(defn config-panel
  []
  (let [editing? (r/atom false)
        show-inter-model-links  (rf/subscribe [::cfsb/show-inter-model-links])
        show-markdown-viewer    (rf/subscribe [::cfsb/show-markdown-viewer])
        show-structured-editor  (rf/subscribe [::cfsb/show-structured-editor])
        update-markdown-on-save (rf/subscribe [::cfsb/update-markdown-on-save])
        show-icon-manager       (rf/subscribe [::cfsb/show-icon-manager])]
    (fn []
      (let [config @(rf/subscribe [::cfsb/config])
            current-proc-rad (get-in config [:sa-draw.events/drawing-defaults :sa-draw.config.defaults/proc-radius])
            proc-rad (r/atom current-proc-rad)]

        [:> mui/Drawer {:anchor "right"
                        :variant "persistent"
                        :open (or @config-drawer-open @config-drawer-pinned @editing?)
                        :onClick #(reset! config-drawer-open false)}
          (if  (or @config-drawer-open @config-drawer-pinned @editing?)

            ;SET THE MARGIN_TOP OTHERWISE THE SLIDE GOES UNDER THE TOP BAR
            [:> mui/GridList {:cols 1
                              :cellHeight "auto"
                              :style {:width "230px"
                                      :margin-top "130px"}}
                [:> mui/Typography {:variant "h5" :style {:margin 5}} "Local Config"]
                [:> mui/Divider]

                [:> mui/Typography {:variant "h6" :style {:margin 5}} "Drawing Config"]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :style {:margin 5}}
                  [:p "Show inter model links"]
                  [:> mui/Checkbox  {:checked (true? @show-inter-model-links)
                                     :value "show inter model links"
                                     :color "default"
                                     :name "show inter model links"
                                     :onChange (fn [] (rf/dispatch [::cfev/show-inter-model-links (not @show-inter-model-links)]))}]]
                [:> mui/Divider {:style {:width "100%"}}]
                [:> mui/Typography {:variant "h6" :style {:margin 5}} "Markdown Viewer Config"]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :style {:margin 5}}
                  [:p "Use local markdown parser"]
                  [:> mui/Checkbox  {:checked (true? @show-markdown-viewer)
                                     :value "local markdown parser"
                                     :color "default"
                                     :name "local markdown parser"
                                     :onChange (fn [] (rf/dispatch [::mvev/markdown-from-json])
                                                      (rf/dispatch [::cfev/show-markdown-viewer (not @show-markdown-viewer)]))}]]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :style {:margin 5}}
                  [:p "Show markdown viewer"]
                  [:> mui/Checkbox  {:checked (true? @show-markdown-viewer)
                                     :value "show markdown viewer"
                                     :color "default"
                                     :name "show markdown viewer"
                                     :onChange (fn [] (rf/dispatch [::mvev/markdown-from-json])
                                                      (rf/dispatch [::cfev/show-markdown-viewer (not @show-markdown-viewer)]))}]]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :style {:margin 5}}
                  [:p "Update markdown on save"]
                  [:> mui/Checkbox  {:checked (true? @update-markdown-on-save)
                                     :value "update markdown viewer on save"
                                     :color "default"
                                     :name "update markdown viewer on save"
                                     :onChange (fn [] (rf/dispatch [::cfev/update-markdown-on-save (not @update-markdown-on-save)]))}]]
                [:> mui/Divider {:style {:width "100%"}}]
                [:> mui/Typography {:variant "h6" :style {:margin 5}} "Markdown Editor Config"]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :style {:margin 5}}
                  [:p "Show Markdown Editor"]
                  [:> mui/Checkbox  {:checked (true? @show-structured-editor)
                                     :value "show markdown editor"
                                     :color "default"
                                     :name  "show markdown editor"
                                     :onChange (fn [] (rf/dispatch [::mvev/markdown-from-json])
                                                      (rf/dispatch [::cfev/show-structured-editor (not @show-structured-editor)]))}]]
                [:> mui/Divider {:style {:width "100%"}}]
                [:> mui/Typography {:variant "h6" :style {:margin 5}} "Icon Manager Config"]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :justify      "space-between"
                              :style {:margin 5}}
                  [:p "Show Icon Manager"]
                  [:> mui/Checkbox  {:checked (true? @show-icon-manager)
                                     :value "show icon manager"
                                     :color "default"
                                     :name  "show icon manager"
                                     :onChange (fn [] (rf/dispatch [::cfev/show-icon-manager (not @show-icon-manager)]))}]]
                [:> mui/Divider]
                [:> mui/Grid {:container    true
                              :direction    "row"
                              :item         true
                              :justify      "flex-start"
                              :alignItems   "center"}
                  (if @config-drawer-pinned
                    [:> mui/Tooltip {:title "Unpin config editor"}
                      [:> mui/IconButton {:onClick #(swap! config-drawer-pinned not @config-drawer-pinned)}
                        [:> muicons/Close]]]
                    [:> mui/Tooltip {:title "Pin config editor"}
                      [:> mui/IconButton {:onClick #(swap! config-drawer-pinned not @config-drawer-pinned)}
                        [:> muicons/OfflinePin]]])
                  [:> mui/Tooltip {:title "Save changes"}
                    [:> mui/IconButton {:onClick (fn []
                                                   (let [updated-config (assoc-in config [:sa-draw.events/drawing-defaults :sa-draw.config.defaults/proc-radius] (js/parseInt @proc-rad))]
                                                     (reset! editing? false)
                                                     (rf/dispatch [::cfev/config-change updated-config])))}
                      [:> muicons/Save]]]]
              [:div]])]))))
