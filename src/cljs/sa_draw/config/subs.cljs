(ns sa-draw.config.subs
  (:require [re-frame.core :as rf]))

(rf/reg-sub
  ::config
  (fn [db _]
    (get-in db [:sa-draw.events/config])))

(rf/reg-sub
  ::drawing-defaults :<- [::config]
  (fn [config _]
    (:sa-draw.events/drawing-defaults config)))

(rf/reg-sub
  ::show-inter-model-links :<- [::drawing-defaults]
  (fn [drawing-defaults _]
    (:sa-draw.config.defaults/show-inter-model-links drawing-defaults)))

(rf/reg-sub
  ::show-sub-model-links :<- [::drawing-defaults]
  (fn [drawing-defaults _]
    (:sa-draw.config.defaults/show-sub-model-links drawing-defaults)))

(rf/reg-sub
  ::show-markdown-viewer  :<- [::drawing-defaults]
  (fn [drawing-defaults _]
    (:sa-draw.config.defaults/show-markdown-viewer drawing-defaults)))

(rf/reg-sub
  ::update-markdown-on-save  :<- [::drawing-defaults]
  (fn [drawing-defaults _]
    (:sa-draw.config.defaults/update-markdown-on-save drawing-defaults)))


(rf/reg-sub
  ::show-structured-editor  :<- [::drawing-defaults]
  (fn [drawing-defaults _]
    (:sa-draw.config.defaults/show-structured-editor drawing-defaults)))

(rf/reg-sub
  ::show-icon-manager :<- [::drawing-defaults]
  (fn [drawing-defaults _]
    (:sa-draw.config.defaults/show-icon-manager drawing-defaults)))
