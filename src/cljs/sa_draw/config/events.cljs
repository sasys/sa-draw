(ns sa-draw.config.events
  (:require [re-frame.core            :as rf]
            [sa-draw.views.pspec.events :as pev]))


(rf/reg-event-fx
  ::config-change
  (fn [cofx [_ new-config]]
    (let [db (:db cofx)
          model (:sa-draw.events/model db)
          layer (-> db :sa-draw.events/graphics :sa-draw.events/current-layer :sa-draw.events/layer)]
      {::config {::layer layer
                 ::layout (-> db :sa-draw.events/graphics :sa-draw.events/layout)
                 ::model model
                 ::drawing-config (:sa-draw.events/drawing-defaults new-config)}
       :db (assoc-in db [:sa-draw.events/config] new-config)
       ::pev/update-layer-drawing {::pev/model model
                                   ::pev/layer layer
                                   ::pev/layout (-> db :sa-draw.events/graphics :sa-draw.events/layout)
                                   ::pev/drawing-config (:sa-draw.events/drawing-defaults new-config)}})))

(rf/reg-event-db
  ::show-inter-model-links
  (fn [db [_ inter-model-links]]
    (assoc-in db [:sa-draw.events/config :sa-draw.events/drawing-defaults :sa-draw.config.defaults/show-inter-model-links] inter-model-links)))


(rf/reg-event-db
  ::show-sub-model-links
  (fn [db [_ sub-model-links]]
    (assoc-in db [:sa-draw.events/config :sa-draw.events/drawing-defaults :sa-draw.config.defaults/show-sub-model-links] sub-model-links)))


(rf/reg-event-db
  ::show-markdown-viewer
  (fn [db [_ show-markdown-viewer]]
    (assoc-in db [:sa-draw.events/config :sa-draw.events/drawing-defaults :sa-draw.config.defaults/show-markdown-viewer] show-markdown-viewer)))

(rf/reg-event-db
  ::update-markdown-on-save
  (fn [db [_ update-markdown-on-save]]
    (assoc-in db [:sa-draw.events/config :sa-draw.events/drawing-defaults :sa-draw.config.defaults/update-markdown-on-save] update-markdown-on-save)))


(rf/reg-event-db
  ::show-structured-editor
  (fn [db [_ show-structured-editor]]
    (assoc-in db [:sa-draw.events/config :sa-draw.events/drawing-defaults :sa-draw.config.defaults/show-structured-editor] show-structured-editor)))

(rf/reg-event-db
  ::show-icon-manager
  (fn [db [_ show-icon-manager]]
    (assoc-in db [:sa-draw.events/config :sa-draw.events/drawing-defaults :sa-draw.config.defaults/show-icon-manager] show-icon-manager)))
