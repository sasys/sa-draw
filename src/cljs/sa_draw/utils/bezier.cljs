(ns sa-draw.utils.bezier)
"
 Original algorith taken from here
 http://www.independent-software.com/determining-coordinates-on-a-html-canvas-bezier-curve.html
"

;function getBezierXY(t, sx, sy, cp1x, cp1y, cp2x, cp2y, ex, ey) {}
;  return {[]}
;    x: Math.pow(1-t,3) * sx + 3 * t * Math.pow(1 - t, 2) * cp1x
;      + 3 * t * t * (1 - t) * cp2x + t * t * t * ex,
;    y: Math.pow(1-t,3) * sy + 3 * t * Math.pow(1 - t, 2) * cp1y
;      + 3 * t * t * (1 - t) * cp2y + t * t * t * ey)
;

(defn bez
  "Given the start, two control points an end and a t value between
   0 and 1 we calculate the coordinate of a location on the curve."
  [t sx sy cp1x cp1y cp2x cp2y ex ey]
  (let [x (+ (* (Math/pow (- 1 t) 3) sx)
             (* 3 t (Math/pow (- 1 t) 2) cp1x)
             (* 3 t t (- 1 t) cp2x)
             (* t t t ex))
        y (+ (* (Math/pow (- 1 t) 3) sy)
             (* 3 t (Math/pow (- 1 t) 2) cp1y)
             (* 3 t t (- 1 t) cp2y)
             (* t t t ey))]
    [x y]))
