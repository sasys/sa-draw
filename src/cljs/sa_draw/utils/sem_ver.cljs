(ns sa-draw.utils.sem-ver)

(defn format-semver [version]
  (if version
    (let [major (:sa-draw.config.defaults/major version)
          minor (:sa-draw.config.defaults/minor version)
          patch (:sa-draw.config.defaults/patch version)
          pre   (:sa-draw.config.defaults/pre-release version)
          build (:sa-draw.config.defaults/build-meta version)]
      (str major "." minor "." patch (if pre (str "-" pre)) (if build (str "+" build))))
    "MISSING"))
