(ns sa-draw.utils.download
  "Utility to enable downloads.  We want to be able to download the svg of any
   layer as required"
  (:require  [cognitect.transit :as transit]
             [sa-draw.config.defaults :as defaults]))

(def p "data:text/plain;charset=utf-8,")

;Downloaded file name extensions
(def default-svg-filename-extension ".svg")
(def default-markdown-filename-extension ".md")

(def extensions {:json ".json" :json-verbose ".verbose.json" :msgpack ".mp"})

(defn download!
  "
    General download operation, given a file name and a block of text.
  "
  [filename text]
  (let [a (.createElement js/document "a")
        link (str p (js/encodeURIComponent text))]
    (set! (. a -href) link)
    (set! (. a -download) filename)
    (set! (.. a  -style -display) "none")
    (.appendChild (.-body js/document) a)
    (.click a)
    (.removeChild (.-body js/document) a)))


(defn find-svg-node
  "Given the id of a specific svg node we grab the markup"
  [node-id]
  (let [proc (.getElementById js/document (if (= node-id "context") (str "svg-model-" node-id) (str "svg-model-proc-" node-id)))]
    (. proc -outerHTML)))


(defn download
  "Download the svg of the decomposition of the given proc-id"
  ([proc-id]
   (download "model" proc-id))
  ([model-name proc-id]
   (download! (str model-name "-" proc-id default-svg-filename-extension) (str (::defaults/svg-export-header defaults/default-config) (find-svg-node proc-id) "</svg>")) ;(str "<?xml version=\"1.0\" encoding=\"UTF-8\"?><svg id=\"svg\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" (find-svg-node proc-id) "</svg>"))
   nil)
  ([model-name model-uuid proc-id]
   (download! (str model-name "-" model-uuid "-" proc-id default-svg-filename-extension) (str (::defaults/svg-export-header defaults/default-config) (find-svg-node proc-id) "</svg>"))))

;;FIXME: Should be a part of the storage subsystem
(defn download-model! [title id model]
  (let [w (transit/writer :json)]
    (download! (str "sa-model-" title "-" id "." (:json extensions)) (transit/write w model))))
