(ns sa-draw.utils.geom)

(defn deg-to-rad
  [deg]
  (/ (* deg (.-PI js/Math)) 180))

(defn sq
  [x]
  (* x x))

(defn point-in-circle?
  "Is the given xy position in the given circle"
  [center-xy r xy]
  (let [[xc yc] center-xy
        [xp yp] xy
        rsq (sq r)
        dsq (+ (sq (- xp xc)) (sq (- yp yc)))]
    (<= dsq rsq)))
