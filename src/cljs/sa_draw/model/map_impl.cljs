(ns sa-draw.model.map-impl
  "
    Provides an implementation of the core data model based on a map.
  "
  (:require [sa-draw.model.proto-model   :as proto-model :refer [SaModel ContextObjects SaProcs]]
            [sa-draw.model.default-model :refer [default-model new-model default-namespace]]
            [sa-draw.model.utils         :refer [find-node-in-tree cids] :as ut]
            [sa-draw.config.defaults     :refer [version]]))

(def name-space default-namespace)

(def sources-keyword (keyword name-space "sources"))
(def sinks-keyword (keyword name-space "sinks"))
(def producers-keyword (keyword name-space "producers"))
(def consumers-keyword (keyword name-space "consumers"))


(deftype MapModel []
    SaModel

    (model-meta-version
      [this model]
      {:major "0"
       :minor "0"
       :inc   "0"})

    (name-space
      [this]
      default-namespace)

    (new-model
      [this]
      new-model)

    (new-model-from-model
      [this model]
      model)

    (new-model-from-model
      [this model model-uuid model-title]
      (-> model
          (assoc-in [:sadf/meta :sadf/name] model-title)
          (assoc-in [:sadf/meta :sadf/uuid] model-uuid)))

    (make-model
      [this model-id model-title]
      "
        Creates a model with the given id and title.
      "
      (-> new-model
          (assoc-in [:sadf/meta :sadf/name] model-title)
          (assoc-in [:sadf/meta :sadf/uuid] model-id)
          (assoc-in [:sadf/meta :sadf/app-version] version)))

    (find-process-model
      [this model]
      ((keyword default-namespace "procs") model))

    (find-flow-model
      [this model]
      ((keyword default-namespace "flows") model))

    (find-dictionary
      [this model]
      ((keyword default-namespace "dictionary") model))

    (find-context-model
      [this model]
      ((keyword name-space "context-objects") model))

    (find-icon-model
      [this model]
      ((keyword name-space "icons") model))

    (find-prototype-model
      [this model]
      ((keyword name-space "default-cloud-icons") model))

    (update-process-model
      [this model proc-model]
      (assoc-in model [(keyword name-space "procs")] proc-model))

    (update-flow-model
      [this model flow-model]
      (assoc-in model [(keyword name-space "flows")] flow-model))

    (update-icon-model
      [this model iconsv]
      (assoc-in model [(keyword name-space "icons")] iconsv))

    (update-prototype-icon-model
      [this model iconsv]
      (assoc-in model [(keyword name-space "default-cloud-icons")] iconsv))

    (update-dictionary
      [this model dictionary]
      (assoc-in model [(keyword name-space "dictionary")] dictionary))

    (context-objects-map
      [this model]
      ((keyword name-space "context-objects") model))

    (context-objects
      [this model]
      [(get-in model [(keyword name-space "context-objects") (keyword name-space "sources")])
       (get-in model [(keyword name-space "context-objects") (keyword name-space "sinks")])
       (get-in model [(keyword name-space "context-objects") (keyword name-space "producers")])
       (get-in model [(keyword name-space "context-objects") (keyword name-space "consumers")])])


    (update-model-context-objects
      [this model context-objects]
      (assoc-in model [(keyword name-space "context-objects")] context-objects)))

(defn new-prod-con
  [context-type id title description terms dic-link link icon]
  (let [id-key (keyword name-space "id")
        dic-key (keyword name-space "dictionary-link")
        terms-key (keyword name-space "terms")
        title-key (keyword name-space "title")
        description-key (keyword name-space "description")
        link-key (keyword name-space "link")
        type-key (keyword name-space "context-type")
        icon-key  (keyword name-space "icon")]
    {id-key id
     title-key title
     dic-key dic-link
     terms-key terms
     description-key description
     link-key link
     type-key context-type
     icon-key icon}))


(defn new-source-sink
  [context-type id title description dic-link icon]
  (let [id-key    (keyword name-space "id")
        dic-key   (keyword name-space "dictionary-link")
        title-key (keyword name-space "title")
        description-key (keyword name-space "description")
        type-key  (keyword name-space "context-type")
        icon-key  (keyword name-space "icon")]
    {id-key id
     title-key title
     dic-key dic-link
     description-key description
     type-key context-type
     icon-key icon}))



(deftype ContextObjectModel []
  ContextObjects

  (find-producer
    [this context-objects id]
    (first (filter #(= id ((keyword name-space "id") %)) (nth context-objects 2))))

  (find-context-object-by-id
    [this context-objects id]
    (let [all-context-objects (flatten context-objects)]
      (first (filter #(= ((keyword name-space "id") %) id ) all-context-objects))))

  (update-object
    [this context-object title description terms link]
    (-> context-object
        (assoc (keyword name-space "title") title)
        (assoc (keyword name-space "description") description)
        (assoc (keyword name-space "link") link)
        (assoc (keyword name-space "terms") terms)))

  (new-producer
    [this context-type id title description dic-link link]
    (new-prod-con context-type id title description [] dic-link link nil))

  (new-producer
    [this context-type id title description dic-link link icon]
    (new-prod-con context-type id title description [] dic-link link icon))

  (new-consumer
    [this context-type id title description dic-link link]
    (new-prod-con context-type id title description [] dic-link link nil))

  (new-consumer
    [this context-type id title description dic-link link icon]
    (new-prod-con context-type id title description [] dic-link link icon))

  (new-source
    [this context-type id title description dic-link]
    (new-source-sink context-type id title description dic-link nil))

  (new-source
    [this context-type id title description dic-link icon]
    (new-source-sink context-type id title description dic-link icon))

  (new-sink
    [this context-type id title description dic-link]
    (new-source-sink context-type id title description dic-link nil))

  (new-sink
    [this context-type id title description dic-link icon]
    (new-source-sink context-type id title description dic-link icon))

  (context-object-id
    [this context-object]
    ((keyword name-space "id") context-object))

  (context-object-title
    [this context-object]
    ((keyword name-space "title") context-object))

  (context-object-description
    [this context-object]
    ((keyword name-space "description") context-object))

  (dic-link
    [this context-object]
    ((keyword name-space "dictionary-link") context-object))

  (terms
    [this context-object]
    (let [terms ((keyword name-space "terms") context-object)]
      (if (nil? terms) [] terms)))

  (link
    [this context-object]
    ((keyword name-space "link") context-object))

  (context-object-type
    [this context-object]
    ((keyword name-space "context-type") context-object))

  (context-object-icon
    [this context-object]
    ((keyword name-space "icon") context-object))

  (context-details
    [this context-object]
    [[]
     "context"
     (proto-model/context-object-title this context-object)
     (proto-model/context-object-description this context-object)
     [] ;data in
     [] ;data out
     name-space])


  (context-object-details
    [this context-object]
    [(proto-model/context-object-id this context-object)
     (proto-model/context-object-title this context-object)
     (proto-model/context-object-description this context-object)
     (proto-model/link this context-object)
     (proto-model/dic-link this context-object)
     (proto-model/terms this context-object)
     (proto-model/context-object-type this context-object)])


  (new-context-object
    [this context-type id title description terms dic-link link]
    (case context-type
      :sadf/sources (new-source-sink context-type id title description dic-link nil)
      :sadf/sinks  (new-source-sink context-type id title description  dic-link nil)
      :sadf/producers (new-prod-con context-type id title description terms dic-link link nil)
      :sadf/consumers (new-prod-con context-type id title description terms dic-link link nil)))

  (new-context-object
    [this context-type id title description terms dic-link link icon]
    (case context-type
      :sadf/sources (new-source-sink context-type id title description dic-link icon)
      :sadf/sinks  (new-source-sink context-type id title description  dic-link icon)
      :sadf/producers (new-prod-con context-type id title description terms dic-link link icon)
      :sadf/consumers (new-prod-con context-type id title description terms dic-link link icon)))


  (update-context-objects
    [this context-objects context-type context-object title description terms link]
    "
      Given the current context objects and the key, add a new context object.
    "
    (let [updated-objects (reduce (fn [l p] (if (= ((keyword name-space "id") p)  ((keyword name-space "id") context-object))
                                              (conj l (proto-model/update-object this p title description terms link))
                                              (conj l p))) [] (context-type context-objects))]
      (assoc context-objects context-type updated-objects)))


  (add-to-context-objects
    [this context-objects context-object-type context-object]
    "
      Given a context object, its key and the current context objects, add
      the context object.  Works for both the map and evtor form (for now!!)
    "
    (if (map? context-objects)
        (let [updated-objects (conj (context-object-type context-objects) context-object)]
          (assoc context-objects context-object-type updated-objects))
        (let [[sources sinks producers consumers] context-objects]
           (case context-object-type
              :sadf/sources [(conj sources context-object sinks producers consumers)]
              :sadf/sinks [sources (conj sinks context-object) producers consumers]
              :sadf/producers [sources sinks (conj producers context-object) consumers]
              :sadf/consumers [sources sinks producers (conj consumers context-object)]))))

  (delete-from-context-objects
    [this context-objects type context-object]
    "
      Given a context object, its key and the current cuontext objects, rmeove it.
    "
    (assoc context-objects type (keep identity  (for [p (type context-objects)]
                                                   (if (not= ((keyword name-space "id") p)  ((keyword name-space "id") context-object))
                                                     p))))))

(deftype ProcModel []
    SaProcs

    (find-node
      [this proc-model node-id]
      "Given a node id and a proc-model, find the proc with the given id
       The 0 node can also be known as context
      "
      (if (= node-id "context")
        (find-node-in-tree "0" proc-model name-space)
        (find-node-in-tree node-id proc-model name-space)))

    (find-node-in-list
      [this id procs]
      "Given a proc id find a node or nil in the given list of nodes.  This
       is a flat list search"
      (into {} (ut/find-node-in-list id procs name-space)))

    (remove-node-from-list
      [this id procs]
      "Removes the node with the given id from a list of nodes or does nothing."
      (ut/remove-node-from-list id procs name-space))

    (remove-node
      [this proc-model node-id]
      "
      Removes a node and all its children from the model.
      "
      (ut/remove-node-from-model node-id proc-model name-space))

    (update-node
      [this id updated-node procs]
      "
        Replaces the node with the given id with provided updated-node.
      "
      (ut/update-node id updated-node procs name-space))

    (patch-node
      [this proc-model id title description input-data output-data]
      "
        Updates parts of the node with the given id.
      "
      (let [node (proto-model/find-node this proc-model id)]
        (ut/patch-node node title description input-data output-data name-space)))

    (patch-node
      [this proc-model id title description input-data output-data new-link]
      "
        Updates parts of the node with the given id.
      "
      (let [node (proto-model/find-node this proc-model id)]
        (if new-link
          (ut/patch-node node title description input-data output-data name-space new-link)
          (ut/patch-node node title description input-data output-data name-space))))

    (patch-node
      [this proc-model id title description input-data output-data new-link icon]
      "
        Updates parts of the node with the given id.
      "
      (let [node (proto-model/find-node this proc-model id)]
        (if new-link
          (ut/patch-node node title description input-data output-data name-space new-link icon)
          (ut/patch-node node title description input-data output-data name-space))))

    (make-iconic-node
      [this id title data-in data-out description icon]
      "
          The icon is a map like {:type type :source source :name name}
          that maps into the prototype icon collection.
      "
      (ut/make-iconic-node id title data-in data-out description name-space icon))

    (make-node
      [this id title data-in data-out description new-link]
      (if new-link
        (ut/make-node id title data-in data-out description name-space new-link)
        (ut/make-node id title data-in data-out description name-space)))

    (add-node
      [this id new proc-model]
      (ut/add-node id new proc-model name-space))

    (children
      [this node]
      ((keyword name-space "children") node))

    (next-child
      [this parent-id children]
      (ut/next-child parent-id children name-space))

    (swap-left
      [this proc-model id]
      "
        If this node is not logically, the lowest id node on a layer,
        swap its id with the node logically on the left of it.  If there
        is no node on the left, do nothing.
      "
      (ut/swap-left proc-model name-space id))

    (swap-right
      [this proc-model id]
      "
        If this node is not logically, the highest id node on a layer,
        swap its id with the node logically on the right of it.  If there
        is no node on the right, do nothing.
      "
      (ut/swap-right proc-model name-space id))

    (compact
      [this node-list]
      "
        Given a list of nodes, renumber nodes to fill in any gaps.
      "
      (ut/compact-node-list node-list))

    (compact
      [this node-list namespace]
      "
        Given a list of nodes, renumber nodes to fill in any gaps.
      "
      (ut/compact-node-list node-list namespace))

    (move-up
      [this proc-model id]
      "
          If this node is not logically, the highest id node on a layer,
          move it and its subtree up a layer or do nothing,
        "
      (ut/move-up proc-model name-space id))

    (add-new-child-node
      [this proc-model id]
      "
          Add a new child node to the given node.
      "
      (ut/add-new-child-node proc-model name-space id))

    (add-new-child-node-with-icon
      [this proc-model id icon-spec]
      "
          Add a new child node with the specified icon to the given node.
      "
      (ut/add-new-child-node proc-model name-space id icon-spec))


    (id
      [this node]
      ((keyword name-space "id") node))

    (data-in
      [this node]
      "
      Gets the data inputs for the node
    "
      ((keyword name-space "data-in") node))

    (data-out
      [this node]
      "
      Gets the data outputs for the node
    "
      ((keyword name-space "data-out") node))

    (title
      [this node]
      "
      Gets the title of the given node
    "
      ((keyword name-space "title") node))

    (description
      [this node]
      "
      Gets the description of the given node
    "
      ((keyword name-space "description") node))

    (delegate
      [this node]
      "
      Gets the delegate model of the given node
    "
      ((keyword name-space "delegate") node))

    (icon
      [this node]
      "
    Gets the icon model for the proc or nil if there isnt one.
    "
      ((keyword name-space "icon") node))


    (details
      [this node]
      "
      Breaks out the details of a node.
    "
      [(proto-model/children this node)
       (proto-model/id this node)
       (proto-model/title this node)
       (proto-model/description this node)
       (proto-model/data-in this node)
       (proto-model/data-out this node)
       name-space])

    (cids
      [this node]
      "
      Child ids of a node.
    "
      (cids node name-space))

    (id-as-key
      [this node]
      (keyword name-space (proto-model/id this node)))

    (char-id-as-key
      [this id]
      "
      Given a node id make a keyword out of the id
      like :namespace/id
    "
      (keyword name-space id))

    (parent-id
      [this id]
      (ut/parent-id id)))




(def map-model (MapModel.))
(def proc-model (ProcModel.))
(def context-model (ContextObjectModel.))
