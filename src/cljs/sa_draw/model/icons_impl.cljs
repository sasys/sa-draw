(ns sa-draw.model.icons-impl
  (:require [sa-draw.model.icons          :refer [Icons]]
            [sa-draw.model.default-model  :refer [default-namespace]]
            [sa-draw.model.utils          :as ut :refer [uid]]))

(def name-space default-namespace)

(def mns (partial keyword name-space))

(def empty-svg-icon [:svg {:width 100 :height 100}
                      [:g {:id "layer1", :transform "translate(0, 0) scale(2.5, 2.5)"}
                        [:path {:d "M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"}]]])

(deftype IconsModel []
    Icons

  (new-icon
    [this prototype-icon source icon-source-title layer]
    {(mns :layer) layer
     (mns :id)    (uid)
     (mns :type) icon-source-title
     (mns :title) ""
     (mns :description) ""
     (mns :link) nil
     (mns :name) (:name prototype-icon)
     (mns :meta) {}
     (mns :source) source})


  (new-icon
    [this prototype-icon source icon-source-title layer description]
    {(mns :layer) layer
     (mns :id)    (uid)
     (mns :type) icon-source-title
     (mns :title) ""
     (mns :description) description
     (mns :link) nil
     (mns :name) (:name prototype-icon)
     (mns :meta) {}
     (mns :source) source})

  (new-icon
    [this prototype-icon source icon-source-title layer description title]
    "
      Create a new icon instance from a proptotype icon.
    "
    {(mns :layer) layer
     (mns :id)    (uid)
     (mns :type) icon-source-title
     (mns :title) title
     (mns :description) description
     (mns :link) nil
     (mns :name) (:name prototype-icon)
     (mns :meta) {}
     (mns :source) (:name prototype-icon)})

  (add-icon
    [this iconsv new-icon]
    "
      Add the given icon to the icons model.
      If the icon has no uuid we create one.
      Returns the icons model with the icon added.
    "
    (conj iconsv new-icon))

  (add-icons
    [this iconsv new-icons]
    "
      Add the given icons to the icons model.
      If the icon has no uuid we create one.
      Returns the icons model with the icon added.
    "
    (concat iconsv new-icons))

  (find-icon-instance
    [this iconsv icon-id]
    "
      Find an icon given the icon model and the icon id.
      Returns the icon instance or nill.
    "
    (if (and iconsv icon-id)
      (first (filter #(= ((mns :id) %) icon-id) iconsv))
      nil))

  (find-icon-instances
    [this iconsv source title icon-name]
    "
      Find all the icon instances for the given the icon model and the prototype icon..
      Returns a vector of instances that may be empty.
    "
    (filter (fn [icon] (and (= title  ((mns :type) icon))
                            (= source ((mns :source) icon))
                            (= icon-name ((mns :name) icon)))) iconsv))

  (icons-for-layer
    [this iconsv layer]
    "
      Find all the icons that are on the given layer/
    "
    (if iconsv
      (filter (fn [icon] (= ((mns :layer) icon) layer)) iconsv)
      []))

  (title
    [this icon]
    "
      The title for the icon.
    "
    ((mns :title) icon))

  (description
    [this icon]
    "
      The description for the icon.
    "
    ((mns :description) icon))

  (link
    [this icon]
    "
      The link for the icon.
    "
    ((mns :link) icon))

  (id
    [this icon]
    "
    "
    ((mns :id) icon))

  (svg-for-icon
    [this prototype-icons icon]
    "
      Given a prototype icon get the svg for that icon or default svg
      if none found.
    "
    (if (and prototype-icons icon)
      (let [type ((mns :type) icon)
            source ((mns :source) icon)
            icon-name ((mns :name) icon)
            icon-source (first (filter (fn [proto-icon] (and (= source (:source proto-icon))
                                                             (= type (:title proto-icon)))) prototype-icons))]
        (if-let [icons (:symbols icon-source)]
          (if-let [matches (filter #(= icon-name (:name %)) icons)]
            (:svg (first matches))
            empty-svg-icon)
          empty-svg-icon))
      empty-svg-icon))

  (remove-icon
    [this iconsv icon]
    "
      Remove the given icon from the icons model.
      Returns the updated icons model.
    "
    (filter (fn [i] (and (not= ((mns :id) i) ((mns :id) icon)))) iconsv))

  (update-title
    [this title icon]
    "
      Update the title of the given icon
    "
    (assoc icon (mns :title) title))

  (update-description
    [this description icon]
    "
      Update the description of the given icon
    "
    (assoc icon (mns :description) description))

  (update-link
    [this link icon]
    "
      Update the link of the given icon
    "
    (assoc icon (mns :link) link))

  (update-icon
    [this iconsv icon]
    "
      Replace the given icon with the new one.
    "
    (let [icons (filter #(not= ((mns :id) %) ((mns :id) icon)) iconsv)]
      (conj icons icon))))




(def im (IconsModel.))
