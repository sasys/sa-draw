(ns sa-draw.model.simple-flow-impl
  "A simple implementation f the flow model"
  (:require [sa-draw.model.flows :as flows :refer [SaFlows]]
            [sa-draw.model.utils :as ut :refer [uid]]
            [sa-draw.model.default-model  :refer [default-model default-namespace]]))

(def name-space default-namespace)

(def mns (partial keyword name-space))

;The namespaced keys
(def id-key (mns "id"))
(def data-key (mns "data"))
(def source-key (mns "source"))
(def sink-key (mns "sink"))
(def cp-key (mns "control-point"))
(def proc-id-key (mns "proc-id"))
(def description-key (mns "description"))
(def posn-key (mns "posn"))

(def std-cp-offset-1
  {(mns "id") (uid)
   :sadf/posn [100 100]
   :sadf/id  (uid)
   (mns "posn") [100 100]})

(defn flow-endpoint
  [flow key]
  ;just the first flow in the source or sink for now
  (proc-id-key (first (key flow))))


(deftype FlowModel [id-key data-key source-key sink-key cp-key proc-id-key description-key posn-key]
    SaFlows

    (new-flow
     [this v sourcev sinkv]
     (flows/new-flow this v sourcev sinkv ""))


    (new-flow
     [this v sourcev sinkv description]
     {id-key (uid)
      data-key v
      source-key sourcev
      sink-key sinkv
      cp-key [std-cp-offset-1 std-cp-offset-1]
      description-key description})

    (id
     [this flow]
     (id-key flow))


    (find-flow
      [this flowsv flow-id]
      (into {} (filter #(= (id-key %) flow-id) flowsv)))

    (remove-flow
      [this flowsv flow]
      (filter #(not= % flow) flowsv))

    (remove-flow-by-id
      [this flowsv flow-id]
      (into [] (filter #(not= (id-key %) flow-id) flowsv)))

    (update-flow
      [this flowsv flow id]
      (-> (flows/remove-flow-by-id this flowsv id)
          (conj  (assoc flow id-key id))))

    (update-flow-description
      [this flow description]
      (assoc-in flow [description-key] description))

    (update-flow-data
      [this flow data]
      (assoc-in flow [data-key] data))


    (data-items
      [this flow]
      (data-key flow))

    (sources
      [this flow]
      (source-key flow))

    (sources-sinks-for-layer
      [this all-flows layer]
      (into []
        (filter #(not= nil %) (for [flow all-flows]
                                (let [srk (flow-endpoint flow source-key)
                                      snk (flow-endpoint flow sink-key)]
                                  (if (and srk (= (name srk) layer))
                                    flow
                                    (if (and snk (= (name snk) layer))
                                       flow
                                       nil)))))))

    (endpoint-details
      [this endpoint]
      [(id-key endpoint) (proc-id-key endpoint) (posn-key endpoint)])


    (sinks
      [this flow]
      (sink-key flow))

    (cps
      [this flow]
      (cp-key flow))

    (description
      [this flow]
      (description-key flow))


    (details
      [this flow]
      [(id-key flow)
       (data-key flow)
       (source-key flow)
       (sink-key flow)
       (cp-key flow)
       (description-key flow)])


    (duplicate-flow-to-layer
      [this flow layer]
      (let [[_ data sources sinks _ description] (flows/details this flow)
            new-source (-> (first sources)
                           (assoc-in  [:sadf/proc-id] layer)
                           (assoc-in  [:sadf/id] (uid)))
            new-sink (->   (first sinks)
                           (assoc-in [:sadf/proc-id] layer)
                           (assoc-in [:sadf/id] (uid)))]

        (flows/new-flow this data [new-source] [new-sink] description))))




(def fm (FlowModel. id-key data-key source-key sink-key cp-key proc-id-key description-key posn-key))
