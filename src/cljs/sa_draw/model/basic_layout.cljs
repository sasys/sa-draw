(ns sa-draw.model.basic-layout
  "
    Provides a simple implementation of the layout protocol.

    This protocol is needed becuase using a key like 1.1 or :sadf/1.1 produces
    errors in serialization in edn and json.


    In this implementation an id is a stringt o the client.
  "
  (:require [sa-draw.model.layout :refer [SaLayout known-layout? new-layout add-layout find-layout]]
            [cognitect.transit    :as transit]))

(def store-type ::basic)
(def default-key-ns "sadf")
(def default-key-type ::string)

(defn make-key [id]
  (keyword (str id)))

(defn make-entry [id posn]
  {:key-name-space default-key-ns
   :position posn
   :id id})

(deftype BasicLayout []
  SaLayout

  (new-layout
    [this]
    "
      Creates an empty layout store
     "
    {:store-type store-type})

  (new-layout
    [this [max-x max-y]]
    (add-layout this (new-layout this) "0" [(/ max-x 2) (/ max-y 2)]))

  (find-layout
    [this layout id]
    "
      Finds the layout for the given id.  If the id doesnt exist we return nil.
    "
    (let [made-key (make-key id)]
      (if (known-layout? this layout id)
        (get-in layout [made-key :position])
        nil)))

  (add-layout
    [this layout id posn]
    "
      Adds a new layout for the given id.
      Will overwrite if the layout already exists.
    "
    (assoc layout (make-key id) (make-entry id posn)))

  (known-layout?
    [this layout id]
    "
      true => we have the layout information for the given id.
    "
    (let [made-key (make-key id)]
      (seq (filter #(= made-key %) (keys layout)))))

  (remove-layout
    [this layout id]
    "
      Removes the layout with the given id.
    "
    (dissoc layout (make-key id)))


  (key-namespace
    [this layout id]
    (:key-name-space (find-layout this layout id)))

  (serialise
    [this layout]
    (let [tr-wtr         (transit/writer :json-verbose)]
      (transit/write tr-wtr layout)))

  (deserialise
    [this in]
    (let [tr-rdr (transit/reader :json-verbose)]
      (transit/read tr-rdr in))))


(def blm (BasicLayout.))
