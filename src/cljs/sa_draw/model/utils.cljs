(ns sa-draw.model.utils
  "
    Contains functions that implement model transformation operations.
  "
  (:require [clojure.string :refer [split lower-case replace-first]]
            [clojure.set    :refer [rename-keys]]))


(defn uid
  "
      Create a random uuid.
  "
  []
  (str (random-uuid)))


(def default-namespace "sadf")


(defn find-summary [model-id summaries]
  "
  Given a list of summaries and a model id find the model summary.
  "
  (into {} (filter #(= (:sadf/uri %) (str "sbnl.sa." model-id)) summaries)))


(defn id-compare
  "
    Given two node ids , return -1 if the left is logically smaller than the right,
    0 if they are logically the same, and 1 if the left is logically more than the right
  "
  [node-id-1 node-id-2]
  (let [d1 (-> node-id-1
               (split #"\.")
               last
               js/parseInt)
        d2 (-> node-id-2
               (split #"\.")
               last
               js/parseInt)]
    (compare d1 d2)))


(defn node-comparator
  "
      Compare the ids of two nodes which must be on the same level.
      -1 => first earlier than the second
       0 => first and second the same
       1 => first is later than the second
      -2 => an error, they are not comparable
  "
  [namespace node-1 node-2]
  (let [idk (keyword namespace "id")
        id1 (idk node-1)
        id2 (idk node-2)]
    (if (= (count (split id1 #"\.")) (count (split id2 #"\.")))
      (id-compare id1 id2)
      -2)))

(defn sort-id-list
  "
    Given a list of ids, sort them into assending order
    If the list has a single item or is empty return the list
    If the items are not comparable, return the list (ie different lengths)
  "
  [id-list]
  (if (> (count id-list) 1)
    (sort id-compare id-list)
    id-list))




(defn sort-node-list
  "
    Given a list of nodes, sort them into logical ascending order
  "
  ([name-space node-list]
   (sort-node-list node-comparator name-space node-list))
  ([comparator name-space node-list]
   (let [node-cmp (partial comparator name-space)]
     (sort node-cmp node-list))))

(defn update-node
 "
  Given a proc-id and a replacement node, update the node in the model or
  add it if it doesnt exist.  In here the old-node would most likely be the head
  of the model ie the head from which to start searching.  Returns an updated node.
 "
  [proc-id new node namespace]
  (let [id (keyword namespace "id")
        children (keyword namespace "children")
        r (if (= (id node) proc-id)
           (merge node new)
           (assoc node children (map #(update-node proc-id new % namespace) (children node))))]
     r))


(defn patch-node
  "
    Given an existing node, patch with updtaed fields.
  "
  ( [node title description data-in data-out name-space]
    (let [title-key (keyword name-space "title")
          description-key (keyword name-space "description")
          data-in-key (keyword name-space "data-in")
          data-out-key (keyword name-space "data-out")]
      (-> node (assoc-in [title-key] title)
               (assoc-in [description-key] description)
               (assoc-in [data-in-key] data-in)
               (assoc-in [data-out-key] data-out))))

  ( [node title description data-in data-out name-space link]
    (let [title-key (keyword name-space "title")
          description-key (keyword name-space "description")
          data-in-key (keyword name-space "data-in")
          data-out-key (keyword name-space "data-out")
          delegate-key (keyword name-space "delegate")]
      (-> node (assoc-in [title-key] title)
               (assoc-in [description-key] description)
               (assoc-in [data-in-key] data-in)
               (assoc-in [data-out-key] data-out)
               (assoc-in [delegate-key] link))))

 ( [node title description data-in data-out name-space link icon]
   (let [title-key (keyword name-space "title")
         description-key (keyword name-space "description")
         data-in-key (keyword name-space "data-in")
         data-out-key (keyword name-space "data-out")
         delegate-key (keyword name-space "delegate")
         icon-key (keyword name-space "icon")]
     (-> node (assoc-in [title-key] title)
              (assoc-in [description-key] description)
              (assoc-in [data-in-key] data-in)
              (assoc-in [data-out-key] data-out)
              (assoc-in [delegate-key] link)
              (assoc-in [icon-key] icon)))))



(defn last-digit
  "Given a dot separated id like 1.2.3 finds the last digit."
  [id]
  (last (split id #"[.]")))


(defn parent-id
  "Finds the parent id of a proc ie 1.1.1 -> 1.1
   0 -> nil
   nil -> nil"
  [proc-id]
  (cond
    (= nil proc-id) nil
    (= "0" proc-id)  "0"
    (= 0 (count (filter #(= "." %) proc-id))) "0"
    :else (let [last (last-digit proc-id)]
           (apply str (drop-last (inc (count last)) proc-id)))))





(defn next-child
  "Given a list of children finds the next child id.
   If the parent child is 0 then the child number should be like 1,
   not like 0.1"
  ([parent children]
   (next-child parent children "sa-trees.proc.tree"))
  ([parent children namespace]
   ;(str parent "."
   (let [new-id (if (seq children)
                   (->> (map #(int (last-digit ((keyword namespace "id") %))) children)
                        (apply max)
                        inc
                        str)
                   "1")]
      (if (= (first parent) "0")
        (str new-id)
        (str parent "." new-id)))))


(defn find-node-ids [node namespace]
  "Given a model (or a node) make a list of all the ids of the children within that node"
  (let [name-space-children (keyword namespace "children")
        name-space-id (keyword namespace "id")]
    (map name-space-id (name-space-children node))))



(defn find-node-in-tree
   "Given a proc id find a node or nil.  Searches the complete tree and returns
    the first match."
   [proc-id node name-space]
   (let [kid (keyword name-space "id")
         chil (keyword name-space "children")]
       (if (= (kid node) proc-id)
         node
         (into {}
           (for [cnode (chil node)]
             (find-node-in-tree proc-id cnode name-space))))))


(defn cids
  "
    Find the child ids of the given node.
  "
  ([node name-space]
   (let [kid (keyword name-space "id")
         chil (keyword name-space "children")]
    (cids [] node kid chil)))
  ([result node kid chil]
   "result should be passed in as []"
   (let [res (conj result (kid node))
         children (chil node)]
    (reduce #(cids %1 %2 kid chil) res children))))



(defn add-node
  "Adds the given node to the children of the node with the
   given id in the given tree.
   Here, node is most probably the complete proc tree but it doesnt have to be.
   new is the new child node
   id is the id of the parent of the new child."
  [id new node namespace]
  (let [child-key (keyword namespace "children")
        id-key (keyword namespace "id")
        r (if (= (id-key node) id)
           (assoc-in node [child-key] (sort-node-list namespace (conj (child-key node) new)))
           (assoc node child-key (map #(add-node id new % namespace) (child-key node))))]
     r))



(defn make-iconic-node
   "
      The icon is a map like {:type type :source source :name name}
      that maps into the prototype icon collection.
   "
   [id title data-in data-out description namespace icon]
   {(keyword namespace "id") id
    (keyword namespace "title") title
    (keyword namespace "data-in") data-in
    (keyword namespace "data-out") data-out
    (keyword namespace "description") description
    (keyword namespace "icon") icon
    (keyword namespace "children") []
    (keyword namespace "delegate") ""})

(defn make-node
  ([id title data-in data-out description namespace]
   {(keyword namespace "id") id
    (keyword namespace "title") title
    (keyword namespace "data-in") data-in
    (keyword namespace "data-out") data-out
    (keyword namespace "description") description
    (keyword namespace "icon") nil
    (keyword namespace "children") []
    (keyword namespace "delegate") ""})
  ([id title data-in data-out description namespace new-link]
   {(keyword namespace "id") id
    (keyword namespace "title") title
    (keyword namespace "data-in") data-in
    (keyword namespace "data-out") data-out
    (keyword namespace "description") description
    (keyword namespace "icon") nil
    (keyword namespace "children") []
    (keyword namespace "delegate") new-link}))


(defn new-flow
 ([layer]
  (new-flow layer "sa-draw.test-model"))
 ([layer namespace]
  (let [cp {(keyword namespace "id") (str (random-uuid)) (keyword namespace "posn") [50 50]}]
    {(keyword namespace "id")    (str (random-uuid))
     (keyword namespace "source") [{:sa-draw.test-model/id (keyword "sa-trees.proc.tree" layer) :sa-draw.test-model/posn [90 90]}]
     (keyword namespace "sink")   [{:sa-draw.test-model/id (keyword "sa-trees.proc.tree" layer) :sa-draw.test-model/posn [120 120]}]
     (keyword namespace "control-point") [cp cp]})))


(defn find-flows
  "
  Creates a list of flows that are either inputs or ouputs of the processes in
  the list.
  "
  ([procs]
   (find-flows procs default-namespace))
  ([procs namespace]
   (let [data-in-key (keyword namespace "data-in")
         data-out-key (keyword namespace "data-out")]
     (flatten  (for [proc procs]
                  (concat  (->> proc
                                data-in-key)
                           (->> proc
                                data-out-key)))))))


(defn find-node-in-list
  "Given a proc id find a node or nil in the given list of nodes.  This
   is a flat list search"
  [id procs name-space]
  (filter #(= ((keyword name-space "id") %) id) procs))


(defn remove-node-from-list
  "Removes the node with the given id from a list of nodes or does nothing."
  [id procs name-space]
  (filter #(not= ((keyword name-space "id") %) id) procs))



(defn remove-node-from-model
  "Removes the node with the given id and all its children etc from the model"
  [id model name-space]
  (let [children-keyword (keyword name-space "children")
        parent-id (parent-id id)
        parent-node (find-node-in-tree parent-id model name-space)
        peers (children-keyword parent-node)
        updated-parent (assoc-in parent-node [children-keyword] (remove-node-from-list id peers name-space))]
    (update-node parent-id updated-parent model name-space)))

(defn prev-next
  "
        Given a sorted list and an item returns a pair with the prev and next items
        from the given item, may contain nil.
      "
  [sorted-list namespace id]
  (let [pairs  (map list (into [nil] (vec sorted-list)) (conj (vec sorted-list) nil))
        this   (first (filter (fn [%] (= ((keyword namespace "id") %) id)) sorted-list))
        prev   (first (first (filter (fn [%] (= ((keyword namespace "id") (second %)) id)) pairs)))
        next   (second (first (filter (fn [%] (= ((keyword namespace "id") (first %)) id)) pairs)))]
    [prev this next]))



(defn fix-subtree-number
  "
    Maps an old subtree number onto a new one.
    For example, if we swap a node with an id of
    1.1 to the right, ie it becomes 1.2 (and 1.2 becomes 1.1) then
    the associated subtrees need to be remapped as well.

    1.1       => 1.2
    1.1.1     => 1.2.1
    1.1.4     => 1.2.4

    Also
    1.2       => 1.1
    1.2.1     => 1.1.1
    1.2.4     => 1.1.4

    But
    1.3 => 1.3

    etc


  "
  ([node old-prefix new-prefix]
   (fix-subtree-number node default-namespace old-prefix new-prefix))
  ([node namespace old-prefix new-prefix]
   (update-in node [(keyword namespace "id")] (fn [old a b] (replace-first old a b)) old-prefix new-prefix)))





(defn find-missing-node-ids
  "
    Find the ids of any nodes that may be missing.  If no node is missing or the
    node list is empty return and empty list otherwise return the id.
    eg [{:sadf/id 1.1} {:sadf/id 1.4} {:sadf/id 1.5}] => [1.2 1.3]
  "
  [node-list namespace]
  (if (seq node-list)
    (let [sorted-node-list (sort-node-list namespace node-list)
          pid (parent-id ((keyword namespace "id") (first sorted-node-list)))
          node-ids (set (map #((keyword namespace "id") %) sorted-node-list))
          last-number (js/parseInt (last-digit ((keyword namespace "id") (last sorted-node-list))))
          ;If the id is numbered like "1" there is no parent to add
          full-seq (if (= pid "0")
                     (set (map str (take last-number (iterate inc 1))))
                     (set (map #(str pid "." %) (seq (take last-number (iterate inc 1))))))
          missing-ids (clojure.set/difference full-seq node-ids)]
      (if (some? (seq missing-ids)) (seq missing-ids) []))
    []))

(defn has-missing-nodes?
  "
        If the list of nodes has a gap return true otherwise return false.
        eg [1.1 1.2 1.3] => false
           [1.1 1.2 1.4] => true
      "
  [node-list namespace]
  (some? (seq (find-missing-node-ids node-list namespace))))

(defn fix-subtree-numbers
  "
    For the given node, change all the subtree ids at the given level
    for the new number.  For example:

    level = 2 id = 2

    1.1.1   => 1.1.2
    1.1.1.1 => 1.1.2.1
    1.1.1.2 => 1.1.2.2
  "
  [node namespace old-prefix new-prefix]
  ;if its just an empty node, fix it and return
  (if (not (some? (seq ((keyword namespace "children") node))))
    (fix-subtree-number node namespace old-prefix new-prefix)
    (assoc (fix-subtree-number node namespace old-prefix new-prefix) (keyword namespace "children")
        (reduce
          (fn [%1 %2] (conj %1 (fix-subtree-numbers %2 namespace old-prefix new-prefix)))
          [] ((keyword namespace "children") node)))))


(defn compact-node-list
  "
    If a node list has a gap, eg [{:sadf/id 1.1} {:sadf/id 1.3}]
    then renumber the nodes so that the gap is eliminated.
    ie [{:sadf/id 1.1} {:sadf/id 1.3}] => [{:sadf/id 1.1} {:sadf/id 1.2}]
       [{:sadf/id 1.2} {:sadf/id 1.3}] => [{:sadf/id 1.1} {:sadf/id 1.2}]
  "
  ([node-list]
   (compact-node-list node-list default-namespace))
  ([node-list namespace]
   (let [missing-ids (sort-id-list (find-missing-node-ids (sort-node-list namespace node-list) namespace))]
     (if (some? (seq missing-ids))
       (loop [nodes node-list
              missing missing-ids]
         (println (str "MISSING IDS " missing))
         (println (str "NODES " nodes))
         (let [next-id (next-child (parent-id (last missing)) [{(keyword namespace "id") (last missing)}] namespace)
               fixed (fix-subtree-numbers (first (filter #(= ((keyword namespace "id") %) next-id) nodes)) namespace next-id (last missing))
               dropped (filter #(not= ((keyword namespace "id") %) next-id) nodes)
               compacted (sort-node-list namespace (conj dropped fixed))]
           (if (<= (count (find-missing-node-ids (sort-node-list namespace compacted) namespace)) 1)
             compacted
             (recur compacted
                    (sort-id-list (find-missing-node-ids (sort-node-list namespace compacted) namespace))))));(drop-last (sort-id-list missing))))))
       node-list))))



(defn swap-right
  "
    If this node does not have any nodes logically to the right of it do nothing
    otherwise swap to the right

    Returns an updated proc model (with possibly no changes).
  "
  ([proc-model id]
   (swap-right proc-model default-namespace id))
  ([proc-model namespace id]

   (if (or (= id "0") (= id "context"))
     proc-model
     (let [parent-id   (parent-id id)
           parent-node (find-node-in-tree parent-id proc-model namespace)
           peers       ((keyword namespace "children") parent-node)
           sorted      (sort-node-list namespace peers)
           ;A triple of prev, this and next nodes where nil is a node unavailable
           pn-pair     (prev-next sorted namespace id)]

       ;if the second is nil there was nothing to the right so do nothing
       (if (nth pn-pair 2)

          (let [new-left (-> (fix-subtree-numbers (second pn-pair) namespace ((keyword namespace "id") (second pn-pair))
                                                                            ((keyword namespace "id") (nth pn-pair 2)))
                             (assoc-in  [(keyword namespace "id")] ((keyword namespace "id") (nth pn-pair 2))))



                new-right (-> (fix-subtree-numbers (nth pn-pair 2) namespace ((keyword namespace "id") (nth pn-pair 2))
                                                                             ((keyword namespace "id") (second pn-pair)))

                             (assoc-in [(keyword namespace "id")] ((keyword namespace "id") (second pn-pair))))]

            (as-> proc-model model
                  (update-node ((keyword namespace "id") new-left) new-left model namespace)
                  (update-node ((keyword namespace "id") new-right) new-right model namespace)))
          proc-model)))))


(defn swap-left
  "
                        If this node does not have any nodes logically to the left of it do nothing
                        otherwise swap to the left

                        Returns an updated proc model (with possibly no changes).
                      "
  ([proc-model id]
   (swap-left proc-model default-namespace id))
  ([proc-model namespace id]

   (if (or (= id "0") (= id "context"))
     proc-model
     (let [parent-id   (parent-id id)
           parent-node (find-node-in-tree parent-id proc-model namespace)
           peers       ((keyword namespace "children") parent-node)
           sorted      (sort-node-list namespace peers)
           ;A triple of prev, this and next nodes where nil is a node unavailable
           pn-pair     (prev-next sorted namespace id)]

       ;if the second is nil there was nothing to the right so do nothing
       (if (first pn-pair)

          (let [new-left (-> (fix-subtree-numbers (second pn-pair) namespace ((keyword namespace "id") (second pn-pair))
                                                                             ((keyword namespace "id") (first pn-pair)))
                             (assoc-in  [(keyword namespace "id")] ((keyword namespace "id") (first pn-pair))))

                new-right (-> (fix-subtree-numbers (first pn-pair) namespace ((keyword namespace "id") (first pn-pair))
                                                                             ((keyword namespace "id") (second pn-pair)))
                             (assoc-in [(keyword namespace "id")] ((keyword namespace "id") (second pn-pair))))]
            (as-> proc-model model
                  (update-node ((keyword namespace "id") new-left) new-left model namespace)
                  (update-node ((keyword namespace "id") new-right) new-right model namespace)))
          proc-model)))))


(defn move-up
  "
              Moves the given node up a level, ie becomes a sibling of its parent by intorducing
              a new node above it and moving its sub tree.  The old node and its subtree are removed.

              For example if 1.1.1 is a child of 1.1 and 1.1 and 1.2 are children of 0 then
              shifting 1.1.1 up will make it node 1.3, with same rule applying recursively
              to all the children of 1.1.1


            "
  ([proc-model id]
   (move-up proc-model default-namespace id))
  ([proc-model namespace id]
   ;check the id, it cant be 0 or a level below zer (ie 1,2 etc) it must be
   ;at least 1.1, 2,3 etc or below.
   (if (or (= (parent-id id) "0") (= (parent-id id) 0) (= (parent-id id) \0))
     proc-model
     (let [node               (find-node-in-tree id proc-model namespace)
           p-id               (parent-id id)
           pp-id              (parent-id p-id)
           parent-parent-node (find-node-in-tree pp-id proc-model namespace)
           peers              ((keyword namespace "children") parent-parent-node)
           new-id             (next-child pp-id peers namespace)
           updated-node       (assoc node (keyword namespace "id") new-id)
           fixed-updated-node  (fix-subtree-numbers updated-node namespace id new-id)]
        (as-> proc-model model
          (add-node pp-id fixed-updated-node model namespace)
          (remove-node-from-model id model namespace))))))


(defn add-new-child-node
  "
    Add a new empty node as a child of the given id
  "
  ([proc-model id]
   (add-new-child-node proc-model default-namespace id))

  ([proc-model namespace id]
   (add-new-child-node proc-model default-namespace id nil))

  ([proc-model namespace id icon-spec]
   (let [node    (find-node-in-tree id proc-model namespace)
         peers   ((keyword namespace "children") node)
         new-id  (next-child id peers namespace)]
    (if icon-spec
      (add-node id (make-iconic-node new-id "Title" [] [] "Description" namespace icon-spec) proc-model namespace)
      (add-node id (make-node new-id "Title" [] [] "Description" namespace) proc-model namespace)))))


(defn fix-namespace
  ([node]
   (fix-namespace node default-namespace))
  ([node namespace]
   (let [ks (reduce #(assoc %1 %2 (keyword namespace (name %2))) {} (keys node))]
     (-> (rename-keys node ks)
         (assoc (keyword namespace "icon") nil)
         (assoc (keyword namespace "delegate") "")))))


(defn add-change-namespace
  "
    Either add the given name space or changes any namespaces already present
    to the given name space.
  "
  ([proc-model]
   (add-change-namespace proc-model default-namespace))
  ([proc-model namespace]
   (let [fixed-node (fix-namespace proc-model namespace)]
     (if (some? (seq ((keyword namespace "children") fixed-node)))
       (assoc fixed-node (keyword namespace "children")
           (reduce
             (fn [%1 %2] (conj %1 (add-change-namespace %2 namespace)))
             [] ((keyword namespace "children") fixed-node)))
       fixed-node))))



(defn remove-context-objects
  "
    Given a model and an optional namespace, remove all the context objects.
  "
  ([model]
   (remove-context-objects model default-namespace))
  ([model namespace]
   (let [context-objects (keyword namespace "context-objects")]
     (-> model
         (assoc-in  [context-objects (keyword namespace "sources")] [])
         (assoc-in  [context-objects (keyword namespace "sinks")] [])
         (assoc-in  [context-objects (keyword namespace "consumers")] [])
         (assoc-in  [context-objects (keyword namespace "producers")] [])))))
