(ns sa-draw.events
  "
    Top level events.
  "
  (:require [re-frame.core                          :as re-frame]
            [goog.object                            :as gobj]
            [material-ui                            :as mui]
            ["@material-ui/core/colors"             :as muic]
            [sa-draw.config.defaults                :as decon :refer [default-config version]]
            [cljs.core.async                        :refer [chan <! >! close! timeout alts! go-loop] :refer-macros [go]]
            [cljs-time.core                         :as dt]
            [cljs-time.format                       :as dtfm]
            [sa.sa-store.store                      :as sa]
            [sa-draw.utils.download                 :as dld]
            [sa.sa-store.store-factory              :as sf]
            [sa-draw.model.proto-model              :as proto-model]
            [sa-draw.model.default-model            :as dm]
            [sa-draw.model.map-impl                 :refer [map-model proc-model]]))



;Initialise the state of the app
(re-frame/reg-event-db
  ::initialize-db
  (fn  [_ [_ layout model _ [_ _ title description data-in data-out name-space]]]
     ;The default is 1100 however, changing the drawer z index below didnt seem to have an
     ;effect so Ive raised the app bar index.- see https://material-ui.com/customization/z-index/
     (let [app-bar-zindex 1201
           theme (mui/createMuiTheme (clj->js  {:palette   { :primary  {:light "#757ce8"
                                                                        :main (gobj/get (.-blue muic) 200)
                                                                        :dark "#002884"
                                                                        :contrastText "#fff"}
                                                             :secondary {:light "#ff7961"
                                                                         :main (gobj/get (.-yellow muic) 100)
                                                                         :dark "#ba000d"
                                                                         :contrastText "##000"}
                                                             :background {:paper (gobj/get (.-blue muic) 50)}}
                                                :zIndex {:appBar app-bar-zindex
                                                         :drawer (- app-bar-zindex 1) ;Put the drawers under the app bar
                                                         :popper 1}
                                                :chip {:color "#757ce8"}
                                                :typography {:data-term-button {:textTransform "none"}}
                                                :spacing 8}))]

          {::graphics {::current-layer {::layer "context"
                                        ::title title
                                        ::description description
                                        ::data-in data-in
                                        ::data-out data-out
                                        ::name-space name-space}
                       ::edit-locked true
                       ::layout layout}

           ::config
             {::style
               {::theme theme
                ::dimensions {::app-bar-height 44
                              ::model-bar-height 44}}
              ::defaults default-config
              ::drawing-defaults default-config
              ::current-store ::sf/key-store}
           ::views {::home? true
                    :sa-draw.views.help.help-events/help {:sa-draw.views.help.help-events/open? false}}
           ::user {::user-name "Fred Smith"
                   ::user-email "fredsmith@provider.com"}
           ::projects {}
           ::drawing-mode ::svg ;::can
           ::model model
           ::model-summaries {}
           ::app-version version
           ::dirty false})))

(re-frame/reg-event-fx
  ::new-current-layer
  (fn [cofx [_ layer-id title description data-in data-out]]
      {:db (assoc-in (:db cofx) [::graphics ::current-layer] {::layer layer-id
                                                              ::title title
                                                              ::description description
                                                              ::data-in data-in
                                                              ::data-out data-out
                                                              ::name-space "sadf"})}))

;This event is like the one above but allows for a destructured vector of parameters
;to be used.
(re-frame/reg-event-fx
  ::new-current-layer-v
  (fn [cofx [_ [_ layer-id title description data-in data-out _]]]
      {:db (assoc-in (:db cofx) [::graphics ::current-layer] {::layer layer-id
                                                              ::title title
                                                              ::description description
                                                              ::data-in data-in
                                                              ::data-out data-out
                                                              ::name-space "sadf"})}))


(re-frame/reg-event-fx
  ::lock-edit
  (fn [cofx [_ edit-locked?]]
    (let [db (:db cofx)
          updated-db (assoc-in  db [::graphics ::edit-locked] edit-locked?)]
        {:db updated-db})))

(re-frame/reg-event-db
  ::freehand-pen
  (fn [db _]
     db))

(re-frame/reg-event-db
  ::freehand-mouse
  (fn [db _]
    db))


(re-frame/reg-event-db
  ::prepacked
  (fn [db _]
    db))

(re-frame/reg-event-db
  ::storage
  (fn [db store]
    (assoc-in db [::config ::store] store)))




;;;;;;;;;;;;;;;;;;;;;;;
;Save Model
;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-db
 :incoming-model
 (fn [db [_ model]]
  (let [layout (:sadf/layout model)]
      (-> db
        (assoc-in  [::graphics ::layout] layout)
        (assoc-in  [::model] model)))))


(re-frame/reg-fx
 ::save-model-fx
 (fn [req]
   (let [update-chan (::update-chan req)]
     (sa/update-model (sf/get-store (::store-id req))
                      update-chan
                      (::uuid req)
                      (::updated-model req)))
   (re-frame/dispatch [::load-summaries (::store-id req)])))


(re-frame/reg-event-fx
  ::save-model
  (fn [cofx [_ store-id]]
    (let [update-chan   (chan)
          layout        (-> cofx :db ::graphics ::layout)
          updated-model (assoc-in  (-> cofx :db ::model ) [:sadf/layout] layout)
          versioned-model (assoc-in  updated-model [:sadf/meta :sadf/app-version] (-> cofx :db ::app-version))]
      {::save-model-fx {::store-id store-id
                        ::update-chan update-chan
                        ::uuid (-> cofx
                                   :db
                                   ::model
                                   :sadf/meta
                                   :sadf/uuid)
                        ::updated-model versioned-model}})))


;;;;;;;;;;;;;
;Load a completely empty model as a reset condition for the db.
;This should be done synchronously
;;;;;;;;;;;;;
(re-frame/reg-event-db
  ::empty-model
  (fn [db _]
    (-> db
      (assoc ::model dm/empty-model))))
      ; (assoc [::user ::user-name] "")
      ; (assoc [::user ::user-email] "")
      ; (assoc-in [::views ::home?] true)
      ; (assoc-in [:sa-draw.views.help.help-events/help :sa-draw.views.help.help-events/open?] false))))


;;;;;;;;;;;;;
;Load External (Github/Bitbucket raw URL) Model
;;;;;;;;;;;;;

(defn load-handler
  "Given a model we raise a re-load event to get the model into the
   state store."
  [model db]
  (if-not (= :sa.sa-store.browser-keystore/not-found model)
    (let [procs-model (proto-model/find-process-model map-model model)
          context-node (proto-model/find-node proc-model procs-model "0")
          [_ _ title description data-in data-out] (proto-model/details proc-model context-node)]
      (re-frame/dispatch [:incoming-model model])
      (re-frame/dispatch [::new-current-layer "context" title description data-in data-out])
      (re-frame/dispatch [:sa-draw.views.markdown.markdown-viewer-events/markdown-from-json]))))

(re-frame/reg-fx
  ::load-raw-git-model-fx
  (fn [req]
    (go (let [get-chan (chan)]
          (sa/get-model-by-uri  (sf/get-store (::store-id req))
                                get-chan
                                (::model-url req))
          (load-handler (<! get-chan) (:db req))))))


(re-frame/reg-event-fx
  ::load-raw-git-model
  (fn [cofx [_ model-url store-id]]
    {::load-raw-git-model-fx {::store-id store-id
                              ::model-url model-url
                              ::db (:db cofx)}}))


;;;;;;;;;;;;;
;Load Model
;;;;;;;;;;;;;

(re-frame/reg-fx
  ::load-model-fx
  (fn [req]
    (go (let [get-chan (chan)]
          (sa/get-model (sf/get-store (::store-id req))
                        get-chan
                        (::model-name req))
          (load-handler (<! get-chan) (:db req))))))


(re-frame/reg-event-fx
  ::load-model
  (fn [cofx [_ model-name store-id]]
    {::load-model-fx {::store-id store-id
                      ::model-name model-name
                      ::db (:db cofx)}}))


;EDIT MODEL OPERATIONS


;when we add a new node we need to add it to the layout.
(re-frame/reg-event-db
  ::add-new-node
  (fn [db [_ current-layer node new-id]]
     (let [procs-model (proto-model/find-process-model map-model (::model db))
           updated-procs-model (proto-model/add-node proc-model current-layer node procs-model)
           updated-model (proto-model/update-process-model map-model (::model db) updated-procs-model)]
       (-> db
         (assoc-in [::model] updated-model)
         (assoc-in [::graphics ::layout (keyword "sadf" new-id)] [60 60])))))

(defn find-summaries-for-store
  [store-id summaries]
  (filter (fn [summary] (= store-id (::store summary))) summaries))

;THIS NEEDS TO EITHER MERGE IN THE STORE OR INSERT IT
(re-frame/reg-event-db
  ::update-summaries
  (fn [db [_ summaries store-id]]
    (let [old-summaries (-> db ::model-summaries store-id)]
      (if old-summaries
        (assoc-in db [::model-summaries] (merge (get-in db [::model-summaries]) {store-id summaries}))
        (assoc-in db [::model-summaries store-id] summaries)))))


(defn load-summaries-handler
  "Given a vector of model summaries we raise an update summaries event."
  [summaries store-id]
  (if (and (not= :sa.sa-store.browser-keystore/not-found summaries)
           (not= false summaries))
    (re-frame/dispatch [::update-summaries summaries store-id])))


(re-frame/reg-fx
  ::load-summaries-fx
  (fn [req]
    (go (let [store (sf/get-store (::store-id req))
              sum-chan (chan)]
          (sa/summarise-all store sum-chan)
          (load-summaries-handler (<! sum-chan) (::store-id req))))))


(re-frame/reg-event-fx
  ::load-summaries
  (fn [cofx [_ store-id]]
    {::load-summaries-fx {::store-id store-id
                          ::db (::db cofx)}}))

;;;;;;;;;;;;;;;;;;;;;;
;;;;NEW MODEL CREATION
;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-fx
  ::new-model-fx
  (fn [{::keys [store-id model-name model-id creation-date-time]}]
      (go (let [store (sf/get-store store-id)
                create-chan (chan)
                sum-chan (chan)
                new-model (assoc-in (proto-model/make-model map-model model-id model-name) [:sadf/meta :sadf/creation-date-time] creation-date-time)]
            (sa/update-model store create-chan model-id new-model)
            (sa/summarise-all store create-chan)
            (load-summaries-handler (<! sum-chan) store)))))

(re-frame/reg-event-fx
  ::new-model
  (fn [cofx [_ model-name model-id store-id]]
    {::new-model-fx {::store-id store-id
                     ::model-name model-name
                     ::model-id model-id
                     ::creation-date-time (->> (dt/now) (dtfm/unparse (dtfm/formatter "yyyy-MM-dd HH:mm:ss")))
                     ::db (::db cofx)}
     ::load-summaries-fx {::store-id store-id
                          ::db (::db cofx)}}))

;;;;;;;;;;
;;;; DELETE A MODEL
;;;;;;;;;
(re-frame/reg-event-db
  ::delete-model-by-id
  (fn [db [_ store-id id]]
    (let [store (sf/get-store store-id)
          resp-chan (chan)
          timed (timeout 2000)]
      (sa/delete-model store resp-chan id)
      (go
        (let [[v c] (alts! [resp-chan timed])]
          (if (not= c timed)
            (print (str "Deleted " v))
            (print "Failed to delete in time")))))
    db))


(re-frame/reg-event-db
  ::drawing-mode
  (fn [db [_]]
    (if (= (::drawing-mode db) ::svg)
      (assoc db ::drawing-mode ::can)
      (assoc db ::drawing-mode ::svg))))

(def old-lock-state (atom false))


(re-frame/reg-fx
  ::set-edit-lock
  (fn [req]
    (re-frame/dispatch [::lock-edit  true])))


(re-frame/reg-fx
  ::reset-edit-lock
  (fn [req]
    (re-frame/dispatch [::lock-edit (::old-lock-state req)])))

;;FIXME: It would be better if we had a notification of when the svg has been updated.
;;       WIthout the timeout we end up capturing the svg before the edit markers have been removed.
(re-frame/reg-fx
  ::download-svg-fx
  (fn [req]
    (go
      (let [_ (<! (timeout 1000))] ;1 second pause before downloading the svg
        (dld/download (::model-name req) (::model-uuid req) (::layer req))
        (re-frame/dispatch [::lock-edit @old-lock-state])))))

(re-frame/reg-event-fx
  ::download-svg
  (fn [cofx [_ model-name model-uuid layer lock-state]]
    (reset! old-lock-state lock-state)
    {::set-edit-lock true
     ::download-svg-fx {::model-name model-name
                        ::model-uuid model-uuid
                        ::layer layer}
     :db (:db cofx)}))



;;FIXME: This operation to download from the browser to the local file system and upload from
;;       the file system to the browser should be a store on the sa-store module.  Then the read model
;;       and write model code below will just be another store operation.


(re-frame/reg-fx
  ::download-to-file-System
  (fn [req]
    (go (let [get-chan (chan)]
          (sa/get-model (sf/get-store (::store-id req))
                        get-chan
                        (::model-id req))
          (dld/download-model! (::model-title req) (::model-id req) (<! get-chan))))))


(re-frame/reg-event-fx
  ::download-transit
  (fn [cofx [_ title id]]
    {::download-to-file-System {::model-title title
                                ::model-id id
                                ::store-id :sa.sa-store.store-factory/key-store}}))

(re-frame/reg-fx
  ::store-uploaded-model
  (fn [req]
    (let [model (::uploaded-model req)
          db    (:db req)]
      (load-handler model db))))

(re-frame/reg-event-fx
  ::upload-model
  (fn [cofx [_ model]]
     ; {::store-uploaded-model {::uploaded-model model}
     ;                          :db (:db cofx)}))

    (let [update-chan (chan)]
      {::save-model-fx {::store-id ::sf/key-store
                        ::update-chan update-chan
                        ::uuid (-> model
                                   :sadf/meta
                                   :sadf/uuid)
                        ::updated-model model}})))

(re-frame/reg-event-fx
  ::dirty
  ;(fn-traced  [cofx [_ dirty?]]
  (fn [cofx [_ dirty?]]
    {:db (assoc-in (:db cofx) [::dirty] dirty?)}))


;;;;;;Markdown related
(re-frame/reg-event-db
  ::update-markdown
  (fn [db [_ md]]
    (let [markdown-div (-> js/document
                           (.getElementById "markdown-content"))]
      (set! (. markdown-div -innerHTML) (js/marked (clj->js md))))
    (assoc-in db [::markdown] md)))
