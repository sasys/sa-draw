(ns sa-draw.components.card-node-edit
  (:require
    [reagent.core                                  :as r]
    [re-frame.core                                 :as rf]
    [material-ui                                   :as mui]
    [material-ui-icons                             :as muicons]
    [sa-draw.subs                                  :as subs]
    [sa-draw.views.editors.node-events             :as ne]
    [sa-draw.views.pspec.events                    :as pev]
    [sa-draw.views.editors.md.events               :as meev]
    [sa-draw.events                                :as ev]
    [sa-draw.views.markdown.markdown-viewer-events :as mdev]
    [sa-draw.model.icons                           :as icons]
    [sa-draw.views.dfd.svg.draw                    :as draw]
    [sa-draw.model.map-impl                        :refer [map-model proc-model]]
    [sa-draw.model.icons-impl                      :as iconsim :refer [im]]
    [sa-draw.model.proto-model                     :as proto-model]))

(def DEFAULT-MD-ICON {:sadf/layer   "0"
                      :sadf/link    nil
                      :sadf/description ""
                      :sadf/meta    {}
                      :uri          "/svg/local/note.svg"
                      :sadf/name    "note"
                      :sadf/type    "Local"
                      :sadf/source  "Software By Numbers Ltd"
                      :sadf/title   ""})

(defn edit
  "A node editor based on a card, built from scratch as a type 3 react component.
   Updates the supplied value atom with changed description text.
   open? is an atom indicating if the node is open"
  [node open-nodes]

  ;If this node does not have an entry in the in the open-nodes map, add it as closed.
  (if (not (contains? (set (keys @open-nodes)) (keyword (:sadf/id node)))) (swap! open-nodes assoc (keyword (:sadf/id node)) false))
  (let [id       (atom (random-uuid))
        value    (atom (:sadf/description node))
        node-id  (:sadf/id node)
        model    @(rf/subscribe [::subs/model])
        procs-model (proto-model/find-process-model map-model model)
        current-layer @(rf/subscribe [::subs/current-layer])
        current-node (proto-model/find-node proc-model procs-model current-layer)
        oninput-handler (fn [title]  (reset! value (-> js/document
                                                       (.getElementById @id)
                                                       (.-innerText)))
                                (rf/dispatch [::pev/update-node node-id title @value [] [] ""])
                                (rf/dispatch [::mdev/markdown-from-json])
                                (rf/dispatch [::ev/dirty true]))
        ondelete-handler (fn []
                           (rf/dispatch [::ne/node-edit-delete node-id])
                           (rf/dispatch [::mdev/markdown-from-json])
                           (rf/dispatch [::ev/new-current-layer-v (proto-model/details proc-model current-node)]))]

    (r/create-class
      {:display-name  (str "node-edit" @id)

       :component-did-mount
       (fn [this])

       :component-did-update
       (fn [this old-argv]
         (let [new-argv (rest (r/argv this))]))

       :reagent-render
       (fn [node open-nodes]
         (let  [open?  ((keyword node-id) @open-nodes)
                more  (r/create-element
                       "my-more"
                       #js{:className "my-more"}
                       (r/as-element  (if ((keyword node-id) @open-nodes)
                                        [:> mui/Tooltip {:title "Close"}  [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(swap! open-nodes assoc (keyword node-id) false)} [:> muicons/ExpandLess]]]
                                        [:> mui/Tooltip {:title "Expand"} [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(swap! open-nodes assoc (keyword node-id) true)}  [:> muicons/ExpandMore]]])))
                prototype-icons     (rf/subscribe [::subs/default-icons])
                icon   (:sadf/icon node)

                header-icon  (if (icons/svg-for-icon im @prototype-icons icon)
                               (r/create-element
                                   "header-icon"
                                   #js{:classname "header-icon"}
                                   (r/as-element [:> mui/SvgIcon (draw/scale-only-icon (icons/svg-for-icon im @prototype-icons icon) 1)]))

                               (r/create-element
                                  "header-icon"
                                  #js{:classname "header-icon"}
                                  (r/as-element [:> mui/SvgIcon [:> muicons/PanoramaFishEye]])))]
                                  ; (r/as-element [:> mui/SvgIcon
                                  ;                     [:path {:class "st1" :d "M 20 2 L 4 2 C 2.894531 2 2 2.894531 2 4 L 2 20 C 2 21.105469 2.894531 22 4 22 L 20 22 C 21.105469 22 22 21.105469 22 20 L 22 4 C 22 2.894531 21.105469 2 20 2 Z M 11 18 C 11 18.550781 10.550781 19 10 19 L 5 19 C 4.445313 19 4 18.550781 4 18 L 4 5 C 4 4.449219 4.445313 4 5 4 L 10 4 C 10.550781 4 11 4.449219 11 5 Z M 20 11.996094 C 20 12.550781 19.550781 13 18.996094 13 L 14.003906 13 C 13.449219 13 13 12.550781 13 11.996094 L 13 5.003906 C 13 4.449219 13.449219 4 14.003906 4 L 18.996094 4 C 19.550781 4 20 4.449219 20 5.003906 Z "}]])))]

          (reset! value (:sadf/description node))
          [:> mui/Card {:width "100%" :style {:margin "5px"}}
           [:> mui/CardHeader {:avatar header-icon :title (:sadf/id node) :subheader (:sadf/title node) :action more}]

           [:> mui/CardActions
             [:div {:style {:display (if open? "block" "none")}}
               [:> mui/Tooltip {:key (str node-id "-ttsave") :title "Save changes"}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick (partial oninput-handler (:sadf/title node))}
                     [:> muicons/Save {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttdel") :title "Delete this node AND SUBTREE"}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick ondelete-handler}
                     [:> muicons/Delete {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttnew") :title "Add a new (child) node"}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(do (rf/dispatch [::meev/add-node-with-icon node-id DEFAULT-MD-ICON])
                                                                                                  (rf/dispatch [::mdev/markdown-from-json])
                                                                                                  (rf/dispatch [::ev/dirty true]))}
                     [:> muicons/Add {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttright") :title "Shift this node right (swap right)"}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(do (rf/dispatch [::meev/move-right node-id])
                                                                                                  (rf/dispatch [::mdev/markdown-from-json])
                                                                                                  (rf/dispatch [::ev/dirty true]))}
                     [:> muicons/ArrowForward {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttleft") :title "Shift this node left (swap left)"}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(do (rf/dispatch [::meev/move-left node-id])
                                                                                                  (rf/dispatch [::mdev/markdown-from-json])
                                                                                                  (rf/dispatch [::ev/dirty true]))}
                     [:> muicons/ArrowBack {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttup") :title "Shift this node up"}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(do (rf/dispatch [::meev/move-up node-id])
                                                                                                  (rf/dispatch [::mdev/markdown-from-json])
                                                                                                  (rf/dispatch [::ev/dirty true]))}
                     [:> muicons/ArrowUpward {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttdown") :title "Not yet implemented:Shift this node down (insert parent)"}
                 [:span
                   [:> mui/IconButton {:disabled true :variant "text" :size "small" :type "button" :onClick (partial oninput-handler (:sadf/title node))}
                     [:> muicons/ArrowDownward {:classes {:root "material-icons md-18"}}]]]]

               [:> mui/Tooltip {:key (str node-id "-ttcompact") :title "Compact the children of this node (re-number ids).\n\nYou may need to run this more than once if there are sequential gaps."}
                 [:span
                   [:> mui/IconButton {:variant "text" :size "small" :type "button" :onClick #(do (rf/dispatch [::meev/compact node-id])
                                                                                                  (rf/dispatch [::mdev/markdown-from-json])
                                                                                                  (rf/dispatch [::ev/dirty true]))}
                     [:> muicons/Transform {:classes {:root "material-icons md-18"}}]]]]]]

           [:> mui/CardContent
             [:div {:style {:padding "5px" :background-color "lightgrey" :white-space "pre-wrap" :display (if open? "block" "none")} :suppressContentEditableWarning true :contentEditable true :id @id }  @value]]]))})))
