(ns sa-draw.code-splits.editor-split
  "
    Lazy load the parts of the editor.
    Note, we load the theme for each render operation.  If this is not done the mui
    components load the default theme which then overwrites the the custom theme for
    the rest of the application.
  "
  (:require [cljs.loader                                      :as loader]
            [reagent.core                                     :as r]
            [re-frame.core                                    :as rf]
            [material-ui                                      :as mui]
            [sa-draw.subs                                     :as subs]
            [sa-draw.views.editors.dispatcher                 :as disp]
            [sa-draw.views.markdown.markdown-viewer           :as md]
            [sa-draw.views.editors.md.editor                  :as mded]
            [sa-draw.views.icon-manager.editor                :as icman]
            [sa-draw.views.dfd.svg.process-card               :as svpc]
            [sa-draw.views.layers                             :refer [layers-panel]]
            [sa-draw.config.config-drawer                     :refer [config-panel]]
            [sa-draw.views.menus.model-info.model-info-drawer :refer [model-info-panel]]
            [sa-draw.views.tool-bar                           :refer [project-tools]]))

(defn edit-tools
  "
    Renders the editor components into the div with the given id
    and the tool bar components into the div with the given id.
  "
  [layer svg-name  edit-div tool-bar tool-bar-drawers node-div spec-div markdown-div markdown-editor-div icon-manager-div]
  (print "LOADING EDIT SPLIT")

  (let [theme  @(rf/subscribe [::subs/theme])]

    (r/render ;The tool bar

              [:> mui/MuiThemeProvider {:theme theme}
                [project-tools]]
              (-> js/document
                  (.getElementById tool-bar)))


    (r/render ;slide over panels
              [:> mui/MuiThemeProvider {:theme theme}
                [layers-panel]
                [config-panel]
                [model-info-panel]]
              (-> js/document
                  (.getElementById tool-bar-drawers)))

    (r/render
                ;The svg view of any node
              [:> mui/MuiThemeProvider {:theme theme}
                [svpc/show-node svg-name layer]]
              (-> js/document
                  (.getElementById node-div)))

    ; (r/render
    ;             ;The spec of any node
    ;           [:> mui/MuiThemeProvider {:theme theme}
    ;             [ps/show-spec layer "Context" [] []]]
    ;           (-> js/document
    ;               (.getElementById spec-div)))

    (r/render
                ;The markdown viewer
              [:> mui/MuiThemeProvider {:theme theme}
                [md/markdown-view]]
              (-> js/document
                  (.getElementById markdown-div)))

    (r/render
                ;The markdown editor
              [:> mui/MuiThemeProvider {:theme theme}
                [mded/editor-ui]]
              (-> js/document
                  (.getElementById markdown-editor-div)))

    (r/render
                ;The icon manager
              [:> mui/MuiThemeProvider {:theme theme}
                [icman/icon-manager-ui]]
              (-> js/document
                  (.getElementById icon-manager-div)))

    (r/render
              [:> mui/MuiThemeProvider {:theme theme}
                ;node editor
                [disp/get-editor ::disp/node-editor]
                [disp/get-editor ::disp/new-node-editor]
                ;context object editors
                [disp/get-editor ::disp/source]
                [disp/get-editor ::disp/sink]
                [disp/get-editor ::disp/prod]
                [disp/get-editor ::disp/cons]
                [disp/get-editor ::disp/create-context-object]
                ;flow editors
                [disp/get-editor ::disp/flow-editor]
                [disp/get-editor ::disp/quick-flow]
                [disp/get-editor ::disp/file-upload-selector]
                [disp/get-editor ::disp/markdown-importer]
                ;symbols and icons
                [disp/get-editor ::disp/symbol-editor]
                [disp/get-editor ::disp/icon-editor]]


              (-> js/document
                  (.getElementById edit-div)))
    (print "\n\n EDIT SCREEN LOADED \n\n")))

(loader/set-loaded! :edit)
