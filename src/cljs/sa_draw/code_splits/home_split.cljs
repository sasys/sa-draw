(ns sa-draw.code-splits.home-split
  (:require [cljs.loader :as loader]
            [reagent.core                                     :as r]
            [re-frame.core                                    :as rf]
            [material-ui                                      :as mui]
            [sa-draw.subs                                     :as subs]
            [sa-draw.views.home-view.home                     :as home]
            [sa-draw.views.editors.meta-data-editor           :as meta-data-editor]
            [sa.sa-store.store-factory]))


(defn load-home-screen
  [home? home-screen]

  ;Get the summaries for the home screen.
  (doseq [store (descendants sa.sa-store.store-factory/store-hierarchy :sa.sa-store.store-factory/store)]
    (rf/dispatch [:sa-draw.events/load-summaries store]))

  (let [theme  @(rf/subscribe [::subs/theme])]
    (r/render
      [:> mui/MuiThemeProvider {:theme theme}
        [home/show-home-screen-new-model home?]
        [home/show-home-screen-models home?]
        [meta-data-editor/show-meta-data-editor home?]]
      (-> js/document
          (.getElementById home-screen))))
  (print "\n\n HOME SCREEN LOADED \n\n"))

(loader/set-loaded! :home)
