(ns sa-draw.utils.test-sem-ver
  (:require [cljs.test              :refer-macros [deftest is testing run-tests]]
            [sa-draw.utils.sem-ver  :as smvr]))

(def sem-ver-1
  {:sa-draw.config.defaults/major       0
   :sa-draw.config.defaults/minor       0
   :sa-draw.config.defaults/patch       0
   :sa-draw.config.defaults/pre-release 0
   :sa-draw.config.defaults/build-meta  0})
(def formatted-sem-ver-1 "0.0.0-0+0")


(deftest test-format-semver
  (testing "New sem ver"
    (let [ver (smvr/format-semver sem-ver-1)]
      (is (not (nil? ver)) "Not nil")
      (is (= formatted-sem-ver-1 ver) "Correct version"))))
