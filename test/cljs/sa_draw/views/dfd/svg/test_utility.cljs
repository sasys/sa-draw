(ns sa-draw.views.dfd.svg.test-utility
  "Testing dfd utilities"
  (:require [hickory.core                       :as h]
            [cljs.test                          :refer-macros [deftest is testing run-tests]]
            [sa-draw.views.dfd.svg.utility      :refer [find-transform-term string->tokens tokens->string tokens->map style->map
                                                        react-style->hiccup hiccup-style->react position-string->xy]]))



(def good-style "color:red;background:black;font-style:normal;font-size:20px")
(def html-fragment
  (str "<div style='" good-style "'><div id='a' class='btn' style='font-size:30px;color:white'>test1</div>test2</div>"))

(deftest test-position-string->xy
  (testing "expected pair"
    (is (= [22 22] (position-string->xy "22,22")))
    (is (= [22 22] (position-string->xy "22 22")))))



(deftest test-string-to-tokens
  (testing "Test read svg tag string into tokens"
    (is (= '("color" "red", "background" "black", "font-style" "normal", "font-size" "20px") (string->tokens good-style)))
    (is (= {"color" "red", "background" "black", "font-style" "normal", "font-size" "20px"} (tokens->map (string->tokens good-style))))
    (is (= {"color" "red", "background" "black", "font-style" "normal", "font-size" "20px"} (style->map good-style)))
    (is (= [:div {:style {"color" "red", "background" "black", "font-style" "normal", "font-size" "20px"}} [:div {:id "a", :class "btn", :style {"font-size" "30px", "color" "white"}} "test1"] "test2"]
           (react-style->hiccup (first (map h/as-hiccup (h/parse-fragment html-fragment))))))))

(deftest test-tokens-to-string
  (testing "test we can make name vals into a string"
    (let [style-map-1 {"name" "fred" "age" "22" "background" "green"}]
      (is (= "name:fred;age:22;background:green" (tokens->string style-map-1)))
      (is (= good-style (tokens->string {"color" "red", "background" "black", "font-style" "normal", "font-size" "20px"}))))))

(deftest test-hiccup-style->react
  (testing "test we can traverse a hiccup shape and fix any style attributes"
    (let [hiccup-1 [:svg [:g {:style {"name" "fred" "age" "22" "background" "green"}}]]
          hiccup-2 [:svg {:style {:a "b" "c" "d"}} [:g {:style {"name" "fred" "age" "22" "background" "green"}}]]]
      (is (= [:svg [:g {:style "name:fred;age:22;background:green"}]] (hiccup-style->react hiccup-1)))
      (is (= [:svg {:style "a:b;c:d"}[:g {:style "name:fred;age:22;background:green"}]] (hiccup-style->react hiccup-2))))))
(deftest test-find-translate
  (testing "Processing the svg transform string"
    (let [s1 "translate(-10,0,-23.1234) scale(1.4,1.4)"
          s2 "translate(-10,0,-23.1234) scale(1.4,1.4) translate(-15, -22)    scale(22.0 99)"]
      (is (= "translate(-10,0,-23.1234)" (find-transform-term s1 "translate")))
      (is (= "scale(1.4,1.4)" (find-transform-term s1 "scale")))
      (is (= "translate(-10,0,-23.1234)" (find-transform-term s2 "translate")))
      (is (= "scale(1.4,1.4)" (find-transform-term s2 "scale"))))))
