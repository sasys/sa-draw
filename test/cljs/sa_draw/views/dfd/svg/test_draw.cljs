(ns sa-draw.views.dfd.svg.test-draw
  "Testing svg drawing."
  (:require [cljs.test                          :refer-macros [deftest is testing run-tests]]
            [sa-draw.views.dfd.svg.draw         :as sdraw]
            [hickory.core                       :as hick]
            [hickory.zip                        :as hzip]
            [hickory.render                     :as hickr]
            [clojure.zip                        :as zip]))


(def svg-1     "<svg>
                   <g id='layer1'
                      transform='translate(-1.335742,-1.510374)'>
                    <path
                       id='path6553'
                       d='m 1.335742,12.335996 1.70997,-7.491565 6.92342,-3.334057 6.92339,3.334057 1.70998,7.491565 -4.7912,6.007796 h -7.68435 z'
                       style='fill:#326ce5;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.26458332;stroke-linecap:square;stroke-miterlimit:10' />
                    <path
                       style='fill:#eeeeee;fill-opacity:1;stroke-width:0.46185368'
                       d='m 14.636912,9.474263 h -0.69278 V 7.626847 c 0,-0.508038 -0.415668,-0.923706 -0.923706,-0.923706 H 11.17301 v -0.69278 c 0,-0.637359 -0.517276,-1.154636 -1.154634,-1.154636 -0.637358,0 -1.154634,0.517277 -1.154634,1.154636 v 0.69278 H 7.016326 c -0.508038,0 -0.919089,0.415668 -0.919089,0.923706 v 1.755045 h 0.688164 c 0.688161,0 1.247005,0.558843 1.247005,1.247005 0,0.688162 -0.558844,1.247005 -1.247005,1.247005 H 6.09262 v 1.755045 c 0,0.508038 0.415668,0.923706 0.923706,0.923706 h 1.755045 v -0.692781 c 0,-0.688161 0.558843,-1.247005 1.247005,-1.247005 0.688162,0 1.247005,0.558844 1.247005,1.247005 v 0.692781 h 1.755045 c 0.508038,0 0.923706,-0.415668 0.923706,-0.923706 v -1.847416 h 0.69278 c 0.637359,0 1.154636,-0.517276 1.154636,-1.154634 0,-0.637358 -0.517277,-1.154634 -1.154636,-1.154634 z'
                       id='path7415' />
                  </g>
                </svg>")

(def svg-g  "<svg><g id='layer1'
                 transform='translate(-1.335742,-1.510374)' style='font-size:30px'>
                <path id='path6553'
                       d='m 1.335742,12.335996 1.70997,-7.491565 6.92342,-3.334057 6.92339,3.334057 1.70998,7.491565 -4.7912,6.007796 h -7.68435 z'
                      style='fill:#326ce5;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.26458332;stroke-linecap:square;stroke-miterlimit:10'/>
                <path style='fill:#eeeeee;fill-opacity:1;stroke-width:0.46185368'
                          d='m 14.636912,9.474263 h -0.69278 V 7.626847 c 0,-0.508038 -0.415668,-0.923706 -0.923706,-0.923706 H 11.17301 v -0.69278 c 0,-0.637359 -0.517276,-1.154636 -1.154634,-1.154636 -0.637358,0 -1.154634,0.517277 -1.154634,1.154636 v 0.69278 H 7.016326 c -0.508038,0 -0.919089,0.415668 -0.919089,0.923706 v 1.755045 h 0.688164 c 0.688161,0 1.247005,0.558843 1.247005,1.247005 0,0.688162 -0.558844,1.247005 -1.247005,1.247005 H 6.09262 v 1.755045 c 0,0.508038 0.415668,0.923706    0.923706,0.923706 h 1.755045 v -0.692781 c 0,-0.688161 0.558843,-1.247005 1.247005,-1.247005 0.688162,0 1.247005,0.558844 1.247005,1.247005 v 0.692781 h 1.755045 c 0.508038,0 0.923706,-0.415668 0.923706,-0.923706 v -1.847416 h 0.69278 c 0.637359,0 1.154636,-0.517276 1.154636,-1.154634 0,-0.637358 -0.517277,-1.154634 -1.154636,-1.154634 z'
                         id='path7415' />
             </g></svg>")


(def svg-3  "<svg
                xmlns:dc=\"http://purl.org/dc/elements/1.1/\"
                xmlns:cc=\"http://creativecommons.org/ns#\"
                xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
                xmlns:svg=\"http://www.w3.org/2000/svg\"
                xmlns=\"http://www.w3.org/2000/svg\"
                xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"
                xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\">
                  <defs
                    id=\"defs13930\" />

                   <g id='layer1'
                      transform='translate(-1.335742,-1.510374)'>
                    <path
                       id='path6553'
                       d='m 1.335742,12.335996 1.70997,-7.491565 6.92342,-3.334057 6.92339,3.334057 1.70998,7.491565 -4.7912,6.007796 h -7.68435 z'
                       style='fill:#326ce5;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.26458332;stroke-linecap:square;stroke-miterlimit:10' />
                    <path
                       style='fill:#eeeeee;fill-opacity:1;stroke-width:0.46185368'
                       d='m 14.636912,9.474263 h -0.69278 V 7.626847 c 0,-0.508038 -0.415668,-0.923706 -0.923706,-0.923706 H 11.17301 v -0.69278 c 0,-0.637359 -0.517276,-1.154636 -1.154634,-1.154636 -0.637358,0 -1.154634,0.517277 -1.154634,1.154636 v 0.69278 H 7.016326 c -0.508038,0 -0.919089,0.415668 -0.919089,0.923706 v 1.755045 h 0.688164 c 0.688161,0 1.247005,0.558843 1.247005,1.247005 0,0.688162 -0.558844,1.247005 -1.247005,1.247005 H 6.09262 v 1.755045 c 0,0.508038 0.415668,0.923706 0.923706,0.923706 h 1.755045 v -0.692781 c 0,-0.688161 0.558843,-1.247005 1.247005,-1.247005 0.688162,0 1.247005,0.558844 1.247005,1.247005 v 0.692781 h 1.755045 c 0.508038,0 0.923706,-0.415668 0.923706,-0.923706 v -1.847416 h 0.69278 c 0.637359,0 1.154636,-0.517276 1.154636,-1.154634 0,-0.637358 -0.517277,-1.154634 -1.154636,-1.154634 z'
                       id='path7415' />
                  </g>
            </svg>")

(def svg-g-3 (str (str "<g id='layer1' transform='translate(-1.335742,-1.510374)'")
                  (str "  <path id='path6553' d='m 1.335742,12.335996 1.70997,-7.491565 6.92342,-3.334057 6.92339,3.334057 1.70998,7.491565 -4.7912,6.007796 h -7.68435 z' style='fill:#326ce5);fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.26458332;stroke-linecap:square;stroke-miterlimit:10' />")
                  (str "  <path style='fill:#eeeeee;fill-opacity:1;stroke-width:0.46185368' d='m 14.636912,9.474263 h -0.69278 V 7.626847 c 0,-0.508038 -0.415668,-0.923706 -0.923706,-0.923706 H 11.17301 v -0.69278 c 0,-0.637359 -0.517276,-1.154636 -1.154634,-1.154636 -0.637358,0 -1.154634,0.517277 -1.154634,1.154636 v 0.69278 H 7.016326 c -0.508038,0 -0.919089,0.415668 -0.919089,0.923706 v 1.755045 h 0.688164 c 0.688161,0 1.247005,0.558843 1.247005,1.247005 0,0.688162 -0.558844,1.247005 -1.247005,1.247005 H 6.09262 v 1.755045 c 0,0.508038 0.415668,0.923706" "0.923706,0.923706 h 1.755045 v -0.692781 c 0,-0.688161 0.558843,-1.247005 1.247005,-1.247005 0.688162,0 1.247005,0.558844 1.247005,1.247005 v 0.692781 h 1.755045 c 0.508038,0 0.923706,-0.415668 0.923706,-0.923706 v -1.847416 h 0.69278 c 0.637359,0 1.154636,-0.517276 1.154636,-1.154634 0,-0.637358 -0.517277,-1.154634 -1.154636,-1.154634 z'")
                  "        id='path7415' />"
                  (str "/g>")))

(def svg-g-3-1 "    <g id='layer1'
                      transform='translate(-1.335742,-1.510374)'>
                    <path
                       id='path6553'
                       d='m 1.335742,12.335996 1.70997,-7.491565 6.92342,-3.334057 6.92339,3.334057 1.70998,7.491565 -4.7912,6.007796 h -7.68435 z'
                       style='fill:#326ce5;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.26458332;stroke-linecap:square;stroke-miterlimit:10' />
                    <path
                       style='fill:#eeeeee;fill-opacity:1;stroke-width:0.46185368'
                       d='m 14.636912,9.474263 h -0.69278 V 7.626847 c 0,-0.508038 -0.415668,-0.923706 -0.923706,-0.923706 H 11.17301 v -0.69278 c 0,-0.637359 -0.517276,-1.154636 -1.154634,-1.154636 -0.637358,0 -1.154634,0.517277 -1.154634,1.154636 v 0.69278 H 7.016326 c -0.508038,0 -0.919089,0.415668 -0.919089,0.923706 v 1.755045 h 0.688164 c 0.688161,0 1.247005,0.558843 1.247005,1.247005 0,0.688162 -0.558844,1.247005 -1.247005,1.247005 H 6.09262 v 1.755045 c 0,0.508038 0.415668,0.923706 0.923706,0.923706 h 1.755045 v -0.692781 c 0,-0.688161 0.558843,-1.247005 1.247005,-1.247005 0.688162,0 1.247005,0.558844 1.247005,1.247005 v 0.692781 h 1.755045 c 0.508038,0 0.923706,-0.415668 0.923706,-0.923706 v -1.847416 h 0.69278 c 0.637359,0 1.154636,-0.517276 1.154636,-1.154634 0,-0.637358 -0.517277,-1.154634 -1.154636,-1.154634 z'
                       id='path7415' />
                  </g>")

(def simple-svg "<g id='fred'></g>")

(def svg-no-g  "<a href=foo>bar<br></a>")

(def good-style "color:red;background:black; font-style: normal    ;font-size : 20px")
(def html-fragment
  (str "<div style='" good-style "'><div id='a' class='btn' style='font-size:30px;color:white'>test1</div>test2</div>"))

(def svg-f "<g inkscape:label='Calque 1' inkscape:groupmode='layer' id='layer1'></g>")
(def svg-fragment "<g inkscape:label=\"Calque 1\" inkscape:groupmode=\"layer\" id=\"layer1\"></g>")
(def svg-fragment-1 "<svg><g inkscape:label=\"Calque 1\" inkscape:groupmode=\"layer\" id=\"layer1\"></g></svg>")

(def svg-fragment-with-transform "<svg><g id='layer1' transform='translate(-1.335742,-1.510374)'>")
(def svg-fragment-with-translate-transform "<svg><g id='layer1' transform='translate(-1.335742,-1.510374)'>")
(def svg-fragment-with-scale-transform "<svg><g id='layer1' transform='scale(-20,-15.22)'>")
(def svg-fragment-with-no-transform "<svg><g id='layer1'>")


(deftest test-svg->hiccup
  (let [svg-1 "<svg><g><g><path/></g></g></svg>"]
    (testing "Simple svg conversion"
      (is (= '([:svg {} [:g {} [:g {} [:path {}]]]]) (sdraw/svg->hiccup svg-1))))))

(deftest test-find-first-coord-pair
  (let [svg-1 "<svg><g><g><path d=\"m 12,22 33, 44 11,200\"/><path d=\"m 33 200\"/></g></g></svg>"
        svg-2 "<svg><g><g><path d=\"\"/><path d=\"m 12,22 33, 44 11,200\"/></g></g></svg>"
        svg-3 "<svg><g><g><path d=\"\"/><path d=\"\"/></g></g></svg>"
        hiccup-2  [[:svg {:width 100, :height 100}
                    [:g {:id "layer1", :transform "translate(0, 0) scale(2.5, 2.5)"}
                      [:path {:d "m 14.04516,34.825581 1.70997,-7.491565 6.92344,-3.334057 6.92343,3.334057 1.70999,7.491565 -4.79123,6.007796 h -7.68439 z, :id path2072, :style {fill #326ce5, fill-opacity 1, fill-rule evenodd, stroke none, stroke-width 0.26458332, stroke-linecap square, stroke-miterlimit 10"}]]]]]

      (testing "Find first good coord pair from the first path"
          (let [hiccup-svg    (sdraw/svg->hiccup svg-1)
                hickory-zipper (sdraw/hiccup-svg->hickoryzipper hiccup-svg)]
            (is (= "12,22" (sdraw/find-first-coord-pair hickory-zipper)))))

      (testing "Find first good coord pair from the second path"
          (let [hiccup-svg    (sdraw/svg->hiccup svg-2)
                hickory-zipper (sdraw/hiccup-svg->hickoryzipper hiccup-svg)]
             (is (= "12,22" (sdraw/find-first-coord-pair hickory-zipper)))))

      (testing "Find first good coord pair from the second path"
          (let [hiccup-svg    (sdraw/svg->hiccup svg-3)
                hickory-zipper (sdraw/hiccup-svg->hickoryzipper hiccup-svg)]
             (is (= nil (sdraw/find-first-coord-pair hickory-zipper)))))

      (testing ""
          (let [hickory-zipper (sdraw/hiccup-svg->hickoryzipper hiccup-2)]
             (is (= "14.04516,34.825581" (sdraw/find-first-coord-pair hickory-zipper)))))))


(deftest find-group
  (testing "Test to find a deeply nested group"
    (let [hick-zip (-> "<svg><a><b><c><d><g></g></c></b></a></svg>"
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags")))

  (testing "Test to find the group in an <svg> fragment"
    (let [hick-zip (-> svg-1
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags")))

  (testing "Test to find the group in a complex svg fragment"
    (let [hick-zip (-> svg-g
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags")))

  (testing "Test to find the group in a complex svg fragment"
    (let [hick-zip (-> svg-3
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags")))

  (testing "Test to find the group in a complex svg fragment"
    (let [hick-zip (-> svg-g-3
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags")))

  (testing "Test to find the group in a fragment"
    (let [hick-zip (-> svg-fragment
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags")))


  (testing "Test to find the group in an <svg> fragment"
    (let [hick-zip (-> svg-fragment-1
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip)]
      (is (= :g (:tag (zip/node (-> hick-zip
                                    sdraw/find-group)))) "svg-fragment with inkscape tags"))))

(deftest find-group-node
  (testing "Test Find Group Tag With Hickory"
    (is (= :g (:tag (-> svg-1
                        sdraw/find-group-node))))
    (is (= :g (:tag (-> svg-3
                        sdraw/find-group-node))))
    (is (= :g (:tag (-> svg-g-3
                        sdraw/find-group-node))))
    (is (= :g (:tag (-> svg-g
                        sdraw/find-group-node))))
    (is (= :g (:tag (-> svg-fragment
                        sdraw/find-group-node))) "svg-fragment with inkscape tags")
    (is (= nil (:tag (-> svg-no-g
                         sdraw/find-group-node))) "svg missing group tag")
    (is (= nil (:tag (-> html-fragment
                         sdraw/find-group-node))) "html fragment")))

(deftest convert-hickory
  (testing "Hickory to html"
    (let [g-node (-> simple-svg
                     hick/parse
                     hick/as-hickory
                     hzip/hickory-zip
                     (sdraw/filter-kvs sdraw/edit-remove-attrs sdraw/default-unwanted-keys)
                     zip/root
                     hickr/hickory-to-html
                     sdraw/find-group-node)]
      (is (= [:g {:id "fred"}] (sdraw/hickory-to-svg-hiccup g-node)))))

  (testing "Hickory to html"
    (let [g-node (-> svg-fragment ;svg-fragment-1 ;simple-svg
                     hick/parse
                     hick/as-hickory
                     hzip/hickory-zip
                     (sdraw/filter-kvs sdraw/edit-remove-attrs sdraw/default-unwanted-keys)
                     zip/root
                     hickr/hickory-to-html
                     sdraw/find-group-node)]
      (is (= [:g {:id "layer1"}] (sdraw/hickory-to-svg-hiccup g-node)))))


  (testing "Hickory to html"
    (let [g-node (-> svg-3
                     hick/parse
                     hick/as-hickory
                     hzip/hickory-zip
                     (sdraw/filter-kvs sdraw/edit-remove-attrs sdraw/default-unwanted-keys)
                     zip/root
                     hickr/hickory-to-html
                     sdraw/find-group-node)]
      (is (=
               [:g {:id "layer1", :transform "translate(-1.335742,-1.510374)"} "\n                    "
                 [:path {:id "path6553", :d "m 1.335742,12.335996 1.70997,-7.491565 6.92342,-3.334057 6.92339,3.334057 1.70998,7.491565 -4.7912,6.007796 h -7.68435 z", :style "fill:#326ce5;fill-opacity:1;fill-rule:evenodd;stroke:none;stroke-width:0.26458332;stroke-linecap:square;stroke-miterlimit:10"}] "\n                    " [:path {:style "fill:#eeeeee;fill-opacity:1;stroke-width:0.46185368", :d "m 14.636912,9.474263 h -0.69278 V 7.626847 c 0,-0.508038 -0.415668,-0.923706 -0.923706,-0.923706 H 11.17301 v -0.69278 c 0,-0.637359 -0.517276,-1.154636 -1.154634,-1.154636 -0.637358,0 -1.154634,0.517277 -1.154634,1.154636 v 0.69278 H 7.016326 c -0.508038,0 -0.919089,0.415668 -0.919089,0.923706 v 1.755045 h 0.688164 c 0.688161,0 1.247005,0.558843 1.247005,1.247005 0,0.688162 -0.558844,1.247005 -1.247005,1.247005 H 6.09262 v 1.755045 c 0,0.508038 0.415668,0.923706 0.923706,0.923706 h 1.755045 v -0.692781 c 0,-0.688161 0.558843,-1.247005 1.247005,-1.247005 0.688162,0 1.247005,0.558844 1.247005,1.247005 v 0.692781 h 1.755045 c 0.508038,0 0.923706,-0.415668 0.923706,-0.923706 v -1.847416 h 0.69278 c 0.637359,0 1.154636,-0.517276 1.154636,-1.154634 0,-0.637358 -0.517277,-1.154634 -1.154636,-1.154634 z", :id "path7415"}] "\n                  "]
             (sdraw/hickory-to-svg-hiccup g-node))))))


(deftest edit-remove-attrs
  (testing "Edit of simple <g> node attributes"
    (let [hick-zip (-> svg-fragment
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip
                       sdraw/find-group)]
      (is (= {:type :element, :attrs {:id "layer1"}, :tag :g, :content nil}
             (sdraw/edit-remove-attrs (zip/node hick-zip) [:inkscape:groupmode :inkscape:label])) "node should have lost the inkscape tags")))
  (testing "Edit of <g> node embedded in <svg> node attributes"
    (let [hick-zip (-> svg-fragment-1
                       hick/parse
                       hick/as-hickory
                       hzip/hickory-zip
                       sdraw/find-group)]
      (is (= {:type :element, :attrs {:inkscape:groupmode "layer" :id "layer1"}, :tag :g, :content nil}
             (sdraw/edit-remove-attrs (zip/node hick-zip) [:inkscape:label])) "node should have lost the inkscape tags"))))


(deftest filter-ks
  (testing "Filter out two unwanted kvs from attribute maps"
    ;Start with some svg, then filter to fix the attribute problem
    (let [s (-> svg-fragment
                hick/parse
                hick/as-hickory
                hzip/hickory-zip
                (sdraw/filter-kvs sdraw/edit-remove-attrs [:inkscape:groupmode :inkscape:label])
                zip/node)
    ;Make a new zip from the filter svg and find the group
          n (-> s
                hzip/hickory-zip
                zip/down
                sdraw/find-group
                zip/node)]
      (is (= {:type :element,
               :attrs {:id "layer1"},
               :tag :g,
               :content nil}
             n))))

  (testing "Filter out two unwanted kv from attribute maps where they are deeper in the tree"
    (let [s (-> svg-fragment-1
                hick/parse
                hick/as-hickory
                hzip/hickory-zip
                (sdraw/filter-kvs sdraw/edit-remove-attrs [:inkscape:groupmode :inkscape:label])
                zip/node)
   ;Make a new zip from the filter svg and find the group
          n (-> s
                hzip/hickory-zip
                zip/down
                sdraw/find-group
                zip/node)]
      (is (= {:type :element,
              :attrs {:id "layer1"},
              :tag :g,
              :content nil}
             n) "The exracted group node.")))
  (testing "Add or update a scale factor on g"
    (let [s (-> svg-fragment-with-transform
                hick/parse
                hick/as-hickory
                hzip/hickory-zip
                (sdraw/filter-kvs sdraw/add-scale-factor 50)
                zip/node)]
      (is (= {:type :document,
              :content [{:type :element,
                         :attrs nil,
                         :tag :html,
                         :content [{:type :element,
                                    :attrs nil,
                                    :tag :head,
                                    :content nil}
                                   {:type :element,
                                    :attrs nil,
                                    :tag :body,
                                    :content [{:type :element,
                                               :attrs nil,
                                               :tag :svg,
                                               :content [{:type :element,
                                                          :attrs {:id "layer1",
                                                                  :transform "translate(-1.335742,-1.510374) scale(50, 50)"},
                                                          :tag :g,
                                                          :content nil}]}]}]}]} s)))
    (let [s (-> svg-fragment-with-no-transform
                hick/parse
                hick/as-hickory
                hzip/hickory-zip
                (sdraw/filter-kvs sdraw/add-scale-factor 50)
                zip/node)]
      (is (= {:type :document,
              :content [{:type :element,
                         :attrs nil,
                         :tag :html,
                         :content [{:type :element,
                                    :attrs nil,
                                    :tag :head,
                                    :content nil}
                                   {:type :element,
                                    :attrs nil,
                                    :tag :body,
                                    :content [{:type :element,
                                               :attrs nil,
                                               :tag :svg,
                                               :content [{:type :element,
                                                          :attrs {:id "layer1",
                                                                  :transform "scale(50, 50)"},
                                                          :tag :g,
                                                          :content nil}]}]}]}]} s)))))



(deftest scale-svg
  (testing "Add a scale to a translate on a g element"
    (let [  s1 (-> svg-fragment-with-transform
                   hick/parse
                   hick/as-hickory
                   hzip/hickory-zip
                   (sdraw/filter-kvs sdraw/add-scale-factor 99.99)
                   zip/node)
            s2 (-> svg-fragment-with-no-transform
                   hick/parse
                   hick/as-hickory
                   hzip/hickory-zip
                   (sdraw/filter-kvs sdraw/add-scale-factor 33.33)
                   zip/node)
            s3 (-> svg-fragment-with-translate-transform
                   hick/parse
                   hick/as-hickory
                   hzip/hickory-zip
                   (sdraw/filter-kvs sdraw/add-scale-factor 33.33)
                   zip/node)
            s4 (-> svg-fragment-with-scale-transform
                   hick/parse
                   hick/as-hickory
                   hzip/hickory-zip
                   (sdraw/filter-kvs sdraw/add-scale-factor 33.33)
                   zip/node)]
      (is (= {:type :document,
              :content [{:type :element,
                         :attrs nil,
                         :tag :html,
                         :content [{:type :element,
                                    :attrs nil,
                                    :tag :head,
                                    :content nil}
                                   {:type :element,
                                    :attrs nil,
                                    :tag :body,
                                    :content [{:type :element,
                                               :attrs nil,
                                               :tag :svg,
                                               :content [{:type :element,
                                                          :attrs {:id "layer1",
                                                                  :transform "translate(-1.335742,-1.510374) scale(99.99, 99.99)"},
                                                                  :tag :g,
                                                                  :content nil}]}]}]}]}
             s1))
      (is (= {:type :document,
              :content [{:type :element,
                         :attrs nil,
                         :tag :html,
                         :content [{:type :element,
                                    :attrs nil,
                                    :tag :head,
                                    :content nil}
                                   {:type :element,
                                    :attrs nil,
                                    :tag :body,
                                    :content [{:type :element,
                                               :attrs nil,
                                               :tag :svg,
                                               :content [{:type :element,
                                                          :attrs {:id "layer1" :transform "scale(33.33, 33.33)"},
                                                          :tag :g,
                                                          :content nil,}]}]}]}]}
             s2))
      (is (= {:type :document,
              :content [{:type :element,
                         :attrs nil,
                         :tag :html,
                         :content [{:type :element,
                                    :attrs nil,
                                    :tag :head,
                                    :content nil}
                                   {:type :element,
                                    :attrs nil,
                                    :tag :body,
                                    :content [{:type :element,
                                               :attrs nil,
                                               :tag :svg,
                                               :content [{:type :element,
                                                          :attrs {:id "layer1" :transform "translate(-1.335742,-1.510374) scale(33.33, 33.33)"},
                                                          :tag :g,
                                                          :content nil,}]}]}]}]}
             s3))
      (is (= {:type :document,
              :content [{ :type :element,
                          :attrs nil,
                          :tag :html,
                          :content [{:type :element,
                                     :attrs nil,
                                     :tag :head,
                                     :content nil}
                                    {:type :element,
                                     :attrs nil,
                                     :tag :body,
                                     :content [{:type :element,
                                                :attrs nil,
                                                :tag :svg,
                                                :content [{:type :element,
                                                           :attrs {:id "layer1" :transform " scale(33.33, 33.33)"},
                                                           :tag :g,
                                                           :content nil,}]}]}]}]}
             s4)))))



(deftest text-fix-attr-case
  (testing "Test we can change an attr with the name viewboc to viewBox.  viewbox is an error
            in chrome and firefox.  hickory converts viewBox to viewbox!!"
   (let [sample-1 "[:svg {:width 100, :height 100} [:svg {:data-name \"Layer 1\", :height \"64px\", :id \"Layer_1\", :viewbox \"0 0 64 64\", :width \"64px\",\""
         result-1 "[:svg {:width 100, :height 100} [:svg {:data-name \"Layer 1\", :height \"64px\", :id \"Layer_1\", :viewBox \"0 0 64 64\", :width \"64px\",\""]
     (is (= (sdraw/fix-attr-case sample-1) result-1)))))
