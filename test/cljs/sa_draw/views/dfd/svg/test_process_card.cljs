(ns sa-draw.views.dfd.svg.test-process-card
  "Testing svg drawing."
  (:require [cljs.test                          :refer-macros [deftest is testing run-tests]]
            [sa-draw.views.dfd.svg.process-card :as pc]
            [sa-draw.model.default-model        :refer [demo-model-1]]))


(def drawing-list-1
  '({:type ::pc/proc
     :node {:id "1"}
     :order 1}
    {:type ::pc/proc
     :node {:id "2"}
     :order 2}
    {:type ::pc/proc
     :node {:id "1.1"}
     :order 3}
    {:type ::pc/proc
     :node {:id "1.2"}
     :order 4}
    {:type ::pc/proc
     :node {:id "2.1"}
     :order 5}
    {:type ::pc/context
     :node {:id "0"}
     :order 6}
    {:type ::pc/producer
     :node {:id "a"}
     :order 7}
    {:type ::pc/consumer
     :node {:id "b"}
     :order 8}
    {:type ::pc/source
     :node {:id "c"}
     :order 9}
    {:type ::pc/sink
     :node {:id "d"}
     :order 10}))


(def drawing-list-2
  '({:type ::pc/proc
     :node {:id "2"}
     :order 2}
    {:type ::pc/proc
     :node {:id "3"}
     :order 3}
    {:type ::pc/proc
     :node {:id "1"}
     :order 1}))


(def drawing-list-2-sorted
  '({:type ::pc/proc
     :node {:id "1"}
     :order 1}
    {:type ::pc/proc
     :node {:id "2"}
     :order 2}
    {:type ::pc/proc
     :node {:id "3"}
     :order 3}))


(def transformed-model-1
  '({:order 1
     :type ::pc/context
     :node #:sadf{:id "0",
                  :title "Title",
                  :description "Context Diagram",
                  :meta #:sadf {}
                  :data-in []
                  :data-out []}}))


;mouse-up-fn mouse-move-fn node layout
(deftest test-initial-model

  (testing "Test drawing dispatch"
    (let [m1 {:type ::pc/proc :node {:sadf/id 1}}
          m2 {:type ::pc/producer :node {:sadf/id 2}}
          m3 {:type ::pc/consumer :node {:sadf/id 3}}
          m4 {:type ::pc/no-not-me :node {:sadf/id 4}}]
      (is (not (= 99  (pc/draw "" "" m1 {}))) "")
      (is (not (= 99 (pc/draw "" "" m2 {}))) "")
      (is (not (= 99  (pc/draw "" "" m3 {}))) "")
      (is (= 99 (pc/draw "" "" m4 {})) ""))))
