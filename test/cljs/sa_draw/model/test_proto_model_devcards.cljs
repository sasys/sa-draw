(ns sa-draw.model.test-proto-model-devcards
  "
     Contains cljks-test-display based  tests against the proto-model
     protocol.
  "

  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [cljs-test-display.core]
            [sa-draw.model.proto-model  :as proto-model]
            [sa-draw.model.map-impl     :refer [map-model proc-model]]
            [sa-draw.model.test-model   :refer [test-model]]
            [sa-draw.model.test-flows]
            [sa-draw.model.test-icons]
            [sa-draw.data.test-dd]
            [sa-draw.model.test-context-objects]
            [sa-draw.views.dfd.svg.test-process-card]
            [sa-draw.views.dfd.svg.test-draw]
            [sa-draw.views.dfd.svg.test-utility]
            [sa-draw.utils.test-sem-ver]
            [sa-draw.model.test-utils]
            [sa-draw.model.test-basic-layout]))

(deftest test-initial-model
  (let [model (proto-model/new-model map-model)
        meta-version {:major "0"
                      :minor "0"
                      :inc   "0"}
        def-namespace "sadf"
        simple-test-proc    #:sadf{:id "0",
                                   :title "Empty proc",
                                   :description "An empty proc for testing",
                                   :meta #:sadf {}
                                   :data-in []
                                   :data-out []
                                   :children []}]

    (testing "New model has a name space"
        (is (= def-namespace (proto-model/name-space map-model)) "New model has name space"))

    (testing "Model version meta data is OK"
        (is (= meta-version (proto-model/model-meta-version map-model model)) "Found version information"))

    (testing "The process model"
        (is (false? (nil? (proto-model/find-process-model map-model model))) "Found a process model"))

    (testing "The flow model"
        (is (false? (nil? (proto-model/find-flow-model map-model model))) "Found a flow model")
        (is (= []  (proto-model/find-flow-model map-model model)) "Flow model should be empty"))

    (testing "The icons model"
      (is (false? (nil? (proto-model/find-icon-model map-model model))) "Found the icons model")
      (is (= 0  (count (proto-model/find-icon-model map-model model))) "Icon model test state"))

    (testing "Updates to the process model - here we replace the procs with a new simple tree of procs."
        (let [updated-model (proto-model/update-process-model map-model model simple-test-proc)]
          (is (= simple-test-proc (proto-model/find-process-model map-model updated-model)))))))

(deftest map-model-test
  (let [model (proto-model/new-model-from-model map-model test-model)
        procs-model (proto-model/find-process-model map-model model)
        [_ id title description data-in data-out] (proto-model/details proc-model procs-model)]
    (testing "Test we can find the context node"
      (is (= "0" id))
      (is (= "Make Hospital Registration" title))
      (is (= "The context diagram" description))
      (is (= [] data-in))
      (is (= [] data-out))

     (testing "node with id 1"
       (let [node-1 (proto-model/find-node proc-model procs-model "1")
             [children id title description data-in data-out] (proto-model/details proc-model node-1)]
         (is (= "1" id))
         (is (= "Make a new record" title))
         (is (= false (nil? description)))
         (is (= 1 (count children)))
         (is (= nil  data-in))
         (is (= nil data-out)))))))


(deftest icon-model-test
  (let [model (proto-model/new-model map-model)
        simple-icon  {:uuid "333"
                      :layer "1.1"
                      :type "K8S"
                      :title "User Configuration"
                      :description "Stores the user configuration."
                      :name "etc.d"
                      :meta {}
                      :source "octo-technology"}
        icons         (proto-model/find-icon-model map-model model)
        updated-model (proto-model/update-icon-model map-model model [simple-icon])
        updated-icons (proto-model/find-icon-model map-model updated-model)]
    (testing "Replace selected icons"
      (is (= 0 (count icons)) "The original icons")
      (is (= 1 (count updated-icons)) "The updated icons"))))

(deftest proc-model-tests

  (let [model (proto-model/new-model-from-model map-model test-model)
        procs-model (proto-model/find-process-model map-model model)
        node-zero-children (proto-model/children proc-model procs-model)]
    (testing "Find the children of the context node and remove them"
      (is (= 4 (count node-zero-children)) "4 children")
      (let [reduced-children-3 (proto-model/remove-node-from-list proc-model "4" node-zero-children)
            reduced-children-2 (proto-model/remove-node-from-list proc-model "3" reduced-children-3)
            reduced-children-1 (proto-model/remove-node-from-list proc-model "2" reduced-children-2)
            reduced-children-0 (proto-model/remove-node-from-list proc-model "1" reduced-children-1)]
        (is (= 3 (count reduced-children-3)))
        (is (= 2 (count reduced-children-2)))
        (is (= 1 (count reduced-children-1)))
        (is (= 0 (count reduced-children-0)))))

    (testing "Create a new node and add it in to the root node procs"
      (is (= 4 (count node-zero-children)) "4 children")
      (let [next-id (proto-model/next-child proc-model "0" node-zero-children)
            new-node-1  (proto-model/make-node proc-model next-id "New Node 1" [] [] "New Node 1 Description" "")
            updated-procs-model (proto-model/add-node proc-model "0" new-node-1 procs-model)]
        (is (= next-id "5"))
        (is (= next-id (proto-model/id proc-model new-node-1)))
        (is (= 5 (count (proto-model/children proc-model updated-procs-model))))))

    (testing "Create a new node and add it in to the child 3.5.1 node proc"
      (is (= 4 (count node-zero-children)) "4 children")
      (let [node (proto-model/find-node proc-model procs-model "3.5.1")
            children (proto-model/children proc-model node)
            next-id (proto-model/next-child proc-model "3.5.1" children)
            new-node-2  (proto-model/make-node proc-model next-id "New Node 2" [] [] "New Node 2 Description" "")
            updated-procs-model (proto-model/add-node proc-model "3.5.1" new-node-2 procs-model)
            updated-node (proto-model/find-node proc-model updated-procs-model "3.5.1")
            updated-children (proto-model/children proc-model updated-node)]
        (is (= next-id "3.5.1.1"))
        (is (= next-id (proto-model/id proc-model new-node-2)))
        (is (= 1 (count updated-children)))
        (is (= ["3.5.1" "3.5.1.1"] (proto-model/cids proc-model updated-node)) "FInd the cids from the updated parent")))

    (testing "Update a node"
      (let [node (proto-model/find-node proc-model procs-model "3.5.1")
            name-space (proto-model/name-space map-model)
            updated-node (assoc-in node [(keyword name-space "title")] "NEW NODE TITLE")
            updated-procs-model (proto-model/update-node proc-model "3.5.1" updated-node procs-model)
            found-node (proto-model/find-node proc-model updated-procs-model "3.5.1")]
        (is (= "NEW NODE TITLE" (proto-model/title proc-model found-node)))))

    (testing "Patch a node"
      (let [name-space (proto-model/name-space map-model)
            patched-node (proto-model/patch-node proc-model procs-model "3" "New Patch Title" "New patch description" ["Data-A"] ["Data-B"])]
        (is (= "New Patch Title" ((keyword name-space "title") patched-node)))))))

(deftest proc-model-remove-node-tests
  (let [model (proto-model/new-model-from-model map-model test-model)
        procs-model (proto-model/find-process-model map-model model)
        node-3  (proto-model/find-node proc-model procs-model "3")
        node-3-children (proto-model/children proc-model node-3)
        node-3-ids (proto-model/cids proc-model node-3)]
    (testing "Add and then remove a bottom level node"
      (is (= 5 (count node-3-children)) "5 children")
      (is (= 7 (count node-3-ids)) "7 child ids")
      (let [reduced-procs-model (proto-model/remove-node proc-model procs-model "3.5")
            node-3  (proto-model/find-node proc-model reduced-procs-model "3")
            node-3-children (proto-model/children proc-model node-3)
            node-3-ids (proto-model/cids proc-model node-3)]
        (print node-3-ids)
        (is (= 4 (count node-3-children)) "4 children")
        (is (= 5 (count node-3-ids)) "5 child ids (includes itself)")))))


(defn test-run []
  (cljs.test/run-tests
    (cljs-test-display.core/init! "app")
    'sa-draw.model.test-basic-layout
    'sa-draw.model.test-proto-model-devcards
    'sa-draw.model.test-flows
    'sa-draw.model.test-icons
    'sa-draw.data.test-dd
    'sa-draw.model.test-context-objects
    'sa-draw.views.dfd.svg.test-process-card
    'sa-draw.views.dfd.svg.test-draw
    'sa-draw.views.dfd.svg.test-utility
    'sa-draw.utils.test-sem-ver
    'sa-draw.model.test-utils))


(defn ^:export init
  ([]
   (test-run)))
