(ns sa-draw.model.test-context-objects
  "
     Contains cljs-test-display based  tests against the context-object
     protocol.
  "

  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [sa-draw.model.proto-model            :as ctx]
            [sa-draw.model.map-impl :refer [context-model map-model]]
            [sa-draw.model.test-model :refer [test-model]]))

(def new-prod-1
  #:sadf{:id "1234-A"
         :title "New Producer 1"
         :terms []
         :link "link-to-source"
         :description "Description of new producer 1"
         :context-type :sa-draw.views.editors.dispatcher/prod})

(def new-cons-1
  #:sadf{:id "1234-B"
         :title "Operator 1"
         :terms []
         :link "link-to-destination"
         :description "my consumer for testing"
         :context-type :sa-draw.views.editors.dispatcher/cons})

(def new-source-1
  #:sadf{:id "1234-C"
         :title "Operator 1"
         :terms []
         :description "my source for testing"
         :context-type :sa-draw.views.editors.dispatcher/source})

(def new-sink-1
  #:sadf{:id "1234-D"
         :title "Operator 1"
         :terms []
         :description "my sink for testing"
         :context-type :sa-draw.views.editors.dispatcher/sink})

(deftest test-initial-context-model
  (let [ctx-objects-model (ctx/context-objects-map map-model test-model)
        [sources sinks producers consumers] (ctx/context-objects map-model test-model)]
    (testing "simple test model"
      (is (= 0 (count producers)) "producesr")
      (is (= 0 (count consumers)) "consumers")
      (is (= 1 (count sources)) "sources")
      (is (= 1 (count sinks))) "sinks"
      (is (= 4 (count (keys ctx-objects-model)))))))

(deftest test-add-objects
  (let [context-objects (ctx/find-context-model map-model test-model)
        new-prod-context-object (ctx/new-producer context-model  :sadf/producers  "1234-A" "New Producer 1" "Description of new producer 1" "NOT USED" "link-to-producer")
        new-cons-context-object (ctx/new-consumer context-model  :sadf/consumers  "1234-B" "New Consumer 1" "Description of new consumer 1" "NOT USED" "link-to-consumer")
        new-source-context-object (ctx/new-source context-model  :sadf/sources  "1234-C" "New Source 1" "Description of new source 1" "NOT USED")
        new-sink-context-object (ctx/new-sink context-model  :sadf/sinks  "1234-D" "New Sink 1" "Description of new sink 1" "NOT USED")]

    (testing "Add producer to context objects"
      (let [updated-context-objects (ctx/add-to-context-objects context-model context-objects :sadf/producers new-prod-context-object)
            updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
            [new-sources new-sinks new-producers new-consumers] (ctx/context-objects map-model updated-model)]
        (is (= 1 (count new-producers)) "new producers")
        (is (= 0 (count new-consumers)) "consumers")
        (is (= 1 (count new-sources)) "sources")
        (is (= 1 (count new-sinks)) "sinks")))

    (testing "Add consumer to context objects"
      (let [updated-context-objects (ctx/add-to-context-objects context-model context-objects :sadf/consumers new-cons-context-object)
            updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
            [new-sources new-sinks new-producers new-consumers] (ctx/context-objects map-model updated-model)]
        (is (= 0 (count new-producers)) "new producers")
        (is (= 1 (count new-consumers)) "consumers")
        (is (= 1 (count new-sources)) "sources")
        (is (= 1 (count new-sinks)) "sinks")))

    (testing "Add source to context objects"
      (let [updated-context-objects (ctx/add-to-context-objects context-model context-objects :sadf/sources new-source-context-object)
            updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
            [new-sources new-sinks new-producers new-consumers] (ctx/context-objects map-model updated-model)]
        (is (= 0 (count new-producers)) "new producers")
        (is (= 0 (count new-consumers)) "consumers")
        (is (= 2 (count new-sources)) "sources")
        (is (= 1 (count new-sinks)) "sinks")))

    (testing "Add sink to context objects"
      (let [updated-context-objects (ctx/add-to-context-objects context-model context-objects :sadf/sinks new-sink-context-object)
            updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
            [new-sources new-sinks new-producers new-consumers] (ctx/context-objects map-model updated-model)]
        (is (= 0 (count new-producers)) "new producers")
        (is (= 0 (count new-consumers)) "consumers")
        (is (= 1 (count new-sources)) "sources")
        (is (= 2 (count new-sinks)) "sinks")))))

(deftest test-search-objects
  (let [ctx-objects-model (ctx/context-objects map-model test-model)
        found (ctx/find-context-object-by-id context-model ctx-objects-model "1234-A")
        not-found (ctx/find-context-object-by-id context-model ctx-objects-model "1234-Z")]
    (testing "Search for context object by id")
    (is (= (ctx/context-object-id context-model found) "1234-A") "found id is correct")
    (is (= (ctx/context-object-title context-model found) "Source Title 1") "found title is correct")
    (is (= (ctx/context-object-description context-model found) "Source description 1") "found description is correct")
    (is (= (ctx/terms context-model found) []) "no terms")
    (is (= (ctx/context-object-type context-model found) :sa-draw.views.editors.dispatcher/source) "found object type is correct")
    (is (= nil not-found) "not found is nil")
    (is (= ["1234-A" "Source Title 1" "Source description 1" "incoming link source 1" "NOT USED" [] :sa-draw.views.editors.dispatcher/source]
           (ctx/context-object-details context-model found)) "Details of found context object are correct")))

(deftest test-remove-objects
  (let [context-objects (ctx/find-context-model map-model test-model)
        new-prod-context-object (ctx/new-producer context-model  :sadf/producers  "1234-A" "New Producer 1" "Description of new producer 1" "NOT USED" "link-to-producer")
        new-cons-context-object (ctx/new-consumer context-model  :sadf/consumers  "1234-B" "New Consumer 1" "Description of new consumer 1" "NOT USED" "link-to-consumer")
        new-source-context-object (ctx/new-source context-model  :sadf/sources  "1234-C" "New Source 1" "Description of new source 1" "NOT USED")
        new-sink-context-object (ctx/new-sink context-model  :sadf/sinks  "1234-D" "New Sink 1" "Description of new sink 1" "NOT USED")
        updated-context-objects
          (as-> context-objects co
                (ctx/add-to-context-objects context-model co :sadf/producers new-prod-context-object)
                (ctx/add-to-context-objects context-model co :sadf/consumers new-cons-context-object)
                (ctx/add-to-context-objects context-model co :sadf/sources new-source-context-object)
                (ctx/add-to-context-objects context-model co :sadf/sinks new-sink-context-object))
        updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)]

    (testing "remove producer to context objects"
      (let [[sources sinks producers consumers] (ctx/context-objects map-model updated-model)]
        (is (= 1 (count producers)) "producer has been added")
        (is (= 1 (count consumers)) "consumer has been added")
        (is (= 2 (count sources)) "source has been added")
        (is (= 2 (count sinks)) "sink has been added")
        (let [updated-context-objects (ctx/delete-from-context-objects context-model context-objects :sadf/producers new-prod-context-object)
              updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
              [_ _ producers _] (ctx/context-objects map-model updated-model)]
            (is (= 0 (count producers)) "producers"))
        (let [updated-context-objects (ctx/delete-from-context-objects context-model context-objects :sadf/consumers new-cons-context-object)
              updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
              [_ _ _ consumers] (ctx/context-objects map-model updated-model)]
            (is (= 0 (count consumers)) "consumers"))
        (let [updated-context-objects (ctx/delete-from-context-objects context-model context-objects :sadf/sources new-source-context-object)
              updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
              [sources _ _ _] (ctx/context-objects map-model updated-model)]
            (is (= 1 (count sources)) "sources"))
        (let [updated-context-objects (ctx/delete-from-context-objects context-model context-objects :sadf/sinks new-sink-context-object)
              updated-model (ctx/update-model-context-objects map-model test-model updated-context-objects)
              [_ sinks _ _] (ctx/context-objects map-model updated-model)]
            (is (= 1 (count sinks)) "sinks"))))))
