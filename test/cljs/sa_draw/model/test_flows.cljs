(ns sa-draw.model.test-flows
  "
     Contains cljs-test-display based  tests against the flow-model
     protocol.
  "

  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [sa-draw.model.flows            :as flows]
            [sa-draw.model.simple-flow-impl :refer [fm]]))

(deftest test-initial-flow-model
  (let [source-1 {:id (keyword :sa-trees.proc.tree 1)
                  :posn [50 150]}
        sink-1  {:id (keyword :sa-trees.proc.tree 2)
                 :posn [200 300]}
        flow-1 (flows/new-flow fm [{:data "a" :id "22"} "b"] [source-1] [sink-1])

        source-2 {:id (keyword :sa-trees.proc.tree 3)
                  :posn [50 150]}
        sink-2  {:id (keyword :sa-trees.proc.tree 4)
                 :posn [200 300]}
        flow-2 (flows/new-flow fm [{:data "c" :id "33"} "d"] [source-2] [sink-2])
        flows [flow-1 flow-2]]
    (testing "Good flow creation"
        (is (= [{:data "a" :id "22"} "b"] (flows/data-items fm flow-1)))
        (is (= [source-1] (flows/sources fm flow-1)))
        (is (= [sink-1] (flows/sinks fm flow-1)))
        (is (= [{:data "c" :id "33"} "d"] (flows/data-items fm flow-2)))
        (is (= [source-2] (flows/sources fm flow-2)))
        (is (= [sink-2] (flows/sinks fm flow-2)))
        (is (= 2 (count (flows/cps fm flow-1))))
        (is (= 2 (count (flows/cps fm flow-2)))))
    (testing "Find flow by id"
      (let [id-1 (flows/id fm flow-1)
            id-2 (flows/id fm flow-2)]
        (is (not= nil id-1))
        (is (not= nil id-2))
        (is (not= id-1 id-2))
        (is (= flow-1 (flows/find-flow fm flows id-1)))
        (is (= flow-2 (flows/find-flow fm flows id-2)))))

    (testing "Remove-flow"
      (let [id-1 (flows/id fm flow-1)]
        (is (= 1 (count (flows/remove-flow fm flows flow-1))))
        (is (= 1 (count (flows/remove-flow-by-id fm flows id-1))))))

    (testing "Update flows "
      (let [extra-flow (flows/new-flow fm [{:data "z" :id "999"} "bYYYY"] [source-1] [sink-1])
            existing-flow-id (flows/id fm flow-1)
            updated-flows (flows/update-flow fm flows extra-flow existing-flow-id)]
        (is (= 2 (count updated-flows)))))

    (testing "Add flow"
      (let [extra-flow (flows/new-flow fm [{:data "z" :id "999"} "bYYYY"] [source-1] [sink-1])
            extra-flow-id (flows/id fm extra-flow)
            updated-flows (flows/update-flow fm flows extra-flow extra-flow-id)]
        (is (= 3 (count updated-flows)))))))

(deftest test-flow-duplication
    (let [data-1 ["a" "b"]
          source-1 {:sadf/id "123"
                    :sadf/proc-id (keyword "sadf" "1")}
          sink-1 {:sadf/id "456"
                  :sadf/proc-id (keyword "sadf" "2")}

          flow-1 (flows/new-flow fm data-1 [source-1] [sink-1] "description")]
      (testing "Duplicate flow to layer"
        (let [new-flow (flows/duplicate-flow-to-layer fm flow-1 "2.1.1")
              new-source (first (:sadf/source new-flow))
              new-sink (first (:sadf/sink new-flow))]
          (is (= "2.1.1" (:sadf/proc-id new-source)))
          (is (= "2.1.1" (:sadf/proc-id new-sink)))))))
