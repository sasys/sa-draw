(ns sa-draw.model.test-basic-layout
  "
     Contains cljs-test-display based  tests against the layout model
     protocol.
  "

  (:require  [cljs.test                       :refer-macros [deftest is testing run-tests]]
             [sa-draw.model.layout            :as l :refer [new-layout]]
             [sa-draw.model.basic-layout      :refer [blm]]))


(def id-1 "1.1")
(def posn-1 [20 20])
(def id-2 "1.2")
(def posn-2 [20 20])
(def id-3 "1.3")
(def posn-3 [20 20])
 
(deftest test-new-layout
  (let [new-layout (new-layout blm)]

    (testing "known? operation on a new layout"
      (let [updated-layout (as-> new-layout nl
                                 (l/add-layout blm nl id-1 posn-1)
                                 (l/add-layout blm nl id-2 posn-2))]
        (is (some?  (l/known-layout? blm updated-layout id-1)))
        (is (some?  (l/known-layout? blm updated-layout id-2)))
        (is (nil? (l/known-layout? blm updated-layout id-3)))))

    (testing "add operations on a new layout"
      (let [updated-layout (as-> new-layout nl
                                 (l/add-layout blm nl id-1 posn-1)
                                 (l/add-layout blm nl id-2 posn-2))]
        (is (= posn-1 (l/find-layout blm updated-layout id-1)))
        (is (= posn-2 (l/find-layout blm updated-layout id-2)))))

    (testing "remove operations on a new layout"
      (let [updated-layout (as-> new-layout nl
                                 (l/add-layout blm nl id-1 posn-1)
                                 (l/add-layout blm nl id-2 posn-2))]
        (is (= posn-1 (l/find-layout blm updated-layout id-1)))
        (is (= posn-2 (l/find-layout blm updated-layout id-2)))

        (let [removed (l/remove-layout blm updated-layout id-2)]
          (is (= posn-1 (l/find-layout blm removed id-1)))
          (is (= nil    (l/find-layout blm removed id-2))))))

    (testing "odd forms of key"
      (let [updated-layout (as-> new-layout nl
                                 (l/add-layout blm nl "0.0.1.1" posn-1)
                                 (l/add-layout blm nl "0" posn-2))]
        (is (= posn-1 (l/find-layout blm updated-layout "0.0.1.1")))
        (is (= posn-2 (l/find-layout blm updated-layout "0")))))))


(deftest test-overloaded-layout
  (let [new-layout (new-layout blm [200 200])]
    (testing "overloaded new"
      (is (some?  (l/known-layout? blm new-layout "0"))
        (let [[x y] (l/find-layout blm new-layout "0")]
          (is (= x (/ 200 2)))
          (is (= y (/ 200 2))))))))


(deftest test-serialization
  (let [new-layout (new-layout blm [200 200])]
    (testing "json serialisation"
      (let [ul             (as-> new-layout nl
                                 (l/add-layout blm nl "0.1" posn-1)
                                 (l/add-layout blm nl "0" posn-2)
                                 (l/add-layout blm nl "0.1.0.23.78" posn-3))]
        (let [sr (l/serialise blm ul)]
          (is (= ul (l/deserialise blm sr))))))))
