(ns sa-draw.model.test-icons
  "
     Contains cljs-test-display based  tests against the icons protocol.
     The icons protocol defines how to interact with the icons model.
  "

  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [sa-draw.model.proto-model  :as proto-model]
            [sa-draw.model.icons        :as icons-model]
            [sa-draw.model.icons-impl   :refer  [im]]
            [sa-draw.model.map-impl     :refer [map-model proc-model]]))

(deftest test-find-icons
  (let [model (proto-model/new-model map-model)
        proto-icon-instance  {:sadf/uuid "333"
                              :sadf/id "333"
                              :sadf/layer "1.1"
                              :sadf/type "K8S"
                              :sadf/title "Global Configuration"
                              :sadf/description "Stores the global configuration."
                              :sadf/name "node"
                              :sadf/meta {}
                              :sadf/source "octo-technology"}
        simple-icon  {:sadf/uuid "333"
                      :sadf/id "333"
                      :sadf/layer "1.1"
                      :sadf/type "K8S"
                      :sadf/title "User Configuration"
                      :sadf/description "Stores the user configuration."
                      :sadf/name "master"
                      :sadf/meta {}
                      :sadf/source "octo-technology"}
        old-icons         (proto-model/find-icon-model map-model model)
        updated-model (proto-model/update-icon-model map-model model [simple-icon])
        updated-icons (proto-model/find-icon-model map-model updated-model)]
    (is (= 0 (count old-icons))     "The original test icons model has zero entries.")
    (is (= 1 (count updated-icons)) "The new test icons model has a single entry.")
    (is (= simple-icon (icons-model/find-icon-instance im updated-icons "333")) "The uuid of the updated icon.")
    (let [updated-icon-model (->> (proto-model/new-model map-model)
                                  (proto-model/find-icon-model map-model)
                                  (apply conj [proto-icon-instance simple-icon])
                                  (proto-model/update-icon-model map-model model)
                                  (proto-model/find-icon-model map-model))]
      (is (= 2 (count updated-icon-model)) "Should be several icon instances in the model")
      (is (= [proto-icon-instance] (icons-model/find-icon-instances im updated-icon-model "octo-technology" "K8S" "node")) "Only recover the node instance")
      (is (= 0 (count  (icons-model/find-icon-instances im updated-icon-model "octo-technology" "K8S" "no icon name"))) "No instances found"))))

(deftest remove-icons
  (let [model (proto-model/new-model map-model)
        ano-icon {:sadf/uuid "333"
                  :sadf/id "333"
                  :sadf/layer "1.1"
                  :sadf/type "K8S"
                  :sadf/title "Global Configuration"
                  :sadf/description "Stores the global configuration."
                  :sadf/name "node"
                  :sadf/meta {}
                  :sadf/source "octo-technology"}


        simple-icon  {:sadf/uuid "334"
                      :sadf/id "334"
                      :sadf/type "K8S"
                      :sadf/layer "1.1"
                      :sadf/title "User Configuration"
                      :sadf/description "Stores the user configuration."
                      :sadf/name "master"
                      :sadf/meta {}
                      :sadf/source "octo-technology"}]
    (let [updated-icon-model (->> (proto-model/new-model map-model)
                                  (proto-model/find-icon-model map-model)
                                  (apply conj [ano-icon simple-icon])
                                  (proto-model/update-icon-model map-model model)
                                  (proto-model/find-icon-model map-model))]
         (is (= 2 (count updated-icon-model)))
         (let [depleted-icon-model (icons-model/remove-icon im updated-icon-model simple-icon)
               further-depleted-icon-model (icons-model/remove-icon im depleted-icon-model ano-icon)
               not-found-depleted-icon-model (icons-model/remove-icon im further-depleted-icon-model ano-icon)]
           (is (= 1 (count depleted-icon-model)))
           (is (= 0 (count further-depleted-icon-model)))
           (is (= 0 (count not-found-depleted-icon-model)))))))
