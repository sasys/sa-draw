(ns sa-draw.model.test-model)

(def default-namespace "sadf")


(def test-model
    #:sadf{:procs
            #:sadf{:id "0",
                   :title "Make Hospital Registration",
                   :description "The context diagram",
                   :meta #:sadf {}
                   :data-in []
                   :data-out []
                   :children
                     [#:sadf{:id "1", :title "Make a new record", :description "Use the patient id and the docktors \nnote to generate a new patient recorc.",
                             :children
                               [#:sadf{:id "1.1", :title " Do proc A.A", :description "This describes the process of making A.As", :children []}]}
                      #:sadf{:id "2", :title "Do proc B", :description "This describes the process of making Bs", :children [#:sadf{:id "2.1", :title " Do proc B.A", :description "This describes the process of making B.As", :children []}]}
                      #:sadf{:id "3", :title "Do proc C", :description "This describes the process of making Cs", :children [#:sadf{:id "3.1", :title " Do proc C.A", :description "This describes the process of making C.As", :children []}
                                                                                                                             #:sadf{:id "3.2", :title " Do proc C.B", :description "This describes the process of making C.Bs", :children []}
                                                                                                                             #:sadf{:id "3.3", :title " Do proc C.C", :description "This describes the process of making C.Cs", :children []}
                                                                                                                             #:sadf{:id "3.4", :title " Do proc C.D", :description "This describes the process of making C.Ds", :children []}
                                                                                                                             #:sadf{:id "3.5", :title " Do proc C.E", :description "This describes the process of making C.Es", :children
                                                                                                                                                                                                       [#:sadf{:id "3.5.1", :title "Do level 3.5.1 stuff", :description "Level 3.5.1 description in great detail.", :children []}]}]}
                      #:sadf{:id "4", :title "Do proc D", :description "Do stuf to make D work!!", :children [#:sadf{:id "4.3", :title "New Node Title", :description "New Node Text", :children []}
                                                                                                              #:sadf{:id "4.2", :title "New Node Title", :description "New Node Text", :children []}
                                                                                                              #:sadf{:id "4.1", :title "New Node Title", :description "New Node Text", :children [],}]}]}







           :dictionary []

           :flows []

           :context-objects #:sadf{:sources [#:sadf{:id "1234-A"
                                                    :dictionary-link "NOT USED"
                                                    :description "Source description 1"
                                                    :title "Source Title 1"
                                                    :link "incoming link source 1"
                                                    :dic-link "NOT USED"
                                                    :context-type :sa-draw.views.editors.dispatcher/source}]
                                   :sinks [#:sadf{:id "A-1234"
                                                  :dictionary-link "321"
                                                  :temp-label "End User 1"
                                                  :description "Sink description 1"
                                                  :title "Sink Title 1 "
                                                  :terms []
                                                  :context-type :sa-draw.views.editors.dispatcher/sink}]
                                   :producers []
                                   :consumers []}

           :meta #:sadf{:namespace default-namespace
                        :name "test-model-2",
                        :uuid "8a43df75-b0f2-4fc3-af5d-df97f9d03db7",
                        :summary "This is a test model with id 123",
                        :detail #:sadf{:ownership "Owened by dept q", :contact "Fred Smith", :contact-email "a@b.com"}}})
