(ns sa-draw.model.test-utils
  "
  "
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [sa-draw.model.utils :as saut]
            [clojure.string :as  st]))

(def test-namespace "abc")




(def node-1-1 {:abc/id "1.1" :name "fred" :abc/children []})
(def node-1-2 {:abc/id "1.2" :name "simon" :abc/children []})
(def node-1-10 {:abc/id "1.10" :name "sarah" :abc/children []})
(def node-1-11-1 {:abc/id "1.11.1" :name "lucy" :abc/children []})
(def node-1-11 {:abc/id "1.11" :name "john" :abc/children [node-1-11-1]})

(def new-node-1-2 {:abc/id "1.2" :name "new" :abc/children []})

(def new-node-1-3 {:abc/id "1.3" :name "new 1.3" :abc/children []})

(def node-1 {:abc/id "1"
             :abc/children [node-1-1 node-1-2]})

(def updated-node-1 {:abc/id "1"
                     :abc/children [node-1-1 new-node-1-2]})

(def updated-node-1-1 {:abc/id "1.1" :name "fred" :abc/children []
                       :abc/title "new-title" :abc/description "new description"
                       :abc/data-in [] :abc/data-out []})

(def new-node-1-3 {:abc/id "1.3" :name "new node here" :abc/children []})

(deftest find-summary
  (let [summaries [{:sadf/uri "sbnl.sa.1"
                    :name "sum-a"}
                   {:sadf/uri "sbnl.sa.2"
                    :name "sum-b"}
                   {:sadf/uri "sbnl.sa.3"
                    :name "sum-c"}]]
    (testing "Test that we can find a summary from a model."
        (is (= "sum-a" (:name (saut/find-summary "1" summaries))))
        (is (= nil (:name (saut/find-summary "99" summaries)))))))


(deftest update-node
  (testing "Test we can update a node"
    (is (= updated-node-1 (saut/update-node "1.2" new-node-1-2 node-1 test-namespace)) "update a node")))



(deftest patch-node
  (testing "Test we can patch a node"
    (is (= updated-node-1-1 (saut/patch-node node-1-1 "new-title" "new description"  [] [] "abc")))))

(deftest parent-id
  (testing "Test we can find the parent id of a node"
    (is (= "1" (saut/parent-id "1.1")))
    (is (= nil (saut/parent-id nil)))
    (is (= "0" (saut/parent-id "1")))
    (is (= "1.2.3" (saut/parent-id "1.2.3.4")))
    (is (= "1" (saut/parent-id "1.10")))
    (is (= "1.3" (saut/parent-id "1.3.10")))
    (is (= "1.10" (saut/parent-id "1.10.12")))
    (is (= "0" (saut/parent-id "10")))
    (is (= "0" (saut/parent-id "1")))
    (is (= "0" (saut/parent-id "9999")))
    (is (= "0" (saut/parent-id "0")))))


(deftest split
  (testing "test split operation"
    (is (= ["1" "1"] (st/split "1.1" #"[.]")))
    (is (= ["1" "10"] (st/split "1.10" #"[.]")))
    (is (= ["1" "99" "100"] (st/split "1.99.100" #"[.]")))))

(deftest last-digit
  (testing "Test we can find the last digit of an id"

    (is (= "1"   (saut/last-digit "1.1")))
    (is (= "1"   (saut/last-digit "1")))
    (is (= "0"   (saut/last-digit "0")))
    (is (= "6"   (saut/last-digit "1.2.3.4.5.6.7.6")))
    (is (= "10"  (saut/last-digit "1.2.11.100.10")))
    (is (= "999" (saut/last-digit "1.2.11.100.10.999")))))

(deftest next-child
  (testing "Test we can find the next child of a parent"
    (is (= "1.3" (saut/next-child "1" [node-1-1 node-1-2] "abc")))
    (is (= "1.1" (saut/next-child "1" [] "abc")))
    (is (= "1.1.4.1" (saut/next-child "1.1.4" [] "abc")))))

(deftest find-node-ids
  (testing "Test we can find the child ids of a parent"
    (is (= ["1.1" "1.2"] (saut/find-node-ids node-1 "abc")))
    (is (= [] (saut/find-node-ids node-1-1 "abc")))))

(deftest cids
  (testing "Test we can all the ids in a tree"
    (is (= ["1" "1.1" "1.2"] (saut/cids node-1 "abc")))))

;DUPLICATE
; (deftest add-node
;   (testing "Test we can add a node to the tree"
;     (is (= {:abc/id "1"
;                  :abc/children [node-1-1 node-1-2 new-node-1-3]} (saut/add-node "1" new-node-1-3 node-1 "abc")))))

;Test for insertion of new nodes into a more complex tree structure
(def tree-1 {:abc/id "0"
             :abc/name "0"
             :abc/children [{:abc/id "1"
                             :abc/name "1"
                             :abc/children [{:abc/id "1.1"
                                             :abc/name "1.1"
                                             :abc/children [{:abc/id "1.1.1"
                                                             :abc/name "1.1.1"
                                                             :abc/children [{:abc/id "1.1.1.1"
                                                                             :abc/name "1.1.1.1"
                                                                             :abc/children []}]}
                                                            {:abc/id "1.1.2"
                                                             :abc/name "1.1.2"
                                                             :abc/children [{:abc/id "1.1.2.1"
                                                                             :abc/name "1.1.2.1"
                                                                             :abc/children []}]}]}]}

                            {:abc/id "2"
                             :abc/name "2"
                             :abc/children [{:abc/id "2.1"
                                             :abc/name "2.1"
                                             :abc/children [{:abc/id "2.1.1"
                                                             :abc/name "2.1.1"
                                                             :abc/children [{:abc/id "2.1.1.1"
                                                                             :abc/name "2.1.1.1"
                                                                             :abc/children []}]}]}]}]})

(def tree-2 {:abc/id "0"
             :abc/name "0"
             :abc/children [ {:abc/id "1"
                              :abc/name "1"
                              :abc/children [{:abc/id "1.1"
                                              :abc/name "1.1"
                                              :abc/children []}]}

                             {:abc/id "2"
                              :abc/name "2"
                              :abc/children [{:abc/id "2.1"
                                              :abc/name "2.1"
                                              :abc/children []}
                                             {:abc/id "2.2"
                                              :abc/name "2.2"
                                              :abc/children []}]}]})


(deftest find-node-in-tree
  (testing "Test we can find a node in a tree"
    (is (= node-1-1 (saut/find-node-in-tree "1.1" node-1 "abc")))
    (is (= node-1-2 (saut/find-node-in-tree "1.2" node-1 "abc")))
    (is (= node-1 (saut/find-node-in-tree "1" node-1 "abc")))
    (is (= {:abc/id "2.2"
            :abc/name "2.2"
            :abc/children []} (saut/find-node-in-tree "2.2" tree-2 "abc")))
    (is (= {} (saut/find-node-in-tree "9.9" tree-2 "abc")))
    (let [model {:abc/id "0"
                 :abc/children [{:abc/id "1"
                                 :abc/children []}
                                {:abc/id "2"
                                 :abc/children []}
                                {:abc/id "3"
                                 :abc/children []}
                                {:abc/id "4"
                                 :abc/children []}
                                {:abc/id "5"
                                 :abc/children []}
                                {:abc/id "6"
                                 :abc/children []}
                                {:abc/id "7"
                                 :abc/children []}
                                {:abc/id "8"
                                 :abc/children []}
                                {:abc/id "9"
                                 :abc/children []}
                                {:abc/id "10"
                                 :abc/children []}
                                {:abc/id "11"
                                 :abc/children []}]}]
      (is (= {:abc/id "10"
              :abc/children []} (saut/find-node-in-tree "10" model "abc"))))))



(deftest add-node
  (testing "Test we can add a node to the tree"
    (let [updated-tree-1 (saut/add-node "2.1.1" {:abc/id "2.1.1.2" :abc/name "new node 2.1.1.2" :abc/children []}  tree-1 "abc")
          updated-tree-2 (saut/add-node "2.1" {:abc/id "2.1.1" :abc/name "new node 2.1.1" :abc/children []}  tree-2 "abc")
          updated-tree-3 (saut/add-node "1.1" {:abc/id "1.1.1" :abc/name "new node 1.1.1" :abc/children []}  updated-tree-2 "abc")
          updated-tree-4 (saut/add-node "1.1" {:abc/id "1.1.2" :abc/name "new node 1.1.2" :abc/children []}  updated-tree-3 "abc")
          new-node-name(->   updated-tree-1
                             :abc/children
                             second
                             :abc/children
                             first
                             :abc/children
                             first
                             :abc/children
                             second
                             :abc/name)]
      (print (str "\n\n" "tree 2 " updated-tree-2 "\n\n"))
      (print (str "\n\n" "tree 3 " updated-tree-3 "\n\n"))
      (print (str "\n\n" "tree 4 " updated-tree-4 "\n\n"))
      (is (= new-node-name "new node 2.1.1.2")))))


      ; (def node-1-1 {:abc/id "1.1" :name "fred" :abc/children []})
      ; (def node-1-2 {:abc/id "1.2" :name "simon" :abc/children []}))))
      ; (def node-1-10 {:abc/id "1.10" :name "sarah" :abc/children []})
      ; (def node-1-11 {:abc/id "1.11" :name "john" :abc/children [node-11-1]})
      ; (def node-1-11-1 {:abc/id "1.11.1" :name "lucy" :abc/children []}))))

(deftest remove-node-from-list
  (testing "That we can remove a node from a list of nodes (like a child node list)"
    (is (= [node-1-2 node-1-10 node-1-11] (saut/remove-node-from-list "1.1" [node-1-1 node-1-2 node-1-10 node-1-11] "abc")))
    (is (= [node-1-1 node-1-2 node-1-10] (saut/remove-node-from-list "1.11" [node-1-1 node-1-2 node-1-10 node-1-11] "abc")))
    (is (= [node-1-1 node-1-2 node-1-11] (saut/remove-node-from-list "1.10" [node-1-1 node-1-2 node-1-10 node-1-11] "abc")))))


(deftest remove-node-from-model
  (testing "That we can remove a node from a model"
    (let [model {:abc/id "0"
                 :name "context"
                 :abc/children [node-1]}]
      (is (= {:abc/id "0"
              :name "context"
              :abc/children [{:abc/id "1"
                              :abc/children [node-1-1]}]} (saut/remove-node-from-model "1.2" model "abc")))
      (is (= {:abc/id "0"
              :name "context"
              :abc/children [{:abc/id "1"
                              :abc/children [node-1-2]}]} (saut/remove-node-from-model "1.1" model "abc"))))
    ;This check is to test out a bug where ids > 9 could not be deleted!!
    (let [model {:abc/id "0"
                 :name "context"
                 :abc/children [{:abc/id "1"
                                 :abc/children []}
                                {:abc/id "2"
                                 :abc/children []}
                                {:abc/id "3"
                                 :abc/children []}
                                {:abc/id "4"
                                 :abc/children []}
                                {:abc/id "5"
                                 :abc/children []}
                                {:abc/id "6"
                                 :abc/children []}
                                {:abc/id "7"
                                 :abc/children []}
                                {:abc/id "8"
                                 :abc/children []}
                                {:abc/id "9"
                                 :abc/children []}
                                {:abc/id "10"
                                 :abc/children []}
                                {:abc/id "11"
                                 :abc/children []}]}]
      (is (= {:abc/id "0"
              :name "context"
              :abc/children [{:abc/id "1"
                              :abc/children []}
                             {:abc/id "2"
                              :abc/children []}
                             {:abc/id "3"
                              :abc/children []}
                             {:abc/id "4"
                              :abc/children []}
                             {:abc/id "5"
                              :abc/children []}
                             {:abc/id "6"
                              :abc/children []}
                             {:abc/id "7"
                              :abc/children []}
                             {:abc/id "8"
                              :abc/children []}
                             {:abc/id "9"
                              :abc/children []}
                             {:abc/id "11"
                              :abc/children []}]} (saut/remove-node-from-model "10" model "abc"))))))

(deftest test-id-compare
  (testing "Test we can compare ids"
    (is (= 0  (saut/id-compare "1.1" "1.1")))
    (is (= -1 (saut/id-compare "1.1" "1.2")))
    (is (= 1  (saut/id-compare "1.2" "1.1")))
    (is (= 0  (saut/id-compare "12" "12")))
    (is (= -1 (saut/id-compare "11" "12")))
    (is (= 1  (saut/id-compare "12" "11")))
    (is (= -1  (saut/id-compare "9" "10")))))

(deftest test-node-comparator
  (testing "We can compare two nodes of the same level"
    (is (= -1 (saut/node-comparator  "abc" {:abc/id "1.1" :abc/name "fred"} {:abc/id "1.2" :abc/name "john"})))
    (is (=  0 (saut/node-comparator  "abc" {:abc/id "1.2" :abc/name "fred"} {:abc/id "1.2" :abc/name "john"})))
    (is (=  1 (saut/node-comparator  "abc" {:abc/id "1.2" :abc/name "fred"} {:abc/id "1.1" :abc/name "john"})))
    (is (= -1 (saut/node-comparator  "abc" {:abc/id "10" :abc/name "fred"} {:abc/id "11" :abc/name "john"})))
    (is (=  0 (saut/node-comparator  "abc" {:abc/id "12" :abc/name "fred"} {:abc/id "12" :abc/name "john"})))
    (is (=  1 (saut/node-comparator  "abc" {:abc/id "11" :abc/name "fred"} {:abc/id "10" :abc/name "john"})))
    (is (= -2 (saut/node-comparator  "abc" {:abc/id "1.2" :abc/name "fred"} {:abc/id "1.1.1" :abc/name "child john"})))
    (is (= -1 (saut/node-comparator  "abc" {:abc/id "9" :abc/name "fred"} {:abc/id "10" :abc/name "john"})))
    (is (= 1 (saut/node-comparator  "abc" {:abc/id "10" :abc/name "fred"} {:abc/id "9" :abc/name "john"})))))


(deftest test-sort-node-list
  "Test we can sort a list of nodes on the same level"
  (let [sorted-node-list [{:abc/id "1" :abc/name "fred"}
                          {:abc/id "2" :abc/name "fred"}
                          {:abc/id "3" :abc/name "fred"}
                          {:abc/id "4" :abc/name "fred"}
                          {:abc/id "5" :abc/name "fred"}
                          {:abc/id "6" :abc/name "fred"}
                          {:abc/id "7" :abc/name "fred"}
                          {:abc/id "8" :abc/name "fred"}
                          {:abc/id "9" :abc/name "fred"}
                          {:abc/id "10" :abc/name "fred"}
                          {:abc/id "11" :abc/name "fred"}
                          {:abc/id "12" :abc/name "fred"}]

        unsorted-node-list [{:abc/id "12" :abc/name "fred"}
                            {:abc/id "11" :abc/name "fred"}
                            {:abc/id "1" :abc/name "fred"}
                            {:abc/id "2" :abc/name "fred"}
                            {:abc/id "3" :abc/name "fred"}
                            {:abc/id "4" :abc/name "fred"}
                            {:abc/id "5" :abc/name "fred"}
                            {:abc/id "6" :abc/name "fred"}
                            {:abc/id "7" :abc/name "fred"}
                            {:abc/id "8" :abc/name "fred"}
                            {:abc/id "9" :abc/name "fred"}
                            {:abc/id "10" :abc/name "fred"}]]

    (is (= [{:abc/id "1.1" :abc/name "fred"} {:abc/id "1.2" :abc/name "john"}] (saut/sort-node-list "abc" [{:abc/id "1.1" :abc/name "fred"} {:abc/id "1.2" :abc/name "john"}])))
    (is (= [{:abc/id "1.1" :abc/name "fred"} {:abc/id "1.2" :abc/name "john"}] (saut/sort-node-list "abc" [{:abc/id "1.2" :abc/name "john"} {:abc/id "1.1" :abc/name "fred"}])))
    (is (= [{:abc/id "10" :abc/name "fred"} {:abc/id "12" :abc/name "john"}] (saut/sort-node-list "abc" [{:abc/id "12" :abc/name "john"} {:abc/id "10" :abc/name "fred"}])))
    (is (= sorted-node-list (saut/sort-node-list "abc" unsorted-node-list)))))

(deftest move-left
  "Test we can move a node left"
  (let [model {:abc/id "0"
               :abc/name "Context"
               :abc/children [{:abc/id "1"
                               :abc/name "Left"
                               :abc/children []}
                              {:abc/id "2"
                               :abc/name "Right"
                               :abc/children []}]}]
    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children [{:abc/id "1"
                            :abc/name "Right"
                            :abc/children []}
                           {:abc/id "2"
                            :abc/name "Left"
                            :abc/children []}]} (saut/swap-left model "abc" "2")))
    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children [{:abc/id "1"
                            :abc/name "Left"
                            :abc/children []}
                           {:abc/id "2"
                            :abc/name "Right"
                            :abc/children []}]} (saut/swap-left model "abc" "1")))
    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children [{:abc/id "1"
                            :abc/name "Left"
                            :abc/children []}
                           {:abc/id "2"
                            :abc/name "Right"
                            :abc/children []}]}
           (saut/swap-left model "abc" "1")))

    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children []} (saut/swap-left {:abc/id "0"
                                               :abc/name "Context"
                                               :abc/children []} "abc" "1")))
    (is (= {:abc/id "Context"
            :abc/name "Context"
            :abc/children []} (saut/swap-left {:abc/id "Context"
                                               :abc/name "Context"
                                               :abc/children []} "abc" "1")))))

(deftest swap-right
  "Test we can move a node right"
  (let [model {:abc/id "0"
               :abc/name "Context"
               :abc/children [{:abc/id "1"
                               :abc/name "One"
                               :abc/children []}
                              {:abc/id "2"
                               :abc/name "Two"
                               :abc/children []}
                              {:abc/id "3"
                               :abc/name "Three"
                               :abc/children []}]}]
    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children [{:abc/id "1"
                            :abc/name "One"
                            :abc/children []}
                           {:abc/id "2"
                            :abc/name "Three"
                            :abc/children []}
                           {:abc/id "3"
                            :abc/name "Two"
                            :abc/children []}]} (saut/swap-right model "abc" "2")))
    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children [{:abc/id "1"
                            :abc/name "One"
                            :abc/children []}
                           {:abc/id "2"
                            :abc/name "Two"
                            :abc/children []}
                           {:abc/id "3"
                            :abc/name "Three"
                            :abc/children []}]} (saut/swap-right model "abc" "3")))

    (is (= {:abc/id "0"
            :abc/name "Context"
            :abc/children []} (saut/swap-right {:abc/id "0"
                                                :abc/name "Context"
                                                :abc/children []} "abc" "1")))
    (is (= {:abc/id "Context"
            :abc/name "Context"
            :abc/children []} (saut/swap-right {:abc/id "Context"
                                                :abc/name "Context"
                                                :abc/children []} "abc" "1")))))

(deftest test-fix-namespace
  "test we can fix the keywords of a node"
  (let [node1 {:id "1" :name "fred" :children []}
        node2 {:sadf/id "1" :abc/name "fred" :children []}]
    (is (= {:sadf/id "1" :sadf/name "fred" :sadf/icon nil :sadf/delegate "" :sadf/children []} (saut/fix-namespace node1)))
    (is (= {:sadf/id "1" :sadf/name "fred" :sadf/icon nil :sadf/delegate "" :sadf/children []} (saut/fix-namespace node1 "sadf")))
    (is (= {:abc/id "1" :abc/name "fred" :abc/icon nil :abc/delegate "" :abc/children []} (saut/fix-namespace node1 "abc")))
    (is (= {:sadf/id "1" :sadf/name "fred" :sadf/icon nil :sadf/delegate "" :sadf/children []} (saut/fix-namespace node2 "sadf")))))

(deftest test-add-namespace
  "Test we can add a namespace to a model"
  (let [model1 {:sadf/id "1" :sadf/name "fred" :sadf/icon nil :sadf/delegate "" :sadf/children []}
        model2 {:sadf/id "1" :sadf/name "fred" :sadf/icon nil :sadf/delegate "" :sadf/children [{:sadf/id "1.1" :sadf/name "little fred" :sadf/icon nil :sadf/delegate "" :sadf/children []}]}
        model3 {:sadf/id "1" :sadf/name "fred" :sadf/icon nil :sadf/delegate "" :sadf/children [{:sadf/id "1.1" :sadf/name "little fred1" :sadf/icon nil :sadf/delegate "" :sadf/children []}
                                                                                                {:sadf/id "1.2" :sadf/name "little fred2" :sadf/icon nil :sadf/delegate "" :sadf/children []}]}]


    (is (= model1 (saut/add-change-namespace {:id "1" :name "fred" :children []} "sadf")))
    (is (= model2 (saut/add-change-namespace {:id "1" :name "fred" :children [{:id "1.1" :name "little fred"  :children []}]} "sadf")))
    (is (= model3 (saut/add-change-namespace {:id "1" :name "fred" :children [{:id "1.1" :name "little fred1" :children []}
                                                                              {:id "1.2" :name "little fred2" :children []}]} "sadf")))

    ;Now with string keys
    (is (= model1 (saut/add-change-namespace {"id" "1" "name" "fred" "children" []} "sadf")))
    (is (= model3 (saut/add-change-namespace {"id" "1" "name" "fred" "children" [{"id" "1.1" "name" "little fred1" "children" []}
                                                                                 {"id" "1.2" "name" "little fred2" "children" []}]} "sadf")))))



(deftest test-prev-next
  "Test we can find the prev and next ids from a list"
  (let [sample-1 [{:sadf/id "1.1"} {:sadf/id "1.2"} {:sadf/id "1.3"}]
        sample-2 [{:sadf/id "1.2"} {:sadf/id "1.3"}]
        sample-3 [{:sadf/id "1"} {:sadf/id "2"} {:sadf/id "3"}]]
    (is (= [{:sadf/id "1.1"} {:sadf/id "1.2"} {:sadf/id "1.3"}] (saut/prev-next sample-1 "sadf" "1.2")))
    (is (= [nil {:sadf/id "1.1"} {:sadf/id "1.2"}] (saut/prev-next sample-1 "sadf" "1.1")))
    (is (= [{:sadf/id "1.2"} {:sadf/id "1.3"} nil] (saut/prev-next sample-1 "sadf" "1.3")))
    (is (= [nil nil nil] (saut/prev-next sample-2 "sadf" "1.1")))
    (is (= [nil nil nil] (saut/prev-next sample-2 "sadf" "1.4")))
    (is (= [{:sadf/id "1"} {:sadf/id "2"} {:sadf/id "3"} (saut/prev-next sample-3 "sadf" "2")]))
    (is (= [{:sadf/id "2"} {:sadf "3"} nil (saut/prev-next sample-3 "sadf" "3")]))
    (is (= [nil {:sadf/id "1"} {:sadf/id "2"} (saut/prev-next sample-3 "sadf" "1")]))))

(deftest test-has-missing-nodes
  "
    Test we can detect if a sequence of node ids has any missing
    ie [1.1 1.2 1.3] => false
       [1.1 1.2 1.4] => true
  "
  (let [sample-1 [{:sadf/id "1.1"} {:sadf/id "1.2"} {:sadf/id "1.3"}]
        sample-2 [{:sadf/id "1.2"} {:sadf/id "1.3"}]
        sample-3 [{:sadf/id "1.1"} {:sadf/id "1.3"}]
        sample-4 [{:sadf/id "1.1"} {:sadf/id "1.4"}]
        sample-5 [{:sadf/id "1"} {:sadf/id "4"}]]
    (is (= false (saut/has-missing-nodes? sample-1 "sadf")))
    (is (= true (saut/has-missing-nodes? sample-2 "sadf")))
    (is (= true (saut/has-missing-nodes? sample-3 "sadf")))
    (is (= false (saut/has-missing-nodes? [] "sadf")))
    (is (= true (saut/has-missing-nodes? sample-4 "sadf")))
    (is (= true (saut/has-missing-nodes? sample-5 "sadf")))))


(deftest test-find-missing-node-ids
  "
    Test we can find the ids of any missing nodes.
  "
  (let [node-list-1 []
        node-list-2 [{:sadf/id "1.1"} {:sadf/id "1.2"} {:sadf/id "1.3"}]
        node-list-3 [{:sadf/id "1.1"} {:sadf/id "1.3"}]
        node-list-4 [{:sadf/id "1.1"} {:sadf/id "1.4"}]
        node-list-5 [{:sadf/id "1"} {:sadf/id "4"}]
        node-list-6 [{:sadf/id "1"}]
        node-list-7 [{:sadf/id "1.1"}]
        node-list-8 [{:sadf/id "5"}]]
    (is (= [] (saut/find-missing-node-ids node-list-1 "sadf")))
    (is (= [] (saut/find-missing-node-ids node-list-2 "sadf")))
    (is (= ["1.2"] (saut/find-missing-node-ids node-list-3 "sadf")))
    (is (= ["1.2" "1.3"] (saut/find-missing-node-ids node-list-4 "sadf")))
    (is (= ["2" "3"] (saut/find-missing-node-ids node-list-5 "sadf")))
    (is (= [] (saut/find-missing-node-ids node-list-6 "sadf")))
    (is (= [] (saut/find-missing-node-ids node-list-7 "sadf")))
    (is (= ["1" "2" "3" "4"] (saut/find-missing-node-ids node-list-8 "sadf")))))

(deftest test-compact-node-list
  "
  "
  (let [node-list-1 []
        node-list-2 [{:sadf/id "1.1"} {:sadf/id "1.2" :sadf/children [{:sadf/id "1.2.1" :sadf/children []}]} {:sadf/id "1.3"}]
        node-list-3 [{:sadf/id "1.1" :sadf/children []}
                     {:sadf/id "1.3" :sadf/children [{:sadf/id "1.3.1" :sadf/children [{:sadf/id "1.3.1.1" :sadf/children []}]}]}
                     {:sadf/id "1.4" :sadf/children []}]

        node-list-4 [{:sadf/id "1.1" :title "1.1 title" :sadf/children []}
                     {:sadf/id "1.3" :title "1.3 title" :sadf/children [{:sadf/id "1.3.1" :title "1.3.1 title" :sadf/children [{:sadf/id "1.3.1.1" :title "1.3.1.1 title" :sadf/children []}]}]}
                     {:sadf/id "1.6" :title "1.6 title" :sadf/children []}
                     {:sadf/id "1.7" :title "1.7 title" :sadf/children []}
                     {:sadf/id "1.8" :title "1.8 title" :sadf/children []}
                     {:sadf/id "1.9" :title "1.9 title" :sadf/children []}]]
    (is (= [] (saut/compact-node-list node-list-1 "sadf")))
    (is (= [{:sadf/id "1.1"}
            {:sadf/id "1.2" :sadf/children {:sadf/id "1.2.1" :sadf/children []}}
            {:sadf/id "1.3"} (saut/compact-node-list node-list-2 "sadf")]))
    (is (= [{:sadf/id "1.1" :sadf/children []}
            {:sadf/id "1.2" :sadf/children [{:sadf/id "1.2.1" :sadf/children [{:sadf/id "1.2.1.1" :sadf/children []}]}]}
            {:sadf/id "1.3" :sadf/children []} (saut/compact-node-list node-list-3 "sadf")]))))

    ;
    ; This test fails for now.  The compation algorithm needs improving.  As of this cnode
    ; you may need to run the algorithm more than once.
    ;

    ; (is (= [{:sadf/id "1.1" :title "1.1 title" :sadf/children []}
    ;         {:sadf/id "1.2" :title "1.3 title" :sadf/children [{:sadf/id "1.2.1" :title "1.3.1 title" :sadf/children [{:sadf/id "1.2.1.1" :title "1.3.1.1 title" :sadf/children []}]}]}
    ;         {:sadf/id "1.3" :title "1.6 title" :sadf/children []}
    ;         {:sadf/id "1.4" :title "1.7 title" :sadf/children []}
    ;         {:sadf/id "1.5" :title "1.8 title" :sadf/children []}
    ;         {:sadf/id "1.6" :title "1.9 title" :sadf/children []}] (saut/compact-node-list node-list-4 "sadf")))))


(deftest sort-id-list
  "
  "
  (let [id-list-1 []
        id-list-2 ["3" "2" "1"]
        id-list-3 ["3.2" "3.1" "3.5"]
        id-list-4 ["1.1.1.6" "1.1.1.1"]
        id-list-5 ["1.1.1.1"]
        id-list-6 ["1.1" "1"]]
    (is (= [] (saut/sort-id-list id-list-1)))
    (is (= ["1" "2" "3"] (saut/sort-id-list id-list-2)))
    (is (= ["3.1" "3.2" "3.5"] (saut/sort-id-list id-list-3)))
    (is (= ["1.1.1.1" "1.1.1.6"] (saut/sort-id-list id-list-4)))
    (is (= ["1.1.1.1"] (saut/sort-id-list id-list-5)))
    (is (= ["1.1" "1"] (saut/sort-id-list id-list-6)))))
