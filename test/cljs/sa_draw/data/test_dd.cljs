(ns sa-draw.data.test-dd
  "
  Data dictionary and data term tests.
  "
  (:require [cljs.test              :refer-macros [deftest is testing run-tests]]
            [sa-draw.data.dd        :refer [new-dictionary add-term term-by-id new-term id label clear-dd remove-term list-terms term-exists? details clean]]
            [sa-draw.data.simple-dd :refer [dd term]]))

(deftest test-new-item
  (testing "New term creation"
    (let [t (new-term term "data-name")]
      (is (not (nil? (id term t))))
      (is (= "data-name" (label term t))))))

(deftest test-dictionary-ops
  (testing "Basic dictionary operations"
    (let [t (new-term term "data-name")
          dictionary (->> (new-dictionary dd)
                          (add-term dd t))]

      (is (= t (term-by-id dd (id term t) dictionary)))
      (let [dictionary (remove-term dd (id term t) dictionary)]
        (is (= nil (term-by-id dd (id term t) dictionary))))))
  (testing "List terms in dd"
    (let [t1 (new-term term "a")
          t2 (new-term term "b")
          t3 (new-term term "c")
          dictionary (->> (new-dictionary dd)
                          (add-term dd t1)
                          (add-term dd t2)
                          (add-term dd t3))]
      (let [listed (list-terms dd dictionary)]
        (is (= t1 (first listed))  "in order 1 2 3")
        (is (= t2 (nth  listed 1)) "in order 1 2 3")
        (is (= t3 (last listed)))  "in order 1 2 3")
      (let [dictionary (->> (new-dictionary dd)
                            (add-term dd t3)
                            (add-term dd t2)
                            (add-term dd t1))]
        (let [listed (list-terms dd dictionary)]
          (is (= t1 (first listed))  "still in order 1 2 3")
          (is (= t2 (nth  listed 1)) "still in order 1 2 3")
          (is (= t3 (last listed)))) "still in order 1 2 3")))
  (testing "exists"
    (let [t1 (new-term term  "a")
          t2 (new-term term  "b")
          dictionary (->> (new-dictionary dd)
                          (add-term dd t1))]
      (is (not= nil (term-exists? dd t1 dictionary)) "This term exists")
      (is (= nil (term-exists? dd t2 dictionary))) "This term does not exist")))

(deftest test-term-ops
  (testing "Basic term operations"
    (let [t (new-term term "data-name")
          t-plus (assoc t :state (atom :na))
          [label id description] (details term t)
          t-clean (clean term t-plus)]
      (print t-clean)
      (is (not= nil id))
      (is (= label "data-name"))
      (is (nil? description))
      (is (not (nil? (:state t-plus))) "extras present")
      (is (nil? (:state t-clean)) "extras removed"))))
