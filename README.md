# Project sa-draw SPA DFD Drawing

A tool for drawing modern data flow diagram hierarchies.


[![CircleCI](https://circleci.com/bb/sasys/sa-draw.svg?style=svg)](https://circleci.com/bb/sasys/sa-draw)
[![Netlify Status](https://api.netlify.com/api/v1/badges/bc3b0a80-a044-4193-ab66-6f72d6a51874/deploy-status)](https://app.netlify.com/sites/goofy-edison-c6d8e0/deploys)

BitBucket Project : [https://bitbucket.org/sasys/sa-draw](https://bitbucket.org/sasys/sa-draw)

Demo Deployment : [https://sa.demo.softwarebynumbers.com](https://sa.demo.softwarebynumbers.com)

Trello : [https://trello.com/b/KePK2v6d/project-sa](https://trello.com/b/KePK2v6d/project-sa)

Tool provides an SPA that supports the drawing of DFDs.  Tool is currently under test
on Netlify at https://sa.demo.softwarebynumbers.com/

Tool provides an SPA that supports the drawing of DFDs.  

## Features

### Diagram Elements

The original DeMarco approach used circles to represent data transofrmation processes.


* Actor
* Cloud
* Kubernetes
* Note
* Link


### Markdown
Includes a markdown representation of the model and a markdown editor that
supports shift left/right/up for nodes in the model.

Markdown/json parser is implemented in Rust/Wasm

### Download/Upload

* Markdown representation can be down loaded
* Correctly formed requirements in markdown can be uploaded
* Model is stored in the browser key store and can be downloaded to the local file system (in transit)
* Model can be uploaded from the file system to the tool (in transit)
* Diagrams are SVG and can be download as SVG

### Misc
* Dataflow arrows annotated with data names
* Allows linking between models at the context level via Producers and Consumers
* Allows linking to other models via a process with a model delegate
* No server component required, SPA stands alone once loaded



Uses a split build.

## Missing Pieces

* No data dictionary (being developed)
* No consistency checking

## To Do And Planned Features
  Now in Trello https://trello.com/b/KePK2v6d/project-sa


## Screen Shots

Context
![alt text](docs/demo-context-1.png)

Markdown Generation
![alt text](docs/demo-docs-1.png)

  Symbols
![alt text](docs/demo-mix-1.png)


# Build,Test, Docker

## To Build And Run Demo Locally With http-server

You will need the following projects to be cloned and build, all available in
the SA Frontend folder on bitbucket.

* clone-yew-material  
* sa-draw  
* sa-layout  
* sa-models

There are three build options:

__dev build__

    clojure -A:split-dev-build

__demo build__

    clojure -A:split-demo-build

__production build__

    clojure -A:split-prod-build


For local builds the http-server (python) is recommended because the server
must deliver wasm files with the correct mime type.

```
http-server demo -p 3000
```

### Running Tests Locally

Run the tests

```
clojure -A:test-cljs

RUN TESTS
 Temp html file  was created
 Temp html file  can be read
[CLJS]  
[CLJS]  Testing sa-draw.data.test-dd
[CLJS]  {:id 14d53335-7d7c-46b0-b71e-0d51e643e8cb, :label data-name, :description nil}
[CLJS]  
[CLJS]  Testing sa-draw.model.test-flows
[CLJS]  
[CLJS]  Testing sa-draw.model.test-icons
[CLJS]  
[CLJS]  Testing sa-draw.model.test-proto-model-devcards
[CLJS]  [3 3.1 3.2 3.3 3.4]
[CLJS]  
[CLJS]  Testing sa-draw.model.test-context-objects
[CLJS]  
[CLJS]  Testing sa-draw.utils.test-sem-ver
[CLJS]  
[CLJS]  Testing sa-draw.model.test-utils
[CLJS]  
[CLJS]  
[CLJS]  tree 2 {:abc/id "0", :abc/name "0", :abc/children ({:abc/id "1", :abc/name "1", :abc/children ({:abc/id "1.1", :abc/name "1.1", :abc/children ()})} {:abc/id "2", :abc/name "2", :abc/children ({:abc/id "2.1", :abc/name "2.1", :abc/children ({:abc/id "2.1.1", :abc/name "new node 2.1.1", :abc/children []})} {:abc/id "2.2", :abc/name "2.2", :abc/children ()})})}
[CLJS]  
[CLJS]  
[CLJS]  tree 3 {:abc/id "0", :abc/name "0", :abc/children ({:abc/id "1", :abc/name "1", :abc/children ({:abc/id "1.1", :abc/name "1.1", :abc/children ({:abc/id "1.1.1", :abc/name "new node 1.1.1", :abc/children []})})} {:abc/id "2", :abc/name "2", :abc/children ({:abc/id "2.1", :abc/name "2.1", :abc/children ({:abc/id "2.1.1", :abc/name "new node 2.1.1", :abc/children ()})} {:abc/id "2.2", :abc/name "2.2", :abc/children ()})})}
[CLJS]  
[CLJS]  
[CLJS]  tree 4 {:abc/id "0", :abc/name "0", :abc/children ({:abc/id "1", :abc/name "1", :abc/children ({:abc/id "1.1", :abc/name "1.1", :abc/children ({:abc/id "1.1.1", :abc/name "new node 1.1.1", :abc/children []} {:abc/id "1.1.2", :abc/name "new node 1.1.2", :abc/children []})})} {:abc/id "2", :abc/name "2", :abc/children ({:abc/id "2.1", :abc/name "2.1", :abc/children ({:abc/id "2.1.1", :abc/name "new node 2.1.1", :abc/children ()})} {:abc/id "2.2", :abc/name "2.2", :abc/children ()})})}
[CLJS]  
[CLJS]  Testing sa-draw.model.test-basic-layout
[CLJS]  
[CLJS]  Testing sa-draw.views.dfd.svg.test-utility
[CLJS]  
[CLJS]  Testing sa-draw.views.dfd.svg.test-process-card
[CLJS]  ******* Default dispatch value used. ***** {:type :sa-draw.views.dfd.svg.process-card/no-not-me, :node {:sadf/id 4}}
[CLJS]  
[CLJS]  Testing sa-draw.views.dfd.svg.test-draw
[CLJS]  
[CLJS]  Ran 54 tests containing 260 assertions.
[CLJS]  0 failures, 0 errors.

Test was  GOOD

SHUTTING THE SERVER DOWN

```

## CirclCi

### Local Circle CI Build

Running circleci locally can simplify and speed up debugging circle ci builds.
The job names can be found in the circle configuration.

For example build and test the rust wasm:

```
circleci local execute --job build-wasm
```
Build and test the sa-draw app

```
circleci local execute --job test-sa-draw
```


## Netlify

The community branch deploys to site community on netlify via circleci using the netlify cli deploy
command.

For this to work the circleci context sa-draw needs to have an access token in the
NETLIFY_SA_DRAW_ACCESS_TOKEN env var set on the sa-draw context.  

You also need the site id in the NETLIFY_SA_DRAW_SITE_ID token which is the API ID on the netlify site settings page.

For logging we need a REPOSITORY_URL and a BRANCH and a COMMIT_REF tokens.

The token is obtained from netlify at https://app.netlify.com/user/applications/personal





## Docker

To build and run up a docker image delivering this web sites

Build the demo site

    clojure -A:split-demo-build

Then create the docker image

    docker build . -t sa-draw-server:0.0.1

Then run up the image

    docker run --name sa-draw-server -d -p 8080:80 sa-draw-server:0.0.1


## See Also

Loom https://github.com/aysylu/loom

Ubergraph https://github.com/Engelberg/ubergraph

## License

Copyright © 2021 Software By Numbers Ltd

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
